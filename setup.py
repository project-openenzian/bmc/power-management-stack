from setuptools import setup

setup(
    name="enzianbmctools",
    version="1.0.0",
    license_files=["LICENSE"],
    packages=[
        "bmctools",
        "bmctools.dbus",
        "bmctools.pmbus",
        "bmctools.pmbus.configs",
        "bmctools.pmbus.devices",
        "bmctools.testing",
        "enzianbmc"
    ],
    package_dir={"": "src"},
    install_requires=[
        "dbus",
        "gpiod",
        "pygobject",
        "smbus"
    ]
)
