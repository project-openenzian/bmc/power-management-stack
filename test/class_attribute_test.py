from copy import copy

class Dummy(object):
    foo = "foo"
    def __init__(self):
        super(Dummy, self).__init__()
        self.foo = copy(self.foo)


class SubDummy(Dummy):
    def __init__(self):
        self.foo = "bar"
        super(SubDummy, self).__init__()

if __name__ == "__main__":
    d1 = Dummy()
    assert(d1.foo == "foo")
    d1.foo = "bar"
    d2 = Dummy()
    assert(d2.foo == "foo")
    ds1 = SubDummy()
    assert(ds1.foo == "bar")
    assert(d2.foo == "foo")
