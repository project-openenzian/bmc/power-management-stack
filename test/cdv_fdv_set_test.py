#!/usr/bin/env python3

import os
import sys
import mmap
import gpiod
import argparse

def set_dev_mem(dv, value):
    m_dev_mem = None
    f_dev_mem = os.open('/dev/mem', os.O_RDWR | os.O_SYNC)
    m_dev_mem = mmap.mmap(f_dev_mem, 16, mmap.MAP_SHARED, mmap.PROT_READ | mmap.PROT_WRITE, offset=0x41210000)
    os.close(f_dev_mem)

    if (m_dev_mem) is None:
        print("Failed to mmap /dev/mem", file=sys.stderr)

    if dv.upper() == "CDV":
        m_dev_mem[0] = value
    elif dv.upper() == "FDV":
        m_dev_mem[8] = value
    else:
        print(f"Illegal DV '{dv}'", file=sys.stderr)
    
    m_dev_mem.close()


def set_gpiod(dv, value):
    line_names = [f"B_{dv.upper()}_VID{i}" for i in range(0, 8)]
    line0 = gpiod.find_line(line_names[0])
    chip = line0.owner()
    lines = chip.find_lines(line_names)

    # Request the lines as output but leave the values unchanged
    lines.request("cdv_fdv_set_test", gpiod.LINE_REQ_DIR_IN)
    def_values = lines.get_values()
    lines.set_direction_output(def_values)

    values = [(value >> i) & 0x1 for i in range(0, 8)]
    lines.set_values(values)
    
    lines.release()
    chip.close()


METHODS = {
    "devmem": set_dev_mem,
    "gpiod": set_gpiod 
}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Test setting DV's with different methods")
    parser.add_argument("method", type=str, choices=METHODS.keys())
    parser.add_argument("dv", type=str, choices=["CDV", "FDV"])
    parser.add_argument("value", type=lambda x: int(x, 0), choices=range(0x00, 0x100))

    args = parser.parse_args()

    METHODS[args.method](args.dv, args.value)
