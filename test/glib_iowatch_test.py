from gi.repository import GLib as glib
from time import sleep

# the path to monitor
CONFIG_TEST_PATH="foobar.dat"


def callback(source, condition, arg1, arg2):
    print("Callback Called\n")

    #status, buf = source.read_chars()
    status, buf, length, term = source.read_line()


    print(status)
    print(buf)
    print(length)
    print(term)

 #   print("Value=%d" % int(buf[0]))
    sleep(1)

    source.seek_position(0, glib.SeekType.SET)

    # do it again
    return True


# open the file
fobj = open(CONFIG_TEST_PATH, "rb" )

# get the channel
channel = glib.IOChannel.unix_new(fobj.fileno())

# conditions | glib.IO_IN
cond = glib.IO_PRI

# the priority  GLib.PRIORITY_HIGH or GLib.PRIORITY_DEFAULT
prio = glib.PRIORITY_DEFAULT

arg1 = "foo"
arg2 = "bar"

# evid = GLib.io_add_watch(channel, priority, condition, func, *user_data)
evid = glib.io_add_watch(channel, prio, cond, callback, arg1, arg2)


# starting the glib.Mainloop()
mainloop = glib.MainLoop()
try:
    mainloop.run()
except KeyboardInterrupt:
    pass

print("terminated")