#!/usr/bin/env python3
from time import sleep

from gi.repository import GLib as glib
import gpiod;

ALERT_PIN = "B_PWR_ALERT_N"

def callback(source, condition, line, read_all=True):
    print("Callback called\n")

    if read_all:
        events = line.event_read_multiple()
        for i, e in enumerate(events):
            print(f"{i}: {e}")
    else:
        event = line.event_read()
        print(event)

    return True


alert = gpiod.find_line(ALERT_PIN)
alert.request('test', gpiod.LINE_REQ_DIR_IN | gpiod.LINE_REQ_EV_BOTH_EDGES)

glib.io_add_watch(alert.event_get_fd(), glib.IO_IN | glib.IO_PRI, callback, alert)

sleep(5)

mainloop = glib.MainLoop()
try:
    mainloop.run()
except KeyboardInterrupt:
    pass

print("terminated")
