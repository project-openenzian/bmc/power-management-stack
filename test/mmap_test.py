import os, sys
import mmap

f = os.open('mmap_test.txt', os.O_RDWR | os.O_SYNC)
f_mmap = mmap.mmap(f, 0, mmap.MAP_SHARED, mmap.PROT_READ | mmap.PROT_WRITE, offset=0)

if __name__ == "__main__":
    index = int(sys.argv[1]) if len(sys.argv) > 1 else 0

    print("Length: %d" % len(f_mmap))
    print("Value: %c" % f_mmap[index])
