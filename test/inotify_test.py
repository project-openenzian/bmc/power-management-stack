from gi.repository import GLib as glib
from os import strerror, read                # various OS functions
from io import FileIO                        # the FileIO class
from fcntl import ioctl                      # ioctl call
from termios import FIONREAD                 # the ioctl command
from ctypes import CDLL, get_errno, c_int    # opening the c library and ctypes
from struct import unpack_from, calcsize     # packing and unpacking the c structs
from select import epoll                     # polling a fd for i/o events (edge triggered)

from sys import version_info, getfilesystemencoding
if version_info.major < 3 :
    fsencode = lambda s: s if isinstance(s, str) else s.encode(getfilesystemencoding())
    fsdecode = lambda s: s if isinstance(s, str) else s.decode(getfilesystemencoding())
else :
    from os import fsencode, fsdecode


# the path to monitor
CONFIG_TEST_PATH="."


# configuration: the libc version to use
CONFIG_LIBC_VERSION='libc.so.6'

# the libc pointer
_libc = None

"""
INotify wrapper adapted from
https://github.com/chrisjbillington/inotify_simple
"""
class INotify(FileIO):

    """ access and modify """
    EV_ACCESS = 0x00000001
    EV_MODIFY = 0x00000002

    """ create and delete events """
    EV_CREATE = 0x00000100
    EV_DELETE = 0x00000200

    """ change events """
    EV_CHANGE = EV_MODIFY | EV_CREATE | EV_DELETE

    """ All supported events """
    EV_ALL = EV_ACCESS | EV_MODIFY | EV_CREATE | EV_DELETE


    # define the fd property fort he filedescriptor
    fd = property(FileIO.fileno)

    def __init__(self):
        """
        Initialize the INotify class
        """

        # initialize the _libc wrapper
        global _libc; _libc = _libc or CDLL(CONFIG_LIBC_VERSION, use_errno=True)

        CLOEXEC = 0o2000000
        r = _libc.inotify_init1(CLOEXEC)
        # On success, these system calls return a new file descriptor.
        # On error, -1 is returned, and errno is set to indicate the error.
        if r == -1 :
            errno = get_errno()
            print("Could not initialize inotify: %s (%d)" % (strerror(errno), errno))
        else :
            print("Successfully initalized inotify. fd=%d\n" % r)

        # now initialize the file IO object with the file descriptor
        FileIO.__init__(self, r, mode='rb')

        # register the poller using epoll
        self._poller = epoll()
        self._poller.register(self.fileno())


    def add_watch(self, path, mask):
        """
        Adding a watch on a paty. -> returns a watch descriptor (int)
        """

        r = _libc.inotify_add_watch(self.fileno(), fsencode(path) , mask)
        # On success, inotify_add_watch() returns a nonnegative watch descriptor.
        # On error, -1 is returned and errno is set appropriately.
        if r == -1 :
            errno = get_errno()
            print("Could not add watch to path '%s': %s (%d)" % (path, strerror(errno), errno))
            return None
        else :
            print("Successfully created the watch on path '%s' wd=%d" % (path, r))
            return r

    def rm_watch(self, wd):
        """
        Cancel the watch descriptor
        """
        if wd == None :
            return

        r = _libc.inotify_rm_watch(self.fileno(), wd)
        # On success, inotify_rm_watch() returns zero.
        # On error, -1 is returned and errno is set to indicate the cause of the error.
        if r == -1 :
            errno = get_errno()
            print("Could not remove watch wd=%d: %s (%d)" % (wd, strerror(errno), errno))
        else :
            print("Successfully remove watch wd=%d" % wd)



    def get_events(self):
        """
        Read the Inotify File Descriptor
        """

        # do an ioctl on the inotify fd
        bytes_avail = c_int()
        ioctl(self, FIONREAD, bytes_avail)

        if not bytes_avail.value:
            return []

        # read the ioctl() file control
        data = read(self.fileno(), bytes_avail.value)

        pos = 0
        events = []
        while pos < len(data):

            _EVENT_FMT = 'iIII'
            _EVENT_SIZE = calcsize(_EVENT_FMT)

            _, mask, _, namesize = unpack_from(_EVENT_FMT, data, pos)
            pos += _EVENT_SIZE + namesize
            name = data[pos - namesize : pos].split(b'\x00', 1)[0]
            events.append((mask, fsdecode(name)))

        return events


def callback():
    print("Callback Called\n")

    for (m, f) in inotify.get_events():
        print("file change event: %s (%x)" % (f, m))

    return True


inotify = INotify()
wd = inotify.add_watch(CONFIG_TEST_PATH, inotify.EV_CHANGE)

# adding the time out
glib.timeout_add(1000, callback)

# starting the glib.Mainloop()
mainloop = glib.MainLoop()
try:
    mainloop.run()
except KeyboardInterrupt:
    inotify.rm_watch(wd)
    pass

print("terminated")
