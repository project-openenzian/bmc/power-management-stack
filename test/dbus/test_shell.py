#!/usr/bin/env python

import threading
from time import sleep

import argparse
import readline
import cmd
import code

import dbus
import dbus.mainloop.glib
from gi.repository import GLib as glib

class TestShell(cmd.Cmd, object):
    def __init__(self, bus, event_loop):
        super(TestShell, self).__init__()
        self.dbus = bus
        self.enzian = bus.get_object('systems.enzian.test.TestService', '/systems/enzian/test/TestService')
        self.event_loop = event_loop
        self.prompt = "Enzian PWR> "
        self._python_shell = None

    def do_hello(self, args):
        argv = args.split(" ")
        if len(argv) < 2:
            print("Usage: hello first_name last_name")
            return

        response = self.enzian.hello(argv[0], argv[1])
        print(response)

    def do_signup(self, args):
        pass

    def do_fail(self, args):
        pass

    def do_fail_dbus(self, args):
        pass

    def do_heartbeat(self, args):
        signal_match = self.enzian.connect_to_signal("heart_beat", lambda: print("Enzian is alive"))
        try:
            self.event_loop.run()
        except KeyboardInterrupt:
            pass
        signal_match.remove()

    def do_python(self, args):
        if self._python_shell is None:
            variables = {
                "bus"        : self.dbus,
                "enzian"     : self.enzian,
                "event_loop" : self.event_loop
            }
            self._python_shell = code.InteractiveConsole(variables)
        self._python_shell.interact()

    def do_loglevel(self, args):
        level = "INFO"

        argv = args.split(" ")
        if len(argv) > 0 and argv[0] != "":
            level = argv[0]

        try:
            self.enzian.set_loglevel(level)
        except dbus.DBusException as e:
            print(e.get_dbus_message())

    def do_exit(self, args):
        return True

def main(args):
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    bus = dbus.SessionBus()

    event_loop = glib.MainLoop()

    shell = TestShell(bus, event_loop)
    if args.command is None:
        try:
            shell.cmdloop()
        except KeyboardInterrupt:
            pass
    else:
        shell.onecmd(args.command)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Enzian Test Shell")
    parser.add_argument(
        "command", metavar="CMD", type=str, nargs='?',
        help="Command to run"
    )
    args = parser.parse_args()
    main(args)
