#!/usr/bin/env python

import sys
import argparse

import logging

import dbus
import dbus.service
import dbus.mainloop.glib
from gi.repository import GLib as glib

class EnzianTestException(dbus.DBusException):
    def __init__(self, message):
        super(EnzianTestException, self).__init__(message)
        self._dbus_error_name = "systems.enzian.test.EnzianTestException"

class InvalidLogLevelException(dbus.DBusException):
    def __init__(self, level):
        msg = "Invalid log level '%s'." % (level)
        super(InvalidLogLevelException, self).__init__(msg)
        self._dbus_error_name = "systems.enzian.InvalidLogLevelException"

class ComplexResponse(object):
    def __init__(self, name, password):
        super(ComplexResponse, self).__init__()
        self.name = name
        self.password = password

class EnzianTest(dbus.service.Object):

    def __init__(self, bus_name, object_path):
        super(EnzianTest, self).__init__(bus_name, object_path)
        self._logger = logging.getLogger(bus_name.get_name())
        self._logger.info("Instantiated as %s" % (object_path))


    @dbus.service.method(
        dbus_interface="systems.enzian.Test",
        in_signature="ss",
        out_signature="s"
    )
    def hello(self, first_name, last_name):
        self._logger.debug("hello called with '%s' '%s'" % (first_name, last_name))
        return "Hello %s %s!" % (first_name, last_name)


    @dbus.service.method(
        dbus_interface="systems.enzian.Test",
        in_signature="s",
        out_signature="a{ss}"
    )
    def signup(self, name):
        self._logger.debug("New user '%s' signed up" % (name))
        return vars(ComplexResponse(name, "password"))


    @dbus.service.method(
        dbus_interface="systems.enzian.Test",
        in_signature="v",
        out_signature="v"
    )
    def echo(self, value):
        return value


    @dbus.service.method(
        dbus_interface="systems.enzian.test.Exceptions",
        in_signature="",
        out_signature=""
    )
    def fail(self):
        self._logger.error("Failed")
        raise Exception("Game Over")


    @dbus.service.method(
        dbus_interface="systems.enzian.test.Exceptions",
        in_signature="",
        out_signature=""
    )
    def fail_dbus(self):
        self._logger.warn("Failed, but the DBus way")
        raise EnzianTestException()


    @dbus.service.signal(
        dbus_interface="systems.enzian.test.Signals",
        signature=""
    )
    def heart_beat(self):
        self._logger.debug("Emitting heart beat")
        glib.timeout_add_seconds(5, self.heart_beat)


    @dbus.service.method(
        dbus_interface="systems.enzian.test.Logging",
        in_signature="s",
        out_signature=""
    )
    def set_loglevel(self, level):
        log_level = logging.getLevelName(level.upper())
        try:
            self._logger.setLevel(log_level)
        except ValueError, e:
            raise InvalidLogLevelException(level.upper())


def main(args):
    log_level = logging.DEBUG if args.verbose else logging.INFO
    logging.root.setLevel(log_level)

    formatter = logging.Formatter(
        fmt="[%(asctime)s.%(msecs)03d] %(levelname)s %(name)s : %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S"
    )

    if args.log is None:
        sh = logging.StreamHandler()
        sh.setFormatter(formatter)
        logging.root.addHandler(sh)
    else:
        fh = logging.FileHandler(args.log)
        fh.setFormatter(formatter)
        logging.root.addHandler(fh)

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    session_bus = dbus.SessionBus()
    name = dbus.service.BusName("systems.enzian.test.TestService", session_bus)
    enzian_test = EnzianTest(name, "/systems/enzian/test/TestService")
    enzian_test.heart_beat()

    mainloop = glib.MainLoop()
    try:
        mainloop.run()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="DBus test service")
    parser.add_argument("--log", "-l", type=str, metavar="FILE", default=None,
        help="logfile, stdout if not set"
    )
    parser.add_argument("--verbose", "-v", action="store_true",
        help="start services with log level DEBUG"
    )
    args = parser.parse_args()
    main(args)
