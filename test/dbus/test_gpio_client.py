import dbus

from bmctools.dbus.util import run_mainloop

from enzianbmc.gpio_service import get_service

dbusd = dbus.SystemBus()
gpio = get_service(dbusd, True)

signal_match = gpio.connect_to_signal(
    "value_change",
    lambda p, v: print(f"{p}: {v}")
)
run_mainloop()
signal_match.remove()
