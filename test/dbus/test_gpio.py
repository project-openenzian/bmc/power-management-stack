import logging

import dbus

from bmctools.dbus.util import run_mainloop
from bmctools.dbus.gpio import GpioObject

from enzianbmc.gpio_service import GPIO_SERVICE_NAME, GPIO_OBJECT_PATH

logger = logging.getLogger(GPIO_SERVICE_NAME)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

bus_name = dbus.service.BusName(GPIO_SERVICE_NAME, dbus.SystemBus())
gpio = GpioObject(logger, bus_name, GPIO_OBJECT_PATH)

gpio.add_pins([
    ('B_PWR_ALERT_N', False)
])

run_mainloop()
