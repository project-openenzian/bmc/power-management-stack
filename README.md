# Enzian Power Management Stack
Tools to control the Enzian power and clock tree. This includes device drivers, DBus services and a shell to interact with the power management stack.

This repo is structured as follows:
 - `config_dumps`: Configuration dumps for some of the regulators on the Enzian board
 - `src`: Source for the power management stack, the top level contains executable scripts
    - `src/bmctools`: Generic drivers and services not specific to Enzian
    - `src/enzianbmc`: Power management stack for Enzian implemented using `bmctools`
- `test`: Test scripts to test mechanisms and approaches to implementing functionality
- `setup.py`: Setup script to install the source packages with Python's `setuptools`

More documentation can be found on the [Enzian Project Wiki](https://unlimited.ethz.ch/x/qi-cBg).
