# This file contains helper functions for debugging
# These can be used in the interractive python shells

from bmctools.dbus.util import from_dbus, to_dbus

def init_debug_helpers(gpio_service, power_service, fault_service, telemetry_service):
    global gpio, power, fault, telemetry
    gpio = gpio_service
    power = power_service
    fault = fault_service
    telemetry = telemetry_service

###############
# Fan Control #
###############
def fan_enable_test_mode(t_diodes=[0, 1]):
    """Enables the test mode for automatic fan control"""
    for t_diode in t_diodes:
        t_diode_config = from_dbus(power.device_read("fans", ("MFR_TEMP_SENSOR_CONFIG", t_diode + 6)))
        t_diode_config["OFFSET"] = 0x1F
        power.device_write("fans", ("MFR_TEMP_SENSOR_CONFIG", t_diode + 6), to_dbus(t_diode_config))

def fan_clear_log():
    mfr_mode = from_dbus(power.device_read("fans", "MFR_MODE"))
    mfr_mode["CLEAR_NV_FAULT_LOG"] = True
    power.device_write("fans", "MFR_MODE", to_dbus(mfr_mode))

def fan_force_log():
    mfr_mode = from_dbus(power.device_read("fans", "MFR_MODE"))
    mfr_mode["FORCE_NV_FAULT_LOG"] = True
    power.device_write("fans", "MFR_MODE", to_dbus(mfr_mode))

def fan_read_log(sort=True, valid_only=True):
    # Log has 15 entries, read them all
    log = [ from_dbus(power.device_read("fans", "MFR_NV_FAULT_LOG")) for i in range(0, 15) ]
    if valid_only:
        log = filter(lambda i: i["LOG_VALID"] == 0xDD, log)
    if sort:
        log = sorted(log, key=lambda i: i["FAULT_LOG_INDEX"])
    return log

def fan_log_pluck(log, keys=["FAULT_LOG_INDEX", "FAULT_LOG_COUNT", "LOG_VALID"]):
    return [ { k : l[k] for k in keys } for l in log ]
