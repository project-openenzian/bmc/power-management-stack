from bmctools.pmbus.devices.isppac import ISPPAC
from bmctools.pmbus.devices.si5395 import SI5395
from bmctools.pmbus.devices.max31785 import MAX31785
from bmctools.pmbus.devices.ir3581 import IR3581, IR3581_Loop
from bmctools.pmbus.devices.ina226 import INA226
from bmctools.pmbus.devices.max20751 import MAX20751
from bmctools.pmbus.devices.max15301 import MAX15301
from bmctools.pmbus.devices.isl6334d import ISL6334D

# (name, bus number, alerts)
i2c_busses = [
    ("seq",     1, ["B_SEQ_ALERT_N"]),
    ("clk",     3, []),
    ("pwr_fan", 5, ["B_PWR_ALERT_N", "B_FAN_ALERT_N"])
]

# (name, constructor, bus name, address)
i2c_devices = [
    # Power sequencer CPLDs
    ("pac_cpu",                     ISPPAC,      "seq",     0x60),
    ("pac_fpga",                    ISPPAC,      "seq",     0x61),
    # Clock generators
    ("clk_main",                    SI5395,      "clk",     0x68),
    ("clk_cpu",                     SI5395,      "clk",     0x69),
    ("clk_fpga",                    SI5395,      "clk",     0x6A),
    # Fan controller / temperature monitor
    ("fans",                        MAX31785,    "pwr_fan", 0x52),
    # CPU voltage regulators
    ("ir3581",                      IR3581,      "pwr_fan", 0x08),
    ("ir3581_loop_vdd_core",        IR3581_Loop, "pwr_fan", 0x60),
    ("ir3581_loop_0v9_vdd_oct",     IR3581_Loop, "pwr_fan", 0x62),
    ("max15301_1v5_vdd_oct",        MAX15301,    "pwr_fan", 0x10),
    # FPGA voltage regulators
    ("max20751_vccint_fpga",        MAX20751,    "pwr_fan", 0x70),
    ("max20751_mgtavcc_fpga",       MAX20751,    "pwr_fan", 0x72),
    ("max20751_mgtavtt_fpga",       MAX20751,    "pwr_fan", 0x73),
    ("max15301_vcc1v8_fpga",        MAX15301,    "pwr_fan", 0x11),
    ("max15301_util_3v3",           MAX15301,    "pwr_fan", 0x1B),
    ("max15301_vadj_1v8",           MAX15301,    "pwr_fan", 0x12),
    ("max15301_vccintio_bram_fpga", MAX15301,    "pwr_fan", 0x15),
    # DRAM current sensors
    ("ina226_ddr_fpga_13",          INA226,      "pwr_fan", 0x41),
    ("ina226_ddr_fpga_24",          INA226,      "pwr_fan", 0x40),
    ("ina226_ddr_cpu_13",           INA226,      "pwr_fan", 0x44),
    ("ina226_ddr_cpu_24",           INA226,      "pwr_fan", 0x45),
]

# (name, constructor)
gpio_devices = [
    # DRAM voltage regulators
    ("isl6334d_ddr_v_cpu",          ISL6334D, {"VID" : "B_CDV_VID"}),
    ("isl6334d_ddr_v_fpga",         ISL6334D, {"VID" : "B_FDV_VID"}),
]

# These are the enable controls for each voltage node.  True = on, False =
# off.  They are currently physical enable lines from the ISPPACs, but that's
# not necessarily true in general.
enable_map = {
    'UTIL_3V3'           : ( "pac_fpga", "OUT6"  ),
    'SYS_2V5_24'         : ( "pac_fpga", "OUT7"  ),
    'SYS_2V5_13'         : ( "pac_fpga", "OUT8"  ),
    'VCCINT_FPGA'        : ( "pac_fpga", "OUT9"  ),
    'MGTAVCC_FPGA'       : ( "pac_fpga", "OUT10" ),
    'MGTVCCAUX_L'        : ( "pac_fpga", "OUT11" ),
    'MGTVCCAUX_R'        : ( "pac_fpga", "OUT12" ),
    'VCCINTIO_BRAM_FPGA' : ( "pac_fpga", "OUT13" ),
    'MGTAVTT_FPGA'       : ( "pac_fpga", "OUT14" ),
    'VCC1V8_FPGA'        : ( "pac_fpga", "OUT15" ),
    'SYS_1V8'            : ( "pac_fpga", "OUT16" ),
    'VADJ_1V8_FPGA'      : ( "pac_fpga", "OUT17" ),
    'VDD_DDRFPGA24'      : ( "pac_fpga", "OUT18" ),
    'VDD_DDRFPGA13'      : ( "pac_fpga", "OUT19" ),
    'VDD_CORE'           : ( "pac_cpu",  "OUT6"  ),
    '0V9_VDD_OCT'        : ( "pac_cpu",  "OUT7"  ),
    '1V5_VDD_OCT'        : ( "pac_cpu",  "OUT8"  ),
    '2V5_CPU13'          : ( "pac_cpu",  "OUT9"  ),
    '2V5_CPU24'          : ( "pac_cpu",  "OUT10" ),
    'VDD_DDRCPU13'       : ( "pac_cpu",  "OUT11" ),
    'VDD_DDRCPU24'       : ( "pac_cpu",  "OUT12" ),
}

control_map = {
    "VDD_CORE"           : ( "ir3581_loop_vdd_core",        "VOUT"     ),
    "0V9_VDD_OCT"        : ( "ir3581_loop_0v9_vdd_oct",     "VOUT"     ),
    "1V5_VDD_OCT"        : ( "max15301_1v5_vdd_oct",        "VOUT"     ),
    "VCCINT"             : ( "max20751_vccint_fpga",        "VOUT"     ),
    "MGTAVCC_FPGA"       : ( "max20751_mgtavcc_fpga",       "VOUT"     ),
    "MGTAVTT_FPGA"       : ( "max20751_mgtavtt_fpga",       "VOUT"     ),
    "VCC1V8_FPGA"        : ( "max15301_vcc1v8_fpga",        "VOUT"     ),
    "UTIL_3V3"           : ( "max15301_util_3v3",           "VOUT"     ),
    "VADJ_1V8"           : ( "max15301_vadj_1v8",           "VOUT"     ),
    "VCCINTIO_BRAM_FPGA" : ( "max15301_vccintio_bram_fpga", "VOUT"     ),
    "CPU_FAN"            : ( "fans",                        "FAN_0_PWM"),
    "FPGA_FAN"           : ( "fans",                        "FAN_1_PWM"),
    "CASE_FAN_0"         : ( "fans",                        "FAN_2_PWM"),
    "CASE_FAN_1"         : ( "fans",                        "FAN_3_PWM"),
    "CASE_FAN_2"         : ( "fans",                        "FAN_4_PWM"),
    "CASE_FAN_3"         : ( "fans",                        "FAN_5_PWM"),
    # For both CPU and FPGA, the regulator for channels 2&4 uses the same VID
    # signal as that for channels 1&3.  To avoid setting it twice, it's only
    # present here once as a controlled node.
    'VDD_DDRCPU13'       : ( "isl6334d_ddr_v_cpu",              "VID" ),
    'VDD_DDRFPGA13'      : ( "isl6334d_ddr_v_fpga",             "VID" ),
}

# XXX - this sort of constrained choice should be driven by runtime
#       requirements, and not done statically.
suppressed_comparators = set([
# The INA226 can only set alerts against one quantity (voltage, current,
# power) at a time.  We want to monitor DIMM current, so the others are
# suppressed to prevent them being automatically instantiated for the DDR
# voltage limits.
    ( "ina226_ddr_cpu_13",  "VOLTAGE", "WARN_HIGH" ),
    ( "ina226_ddr_cpu_13",  "VOLTAGE", "WARN_LOW"  ),
    ( "ina226_ddr_cpu_13",  "POWER",   "WARN_HIGH" ),
    ( "ina226_ddr_cpu_24",  "VOLTAGE", "WARN_HIGH" ),
    ( "ina226_ddr_cpu_24",  "VOLTAGE", "WARN_LOW"  ),
    ( "ina226_ddr_cpu_24",  "POWER",   "WARN_HIGH" ),
    ( "ina226_ddr_fpga_13", "VOLTAGE", "WARN_HIGH" ),
    ( "ina226_ddr_fpga_13", "VOLTAGE", "WARN_LOW"  ),
    ( "ina226_ddr_fpga_13", "POWER",   "WARN_HIGH" ),
    ( "ina226_ddr_fpga_24", "VOLTAGE", "WARN_HIGH" ),
    ( "ina226_ddr_fpga_24", "VOLTAGE", "WARN_LOW"  ),
    ( "ina226_ddr_fpga_24", "POWER",   "WARN_HIGH" ),
# The MAX20751 has no overvoltage fault comparator on its output.
    ( "max20751_vccint_fpga",  "VOUT", "FAULT_HIGH"),
    ( "max20751_mgtavcc_fpga", "VOUT", "FAULT_HIGH"),
    ( "max20751_mgtavtt_fpga", "VOUT", "FAULT_HIGH"),
# The IR3581 asserts WARN_LOW even when disabled
    ( "ir3581_loop_vdd_core", "VOUT", "WARN_LOW"),
    ( "ir3581_loop_0v9_vdd_oct", "VOUT", "WARN_LOW"),
# *_LOW should only be enabled after the output has stabilised.  The IR3581
# seems to assert these immediately on enable otherwise.
    ( "ir3581_loop_vdd_core", "VOUT", "FAULT_LOW"),
    ( "ir3581_loop_0v9_vdd_oct", "VOUT", "FAULT_LOW"),
])

# The 12V monitor inputs to the isppacs are fed by a 4.7k/3.3k voltage
# divider.
monitor_scale = {
    ( "pac_cpu",  "VMON1_ATT" ) : (4.7e3 + 3.3e3) / 3.3e3,
    ( "pac_fpga", "VMON1_ATT" ) : (4.7e3 + 3.3e3) / 3.3e3,
}

# Some voltage nodes have multiple monitors e.g. the output of the generating
# device and input of a consuming device.  They're listed here in decreasing
# order of 'trustworthiness'.  We trust an independent monitor more than the
# device responsible for generating the voltage.
#
# The first monitor in the list *must* be usable without the rail it's
# monitoring being up.
voltage_monitor_map = {
    'BMC_VCC_5V'         : [
        ( "pac_cpu",  "VCCINP" ),
        ( "pac_fpga", "VCCINP" )
    ],
    'BMC_VCC_3V3'        : [
        ( "pac_cpu",  "VCCA"   ),
        ( "pac_fpga", "VCCA"   )
    ],
    '12V_CPU0_PSUP'      : [
        ( "pac_cpu",  "VMON1_ATT"  ),
        ( "ir3581_loop_vdd_core", "VIN" ),
        ( "ir3581_loop_0v9_vdd_oct", "VIN" ),
        ( "max15301_1v5_vdd_oct", "VIN" )
    ],
    '12V_CPU1_PSUP'      : [
        ( "pac_fpga", "VMON1_ATT"  ),
        ( "max20751_vccint_fpga", "VIN" ),
        ( "max20751_mgtavcc_fpga", "VIN" ),
        ( "max20751_mgtavtt_fpga", "VIN" ),
        ( "max15301_vcc1v8_fpga", "VIN" ),
        ( "max15301_util_3v3", "VIN" ),
        ( "max15301_vadj_1v8", "VIN" ),
        ( "max15301_vccintio_bram_fpga", "VIN" ),
    ],
    '5V_PSUP'            : [
        ( "pac_cpu",  "VMON2_ATT"  ),
        ( "pac_fpga", "VMON2_ATT"  )
    ],
    '3V3_PSUP'           : [
        ( "pac_cpu",  "VMON3_ATT"  )
    ],
    'UTIL_3V3'           : [
        ( "pac_fpga", "VMON3_ATT"  ),
        ( "max15301_util_3v3", "VOUT" )
    ],
    'SYS_1V8'            : [
        ( "pac_fpga", "VMON12" )
    ],
    'SYS_2V5_13'         : [
        ( "pac_fpga", "VMON5_ATT"  )
    ],
    'SYS_2V5_24'         : [
        ( "pac_fpga", "VMON5_ATT"  )
    ],
    'VDD_CORE'           : [
        ( "pac_cpu",  "VMON4"  ),
        ( "ir3581_loop_vdd_core", "VOUT" )
    ],
    '0V9_VDD_OCT'        : [
        ( "pac_cpu",  "VMON5"  ),
        ( "ir3581_loop_0v9_vdd_oct", "VOUT" ),
    ],
    '1V5_VDD_OCT'        : [
        ( "pac_cpu",  "VMON6"  ),
        ( "max15301_1v5_vdd_oct", "VOUT" )
    ],
    '2V5_CPU13'          : [
        ( "pac_cpu",  "VMON7_ATT"  )
    ],
    '2V5_CPU24'          : [
        ( "pac_cpu",  "VMON8_ATT"  )
    ],
    'VDD_DDRCPU13'       : [
        ( "pac_cpu",  "VMON9"  ),
        ( "ina226_ddr_cpu_13", "VOLTAGE" )
    ],
    'VTT_DDRCPU13'       : [
        ( "pac_cpu",  "VMON11" )
    ],
    'VDD_DDRCPU24'       : [
        ( "pac_cpu",  "VMON10" ),
        ( "ina226_ddr_cpu_24", "VOLTAGE" )
    ],
    'VTT_DDRCPU24'       : [
        ( "pac_cpu",  "VMON12" )
    ],
    'VCCINT_FPGA'        : [
        ( "pac_fpga", "VMON6"  ),
        ( "max20751_vccint_fpga", "VOUT" ),
    ],
    'VCCINTIO_BRAM_FPGA' : [
        ( "pac_fpga", "VMON10" ),
        ( "max15301_vccintio_bram_fpga", "VOUT" ),
    ],
    'VCC1V8_FPGA'        : [
        ( "pac_fpga", "VMON11_ATT" ),
        ( "max15301_vcc1v8_fpga", "VOUT" ),
    ],
    'MGTAVCC_FPGA'       : [
        ( "pac_fpga", "VMON7"  ),
        ( "max20751_mgtavcc_fpga", "VOUT" ),
    ],
    'MGTAVTT_FPGA'       : [
        ( "max20751_mgtavtt_fpga", "VOUT")
    ],
    'MGTVCCAUX_L'        : [
        ( "pac_fpga", "VMON8"  )
    ],
    'MGTVCCAUX_R'        : [
        ( "pac_fpga", "VMON9"  )
    ],
    'VADJ_1V8_FPGA'      : [
        ( "max15301_vadj_1v8", "VOUT" )
    ],
    'VDD_DDRFPGA13'      : [
        ( "ina226_ddr_fpga_13", "VOLTAGE" )
    ],
    'VDD_DDRFPGA24'      : [
        ( "ina226_ddr_fpga_24", "VOLTAGE" )
    ],
}

# Current monitors are in shorter supply, and seldom if ever have redundancy.
# It's almost always the IOUT monitor of the regulator.
current_monitor_map = {
    #'BMC_VCC_5V'         : [
        # IOUT on the PSU
    #],
    #'BMC_VCC_3V3'        : [
    #],
    #'12V_CPU0_PSUP'      : [
        # IOUT on the PSU
    #],
    #'12V_CPU1_PSUP'      : [
        # IOUT on the PSU
    #],
    #'5V_PSUP'            : [
        # IOUT on the PSU
    #],
    #'3V3_PSUP'           : [
        # IOUT on the PSU
    #],
    'UTIL_3V3'           : [
        ( "max15301_util_3v3", "IOUT" )
    ],
    #'SYS_1V8'            : [
    #],
    #'SYS_2V5_13'         : [
    #],
    #'SYS_2V5_24'         : [
    #],
    'VDD_CORE'           : [
        ( "ir3581_loop_vdd_core", "IOUT" )
    ],
    '0V9_VDD_OCT'        : [
        ( "ir3581_loop_0v9_vdd_oct", "IOUT" ),
    ],
    '1V5_VDD_OCT'        : [
        ( "max15301_1v5_vdd_oct", "IOUT" )
    ],
    #'2V5_CPU13'          : [
    #],
    #'2V5_CPU24'          : [
    #],
    'VDD_DDRCPU13'       : [
        ( "ina226_ddr_cpu_13", "CURRENT" )
    ],
    #'VTT_DDRCPU13'       : [
    #],
    'VDD_DDRCPU24'       : [
        ( "ina226_ddr_cpu_24", "CURRENT" )
    ],
    #'VTT_DDRCPU24'       : [
    #],
    'VCCINT_FPGA'        : [
        ( "max20751_vccint_fpga", "IOUT" ),
    ],
    'VCCINTIO_BRAM_FPGA' : [
        ( "max15301_vccintio_bram_fpga", "IOUT" ),
    ],
    'VCC1V8_FPGA'        : [
        ( "max15301_vcc1v8_fpga", "IOUT" ),
    ],
    'MGTAVCC_FPGA'       : [
        ( "max20751_mgtavcc_fpga", "IOUT" ),
    ],
    'MGTAVTT_FPGA'       : [
        ( "max20751_mgtavtt_fpga", "IOUT")
    ],
    #'MGTVCCAUX_L'        : [
    #],
    #'MGTVCCAUX_R'        : [
    #],
    'VADJ_1V8_FPGA'      : [
        ( "max15301_vadj_1v8", "IOUT" )
    ],
    'VDD_DDRFPGA13'      : [
        ( "ina226_ddr_fpga_13", "CURRENT" )
    ],
    'VDD_DDRFPGA24'      : [
        ( "ina226_ddr_fpga_24", "CURRENT" )
    ],
}

temp_monitor_map = {
    "CPU"                            : [
        ( "fans",                        "T_DIODE_0" ),
    ],
    "FPGA"                           : [
        ( "fans",                        "T_DIODE_1" ),
    ],
    "CASE"                           : [
        ( "fans",                        "T_INTERNAL" ),
    ],
    "ir3581"                         : [
        ( "ir3581_loop_vdd_core",        "TEMPERATURE_2" ),
    ],
    "ir3581_loop_vdd_core"           : [
        ( "ir3581_loop_vdd_core",        "T_POWERSTAGE" ),
    ],
    "ir3581_loop_0v9_vdd_oct"        : [
        ( "ir3581_loop_0v9_vdd_oct",     "T_POWERSTAGE" ),
    ],
    "max20751_vccint_fpga"           : [
        ( "max20751_vccint_fpga",        "TEMPERATURE_1" ),
    ],
    "max20751_mgtavcc_fpga"          : [
        ( "max20751_mgtavcc_fpga",       "TEMPERATURE_1" ),
    ],
    "max20751_mgtavtt_fpga"          : [
        ( "max20751_mgtavtt_fpga",       "TEMPERATURE_1" ),
    ],
    "max15301_vcc1v8_fpga"           : [
        ( "max15301_vcc1v8_fpga",        "T_INTERNAL" ),
    ],
    "max15301_vcc1v8_fpga_ps"        : [
        ( "max15301_vcc1v8_fpga",        "T_EXTERNAL" ),
    ],
    "max15301_util_3v3"              : [
        ( "max15301_util_3v3",           "T_INTERNAL" ),
    ],
    "max15301_util_3v3_ps"           : [
        ( "max15301_util_3v3",           "T_EXTERNAL" ),
    ],
    "max15301_vadj_1v8"              : [
        ( "max15301_vadj_1v8",           "T_INTERNAL" ),
    ],
    "max15301_vadj_1v8_ps"           : [
        ( "max15301_vadj_1v8",           "T_EXTERNAL" ),
    ],
    "max15301_vccintio_bram_fpga"    : [
        ( "max15301_vccintio_bram_fpga", "T_INTERNAL" ),
    ],
    "max15301_vccintio_bram_fpga_ps" : [
        ( "max15301_vccintio_bram_fpga", "T_EXTERNAL" ),
    ],
}

fanspeed_monitor_map = {
    "CPU"  : [
        ( "fans", "FAN_0_RPM" ),
	( "fans", "FAN_0_PWM" ),
    ],
    "FPGA" : [
        ( "fans", "FAN_1_RPM" ),
	( "fans", "FAN_1_PWM" ),
    ],
    "CASE" : [
        ( "fans", "FAN_2_RPM" ),
	( "fans", "FAN_2_PWM" ),
        ( "fans", "FAN_3_RPM" ),
	( "fans", "FAN_3_PWM" ),
        ( "fans", "FAN_4_RPM" ),
	( "fans", "FAN_4_PWM" ),
        ( "fans", "FAN_5_RPM" ),
	( "fans", "FAN_5_PWM" ),
    ],
}
