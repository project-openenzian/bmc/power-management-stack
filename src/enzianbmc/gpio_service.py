import logging

import dbus.service

from bmctools.dbus.gpio import GpioObject

from bmctools.dbus.util import run_mainloop


GPIO_SERVICE_NAME = "systems.enzian.Gpio"
GPIO_OBJECT_PATH = "/systems/enzian/Gpio"


def testing_setup():
    pass


def start_service(dbus_daemon):
    logger = logging.getLogger(GPIO_SERVICE_NAME)

    bus_name = dbus.service.BusName(GPIO_SERVICE_NAME, dbus_daemon)
    gpio = GpioObject(logger, bus_name, GPIO_OBJECT_PATH)

    gpio.add_pins([
        ('B_SPI_SEL_N', True),
        ('B_USERIO_LED1', True),
        ('B_USERIO_LED2', True),
        ('B_USERIO_LED3', True),
        ('B_USERIO_LED4', True),
        ('B_USERIO_LED5', True),
        ('B_USERIO_LED6', True),
        ('B_USERIO_LED7', True),
        ('B_USERIO_LED8', True),
        ('C_RESET_N', True),
        ('C_PLL_DCOK', True),
        ('B_OCI2_LNK1', True),
        ('B_OCI3_LNK1', True),
        ('B_C2C_NMI', True),
        ('B_FMCC_SEL', True),
        ('B_FAN_RESET_N', True),
        ('B_USB2_EN', True),
        ('C_RESET_OUT_N', False),
        ('B_CLOCK_BLOL', False),
        ('B_CLOCK_CLOL', False),
        ('B_CLOCK_FLOL', False),
        ('B_USB2_OC_N', False),
        ('B_PSUP_ON', True),
        ('C_D_EVENT_N', False),
        ('F_D_EVENT_N', False),
        ('B_USERIO_SW1_N', False),
        ('B_USERIO_SW2_N', False),
        ('B_USERIO_SW3_N', False),
        ('B_USERIO_SW4_N', False),
        ('B_USERIO_SW5_N', False),
        ('B_USERIO_SW6_N', False),
        ('B_USERIO_SW7_N', False),
        ('B_USERIO_SW8_N', False),
        ('B_PWR_ALERT_N', False),
        ('B_SEQ_ALERT_N', False),
        ('B_PSU_ALERT_N', False),
        ('B_FAN_ALERT_N', False),
        ('B_FAN_FAULT_N', False)
    ])

    gpio.add_pin_group('B_CDV_VID', [
        'B_CDV_VID0',
        'B_CDV_VID1',
        'B_CDV_VID2',
        'B_CDV_VID3',
        'B_CDV_VID4',
        'B_CDV_VID5',
        'B_CDV_VID6',
        'B_CDV_VID7',
    ], True)

    gpio.add_pin_group('B_FDV_VID', [
        'B_FDV_VID0',
        'B_FDV_VID1',
        'B_FDV_VID2',
        'B_FDV_VID3',
        'B_FDV_VID4',
        'B_FDV_VID5',
        'B_FDV_VID6',
        'B_FDV_VID7',
    ], True)

    run_mainloop()


def get_service(dbus_daemon, follow_name_owner_changes=False):
    return dbus_daemon.get_object(GPIO_SERVICE_NAME, GPIO_OBJECT_PATH, follow_name_owner_changes=follow_name_owner_changes)
