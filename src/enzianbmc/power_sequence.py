from time import sleep

from bmctools.pmbus.configs.si5395_configuration import SI5395_Configuration
from bmctools.pmbus.configs.ir3581_configuration import IR3581_Configuration

from bmctools.dbus.util import to_dbus

SUCCESS = "SUCCESS"
RETRY = "RETRY"
FAIL = "FAIL"

class PowerSequenceStep(object):
    def __init__(self, description, substeps):
        super(PowerSequenceStep, self).__init__()
        self.description = description
        self.substeps = substeps

    def execute(self, gpio_service, power_service):
        for i, (cmd, success) in enumerate(self.substeps):
            while True:
                v = cmd(gpio_service, power_service)
                r = success(v)
                if v != RETRY:
                    break
            if v == FAIL:
                break
        return (i, self.description, r)


def do(description, cmds, success=lambda v: SUCCESS):
    return PowerSequenceStep(description, [ (c, success) for c in cmds ])


def check_voltage(description, device, signal, lower, upper, atten=True):
    return PowerSequenceStep(description, [(
        lambda gpio, power: power.device_write(device, "adc_mux", to_dbus({ "ATTEN" : atten, "SEL" : signal })),
        lambda v: SUCCESS
    ), (
        lambda gpio, power: power.device_read(device, "adc_value"),
        lambda v: RETRY if not v["DONE"] else (SUCCESS if lower <= v["VALUE"] <= upper else FAIL)
    )])


class PowerSequence(object):
    def __init__(self, gpio_service, power_service, sequence):
        super(PowerSequence, self).__init__()
        self._gpio_service = gpio_service
        self._power_service = power_service
        self._current_step = -1
        self.sequence = sequence


    def reset(self):
        self._current_step = -1


    def _take_step(self):
        if self._current_step < 0 or len(self.sequence) <= self._current_step:
            return None

        step = self.sequence[self._current_step]
        (i, d, r) = step.execute(self._gpio_service, self._power_service)

        if r == SUCCESS:
            return(str(self._current_step), d, r)
        else:
            return ("%d.%d" % (self._current_step, i), d, r)


    def next_step(self):
        self._current_step = self._current_step + 1
        return self._take_step()


    def retry_step(self):
        return self._take_step()


    def execute(self):
        r = self.next_step()
        while r is not None:
            (i, d, s) = r
            if s != SUCCESS:
                break
            r = self.next_step()
        return r


class EnzianCommonUpSequence(PowerSequence):
    def __init__(self, gpio_service, power_service):
        super(EnzianCommonUpSequence, self).__init__(gpio_service, power_service, [
            do(
                "Turn power supply on", [
                    lambda gpio, power: gpio.set_value("B_PSUP_ON", True)
                ]
            ),
            do(
                "Program main clock", [
                    lambda gpio, power: power.device_configure('clk_main', SI5395_Configuration.main)
                ]
            ),
            do(
                "Check B_CLOCK_BLOL", [
                    lambda gpio, power: gpio.get_value("B_CLOCK_BLOL")
                ],
                lambda v: SUCCESS if v else FAIL
            )
        ])

class EnzianCPUUpSequence(PowerSequence):
    def __init__(self, gpio_service, power_service):
        super(EnzianCPUUpSequence, self).__init__(gpio_service, power_service, [
            do(
                "Reset ispPAC", [
                    lambda gpio, power: power.device_write("pac_cpu", "reset", True)
                ]
            ),
            check_voltage(
                "Check CPU:BMC_VCC_3V3 in [3.135, 3.465]V",
                "pac_cpu", "VCCA", 3.135, 3.465
            ),
            check_voltage(
                "Check CPU:BMC_VCC_5V in [4.75, 5.25]V",
                "pac_cpu", "VCCINP", 4.75, 5.25
            ),
            do(
                "Check PSUP_PGOOD", [
                    lambda gpio, power: power.device_read("pac_cpu", "input_status")
                ],
                lambda v: SUCCESS if "IN1" in v else FAIL
            ),
            check_voltage(
                "Check CPU:12V_CPU0_PSUP in [4.71, 5.19]V",
                "pac_cpu", "VMON1", 4.71, 5.19
            ),
            check_voltage(
                "Check CPU:5V_PSUP in [4.75, 5.25]V",
                "pac_cpu", "VMON2", 4.75, 5.25
            ),
            check_voltage(
                "Check CPU:3V3_PSUP in [3.135, 3.465]V",
                "pac_cpu", "VMON3", 3.135, 3.465
            ),
            do(
                "Deassert C_PLL_DCOK & C_RESET_N", [
                    lambda gpio, power: gpio.set_value("C_PLL_DCOK", False),
                    lambda gpio, power: gpio.set_value("C_RESET_N", False)
                ]
            ),
            do(
                "Deassert B_OCI{2,3}_LINK1", [
                    lambda gpio, power: gpio.set_value('B_OCI2_LNK1', False),
                    lambda gpio, power: gpio.set_value('B_OCI3_LNK1', False)
                ]
            ),
            do(
                "Program CPU clock", [
                    lambda gpio, power: power.device_configure('clk_cpu', SI5395_Configuration.cpu)
                ]
            ),
            do(
                "Check B_CLOCK_BLOL", [
                    lambda gpio, power: gpio.get_value("B_CLOCK_BLOL")
                ],
                lambda v: SUCCESS if v else FAIL
            ),
            do(
                "Program IR3581 parent", [
                    lambda gpio, power: power.device_write('ir3581', 'address_lock', False),
                    lambda gpio, power: power.device_write('ir3581', 'loop_1_pmbus_addr', 0x60),
                    lambda gpio, power: power.device_write('ir3581', 'loop_2_pmbus_addr', 0x62),
                    lambda gpio, power: power.device_write('ir3581', 'address_lock', True),
                    lambda gpio, power: power.device_configure('ir3581', IR3581_Configuration.registers)
                ]
            ),
            do(
                "Program IR3581 loop1", [
                    # TODO: correct thresholds
                    lambda gpio, power: power.set_device_threshold("ir3581_loop_vdd_core", "IOUT", "WARN_HIGH", 100),
                    lambda gpio, power: power.set_device_threshold("ir3581_loop_vdd_core", "IOUT", "FAULT_HIGH", 120),
                    lambda gpio, power: power.device_write("ir3581_loop_vdd_core", "IOUT_OC_FAULT_RESPONSE", {'DELAY': 1, 'RETRY': 3, 'RESPONSE': 'DISABLE'})
                ]
            ),
            do(
                "Program IR3581 loop2", [
                    # TODO: correct thresholds
                    lambda gpio, power: power.set_device_threshold("ir3581_loop_1v5_vdd_oct", "IOUT", "WARN_HIGH", 100),
                    lambda gpio, power: power.set_device_threshold("ir3581_loop_1v5_vdd_oct", "IOUT", "FAULT_HIGH", 120),
                    lambda gpio, power: power.device_write("ir3581_loop_1v5_vdd_oct", "IOUT_OC_FAULT_RESPONSE", {'DELAY': 1, 'RETRY': 3, 'RESPONSE': 'DISABLE'})
                ]
            ),
            do(
                "Enable VDD_CORE", [
                    lambda gpio, power: power.device_write("pac_cpu", "gp_output",
                        to_dbus(set(power.device_read("gp_output").union(["OUT6"])))
                    )
                ]
            ),
            check_voltage(
                "Check VDD_CORE in [0.94, 0.96]V",
                "pac_cpu", "VMON4", 0.94, 0.96
            ),
            do(
                "Enable 0V9_VDD_OCT", [
                    lambda gpio, power: power.device_write("pac_cpu", "gp_output",
                        to_dbus(set(power.device_read("gp_output").union(["OUT7"])))
                    )
                ]
            ),
            check_voltage(
                "Check 0V9_VDD_OCT in [0.87, 0.93]V",
                "pac_cpu", "VMON5", 0.87, 0.93
            ),
            do(
                "Enable 1V5_VDD_OCT", [
                    lambda gpio, power: power.device_write("pac_cpu", "gp_output",
                        to_dbus(set(power.device_read("gp_output").union(["OUT8"])))
                    )
                ]
            ),
            check_voltage(
                "Check 1V5_VDD_OCT in [0.87, 0.93]V",
                "pac_cpu", "VMON6", 1.425, 1.675
            ),
            do(
                "Enable 2V5_CPU_13", [
                    lambda gpio, power: power.device_write("pac_cpu", "gp_output",
                        to_dbus(set(power.device_read("gp_output").union(["OUT9"])))
                    )
                ]
            ),
            check_voltage(
                "Check 2V5_CPU_13 in [2.375, 2.625]V",
                "pac_cpu", "VMON7", 2.375, 2.625
            ),
            do(
                "Enable 2V5_CPU_24", [
                    lambda gpio, power: power.device_write("pac_cpu", "gp_output",
                        to_dbus(set(power.device_read("gp_output").union(["OUT10"])))
                    )
                ]
            ),
            check_voltage(
                "Check 2V5_CPU_24 in [2.375, 2.625]V",
                "pac_cpu", "VMON8", 2.375, 2.625
            ),
            do(
                "Enable VDD_DDRCPU13", [
                    lambda gpio, power: power.device_write("pac_cpu", "gp_output",
                        to_dbus(set(power.device_read("gp_output").union(["OUT11"])))
                    )
                ]
            ),
            # check_voltage(
            #     "Check VDD_DDRCPU13 in [2.375, 2.625]V",
            #     "pac_cpu", "VMON9", 2.375, 2.625
            # ),
            do(
                "Enable VDD_DDRCPU24", [
                    lambda gpio, power: power.device_write("pac_cpu", "gp_output",
                        to_dbus(set(power.device_read("gp_output").union(["OUT12"])))
                    )
                ]
            ),
            # check_voltage(
            #     "Check VDD_DDRCPU24 in [2.375, 2.625]V",
            #     "pac_cpu", "VMON10", 2.375, 2.625
            # )
        ])

class EnzianFPGAUpSequence(PowerSequence):
    def __init__(self, gpio_service, power_service):
        super(EnzianFPGAUpSequence, self).__init__(gpio_service, power_service, [
            do(
                "Reset ispPAC", [
                    lambda gpio, power: power.device_write("pac_fpga", "reset", True)
                ]
            ),
            check_voltage(
                "Check FPGA:BMC_VCC_3V3 in [3.135, 3.465]V",
                "pac_fpga", "VCCA", 3.135, 3.465
            ),
            check_voltage(
                "Check FPGA:BMC_VCC_3V3 in [4.75, 5.25]V",
                "pac_fpga", "VCCINP", 4.75, 5.25
            ),
            do(
                "Check PSUP_PGOOD", [
                    lambda gpio, power: power.device_read("pac_fpga", "input_status")
                ],
                lambda v: SUCCESS if "IN1" in v else FAIL
            ),
            check_voltage(
                "Check FPGA:12V_CPU1_PSUP in [4.71, 5.19]V",
                "pac_fpga", "VMON1", 4.71, 5.19
            ),
            check_voltage(
                "Check FPGA:5V_PSUP in [4.75, 5.25]V",
                "pac_fpga", "VMON2", 4.75, 5.25
            ),
            do(
                "Program FPGA clock", [
                    lambda gpio, power: power.device_configure('clk_fpga', SI5395_Configuration.fpga)
                ]
            )
            ,
            do(
                "Check B_CLOCK_BLOL", [
                    lambda gpio, power: gpio.get_value("B_CLOCK_BLOL")
                ],
                lambda v: SUCCESS if v else FAIL
            )
        ])
