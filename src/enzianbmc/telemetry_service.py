import logging

import dbus.service

from bmctools.dbus.telemetry import TelemetryObject

from bmctools.dbus.util import run_mainloop

from .power_service import get_service as get_power_service


TELEMETRY_SERVICE_NAME = "systems.enzian.Telemetry"
TELEMETRY_OBJECT_PATH = "/systems/enzian/Telemetry"


def testing_setup():
    global get_power_service

    import random
    MockPowerObject = type("MockPowerObject", (object,), {
        "read_device_monitor" : lambda self, d, m: random.randint(0, 100)
    })
    get_power_service = lambda *args, **kwargs : MockPowerObject()


def start_service(dbus_daemon):
    logger = logging.getLogger(TELEMETRY_SERVICE_NAME)

    # Telemetry service reconnects to power service if it restarts
    # We can do that as the connection is stateless
    power = get_power_service(dbus_daemon, follow_name_owner_changes=True)

    bus_name = dbus.service.BusName(TELEMETRY_SERVICE_NAME, dbus_daemon)
    telemetry = TelemetryObject(power, logger, bus_name, TELEMETRY_OBJECT_PATH)

    run_mainloop()

def get_service(dbus_daemon, follow_name_owner_changes=False):
    return dbus_daemon.get_object(TELEMETRY_SERVICE_NAME, TELEMETRY_OBJECT_PATH, follow_name_owner_changes=follow_name_owner_changes)
