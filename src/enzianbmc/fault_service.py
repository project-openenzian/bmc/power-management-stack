import logging

import dbus.service

from bmctools.dbus.fault import FaultObject

from bmctools.dbus.util import run_mainloop

from .gpio_service import get_service as get_gpio_service
from .power_service import get_service as get_power_service

from .fault_policy import scram, scram_faults

FAULT_SERVICE_NAME = "systems.enzian.Fault"
FAULT_OBJECT_PATH = "/systems/enzian/Fault"

def start_service(dbus_daemon):
    logger = logging.getLogger(FAULT_SERVICE_NAME)

    bus_name = dbus.service.BusName(FAULT_SERVICE_NAME, dbus_daemon)
    # Reconnect to GPIO service if it restarts
    # We can do that as the connection is stateless
    # (interrupts for input pins are enabled at service startup)
    gpio = get_gpio_service(dbus_daemon, follow_name_owner_changes=True)
    # Reconnect to Power service if it restarts
    # We can do that as the connection is stateless
    power = get_power_service(dbus_daemon, follow_name_owner_changes=True)

    fault = FaultObject(logger, bus_name, FAULT_OBJECT_PATH, gpio, power, scram_faults, scram)

    # Connect to fault signal
    BUS_FAN_FAULT = "B_FAN_FAULT_N"
    fault_signal = \
        gpio.connect_to_signal("value_low", fault.fault_handler,
                               arg0=BUS_FAN_FAULT)

    # Connect to alert signal
    alert_signal = power.connect_to_signal("device_alert", fault.alert_handler)

    # Connect to unknown alerts (devices that don't implement alert response).
    unknown_alert_signal = \
        power.connect_to_signal("unknown_device_alert",
                                fault.unknown_alert_handler)

    run_mainloop()

    fault_signal.remove()
    alert_signal.remove()
    unknown_alert_signal.remove()

def get_service(dbus_daemon, follow_name_owner_changes=False):
    return dbus_daemon.get_object(FAULT_SERVICE_NAME, FAULT_OBJECT_PATH, follow_name_owner_changes=follow_name_owner_changes)
