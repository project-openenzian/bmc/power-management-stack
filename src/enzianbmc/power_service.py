import logging

import dbus.service

from bmctools.dbus.power import PowerObject

from bmctools.pmbus.native_pmbus import Native_PMBus

from bmctools.dbus.util import run_mainloop

from .gpio_service import get_service as get_gpio_service

from .topology import i2c_busses, i2c_devices, gpio_devices

POWER_SERVICE_NAME = "systems.enzian.Power"
POWER_OBJECT_PATH = "/systems/enzian/Power"


def testing_setup():
    global Native_PMBus

    # Mock PMBus
    from bmctools.testing.pmbus import MockPMBus
    Native_PMBus = MockPMBus


def start_service(dbus_daemon):
    logger = logging.getLogger(POWER_SERVICE_NAME)

    # Reconnect to GPIO service
    # We can do this as the connection is stateless
    # (interrupts for input pins are enabled at service startup)
    gpio = get_gpio_service(dbus_daemon, follow_name_owner_changes=True)

    bus_name = dbus.service.BusName(POWER_SERVICE_NAME, dbus_daemon)
    power = PowerObject(gpio, logger, bus_name, POWER_OBJECT_PATH)

    # Add buses
    for name, n, alerts in i2c_busses:
        power.add_bus(name, Native_PMBus(n), alerts)

    # Add I2C devices
    for name, constructor, bus_name, address in i2c_devices:
        power.add_i2c_device(name, constructor, bus_name, address)

    # Add GPIO devices
    for name, constructor, pin_groups in gpio_devices:
        power.add_gpio_device(name, constructor, pin_groups)
    
    run_mainloop()
    logger.error("Service exiting")


def get_service(dbus_daemon, follow_name_owner_changes=False):
    return dbus_daemon.get_object(POWER_SERVICE_NAME, POWER_OBJECT_PATH, follow_name_owner_changes=follow_name_owner_changes)
