default_limits = {
    'BMC_VCC_3V3' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 3.135,
            "WARN_HIGH"  : 3.465,
            "FAULT_LOW"  : 3.135,
            "FAULT_HIGH" : 3.465,
        },
        "CURRENT" : {
        },
    },
    'BMC_VCC_5V' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 4.75,
            "WARN_HIGH"  : 5.25,
            "FAULT_LOW"  : 4.75,
            "FAULT_HIGH" : 5.25,
        },
        "CURRENT" : {
            # "WARN_HIGH"  : XXX,
            # "FAULT_HIGH" : XXX,
        },
    },
    '12V_CPU0_PSUP' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 11.42,
            "WARN_HIGH"  : 12.58,
            "FAULT_LOW"  : 11.42,
            "FAULT_HIGH" : 12.58,
        },
        "CURRENT" : {
            # "WARN_HIGH"  : XXX,
            # "FAULT_HIGH" : XXX,
        },
    },
    '12V_CPU1_PSUP' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 11.42,
            "WARN_HIGH"  : 12.58,
            "FAULT_LOW"  : 11.42,
            "FAULT_HIGH" : 12.58,
        },
        "CURRENT" : {
            # "WARN_HIGH"  : XXX,
            # "FAULT_HIGH" : XXX,
        },
    },
    '5V_PSUP' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 4.75,
            "WARN_HIGH"  : 5.25,
            "FAULT_LOW"  : 4.75,
            "FAULT_HIGH" : 5.25,
        },
        "CURRENT" : {
            # "WARN_HIGH"  : XXX,
            # "FAULT_HIGH" : XXX,
        },
    },
    '3V3_PSUP' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 3.135,
            "WARN_HIGH"  : 3.465,
            "FAULT_LOW"  : 3.135,
            "FAULT_HIGH" : 3.465,
        },
        "CURRENT" : {
            # "WARN_HIGH"  : XXX,
            # "FAULT_HIGH" : XXX,
        },
    },
    'VDD_CORE' : {
        "VOLTAGE" : {
            # table 41-3 on ThunderX manual
            "WARN_LOW"   : 0.91,
            "WARN_HIGH"  : 0.98,
            # Not writable on the IR3581
            # "FAULT_LOW"  : 0.94,
            # "FAULT_HIGH" : 0.96,
        },
        "CURRENT" : {
            "WARN_HIGH"  : 190.0,
            "FAULT_HIGH" : 240.0,
        },
    },
    '0V9_VDD_OCT' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 0.87,
            "WARN_HIGH"  : 0.93,
            # Not writable on the IR3581
            # "FAULT_LOW"  : 0.87,
            # "FAULT_HIGH" : 0.93,
        },
        "CURRENT" : {
            # XXX - Broad estimate
            "WARN_HIGH"  : 10.0,
            "FAULT_HIGH" : 15.0,
        },
    },
    '1V5_VDD_OCT' : {
        "VOLTAGE" : {
            # table 41-3 on ThunderX manual
            "WARN_LOW"   : 1.45,
            "WARN_HIGH"  : 1.55,
            "FAULT_LOW"  : 1.425,
            "FAULT_HIGH" : 1.575,
        },
        "CURRENT" : {
            # XXX - Broad estimate
            "WARN_HIGH"  : 4.0,
            "FAULT_HIGH" : 6.0,
        },
    },
    '2V5_CPU13' : {
        "VOLTAGE" : {
            # table 41-3 on ThunderX manual
            "WARN_LOW"   : 2.375,
            "WARN_HIGH"  : 2.625,
            "FAULT_LOW"  : 2.375,
            "FAULT_HIGH" : 2.625,
        },
        "CURRENT" : {
        },
    },
    '2V5_CPU24' : {
        "VOLTAGE" : {
            # table 41-3 on ThunderX manual
            "WARN_LOW"   : 2.375,
            "WARN_HIGH"  : 2.625,
            "FAULT_LOW"  : 2.375,
            "FAULT_HIGH" : 2.625,
        },
        "CURRENT" : {
        },
    },
    'VDD_DDRCPU13' : {
        "VOLTAGE" : {
            # p. 7 Samsung DDR4 Datasheet
            # https://www.samsung.com/semiconductor/global.semi/file/resource/2018/05/20170731_TSV_128GB_only_DDR4_8Gb_B_die_Registered_DIMM_Rev1.53_Jun.17.pdf
            "WARN_LOW"   : 1.14,
            "WARN_HIGH"  : 1.26,
            "FAULT_LOW"  : 1.08,
            "FAULT_HIGH" : 1.32,
        },
        "CURRENT" : {
            "WARN_HIGH"  : 23.4,
            "FAULT_HIGH" : 24.0,
        },
    },
    'VTT_DDRCPU13' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 0.57,
            "WARN_HIGH"  : 0.63,
            "FAULT_LOW"  : 0.54,
            "FAULT_HIGH" : 0.66,
        },
        "CURRENT" : {
        },
    },
    'VDD_DDRCPU24' : {
        "VOLTAGE" : {
            # p. 7 Samsung DDR4 Datasheet
            # https://www.samsung.com/semiconductor/global.semi/file/resource/2018/05/20170731_TSV_128GB_only_DDR4_8Gb_B_die_Registered_DIMM_Rev1.53_Jun.17.pdf
            "WARN_LOW": 1.14,
            "WARN_HIGH": 1.26,
            "FAULT_LOW": 1.08,
            "FAULT_HIGH": 1.32,
        },
        "CURRENT" : {
            "WARN_HIGH"  : 23.4,
            "FAULT_HIGH" : 24.0,
        },
    },
    'VTT_DDRCPU24' : {
        "VOLTAGE" : {
            "WARN_LOW": 0.57,
            "WARN_HIGH": 0.63,
            "FAULT_LOW": 0.54,
            "FAULT_HIGH": 0.66,
        },
        "CURRENT" : {
        },
    },
    'UTIL_3V3' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 3.135,
            "WARN_HIGH"  : 3.465,
            "FAULT_LOW"  : 3.135,
            "FAULT_HIGH" : 3.465,
        },
        "CURRENT" : {
            # XXX - Broad estimate
            "WARN_HIGH"  : 5.0,
            "FAULT_HIGH" : 8.0,
        },
    },
    'VCCINTIO_BRAM_FPGA' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 0.873,
            "WARN_HIGH"  : 0.923,
            "FAULT_LOW"  : 0.800,
            "FAULT_HIGH" : 0.923,
        },
        "CURRENT" : {
            # XXX - This must be revisited.
            "WARN_HIGH"  : 10.0,
            "FAULT_HIGH" : 20.0,
        },
    },
    'VCC1V8_FPGA' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 1.71,
            "WARN_HIGH"  : 1.89,
            "FAULT_LOW"  : 1.60,
            "FAULT_HIGH" : 1.89,
        },
        "CURRENT" : {
            # XXX - This must be revisited.
            "WARN_HIGH"  : 10.0,
            "FAULT_HIGH" : 20.0,
        },
    },
    'SYS_1V8' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 1.71,
            "WARN_HIGH"  : 1.89,
            "FAULT_LOW"  : 1.71,
            "FAULT_HIGH" : 1.89,
        },
        "CURRENT" : {
        },
    },
    'SYS_2V5_13' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 2.375,
            "WARN_HIGH"  : 2.625,
            "FAULT_LOW"  : 2.375,
            "FAULT_HIGH" : 2.625,
        },
        "CURRENT" : {
        },
    },
    'SYS_2V5_24' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 2.375,
            "WARN_HIGH"  : 2.625,
            "FAULT_LOW"  : 2.375,
            "FAULT_HIGH" : 2.625,
        },
        "CURRENT" : {
        },
    },
    'MGTAVCC_FPGA' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 0.855,
            "WARN_HIGH"  : 0.945,
            "FAULT_LOW"  : 0.855,
            "FAULT_HIGH" : 0.945,
        },
        "CURRENT" : {
            # XXX - This must be revisited.
            "WARN_HIGH"  : 20.0,
            "FAULT_HIGH" : 20.0,
        },
    },
    'MGTAVTT_FPGA' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 1.164,
            "WARN_HIGH"  : 1.236,
            "FAULT_LOW"  : 1.164,
            "FAULT_HIGH" : 1.236,
        },
        "CURRENT" : {
            # XXX - This must be revisited.
            "WARN_HIGH"  : 20.0,
            "FAULT_HIGH" : 20.0,
        },
    },
    'MGTVCCAUX_L' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 1.71,
            "WARN_HIGH"  : 1.89,
            "FAULT_LOW"  : 1.71,
            "FAULT_HIGH" : 1.89,
        },
        "CURRENT" : {
        },
    },
    'MGTVCCAUX_R' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 1.71,
            "WARN_HIGH"  : 1.89,
            "FAULT_LOW"  : 1.71,
            "FAULT_HIGH" : 1.89,
        },
        "CURRENT" : {
        },
    },
    'VCCINT_FPGA' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 0.873,
            "WARN_HIGH"  : 0.923,
            "FAULT_LOW"  : 0.873,
            "FAULT_HIGH" : 0.923,
        },
        "CURRENT" : {
            # Empirically determined as enough to let a -3E grade FPGA
            # complete programming a large bitstream.
            "WARN_HIGH"  : 120.0,
            "FAULT_HIGH" : 120.0,
        },
    },
    'VDD_DDRFPGA13' : {
        "VOLTAGE" : {
            "WARN_LOW": 1.14,
            "WARN_HIGH": 1.26,
            "FAULT_LOW": 1.08,
            "FAULT_HIGH": 1.32,
        },
        "CURRENT" : {
            "WARN_HIGH"  : 23.4,
            "FAULT_HIGH" : 24.0,
        },
    },
    'VTT_DDRFPGA13' : {
        "VOLTAGE" : {
            "WARN_LOW": 0.57,
            "WARN_HIGH": 0.63,
            "FAULT_LOW": 0.54,
            "FAULT_HIGH": 0.66,
        },
        "CURRENT" : {
        },
    },
    'VDD_DDRFPGA24' : {
        "VOLTAGE" : {
            "WARN_LOW": 1.14,
            "WARN_HIGH": 1.26,
            "FAULT_LOW": 1.08,
            "FAULT_HIGH": 1.32,
        },
        "CURRENT" : {
            "WARN_HIGH"  : 23.4,
            "FAULT_HIGH" : 24.0,
        },
    },
    'VTT_DDRFPGA24' : {
        "VOLTAGE" : {
            "WARN_LOW": 0.57,
            "WARN_HIGH": 0.63,
            "FAULT_LOW": 0.54,
            "FAULT_HIGH": 0.66,
        },
        "CURRENT" : {
        },
    },
    'VADJ_1V8_FPGA' : {
        "VOLTAGE" : {
            "WARN_LOW"   : 1.71,
            "WARN_HIGH"  : 1.89,
            "FAULT_LOW"  : 1.71,
            "FAULT_HIGH" : 1.89,
        },
        "CURRENT" : {
            # XXX - wild mad guessing
            "WARN_HIGH"  : 1.0,
            "FAULT_HIGH" : 2.0,
        },
    },
    "CPU" : {
        "TEMPERATURE" : {
            "FAULT_HIGH" : 90.0,
        },
    },
    "FPGA" : {
        "TEMPERATURE" : {
            "FAULT_HIGH" : 90.0,
        },
    },
    "ir3581_loop_vdd_core" : {
        "TEMPERATURE" : {
	    "WARN_HIGH" : 105.0,
            "FAULT_HIGH" : 125.0,
        },
    },
    "ir3581_loop_0v9_vdd_oct" : {
        "TEMPERATURE" : {
            "FAULT_HIGH" : 100.0,
        },
    },
    "max20751_vccint_fpga" : {
        "TEMPERATURE" : {
            "FAULT_HIGH" : 100.0,
        },
    },
    "max20751_mgtavcc_fpga" : {
        "TEMPERATURE" : {
            "FAULT_HIGH" : 100.0,
        },
    },
    "max20751_mgtavtt_fpga" : {
        "TEMPERATURE" : {
            "FAULT_HIGH" : 100.0,
        },
    },
    # XXX - add MAX15301
}
