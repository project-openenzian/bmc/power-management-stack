scram_faults = set([
    ("ir3581_loop_vdd_core",        "IOUT",    "FAULT_HIGH"),
    ("ir3581_loop_vdd_core",        "VOUT",    "FAULT_HIGH"),
    ("ir3581_loop_0v9_vdd_oct",     "IOUT",    "FAULT_HIGH"),
    ("ir3581_loop_0v9_vdd_oct",     "VOUT",    "FAULT_HIGH"),
    # XXX - The 9P draws a large transient current on startup (>200A), so this
    # needs to be a non-scram fault, at least at startup.  We need a more
    # sophisticated fault response for this.
    # ("max20751_vccint_fpga",        "IOUT",    "WARN_HIGH"),
    ("max20751_mgtavcc_fpga",       "IOUT",    "WARN_HIGH"),
    ("max20751_mgtavtt_fpga",       "IOUT",    "WARN_HIGH"),
    ("max15301_vcc1v8_fpga",        "IOUT",    "FAULT_HIGH"),
    ("max15301_vcc1v8_fpga",        "VOUT",    "FAULT_HIGH"),
    ("max15301_util_3v3",           "IOUT",    "FAULT_HIGH"),
    ("max15301_util_3v3",           "VOUT",    "FAULT_HIGH"),
    ("max15301_vadj_1v8",           "IOUT",    "FAULT_HIGH"),
    ("max15301_vadj_1v8",           "VOUT",    "FAULT_HIGH"),
    ("max15301_vccintio_bram_fpga", "IOUT",    "FAULT_HIGH"),
    ("max15301_vccintio_bram_fpga", "VOUT",    "FAULT_HIGH"),
    # XXX - alert pin not connected.
    # ("ina226_ddr_cpu_13",           "CURRENT", "WARN_HIGH" ),
    # ("ina226_ddr_cpu_24",           "CURRENT", "WARN_HIGH" ),
    # ("ina226_ddr_fpga_13",          "CURRENT", "WARN_HIGH" ),
    # ("ina226_ddr_fpga_24",          "CURRENT", "WARN_HIGH" ),
    ("fans",                     "T_DIODE_0", "FAULT_HIGH"),
    ("fans",                     "T_DIODE_1", "FAULT_HIGH"),
    ("fans",                    "T_INTERNAL", "FAULT_HIGH"),
])

def scram(logger, gpio, power):
    """
    Emergency shutdown on all rails.
    """
    # XXX - This is topology-dependent policy, and should be unified and
    # automatically generated.

    # Set CPU + FPGA fans to 100%
    # This also works if they are on automatic control as they
    # go to 100% if no update to the fan speed is made within 10s
    power.enable_device_control("fans", 'FAN_0_PWM')
    power.enable_device_control("fans", 'FAN_1_PWM')
    power.set_device_control("fans", "FAN_0_PWM", 100.0)
    power.set_device_control("fans", "FAN_1_PWM", 100.0)

    # Place CPU in reset.
    gpio.set_value('C_PLL_DCOK', False)
    gpio.set_value('C_RESET_N', False)

    # Bring all enables low.
    power.device_write("pac_fpga", "GP_OUT20",  False)
    power.device_write("pac_fpga", "GP_OUT19",  False)
    power.device_write("pac_fpga", "GP_OUT18",  False)
    power.device_write("pac_fpga", "GP_OUT17",  False)
    power.device_write("pac_fpga", "GP_OUT16",  False)
    power.device_write("pac_fpga", "GP_OUT15",  False)
    power.device_write("pac_fpga", "GP_OUT14",  False)
    power.device_write("pac_fpga", "GP_OUT13",  False)
    power.device_write("pac_fpga", "GP_OUT12",  False)
    power.device_write("pac_fpga", "GP_OUT11",  False)
    power.device_write("pac_fpga", "GP_OUT10",  False)
    power.device_write("pac_fpga", "GP_OUT9",   False)
    power.device_write("pac_fpga", "GP_OUT8",   False)
    power.device_write("pac_fpga", "GP_OUT7",   False)
    power.device_write("pac_fpga", "GP_OUT6",   False)
    power.device_write("pac_fpga", "GP_OUT5",   False)
    power.device_write("pac_fpga", "GP_HVOUT4", False)
    power.device_write("pac_fpga", "GP_HVOUT3", False)
    power.device_write("pac_fpga", "GP_HVOUT2", False)
    power.device_write("pac_fpga", "GP_HVOUT1", False)

    power.device_write("pac_cpu", "GP_OUT20",  False)
    power.device_write("pac_cpu", "GP_OUT19",  False)
    power.device_write("pac_cpu", "GP_OUT18",  False)
    power.device_write("pac_cpu", "GP_OUT17",  False)
    power.device_write("pac_cpu", "GP_OUT16",  False)
    power.device_write("pac_cpu", "GP_OUT15",  False)
    power.device_write("pac_cpu", "GP_OUT14",  False)
    power.device_write("pac_cpu", "GP_OUT13",  False)
    power.device_write("pac_cpu", "GP_OUT12",  False)
    power.device_write("pac_cpu", "GP_OUT11",  False)
    power.device_write("pac_cpu", "GP_OUT10",  False)
    power.device_write("pac_cpu", "GP_OUT9",   False)
    power.device_write("pac_cpu", "GP_OUT8",   False)
    power.device_write("pac_cpu", "GP_OUT7",   False)
    power.device_write("pac_cpu", "GP_OUT6",   False)
    power.device_write("pac_cpu", "GP_OUT5",   False)
    power.device_write("pac_cpu", "GP_HVOUT4", False)
    power.device_write("pac_cpu", "GP_HVOUT3", False)
    power.device_write("pac_cpu", "GP_HVOUT2", False)
    power.device_write("pac_cpu", "GP_HVOUT1", False)

    # Turn PSU off but first disable alerts for devices on PSUP rails
    power.disable_bus_alert("pwr_fan", "B_PWR_ALERT_N")
    gpio.set_value("B_PSUP_ON", False)

    logger.error("SCRAM: emergency shutdown complete")
