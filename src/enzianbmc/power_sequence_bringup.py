from collections import defaultdict
import logging, ctypes
import time

from bmctools.pmbus.configs.ir3581_configuration import IR3581_Configuration
from bmctools.pmbus.configs.si5395_configuration import SI5395_Configuration

from bmctools.dbus.util import to_dbus, from_dbus, run_mainloop
from dbus import DBusException

from .limits import default_limits
from .topology import (
    enable_map, voltage_monitor_map, current_monitor_map,
    temp_monitor_map, fanspeed_monitor_map, control_map, suppressed_comparators,
    monitor_scale
)
from .device_configuration import (
    startup_config, startup_control_state
)

class OutOfRange(Exception):
    pass

class Timeout(Exception):
    pass

class SequenceFailure(Exception):
    pass

def init_bringup_script(gpio_service, power_service, fault_service,
                        telemetry_service):
    global power, gpio, fault, telemetry
    global monitors_by_device, controls_by_device
    gpio = gpio_service
    power = power_service
    fault = fault_service
    telemetry = telemetry_service

    # Construct device->monitor mapping from topology.
    monitors_by_device= defaultdict(lambda: [])
    for node in voltage_monitor_map:
        for device, monitor in voltage_monitor_map[node]:
            monitors_by_device[device].append((monitor, node, "VOLTAGE"))
    for node in current_monitor_map:
        for device, monitor in current_monitor_map[node]:
            monitors_by_device[device].append((monitor, node, "CURRENT"))
    for node in temp_monitor_map:
        for device, monitor in temp_monitor_map[node]:
            monitors_by_device[device].append((monitor, node, "TEMPERATURE"))
    for node in fanspeed_monitor_map:
        for device, monitor in fanspeed_monitor_map[node]:
            monitors_by_device[device].append((monitor, node, "FANSPEED"))

    # Construct device->control mapping from topology.
    controls_by_device= defaultdict(lambda: [])
    for node in control_map:
        device, control= control_map[node]
        controls_by_device[device].append((control, node))

class timespec(ctypes.Structure):
    _fields_ = [
        ('tv_sec', ctypes.c_long),
        ('tv_nsec', ctypes.c_long)
    ]

librt = ctypes.CDLL('librt.so.1', use_errno=True)
clock_gettime = librt.clock_gettime
clock_gettime.argtypes = [ctypes.c_int, ctypes.POINTER(timespec)]

"""thresholds """
MAX_DDR_V = 1.5
MIN_DDR_V = 0.0
""" ISL6334 Accuracy from datasheet
    1V -> 1V6 +- 0.5% VID
    0.5V -> 1V +- 5mV
"""
THRESH = 0.05

def read_scaled_monitor(device, monitor):
    if (device, monitor) in monitor_scale:
        scale= monitor_scale[(device, monitor)]
    else:
        scale= 1.0

    return power.read_device_monitor(device, monitor) * scale

def read_voltage(node):
    """
    Read the 'most trusted' voltage monitor for a node.
    """
    try:
        device, monitor= voltage_monitor_map[node][0]
    except KeyError:
        raise ValueError("Unknown node %s" % node)
    except IndexError:
        raise ValueError("No voltage monitors for %s" % node)

    try:
        return read_scaled_monitor(device, monitor)
    except DBusException:
        return None

def read_voltage_all_monitors(node):
    """
    Read all voltage monitors for a node.
    """
    try:
        monitors= voltage_monitor_map[node]
    except KeyError:
        raise ValueError("Unknown node %s" % node)

    values= []
    for device, monitor in monitors:
        try:
            values.append((device, monitor,
                          read_scaled_monitor(device, monitor)))
        except DBusException:
            values.append(None)
    return values

def read_voltage_all():
    values= {}
    for node in voltage_monitor_map:
        values[node]= read_voltage_all_monitors(node)
    return values

def print_voltage_all():
    readings= read_voltage_all()
    nodes= readings.keys()
    for node in sorted(nodes):
        out= "%30s" % node
        for reading in readings[node]:
            if reading is None:
                out+= "      -"
            else:
                device, monitor, value= reading
                out+= " %6.2f" % value
        print(out)

def read_current(node):
    """
    Read the 'most trusted' current monitor for a node.
    """
    try:
        device, monitor= current_monitor_map[node][0]
    except KeyError:
        raise ValueError("Unknown node %s" % node)
    except IndexError:
        raise ValueError("No current monitors for %s" % node)

    try:
        return read_scaled_monitor(device, monitor)
    except DBusException:
        return None

def read_current_all_monitors(node):
    """
    Read all current monitors for a node.
    """
    try:
        monitors= current_monitor_map[node]
    except KeyError:
        raise ValueError("Unknown node %s" % node)

    values= []
    for device, monitor in monitors:
        try:
            values.append((device, monitor,
                          read_scaled_monitor(device, monitor)))
        except DBusException:
            values.append(None)
    return values

def read_current_all():
    values= {}
    for node in current_monitor_map:
        values[node]= read_current_all_monitors(node)
    return values

def print_current_all():
    readings= read_current_all()
    nodes= readings.keys()
    for node in sorted(nodes):
        out= "%30s" % node
        for reading in readings[node]:
            if reading is None:
                out+= "      -"
            else:
                device, monitor, value= reading
                out+= " %6.2f" % value
        print(out)

def read_temp(node):
    """
    Read the 'most trusted' temperature monitor for a node.
    """
    try:
        device, monitor= temp_monitor_map[node]
    except KeyError:
        raise ValueError("Unknown node %s" % node)
    except IndexError:
        raise ValueError("No current monitors for %s" % node)

    try:
        return read_scaled_monitor(device, monitor)
    except DBusException:
        return None

def read_temp_all_monitors(node):
    """
    Read all temperature monitors for a node.
    """
    try:
        monitors= temp_monitor_map[node]
    except KeyError:
        raise ValueError("Unknown node %s" % node)

    values= []
    for device, monitor in monitors:
        try:
            values.append((device, monitor,
                          read_scaled_monitor(device, monitor)))
        except DBusException:
            values.append(None)
    return values

def read_temp_all():
    values= {}
    for node in temp_monitor_map:
        values[node]= read_temp_all_monitors(node)
    return values

def print_temp_all():
    readings= read_temp_all()
    nodes= readings.keys()
    for node in sorted(nodes):
        out= "%30s" % node
        for reading in readings[node]:
            if reading is None:
                out+= "      -"
            else:
                device, monitor, value= reading
                out+= " %6.2f" % value
        print(out)

def read_fan_all_monitors(node):
    """
    Read fan speeds for a node.
    """
    try:
        monitors= fanspeed_monitor_map[node]
    except KeyError:
        raise ValueError("Unknown node %s" % node)

    values= []
    for device, monitor in monitors:
        try:
            values.append((device, monitor,
                          read_scaled_monitor(device, monitor)))
        except DBusException:
            values.append(None)
    return values

def read_fan_all():
    values= {}
    for node in fanspeed_monitor_map:
        values[node]= read_fan_all_monitors(node)
    return values

def print_fan_all():
    readings= read_fan_all()
    nodes= readings.keys()
    for node in sorted(nodes):
        out= "%30s" % node
        for reading in readings[node]:
            if reading is None:
                out+= "      -"
            else:
                device, monitor, value = reading
                if monitor[-3:] == "PWM":
                    out += " (%3d%%)" % value
                else:
                    out += " %6.2f" % value
        print(out)

def wait_for_voltage(node, v_min=None, v_max=None, 
                           rise_time=0, hold_time=0.1):
    """
    Verify that the given voltage node settles to within the given range
    within the given timeout.
    """

    # By default, check against the (tighter) warning limits.
    if v_min is None:
        v_min= default_limits[node]["VOLTAGE"]["WARN_LOW"]
    if v_max is None:
        v_max= default_limits[node]["VOLTAGE"]["WARN_HIGH"]

    logging.info("Waiting for %s to settle between %rV and %rV",
                node, v_min, v_max)

    # Wait for at least as long as the specified rise time.
    time.sleep(rise_time)

    V = 0
    start = time.monotonic()
    while time.monotonic() - start < hold_time:
        V = read_voltage(node)

        if V >= v_min and V <= v_max:
            print('Voltage %s within <%r,%r> after %r' %
                (node, v_min, v_max, rise_time + time.monotonic() - start))
            return
    logging.error('Voltage %s out of range after %rs: %r',
        node, rise_time + hold_time, V)
    raise OutOfRange('Voltage %s out of range after %rs: %r',
        node, rise_time + hold_time, V)

def wait_for(name, f, v, timeout):
    logging.info("Waiting %rs for %s", timeout, name)

    now = time.monotonic()
    while True:
        rv = f()
        elapsed = time.monotonic() - now
        if rv == v:
            print('%s is %r after %r (limit %r)' %
                  (name, rv, elapsed, timeout))
            break
        if elapsed >= timeout:
            raise Timeout('Timeout: %s is %r after %r (limit %r)' %
                          (name, rv, elapsed, timeout))

def enable_node(node, **kwargs):
    """
    Enable a voltage node (turn it on) via the appropriate signal.
    """
    try:
        device, control= enable_map[node]
    except KeyError:
        raise ValueError("Node %s has no enable control defined." % node)

    logging.info("Enabling node %s", node)

    # Turn it on.
    power.set_device_control(device, control, True)

    # Verify that it comes up within limits.
    wait_for_voltage(node, **kwargs)

def disable_node(node):
    """
    Disable a voltage node (turn it off) via the appropriate signal.
    """
    try:
        device, control= enable_map[node]
    except KeyError:
        raise ValueError("Node %s has no enable control defined." % node)

    logging.info("Disabling node %s", node)

    power.set_device_control(device, control, False)

def control_node(node, value):
    """
    Command a controlled node to a desired value.
    """
    try:
        device, control= control_map[node]
    except KeyError:
        raise ValueError("Node %s has no controller defined." % node)

    logging.info("Commanding node %s to %r", node, value)

    power.set_device_control(device, control, value)

def psup_on():
    """
    Turns on power supply
    """
    gpio.set_value("B_PSUP_ON", True)

def psup_off():
    """
    Turns off power supply
    """
    gpio.set_value("B_PSUP_ON", False)

def cpu_fan_on(level=-1):
    """
    Turns the CPU fan on
    """
    device, control = control_map["CPU_FAN"]
    # TSFO should be False for auto fan control and True for manual
    # (controls if it should ramp to 100% on temp reading fault or no manual update within 10s)
    fan_cfg = from_dbus(power.device_read(device, ("MFR_FAN_CONFIG", 0)))
    fan_cfg["TSFO"] = level >= 0
    power.device_write(device, ("MFR_FAN_CONFIG", 0), to_dbus(fan_cfg))
    control_node("CPU_FAN", level)
    power.enable_device_control(device, control)

def fpga_fan_on(level=-1):
    """
    Turns the FPGA fan on
    """
    device, control = control_map["FPGA_FAN"]
    # TSFO should be False for auto fan control and True for manual
    # (controls if it should ramp to 100% on temp reading fault or no manual update within 10s)
    fan_cfg = from_dbus(power.device_read(device, ("MFR_FAN_CONFIG", 1)))
    fan_cfg["TSFO"] = level >= 0
    power.device_write(device, ("MFR_FAN_CONFIG", 1), to_dbus(fan_cfg))
    control_node("FPGA_FAN", level)
    power.enable_device_control(device, control)

def case_fan_0_on(level=-1):
    """
    Turns Case fan 0 on
    """
    device, control = control_map["CASE_FAN_0"]
    # TSFO should be False for auto fan control and True for manual
    # (controls if it should ramp to 100% on temp reading fault or no manual update within 10s)
    fan_cfg = from_dbus(power.device_read(device, ("MFR_FAN_CONFIG", 2)))
    fan_cfg["TSFO"] = level >= 0
    power.device_write(device, ("MFR_FAN_CONFIG", 2), to_dbus(fan_cfg))
    control_node("CASE_FAN_0", level)
    power.enable_device_control(device, control)

def case_fan_1_on(level=-1):
    """
    Turns Case fan 1 on
    """
    device, control = control_map["CASE_FAN_1"]
    # TSFO should be False for auto fan control and True for manual
    # (controls if it should ramp to 100% on temp reading fault or no manual update within 10s)
    fan_cfg = from_dbus(power.device_read(device, ("MFR_FAN_CONFIG", 3)))
    fan_cfg["TSFO"] = level >= 0
    power.device_write(device, ("MFR_FAN_CONFIG", 3), to_dbus(fan_cfg))
    control_node("CASE_FAN_1", level)
    power.enable_device_control(device, control)

def case_fan_2_on(level=-1):
    """
    Turns Case fan 2 on
    """
    device, control = control_map["CASE_FAN_2"]
    # TSFO should be False for auto fan control and True for manual
    # (controls if it should ramp to 100% on temp reading fault or no manual update within 10s)
    fan_cfg = from_dbus(power.device_read(device, ("MFR_FAN_CONFIG", 4)))
    fan_cfg["TSFO"] = level >= 0
    power.device_write(device, ("MFR_FAN_CONFIG", 4), to_dbus(fan_cfg))
    control_node("CASE_FAN_2", level)
    power.enable_device_control(device, control)

def case_fan_3_on(level=-1):
    """
    Turns Case fan 3 on
    """
    device, control = control_map["CASE_FAN_3"]
    # TSFO should be False for auto fan control and True for manual
    # (controls if it should ramp to 100% on temp reading fault or no manual update within 10s)
    fan_cfg = from_dbus(power.device_read(device, ("MFR_FAN_CONFIG", 5)))
    fan_cfg["TSFO"] = level >= 0
    power.device_write(device, ("MFR_FAN_CONFIG", 5), to_dbus(fan_cfg))
    control_node("CASE_FAN_3", level)
    power.enable_device_control(device, control)

def cpu_fan_off():
    """
    Turns the CPU fan off
    """
    device, control = control_map["CPU_FAN"]
    power.disable_device_control(device, control)

def fpga_fan_off():
    """
    Turns the FPGA fan off
    """
    device, control = control_map["FPGA_FAN"]
    power.disable_device_control(device, control)

def case_fan_0_off():
    """
    Turns Case Fan 0 off
    """
    device, control = control_map["CASE_FAN_0"]
    power.disable_device_control(device, control)

def case_fan_1_off():
    """
    Turns Case Fan 1 off
    """
    device, control = control_map["CASE_FAN_1"]
    power.disable_device_control(device, control)

def case_fan_2_off():
    """
    Turns Case Fan 2 off
    """
    device, control = control_map["CASE_FAN_2"]
    power.disable_device_control(device, control)

def case_fan_3_off():
    """
    Turns Case Fan 3 off
    """
    device, control = control_map["CASE_FAN_3"]
    power.disable_device_control(device, control)

def case_fan_all_on(level=-1):
    """
    Turns all case fans on
    """
    case_fan_0_on(level=level)
    case_fan_1_on(level=level)
    case_fan_2_on(level=level)
    case_fan_3_on(level=level)

def case_fan_all_off():
    """
    Turns all case fans off
    """
    case_fan_0_off()
    case_fan_1_off()
    case_fan_2_off()
    case_fan_3_off()

def fans_on(level=-1):
    """
    Turns all fans on
    """
    cpu_fan_on(level=level)
    fpga_fan_on(level=level)
    case_fan_all_on(level=level)

def fans_off():
    """
    Turns all fans on
    """
    cpu_fan_off()
    fpga_fan_off()
    case_fan_all_off()

def set_thresholds(device):
    """
    Set all defined comparator thresholds for the given device.
    """
    power.clear_device_alerts(device)

    for monitor, node, quantity in monitors_by_device[device]:
        if node in default_limits and quantity in default_limits[node]:
            thresholds= default_limits[node][quantity]
            comparators= power.list_comparators(device, monitor)

            for name, value in thresholds.items():
                if (device, monitor, name) in suppressed_comparators or \
                        not name in comparators:
                    continue
                logging.info(
                    "Setting threshold %s, monitor %s, device %s to %r",
                    name, monitor, device, value)
                power.set_device_threshold(device, monitor, name, value)

                logging.info("Enabling alert %s.%s.%s", device, monitor, name)
                power.enable_device_alert(device, monitor, name)

def enable_monitors(device):
    for monitor, node, quantity in monitors_by_device[device]:
        logging.info("Enabling monitor %s on device %s (node %s).",
                     monitor, device, node)
        power.enable_device_monitor(device, monitor)

def init_controls(device):
    for control, node in controls_by_device[device]:
        if node in startup_control_state:
            value, enabled= startup_control_state[node]
            logging.info("Setting control %s.%s for node %s to"
                         " initial value %r", device, control, node, value)
            power.set_device_control(device, control, value)
            if enabled:
                logging.info("Enabling control %s.%s", device, control)
                power.enable_device_control(device, control)
            else:
                logging.info("Disabling control %s.%s", device, control)
                power.disable_device_control(device, control)
        else:
            logging.info("No startup state for controlled node %s, leaving"
                         " disabled.", node)

def init_device(device):
    logging.info("Initialising %s", device)
    power.init_driver(device)
    power.device_configure(device, to_dbus(startup_config[device]))
    enable_monitors(device)
    set_thresholds(device)
    init_controls(device)

def deinit_device(device):
    logging.info("Deinitialising %s", device)
    power.reset_driver(device)

###
# Handcoded Sequences
###

def deinit_sequencers():
    # First disable alert, as we will not be able to handle them after
    # deinitialization
    power.disable_bus_alerts('seq')
    deinit_device('pac_cpu')
    deinit_device('pac_fpga')

def init_sequencers():
    # Enable alert first as initializing might already cause one
    power.enable_bus_alerts('seq')
    init_device('pac_cpu')
    init_device('pac_fpga')

def init_gpios():
    logging.info("Setting CPU controls to a safe value.")
    gpio.set_value('C_PLL_DCOK', False)
    gpio.set_value('C_RESET_N', False)
    gpio.set_value('B_OCI2_LNK1', False)
    gpio.set_value('B_OCI3_LNK1', False)

def psu_power_down():
    # Disable the alerts before we turn the light off
    power.disable_bus_alert('pwr_fan', 'B_PWR_ALERT_N')
    psup_off()

def psu_power_up():
    # Pull PS_ON low.
    logging.info("Enabling PSU")
    psup_on()

    try:
        # Wait for PGOOD
        wait_for('PSUP_PGOOD',
                 lambda: "IN1" in power.device_read("pac_cpu", "INPUT_STATUS"),
                 True, 1)

        # Only enable PWR_ALERT after PGODD asserted so it's stable
        power.enable_bus_alert('pwr_fan', 'B_PWR_ALERT_N')

        logging.info("PSU up, checking voltages.")
        wait_for_voltage('12V_CPU0_PSUP')
        wait_for_voltage('12V_CPU1_PSUP')
        wait_for_voltage('5V_PSUP')
        wait_for_voltage('3V3_PSUP')
    except Exception as e:
        psu_power_down()
        logging.error("psu_power_up failed: %s", e)
        raise SequenceFailure(e)

def deinit_clocks():
    deinit_device('clk_fpga')
    deinit_device('clk_cpu')
    deinit_device('clk_main')

def init_clocks():
    init_device('clk_main')
    init_device('clk_cpu')
    init_device('clk_fpga')

def deinit_ir3581():
    deinit_device('ir3581_loop_0v9_vdd_oct')
    deinit_device('ir3581_loop_vdd_core')
    deinit_device('ir3581')

def init_ir3581():
    init_device('ir3581')
    init_device('ir3581_loop_vdd_core')
    init_device('ir3581_loop_0v9_vdd_oct')

def deinit_fan_control():
    # First disable alert, as we will not be able to handle them after
    # deinitialization
    power.disable_bus_alert("pwr_fan", "B_FAN_ALERT_N")
    deinit_device("fans")

def init_fan_control():
    """
    Initialise fan control and CPU temperature monitor.
    """
    logging.info("Initialising fan and temperature controller")

    # Reset HW and driver.
    gpio.set_value("B_FAN_RESET_N", False)
    time.sleep(0.01)
    power.reset_driver("fans")
    gpio.set_value("B_FAN_RESET_N", True)

    # Enable alert first as initializing might already cause one
    power.enable_bus_alert("pwr_fan", "B_FAN_ALERT_N")
    # Initialize the driver and device config
    init_device("fans")

def deinit_devices_psup():
    deinit_device('isl6334d_ddr_v_cpu')
    deinit_device('isl6334d_ddr_v_fpga')
    deinit_device('max15301_1v5_vdd_oct')
    deinit_device('max15301_vcc1v8_fpga')
    deinit_device('max15301_util_3v3')
    deinit_device('max15301_vadj_1v8')
    deinit_device('max15301_vccintio_bram_fpga')
    deinit_device('ina226_ddr_cpu_13')
    deinit_device('ina226_ddr_cpu_24')
    deinit_ir3581()
    deinit_clocks()

def init_devices_psup():
    """
    Initialise and configure all devices accessible once PSU rails are up.
    """
    init_clocks()

    # The IR3581 is on 12V_CPU0_PSUP.
    init_ir3581()

    # One MAX15301 is on 12V_CPU0_PSUP, and the rest on 12V_CPU1_PSUP.
    init_device('max15301_1v5_vdd_oct')
    init_device('max15301_vcc1v8_fpga')
    init_device('max15301_util_3v3')
    init_device('max15301_vadj_1v8')
    init_device('max15301_vccintio_bram_fpga')

    # The two INA226 for CPU DRAM are on 3V3_PSUP.
    init_device('ina226_ddr_cpu_13')
    init_device('ina226_ddr_cpu_24')

    # The 4 ISL6334s are on 5V_PSUP, and present only one interface for each
    # socket.
    init_device('isl6334d_ddr_v_cpu')
    init_device('isl6334d_ddr_v_fpga')

def common_power_down():
    fault.mask_scram()
    deinit_devices_psup()
    fans_off()
    psu_power_down()
    deinit_sequencers()
    deinit_fan_control()

def common_power_up():
    """
    Power up all components shared between CPU and FPGA power trees.  This
    includes power sequencers and fan controller.

    Once this completes, overlimit fault/alert responses are operational.
    """
    # Alerts are unreliable until at least the fan controller is initialised.
    fault.mask_scram()

    # Fan controller, sequencers and GPIOs don't need PSUP to be on
    init_fan_control()
    init_sequencers()
    init_gpios()
    psu_power_up()
    init_devices_psup()

    fans_on()

    # Once the fan controller is initialised, we can use its FAULT output.
    fault.unmask_scram()

def cpu_power_down():
    # Signal the CPU that power is lost.
    logging.info("Deasserting C_PLL_DCOK.")
    cpu_stop()
    gpio.set_value('C_PLL_DCOK', False)

    # Before powering the CPU down, switch the flash SPI signals back to the
    # BMC, to avoid floating inputs, and to allow for reflashing.
    logging.info("Returning SPI flash control to BMC.")
    gpio.set_value("B_SPI_SEL_N", False)

    # Disable rails in reverse order of power up.
    logging.info("Disabling CPU power rails.")
    disable_node('VDD_DDRCPU24')
    disable_node('VDD_DDRCPU13')
    disable_node('2V5_CPU24')
    disable_node('2V5_CPU13')
    disable_node('1V5_VDD_OCT')
    disable_node('0V9_VDD_OCT')
    disable_node('VDD_CORE')

def cpu_power_up():
    try:
        # Enable rails - they will be checked one-by-one.
        logging.info("Enabling CPU power rails.")
        cpu_stop()
        enable_node('VDD_CORE')
        enable_node('0V9_VDD_OCT')
        enable_node('1V5_VDD_OCT')
        enable_node('2V5_CPU13')
        enable_node('2V5_CPU24')
        enable_node('VDD_DDRCPU13')
        enable_node('VDD_DDRCPU24')

        # Now that the CPU is powered, we can safely switch the flash SPI
        # multiplexer to it, so it can read it to boot.
        logging.info("Handing SPI flash control to CPU.")
        gpio.set_value("B_SPI_SEL_N", True)

        # Signal the CPU that power is good.
        logging.info("Asserting C_PLL_DCOK.")
        gpio.set_value('C_PLL_DCOK', True)
        cpu_run()
    # A rail failed to stabilise in time.
    except Exception as e:
        # Turn everything back off.
        cpu_stop()
        cpu_power_down()

        logging.error("cpu_power_up() failed: %s", e)
        raise SequenceFailure(e)

def cpu_stop():
    logging.info("Placing CPU in reset.")
    gpio.set_value('C_RESET_N', False)

def cpu_run():
    logging.info("Taking CPU out of reset.")
    gpio.set_value('C_RESET_N', True)

def cpu_reset():
    cpu_stop()
    time.sleep(0.01)
    cpu_run()

def deinit_devices_util_3v3():
    deinit_device('max20751_vccint_fpga')
    deinit_device('max20751_mgtavcc_fpga')
    deinit_device('max20751_mgtavtt_fpga')
    deinit_device('ina226_ddr_fpga_13')
    deinit_device('ina226_ddr_fpga_24')

def init_devices_util_3v3():
    """
    Initialise devices that depend on UTIL_3V3.
    """

    logging.info("Initialising devices on UTIL_3V3.")

    # All MAX20751 use this for their logic supply.
    init_device('max20751_vccint_fpga')
    init_device('max20751_mgtavcc_fpga')
    init_device('max20751_mgtavtt_fpga')

    # The FPGA DRAM INA226 can now be initialised.
    init_device('ina226_ddr_fpga_13')
    init_device('ina226_ddr_fpga_24')

def fpga_power_down():
    # Reverse the power-up order.
    disable_node('MGTVCCAUX_R')
    disable_node('MGTVCCAUX_L')
    disable_node('MGTAVTT_FPGA')
    disable_node('MGTAVCC_FPGA')
    disable_node('VADJ_1V8_FPGA')
    disable_node('VDD_DDRFPGA24')
    disable_node('VDD_DDRFPGA13')
    disable_node('SYS_2V5_24')
    disable_node('SYS_2V5_13')
    disable_node('SYS_1V8')
    disable_node('VCC1V8_FPGA')
    disable_node('VCCINTIO_BRAM_FPGA')
    disable_node('VCCINT_FPGA')
    deinit_devices_util_3v3()
    disable_node('UTIL_3V3')

def fpga_power_up():
    try:
        # First UTIL_3V3, on which several regulators depend.
        logging.info("Enabling UTIL_3V3.")
        enable_node('UTIL_3V3', rise_time = 0.1) # MAX15301 needs some time
        init_devices_util_3v3()

        # The rest can be enabled one-by-one
        logging.info("Enabling remaining FPGA rails.")
        enable_node('VCCINT_FPGA')
        enable_node('VCCINTIO_BRAM_FPGA', rise_time = 0.1)
        enable_node('VCC1V8_FPGA', rise_time = 0.1)
        enable_node('SYS_1V8')
        enable_node('SYS_2V5_13')
        enable_node('SYS_2V5_24')
        enable_node('VDD_DDRFPGA13')
        enable_node('VDD_DDRFPGA24')
        enable_node('VADJ_1V8_FPGA', rise_time = 0.1)
        enable_node('MGTAVCC_FPGA')
        enable_node('MGTAVTT_FPGA')
        enable_node('MGTVCCAUX_L')
        enable_node('MGTVCCAUX_R')
    except Exception as e:
        # Turn everything back off.
        fpga_power_down()

        logging.error("fpga_power_up() failed: %s", e)
        raise SequenceFailure(e)

def power_down():
    cpu_stop()
    cpu_power_down()
    fpga_power_down()
    deinit_devices_psup()
    psu_power_down()
    deinit_sequencers()

def power_up():
    try:
        logging.info("Common powerup sequence.")
        common_power_up()

        wait_for('B_CLOCK_CLOL', lambda: gpio.get_value('B_CLOCK_CLOL'),
                 True, 10)
        wait_for('B_CLOCK_FLOL', lambda: gpio.get_value('B_CLOCK_FLOL'),
                 True, 10)
        logging.info("FPGA powerup sequence.")
        fpga_power_up()

        logging.info("CPU powerup sequence.")
        wait_for('B_CLOCK_BLOL', lambda: gpio.get_value('B_CLOCK_BLOL'),
                 True, 10)
        cpu_power_up()

        cpu_run()
    except Exception as e:
        power_down()

        logging.error("power_up() failed: %s", e)
        raise SequenceFailure(e)
