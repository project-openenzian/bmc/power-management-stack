from bmctools.pmbus.configs.ir3581_configuration import IR3581_Configuration
from bmctools.pmbus.configs.si5395_configuration import SI5395_Configuration

# This is the equivalent shunt resistance seen by the INA226.  This ain't
# pretty.
ddr_rshunt_equiv = ((10.7e3 + 866) / 2) * (1e-3 / 324) * (866 / (10.7e3 + 866))

_default_ina226 = {
    "R_SHUNT" : ddr_rshunt_equiv,
    "TARGET_I_LSB" : 1e-3, # 1mA resolution
    "VBUSCT" : 1.100e-3, # 1.1ms conversion time
    "VSHCT" : 1.100e-3,
}

startup_config = {
    "pac_cpu" : {
        "RESET" : True,
    },
    "pac_fpga" : {
        "RESET" : True,
    },
    "clk_main" : SI5395_Configuration.main,
    "clk_cpu" : SI5395_Configuration.cpu,
    "clk_fpga" : SI5395_Configuration.fpga,
    "fans" : {
        # Set CPU and FPGA overtemperature to signal a fault.
        ("MFR_FAULT_RESPONSE", 6) : {
            "NV_LOG"              : True,
            "NV_LOG_OV"           : False,
            "UV_LOG_FILTER"       : False,
            "FAULT_PIN_ENABLE_OV" : True,
            "FAULT_PIN_ENABLE"    : True,
            "FAULT_PIN_MONITOR"   : False,
        },
        ("MFR_FAULT_RESPONSE", 7) : {
            "NV_LOG"              : True,
            "NV_LOG_OV"           : False,
            "UV_LOG_FILTER"       : False,
            "FAULT_PIN_ENABLE_OV" : True,
            "FAULT_PIN_ENABLE"    : True,
            "FAULT_PIN_MONITOR"   : False,
        },
        # Set CPU and FPGA fans to ramp to 100% on fault.
        ("MFR_FAULT_RESPONSE", 0) : {
            "NV_LOG"              : True,
            "NV_LOG_OV"           : False,
            "UV_LOG_FILTER"       : False,
            "FAULT_PIN_ENABLE_OV" : False,
            "FAULT_PIN_ENABLE"    : False,
            "FAULT_PIN_MONITOR"   : True,
        },
        ("MFR_FAULT_RESPONSE", 1) : {
            "NV_LOG"              : True,
            "NV_LOG_OV"           : False,
            "UV_LOG_FILTER"       : False,
            "FAULT_PIN_ENABLE_OV" : False,
            "FAULT_PIN_ENABLE"    : False,
            "FAULT_PIN_MONITOR"   : True,
        },
        # 25kHz PWM, do not spin to 100% on no update.
        ("MFR_FAN_CONFIG", 0) : {
            "FREQ"        : 25e3,
            "DUAL_TACH"   : False,
            "HYS"         : 2.0,
            "TSFO"        : False,
            "TACHO"       : False,
            "RAMP"        : (200.0, 2.0, 6.0),
            "HEALTH"      : False,
            "ROTOR_HI_LO" : False,
            "ROTOR"       : False,
            "SPIN"        : 2,
        },
        ("MFR_FAN_CONFIG", 1) : {
            "FREQ"        : 25e3,
            "DUAL_TACH"   : False,
            "HYS"         : 2.0,
            "TSFO"        : False,
            "TACHO"       : False,
            "RAMP"        : (200.0, 2.0, 6.0),
            "HEALTH"      : False,
            "ROTOR_HI_LO" : False,
            "ROTOR"       : False,
            "SPIN"        : 2,
        },
        # case fans
        ("MFR_FAN_CONFIG", 2) : {
            "FREQ"          : 25e3,
            "DUAL_TACH"     : False,
            "HYS"           : 2.0,
            "TSFO"          : False,
            "TACHO"         : False,
            "RAMP"          : (200.0, 2.0, 6.0),
            "HEALTH"        : False,
            "ROTOR_HI_LO"   : False,
            "ROTOR"         : False,
            "SPIN"          : 2,
        },
        ("MFR_FAN_CONFIG", 3) : {
            "FREQ"          : 25e3,
            "DUAL_TACH"     : False,
            "HYS"           : 2.0,
            "TSFO"          : False,
            "TACHO"         : False,
            "RAMP"          : (200.0, 2.0, 6.0),
            "HEALTH"        : False,
            "ROTOR_HI_LO"   : False,
            "ROTOR"         : False,
            "SPIN"          : 2,
        },
        ("MFR_FAN_CONFIG", 4) : {
            "FREQ"          : 25e3,
            "DUAL_TACH"     : False,
            "HYS"           : 2.0,
            "TSFO"          : False,
            "TACHO"         : False,
            "RAMP"          : (200.0, 2.0, 6.0),
            "HEALTH"        : False,
            "ROTOR_HI_LO"   : False,
            "ROTOR"         : False,
            "SPIN"          : 2,
        },
        ("MFR_FAN_CONFIG", 5) : {
            "FREQ"          : 25e3,
            "DUAL_TACH"     : False,
            "HYS"           : 2.0,
            "TSFO"          : False,
            "TACHO"         : False,
            "RAMP"          : (200.0, 2.0, 6.0),
            "HEALTH"        : False,
            "ROTOR_HI_LO"   : False,
            "ROTOR"         : False,
            "SPIN"          : 2,
        },
        # Automatic fan control for CPU and FPGA
        # Use corresponding temperature sensor to control fans
        ("MFR_TEMP_SENSOR_CONFIG", 6) : {
            "ENABLE" : 1,
            "OFFSET" : 0,
            "FAN0"  : True,
            "FAN1"  : False,
            "FAN2"  : True,
            "FAN3"  : True,
            "FAN4"  : True,
            "FAN5"  : True
        },
        ("MFR_TEMP_SENSOR_CONFIG", 7) : {
            "ENABLE" : 1,
            "OFFSET" : 0,
            "FAN0"  : False,
            "FAN1"  : True,
            "FAN2"  : True,
            "FAN3"  : True,
            "FAN4"  : True,
            "FAN5"  : True
        },
        ("MFR_TEMP_SENSOR_CONFIG", 12) : {
            "ENABLE"    : 1,
            "OFFSET"    : 0,
            "FAN0"      : False,
            "FAN1"      : False,
            "FAN2"      : True,
            "FAN3"      : True,
            "FAN4"      : True,
            "FAN5"      : True
        },
        # Linear ramp from 0% PWM at 30°C to 100% at55°
        ("MFR_FAN_LUT", 0) : [
            {
                "TEMP_STEP"  : 39,
                "SPEED_STEP" : 12
            },
            {
                "TEMP_STEP"  : 42,
                "SPEED_STEP" : 24
            },
            {
                "TEMP_STEP"  : 45,
                "SPEED_STEP" : 36
            },
            {
                "TEMP_STEP"  : 48,
                "SPEED_STEP" : 48
            },
            {
                "TEMP_STEP"  : 51,
                "SPEED_STEP" : 61
            },
            {
                "TEMP_STEP"  : 54,
                "SPEED_STEP" : 74
            },
            {
                "TEMP_STEP"  : 57,
                "SPEED_STEP" : 87
            },
            {
                "TEMP_STEP"  : 60,
                "SPEED_STEP" : 100
            }
        ],
        ("MFR_FAN_LUT", 1) : [
            {
                "TEMP_STEP"  : 44,
                "SPEED_STEP" : 12
            },
            {
                "TEMP_STEP"  : 47,
                "SPEED_STEP" : 24
            },
            {
                "TEMP_STEP"  : 50,
                "SPEED_STEP" : 36
            },
            {
                "TEMP_STEP"  : 53,
                "SPEED_STEP" : 48
            },
            {
                "TEMP_STEP"  : 56,
                "SPEED_STEP" : 61
            },
            {
                "TEMP_STEP"  : 59,
                "SPEED_STEP" : 74
            },
            {
                "TEMP_STEP"  : 62,
                "SPEED_STEP" : 87
            },
            {
                "TEMP_STEP"  : 65,
                "SPEED_STEP" : 100
            }
        ],
        ("MFR_FAN_LUT", 2) : [
            {
                "TEMP_STEP"  : 35,
                "SPEED_STEP" : 20
            },
            {
                "TEMP_STEP"  : 41,
                "SPEED_STEP" : 30
            },
            {
                "TEMP_STEP"  : 47,
                "SPEED_STEP" : 40
            },
            {
                "TEMP_STEP"  : 53,
                "SPEED_STEP" : 50
            },
            {
                "TEMP_STEP"  : 60,
                "SPEED_STEP" : 60
            },
            {
                "TEMP_STEP"  : 66,
                "SPEED_STEP" : 72
            },
            {
                "TEMP_STEP"  : 72,
                "SPEED_STEP" : 80
            },
            {
                "TEMP_STEP"  : 78,
                "SPEED_STEP" : 100
            }
        ],
         ("MFR_FAN_LUT", 3) : [
            {
                "TEMP_STEP"  : 35,
                "SPEED_STEP" : 20
            },
            {
                "TEMP_STEP"  : 41,
                "SPEED_STEP" : 30
            },
            {
                "TEMP_STEP"  : 47,
                "SPEED_STEP" : 40
            },
            {
                "TEMP_STEP"  : 53,
                "SPEED_STEP" : 50
            },
            {
                "TEMP_STEP"  : 60,
                "SPEED_STEP" : 60
            },
            {
                "TEMP_STEP"  : 66,
                "SPEED_STEP" : 72
            },
            {
                "TEMP_STEP"  : 72,
                "SPEED_STEP" : 80
            },
            {
                "TEMP_STEP"  : 78,
                "SPEED_STEP" : 100
            }
        ],
         ("MFR_FAN_LUT", 4) : [
            {
                "TEMP_STEP"  : 35,
                "SPEED_STEP" : 20
            },
            {
                "TEMP_STEP"  : 41,
                "SPEED_STEP" : 30
            },
            {
                "TEMP_STEP"  : 47,
                "SPEED_STEP" : 40
            },
            {
                "TEMP_STEP"  : 53,
                "SPEED_STEP" : 50
            },
            {
                "TEMP_STEP"  : 60,
                "SPEED_STEP" : 60
            },
            {
                "TEMP_STEP"  : 66,
                "SPEED_STEP" : 72
            },
            {
                "TEMP_STEP"  : 72,
                "SPEED_STEP" : 80
            },
            {
                "TEMP_STEP"  : 78,
                "SPEED_STEP" : 100
            }
	],
         ("MFR_FAN_LUT", 5) : [
            {
                "TEMP_STEP"  : 35,
                "SPEED_STEP" : 20
            },
            {
                "TEMP_STEP"  : 41,
                "SPEED_STEP" : 30
            },
            {
                "TEMP_STEP"  : 47,
                "SPEED_STEP" : 40
            },
            {
                "TEMP_STEP"  : 53,
                "SPEED_STEP" : 50
            },
            {
                "TEMP_STEP"  : 60,
                "SPEED_STEP" : 60
            },
            {
                "TEMP_STEP"  : 66,
                "SPEED_STEP" : 72
            },
            {
                "TEMP_STEP"  : 72,
                "SPEED_STEP" : 80
            },
            {
                "TEMP_STEP"  : 78,
                "SPEED_STEP" : 100
            }
	]
},
    "ir3581" : {
        "LOOP_1_PMBUS_ADDR" : 0x60,
        "LOOP_2_PMBUS_ADDR" : 0x62,
        "RAW_REGISTERS" : IR3581_Configuration.registers,
    },
    "ir3581_loop_vdd_core" : {
        # Control is by enable pin (CONTROL)
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        # Default response for OV and OC faults is immediate shutdown.
        "VOUT_OV_FAULT_RESPONSE" : {
            "RESPONSE" : "SHUTDOWN_RETRY", # DISABLE is not supported by the chip
            "RETRY" : 0,
            "DELAY" : 0,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "ir3581_loop_0v9_vdd_oct" : {
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        "VOUT_OV_FAULT_RESPONSE" : {
            "RESPONSE" : "SHUTDOWN_RETRY",
            "RETRY" : 0,
            "DELAY" : 0,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "max15301_1v5_vdd_oct" : {
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        "VOUT_OV_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "max20751_vccint_fpga" :  {
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "max20751_mgtavcc_fpga" :  {
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "max20751_mgtavtt_fpga" :  {
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "max15301_vcc1v8_fpga" :  {
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        "VOUT_OV_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "max15301_util_3v3" :  {
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        "VOUT_OV_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "max15301_vadj_1v8" :  {
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        "VOUT_OV_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "max15301_vccintio_bram_fpga" :  {
        "ON_OFF_CONFIG" : {
            "MANUAL"       : True,
            "REG_CTRL"     : False,
            "PIN_CTRL"     : True,
            "PIN_ACT_HIGH" : True,
            "PIN_IMM_OFF"  : False,
        },
        "VOUT_OV_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
        "IOUT_OC_FAULT_RESPONSE" : {
            "RESPONSE" : "DISABLE",
            "RETRY" : 0,
            "DELAY" : 0,
        },
    },
    "isl6334d_ddr_v_cpu" : {},
    "isl6334d_ddr_v_fpga" : {},
    "ina226_ddr_fpga_13" : _default_ina226,
    "ina226_ddr_fpga_24" : _default_ina226,
    "ina226_ddr_cpu_13"  : _default_ina226,
    "ina226_ddr_cpu_24"  : _default_ina226,
}

# ( startup value, enabled by default )
startup_control_state = {
    "VDD_CORE"           : ( 0.950, False ),
    "0V9_VDD_OCT"        : ( 0.900, False ),
    "1V5_VDD_OCT"        : ( 1.500, False ),
    "VCCINT"             : ( 0.900, False ),
    "MGTAVCC_FPGA"       : ( 0.900, False ),
    "MGTAVTT_FPGA"       : ( 1.200, False ),
    "VCC1V8_FPGA"        : ( 1.800, False ),
    "UTIL_3V3"           : ( 3.300, False ),
    "VADJ_1V8"           : ( 1.800, False ),
    "VCCINTIO_BRAM_FPGA" : ( 0.900, False ),
    "VDD_DDRCPU13"       : ( 1.200, False ),
    "VDD_DDRFPGA13"      : ( 1.200, False ),
    "CPU_FAN"            : ( -1, False  ),
    "FPGA_FAN"           : ( -1, False  ),
    "CASE_FAN_0"         : ( -1, False ),
    "CASE_FAN_1"         : ( -1, False ),
    "CASE_FAN_2"         : ( -1, False ),
    "CASE_FAN_3"         : ( -1, False ),
}
