from ..pmbus.i2cbus import I2CBus_Exception
from ..pmbus.smbus import SMBus
from ..pmbus.pmbus import PMBus
from ..pmbus.native_smbus import is_byte, exc_on_neg

class MockSMBus(SMBus):
    def __init__(self, busnum, *args, **kwargs):
        self._busnum = busnum
        super(MockSMBus,self).__init__(*args, **kwargs)

    def bus_id(self):
        return self._busnum

    def receive_byte(self, address):
        assert(is_byte(address))
        self.tr_logger.debug(
            "receive_byte(0x%02X)", address,
            extra={ "protocol" : "SMBus" }
        )
        res = 0
        self.tr_logger.debug(
            "receive_byte(0x%02X) -> 0x%02x", address, res,
            extra={ "protocol" : "SMBus" }
        )
        exc_on_neg(res)
        #TODO: unchecked
        return bytearray([res])

    def read_byte(self, address, command):
        assert(is_byte(address))
        assert(is_byte(command))
        self.tr_logger.debug(
            "read_byte(0x%02X,0x%02X)", address, command,
            extra={ "protocol" : "SMBus" }
        )
        res = 0
        self.tr_logger.debug(
            "read_byte(0x%02X,0x%02X) -> 0x%02x", address, command, res,
            extra={ "protocol" : "SMBus" }
        )
        exc_on_neg(res)
        # TODO: Unchecked
        return bytearray([res])

    def read_word(self, address, command):
        assert(is_byte(address))
        assert(is_byte(command))
        self.tr_logger.debug(
            "read_word(0x%02X,0x%02X)", address, command,
            extra={ "protocol" : "SMBus" }
        )
        res = 0
        self.tr_logger.debug(
            "read_word(0x%02X,0x%02X) -> 0x%04x", address, command, res,
            extra={ "protocol" : "SMBus" }
        )
        exc_on_neg(res)
        return bytearray([res & 0xff, (res >> 8) & 0xff])

    def read_block(self, address, command):
        assert(is_byte(address))
        assert(is_byte(command))
        self.tr_logger.debug(
            "read_block(0x%02X,0x%02X)", address, command,
            extra={ "protocol" : "SMBus" }
        )
        res = [0]
        self.tr_logger.debug(
            "read_block(0x%02X,0x%02X) -> 0x%s", address, command,
            "".join("%02X" % x for x in res),
            extra={ "protocol" : "SMBus" }
        )
        return bytearray(res)

    def read_block_noncompliant(self, address, command, length):
        assert(is_byte(address))
        assert(is_byte(command))
        assert(length <= 255)
        self.tr_logger.debug(
            "read_block_noncompliant(0x%02X,0x%02X)", address, command,
            extra={ "protocol" : "SMBus" }
        )
        res = [0] * length
        if len(res) != length:
            raise I2CBus_Exception("Read data length didn't match expected")
        
        self.tr_logger.debug(
            "read_block_noncompliant(0x%02X,0x%02X) -> 0x%s", address, command,
            "".join("%02X" % x for x in res),
            extra={ "protocol" : "SMBus" }
        )
        return bytearray(res)

    def send_byte(self, address, value):
        assert(is_byte(address))
        assert(isinstance(value, bytearray))
        assert(len(value) ==  1)
        self.tr_logger.debug(
            "send_byte(0x%02X,0x%02X)", address, value[0],
            extra={ "protocol" : "SMBus" }
        )
        # self.bus.write_byte(address, value[0]) 

    def write_byte(self, address, command, data):
        assert(is_byte(address))
        assert(is_byte(command))
        assert(isinstance(data, bytearray))
        assert(len(data) ==  1)
        self.tr_logger.debug(
            "write_byte(0x%02X,0x%02X,0x%02X)", address,
            command, data[0],
            extra={ "protocol" : "SMBus" }
        )
        # self.bus.write_byte_data(address, command, data[0])

    def write_word(self, address, command, data):
        assert(is_byte(address))
        assert(is_byte(command))
        assert(isinstance(data, bytearray))
        assert(len(data) ==  2)
        self.tr_logger.debug(
            "write_word(0x%02X,0x%02X,0x%02X%02X)",
            address, command, data[0], data[1],
            extra={ "protocol" : "SMBus" }
        )
        # self.bus.write_word_data(address, command, (data[1]<<8) + data[0])

    def write_block(self, address, command, data):
        assert(is_byte(address))
        assert(is_byte(command))
        assert(isinstance(data, bytearray))
        assert(len(data) <= 255)
        self.tr_logger.debug(
            "write_block(0x%02X,0x%02X,0x%s)", address, command,
            "".join("%02X" % x for x in data),
            extra={ "protocol" : "SMBus" }
        )
        # self.bus.write_block_data(address, command, list(data))

    def write_block_noncompliant(self, address, command, data):
        assert(is_byte(address))
        assert(is_byte(command))
        assert(isinstance(data, bytearray))
        assert(len(data) <= 255)
        self.tr_logger.debug(
            "write_block_noncompliant(0x%02X,0x%02X,0x%s)", address, command,
            "".join("%02X" % x for x in data),
            extra={ "protocol" : "SMBus" }
        )
        # self.bus.write_i2c_block_data(address, command, list(data))

    def process_call(self, address, command, data):
        assert(is_byte(address))
        assert(is_byte(command))
        assert(isinstance(data, bytearray))
        assert(len(data) ==  2)
        self.tr_logger.debug(
            "process_call(0x%02X,0x%02X,0x%02X%02X)", address,
            command, data[0], data[1],
            extra={ "protocol" : "SMBus"}
        )
        res = [0]
        return bytearray(res)

    def process_call_block(self, address, command, data):
        assert(is_byte(address))
        assert(is_byte(command))
        assert(isinstance(data, bytearray))
        assert(len(data) <= 255)
        self.tr_logger.debug(
            "process_call_block(0x%02X,0x%02X,0x%s)",
            address, command,
            "".join("%02X" % x for x in data),
            extra={ "protocol" : "SMBus" }
        )
        res = [0]
        return bytearray(res)
        
    def probe(self, address):
        assert(is_byte(address))
        self.tr_logger.debug(
            "probe(0x%02X)", address,
            extra={ "protocol" : "I2C" }
        )
        res = self.bus.read_byte(address)
        self.tr_logger.debug(
            "probe(0x%02X) -> 0x%02X", address, res,
            extra={ "protocol" : "I2C" }
        )
        return res >= 0

    def write_reg(self, address, reg, data):
        assert(is_byte(address))
        assert(is_byte(reg))
        """Write a single register at an 8-bit address."""

        self.tr_logger.debug("write_reg(0x%02X,0x%02X,%s)",
                             address, reg, data,
                             extra={ "protocol" : "I2C" })
        # self.bus.write_i2c_block_data(address, reg, list(bytearray(data)))

    def read_reg(self, address, reg, N):
        assert(is_byte(address))
        assert(is_byte(reg))
        """Read a single register at an 8-bit address."""

        self.tr_logger.debug("read_reg(0x%02X,0x%02X)",
                             address, reg,
                             extra={ "protocol" : "I2C" })
        val = [0] * N
        return bytearray(val)


class MockPMBus(MockSMBus, SMBus):     
    def __init__(self, *args, **kwargs):
        super(MockPMBus, self).__init__(*args, **kwargs)
