import logging

import dbus
import dbus.service


LOGGING_INTERFACE_NAME = "systems.enzian.Logging"

INVALID_LOG_LEVEL_ERROR = LOGGING_INTERFACE_NAME.lower() + ".InvalidLogLevelError"


class InvalidLogLevelException(dbus.DBusException):
    def __init__(self, level):
        """
        :type level: str
        """
        msg = "Invalid log level '%s'." % (level)
        super(InvalidLogLevelException, self).__init__(msg)
        self._dbus_error_name = INVALID_LOG_LEVEL_ERROR


class LoggingObject(dbus.service.Object):

    def __init__(self, logger, bus_name, object_path):
        """
        :type dbus_name: dbus.service.BusName
        :type object_path: str
        """
        super(LoggingObject, self).__init__(bus_name, object_path)
        self.logger = logger
        self.logger.info("Instantiated as %s" % (object_path))


    @dbus.service.method(
        dbus_interface=LOGGING_INTERFACE_NAME,
        in_signature="s",
        out_signature=""
    )
    def set_service_loglevel(self, level):
        """
        :type level: str
        """
        log_level = logging.getLevelName(level.upper())
        try:
            self.logger.setLevel(log_level)
        except ValueError:
            raise InvalidLogLevelException(level.upper())


    @dbus.service.method(
        dbus_interface=LOGGING_INTERFACE_NAME,
        in_signature="ss",
        out_signature=""
    )
    def set_loglevel(self, logger, level):
        """
        :type level: str
        """
        log_level = logging.getLevelName(level.upper())
        try:
            logging.getLogger(logger).setLevel(log_level)
        except ValueError:
            raise InvalidLogLevelException(level.upper())
