import signal
from gi.repository import GLib as glib
import dbus

def add_io_watch(fd, handler, *args):
    glib.io_add_watch(fd, glib.IO_IN | glib.IO_PRI, lambda s, _, *a: handler(s, *a), *args)

def run_deferred(timeout, callback):
    """
    Run the callback after timeout (in ms)
    """
    glib.timeout_add(timeout, callback)

def run_mainloop():
    """
    Run the GLib mainloop
    """
    mainloop = glib.MainLoop()
    def sig_handler(source, condition):
        mainloop.quit()
    signal.signal(signal.SIGTERM, sig_handler)
    
    try:
        mainloop.run()
    except KeyboardInterrupt:
        pass

def to_dbus(value, variant_level=0):
    """
    Convert standard Python types to DBus types.
    This is needed as the DBus library otherwise tries to guess the type,
    which works fine for primitive types but is brittle for container types.
    """
    if isinstance(value, bool):
        return dbus.Boolean(value, variant_level=variant_level)
    if isinstance(value, int):
        return dbus.Int64(value, variant_level=variant_level)
    if isinstance(value, str):
        return dbus.String(value, variant_level=variant_level)
    if isinstance(value, float):
        return dbus.Double(value, variant_level=variant_level)
    if isinstance(value, tuple):
        return dbus.Struct(
            ( to_dbus(v, variant_level=1) for v in value ),
            variant_level=variant_level, signature="v"
        )
    if isinstance(value, dict):
        return dbus.Dictionary(
            # DBus dicts need primitive type key
            # Use Python repr but make sure we don't double wrap into DBus types
            { repr(from_dbus(k)) :
                to_dbus(v, variant_level=1) for k, v in value.items() },
            variant_level=variant_level, signature="sv"
        )
    if isinstance(value, list):
        return dbus.Array(
            [ to_dbus(v, variant_level=1) for v in value ],
            variant_level=variant_level, signature="v"
        )
    if isinstance(value, set):
        return to_dbus(list(value))
    return value

def from_dbus(value):
    if isinstance(value, dbus.Boolean):
        return bool(value)
    if isinstance(value, dbus.Int32):
        return int(value)
    if isinstance(value, dbus.Int64):
        return int(value)
    if isinstance(value, dbus.String):
        return str(value)
    if isinstance(value, dbus.Double):
        return float(value)
    if isinstance(value, dbus.Struct):
        return tuple(from_dbus(v) for v in value)
    if isinstance(value, dbus.Dictionary):
        # Dict keys were serialized with repr
        return { from_dbus(eval(k)): from_dbus(v) for k, v in value.items() }
    if isinstance(value, dbus.Array):
        return [ from_dbus(v) for v in value ]
    return value
