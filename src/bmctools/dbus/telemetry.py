from collections import defaultdict

import dbus
import dbus.service

from .logging import LoggingObject

from .power import UNKNOWN_DEVICE_ERROR, NO_SUCH_MONITOR_ERROR, DEVICE_UNINITIALIZED_ERROR

from .util import to_dbus, from_dbus, run_deferred


TELEMETRY_INTERFACE_NAME = "systems.enzian.Telemetry"


DATA_UNKNOWN = "UNKNOWN"
DATA_UNINIT  = "UNINIT"


class TelemetryObject(LoggingObject):
    def __init__(self, power, logger, bus_name, object_path):
        super(TelemetryObject, self).__init__(logger, bus_name, object_path)
        self._items = defaultdict(set) # { device name : { monitor_name } }
        self._interval = 1000  # in ms
        self._collecting = False
        self._power = power


    def _collect(self):
        try:
            data = self.get_data()
            # Send the signal with updated telemetry data
            self.data(data)
            # Log data
            self.logger.info("Telemetry data: %s", from_dbus(data))
        except Exception as e:
            self.logger.error("Unknown error collecting telemetry data: %s", e)

        if self._collecting: 
            run_deferred(self._interval, lambda: self._collect())

    
    @dbus.service.method(
        dbus_interface=TELEMETRY_INTERFACE_NAME,
        in_signature="ss",
        out_signature=""
    )
    def add_monitor(self, device_name, monitor_name):
        """
        Add a device monitor to the telemetry data
        :type device_name: str
        :type monitor_name: str
        """
        self.logger.debug(
            "Adding monitor '%s' of device '%s' to monitored items",
            monitor_name,
            device_name,
        )     
        self._items[device_name].add(monitor_name)


    @dbus.service.method(
        dbus_interface=TELEMETRY_INTERFACE_NAME,
        in_signature="ss",
        out_signature=""
    )
    def remove_monitor(self, device_name, monitor_name):
        """
        Remove a device monitor to the telemetry data
        :type device_name: str
        :type monitor_name: str
        """
        self.logger.debug(
            "Removing monitor '%s' of device '%s' from monitored items",
            monitor_name,
            device_name,
        )
        if not device_name in self._items:
            return

        if monitor_name in self._items[device_name]:
            self._items[device_name].remove(monitor_name)
            if not bool(self._items[device_name]):
                del self._items[device_name]


    @dbus.service.method(
        dbus_interface=TELEMETRY_INTERFACE_NAME,
        in_signature="i",
        out_signature=""
    )
    def set_interval(self, interval):
        """
        Sets the telemetry collection interval in ms
        :type interval: int
        """
        self._interval = interval


    @dbus.service.method(
        dbus_interface=TELEMETRY_INTERFACE_NAME,
        in_signature="",
        out_signature="a{sv}"
    )
    def get_data(self):
        data = {}
        for device_name, monitors in self._items.items():
            data[device_name] = {}
            for monitor_name in monitors:
                try:
                    value = self._power.read_device_monitor(device_name, monitor_name)
                    self.logger.debug("%s.%s: %s", device_name, monitor_name, value)
                    data[device_name][monitor_name] = value
                except dbus.DBusException as e:
                    error = e.get_dbus_name()
                    self.logger.debug("Got '%s' while getting %s.%s", error, device_name, monitor_name)
                    if error == DEVICE_UNINITIALIZED_ERROR:
                        # The driver is not initialized for this device, we can't get telemetry
                        data[device_name] = DATA_UNINIT
                        break
                    elif error == UNKNOWN_DEVICE_ERROR:
                        # The device doesn't exist
                        # We don't have to look at other monitors for it
                        data[device_name] = DATA_UNKNOWN
                        break
                    elif error == NO_SUCH_MONITOR_ERROR:
                        # The device doesn't have such a monitor
                        data[device_name][monitor_name] = DATA_UNKNOWN
                    else:
                        raise e
                        # We let other exceptions bubble to the caller
                        # Including non DBus Exceptions
        return to_dbus(data)


    @dbus.service.method(
        dbus_interface=TELEMETRY_INTERFACE_NAME,
        in_signature="",
        out_signature=""
    )
    def start(self):
        """
        Starts collecting telemetry
        """
        if not self._collecting:
            self._collecting = True
            self._collect()


    @dbus.service.method(
        dbus_interface=TELEMETRY_INTERFACE_NAME,
        in_signature="",
        out_signature=""
    )
    def stop(self):
        """
        Stops collecting telemetry data
        """
        self._collecting = False


    @dbus.service.signal(
        dbus_interface=TELEMETRY_INTERFACE_NAME,
        signature="a{sv}"
    )
    def data(self, data):
        pass
