import dbus
import dbus.service

import gpiod

from .logging import LoggingObject

from .util import add_io_watch


GPIO_INTERFACE_NAME = "systems.enzian.Gpio"

PIN_NOT_FOUND_ERROR = GPIO_INTERFACE_NAME.lower() + ".PinNotFoundError"
PIN_BUSY_ERROR = GPIO_INTERFACE_NAME.lower() + ".PinBusyError"
PIN_GROUP_ERROR = GPIO_INTERFACE_NAME.lower() + ".PinGroupError"
NO_SUCH_PIN_GROUP_ERROR = GPIO_INTERFACE_NAME.lower() + "NoSuchPinGroupError"
PIN_GROUP_NOT_OUTPUT_ERROR = GPIO_INTERFACE_NAME.lower() + ".PinNotOutputError"
INVALID_VALUE_ERROR = GPIO_INTERFACE_NAME.lower() + ".InvalidValueError"


class PinNotFoundException(dbus.DBusException):
    def __init__(self, name):
        message = "Pin '%s' not found" % (name)
        super(PinNotFoundException, self).__init__(message)
        self._dbus_error_name = PIN_NOT_FOUND_ERROR

class PinGroupException(dbus.DBusException):
    def __init__(self, chip, pin0):
        message = "GPIO chip '%s' does not contain the full group starting with '%s'" % (chip, pin0)
        super(PinGroupException, self).__init__(message)
        self._dbus_error_name = PIN_GROUP_ERROR

class PinBusyException(dbus.DBusException):
    def __init__(self, name, consumer):
        message = "Pin '%s' already in use by '%s'" % (name, consumer)
        super(PinBusyException, self).__init__(message)
        self._dbus_error_name = PIN_BUSY_ERROR

class NoSuchPinGroupException(dbus.DBusException):
    def __init__(self, name):
        message = "Pin group '%s' does not exist" % (name)
        super(NoSuchPinGroupException, self).__init__(message)
        self._dbus_error_name = NO_SUCH_PIN_GROUP_ERROR

class PinGroupNotOutputException(dbus.DBusException):
    def __init__(self, name):
        message = "Pin group '%s' is not writeable" % (name)
        super(PinGroupNotOutputException, self).__init__(message)
        self._dbus_error_name = PIN_GROUP_NOT_OUTPUT_ERROR

class InvalidValueException(dbus.DBusException):
    def __init__(self, name, value, size):
        message = "Value '%s' is to large for pin group '%s' of size %d" % (hex(value), name, size)
        super(InvalidValueException, self).__init__(message)
        self._dbus_error_name = INVALID_VALUE_ERROR


class GpioObject(LoggingObject):
    def __init__(self, logger, dbus_name, object_path):
        """
        :type dbus_name: dbus.service.BusName
        :type object_path: str
        """
        super(GpioObject, self).__init__(logger, dbus_name, object_path)
        self._pin_groups = {}

    def _get_pin_group(self, name):
        if not name in self._pin_groups:
            raise NoSuchPinGroupException(name)
        return self._pin_groups[name]

    def _irq_handler(self, source, gpio_line):
        event = gpio_line.event_read()
        edge = event.type
        if edge == gpiod.LineEvent.FALLING_EDGE:
            self.logger.debug("IRQ handler called (FALLING EDGE)")
            self.value_change(gpio_line.name(), False)
        elif edge == gpiod.LineEvent.RISING_EDGE:
            self.logger.debug("IRQ handler called (RISING EDGE)")
            self.value_change(gpio_line.name(), True)
        else:
            self.logger.error("Unknown IRQ type %d", edge)
        return True
    
    def add_pin_group(self, group_name, pin_names, writeable):
        """
        :type group_name: str
        :type pin_names: [str]
        :type writeable: bool
        """
        if len(pin_names) < 1:
            return
        
        gpio_line0 = gpiod.find_line(pin_names[0])
        if gpio_line0 is None:
            raise PinNotFoundException(pin_names[0])
        try:
            gpio_lines = gpio_line0.owner().find_lines(pin_names)
        except TypeError:
            raise PinGroupException(gpio_line0.owner().name(), pin_names[0])
        
        if writeable:
            gpio_lines.request(
                GPIO_INTERFACE_NAME,
                gpiod.LINE_REQ_DIR_OUT
            )
        else:
            gpio_lines.request(
                GPIO_INTERFACE_NAME,
                gpiod.LINE_REQ_DIR_IN | gpiod.LINE_REQ_EV_BOTH_EDGES
            )
            # We watch each pin individually
            for gpio_line in gpio_lines.to_list():
                add_io_watch(gpio_line.event_get_fd(), self._irq_handler, gpio_line)

        self._pin_groups[group_name] = gpio_lines

    def add_pin(self, pin_name, writeable):
        """
        :type pin_name: string
        :type writeable: bool
        """
        self.add_pin_group(pin_name, [pin_name], writeable)

    def add_pins(self, pins):
        """
        :type pins: [(string, boolean)]
        """
        for pin_name, writeable in pins:
            self.add_pin(pin_name, writeable)

    @dbus.service.signal(
        dbus_interface=GPIO_INTERFACE_NAME,
        signature="sb"
    )
    def value_change(self, pin_name, value):
        if value: 
            self.value_high(pin_name)
        else:
            self.value_low(pin_name)
        self.logger.debug("Emitting value change signal for pin=%s, value=%d", pin_name, value)
        
    @dbus.service.signal(
        dbus_interface=GPIO_INTERFACE_NAME,
        signature="s"
    )
    def value_high(self, pin_name):
        self.logger.debug("Emitting value high signal for pin=%s", pin_name)

    @dbus.service.signal(
        dbus_interface=GPIO_INTERFACE_NAME,
        signature="s"
    )
    def value_low(self, pin_name):
        self.logger.debug("Emitting value low signal for pin=%s", pin_name)

    @dbus.service.method(
        dbus_interface=GPIO_INTERFACE_NAME,
        in_signature="s",
        out_signature="b"
    )
    def get_value(self, pin_group_name):
        self.logger.debug("get_value %s", pin_group_name)
        pin_group = self._get_pin_group(pin_group_name)
        values =  pin_group.get_values()
        self.logger.debug("Raw values: %s", values)
        value = sum([ b << i for i, b in enumerate(values) ])
        return value

    @dbus.service.method(
        dbus_interface=GPIO_INTERFACE_NAME,
        in_signature="sb",
        out_signature=""
    )
    def set_value(self, pin_group_name, value):
        self.logger.debug("set_value %s: %s", pin_group_name, hex(value))
        pin_group = self._get_pin_group(pin_group_name)
        pins = pin_group.to_list()
        num_pins = len(pins)

        if pins[0].direction() != gpiod.Line.DIRECTION_OUTPUT:
            raise PinGroupNotOutputException(pin_group_name)

        if value > ((1 << num_pins) - 1):
            raise InvalidValueException(pin_group_name, value, num_pins)
        
        values = [ (value >> i) & 0x1 for i in range(0, num_pins) ]
        self.logger.debug("Converted value: %s", values)
        current = pin_group.get_values()

        pin_group.set_values(values)
        self.logger.info("Set %s to %d" % (pin_group_name, value))

        for i in range(0, num_pins):
            if (current[i] != values[i]):
                self.value_change(pins[i].name(), values[i])
