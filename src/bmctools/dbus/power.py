import errno

import dbus
import dbus.service

from .logging import LoggingObject

from ..pmbus.pmbus import PMBus_Polled_Device

from .util import to_dbus, from_dbus, run_deferred


POWER_INTERFACE_NAME = "systems.enzian.Power"
POWER_ALERT_INTERFACE_NAME = POWER_INTERFACE_NAME.lower() + ".Alert"
POWER_CONTROL_INTERFACE_NAME = POWER_INTERFACE_NAME.lower() + ".Control"
POWER_MONITOR_INTERFACE_NAME = POWER_INTERFACE_NAME.lower() + ".Monitor"

DUPLICATE_BUS_ERROR = POWER_INTERFACE_NAME.lower() + ".DuplicateBusError"
UNKNOWN_BUS_ERROR = POWER_INTERFACE_NAME.lower() + ".UnknownBusError"
DUPLICATE_DEVICE_ERROR = POWER_INTERFACE_NAME.lower() + ".DuplicateDeviceError"
DOUBLE_INSTANTIATION_ERROR = POWER_INTERFACE_NAME.lower() + ".DoubleInstantiationError"
DUPLICATE_ADDRESS_ERROR = POWER_INTERFACE_NAME.lower() + ".DuplicateAddressError"
UNKNOWN_DEVICE_ERROR = POWER_INTERFACE_NAME.lower() + ".UnknownDeviceError"
DEVICE_INSTANTIATION_ERROR = POWER_INTERFACE_NAME.lower() + ".DeviceInstantiationError"
DEVICE_UNINITIALIZED_ERROR = POWER_INTERFACE_NAME.lower() + ".DeviceUninitializedError"
DEVICE_PROBE_ERROR = POWER_INTERFACE_NAME.lower() + ".DeviceProbeError"
INVALID_ALERT_PIN_ERROR = POWER_ALERT_INTERFACE_NAME.lower() + ".InvalidAlertPinError"
NO_SUCH_MONITOR_ERROR = POWER_MONITOR_INTERFACE_NAME.lower() + ".NoSuchMonitorError"
NO_SUCH_CONTROL_ERROR = POWER_CONTROL_INTERFACE_NAME.lower() + ".NoSuchControlError"

ALERT_ADDRESS_UNKNOWN = "UNKNOWN"
ALERT_ADDRESS_POLL    = "POLL"


class DuplicateBusException(dbus.DBusException):
    def __init__(self, bus_name):
        message = "Bus '%s' already exists" % (bus_name)
        super(DuplicateBusException, self).__init__(message)
        self._dbus_error_name = DUPLICATE_BUS_ERROR
        

class UnknownBusException(dbus.DBusException):
    def __init__(self, bus_name):
        message = "No bus with name '%s'" % (bus_name)
        super(UnknownBusException, self).__init__(message)
        self._dbus_error_name = UNKNOWN_BUS_ERROR
        

class DuplicateDeviceException(dbus.DBusException):
    def __init__(self, device_name):
        message = "Device '%s' already exists" % (device_name)
        super(DuplicateDeviceException, self).__init__(message)
        self._dbus_error_name = DUPLICATE_DEVICE_ERROR


class DoubleInstantiationException(dbus.DBusException):
    def __init__(self, device_record):
        message = "Device %s has already been initialized" % (device_record)
        super(DoubleInstantiationException,self).__init__(message)
        self._dbus_error_name = DOUBLE_INSTANTIATION_ERROR


class DuplicateAddressException(dbus.DBusException):
    def __init__(self, bus_name, address):
        message = "Address 0x%x on bus '%s' is already in use" % (address, bus_name)
        super(DuplicateAddressException, self).__init__(message)
        self._dbus_error_name = DUPLICATE_ADDRESS_ERROR


class UnknownDeviceException(dbus.DBusException):
    def __init__(self, device_name):
        message = "No device with name '%s'" % (device_name)
        super(UnknownDeviceException, self).__init__(message)
        self._dbus_error_name = UNKNOWN_DEVICE_ERROR


class DeviceInstantiationException(dbus.DBusException):
    def __init__(self, device_record):
        message = "Failed to instantiate device %s" % (device_record)
        super(DeviceInstantiationException, self).__init__(message)
        self._dbus_error_name = DEVICE_INSTANTIATION_ERROR


class DeviceUninitializedException(dbus.DBusException):
    def __init__(self, device_record):
        message = "Device %s not yet initialized (try init_driver())" % (
            device_record
        )
        super(DeviceUninitializedException, self).__init__(message)
        self._dbus_error_name = DEVICE_UNINITIALIZED_ERROR


class DeviceProbeException(dbus.DBusException):
    def __init__(self, device_record):
        message = "Device %s was not successfully probed" % (device_record)
        super(DeviceProbeException, self).__init__(message)
        self._dbus_error_name = DEVICE_PROBE_ERROR


class InvalidAlertPinException(dbus.DBusException):
    def __init__(self, bus_name, pin_name):
        message = f"Pin '{pin_name}' is not an alert pin for bus '{bus_name}'"
        super().__init__(message)
        self._dbus_error_name = INVALID_ALERT_PIN_ERROR


class NoSuchMonitorException(dbus.DBusException):
    def __init__(self, device_name, monitor_name):
        message = "No monitor '%s' in device '%s'" % (monitor_name, device_name)
        super(NoSuchMonitorException, self).__init__(message)
        self._dbus_error_name = NO_SUCH_MONITOR_ERROR


class NoSuchControlException(dbus.DBusException):
    def __init__(self, device_name, control_name):
        message = "No control '%s' in device '%s'" % (control_name, device_name)
        super(NoSuchControlException, self).__init__(message)
        self._dbus_error_name = NO_SUCH_CONTROL_ERROR


class PmbusRecord(object):
    def __init__(self, name ,bus, alert_pins):
        super(PmbusRecord, self).__init__()
        self.name = name
        self.bus = bus
        self.alert_pins = alert_pins
        self.devices = {}
        self.polled_devices = []
        self.alert_signals = {}


class DeviceRecord(object):
    def __init__(self, name, dev_class, bus_name=None, address=None, pin_groups=None):
        super(DeviceRecord, self).__init__()
        self.name = name
        self.dev_class = dev_class
        self.bus_name = bus_name
        self.address = address
        self.pin_groups = pin_groups

    def is_i2c(self):
        return self.bus_name is not None
    
    def is_gpio(self):
        return self.pin_groups is not None

    def __str__(self):
        out = "'%s' (%s)" % (self.name, self.dev_class.__name__)
        if self.address is not None:
            out = out + (" at address 0x%02x" % (self.address))
        if self.bus_name is not None:
            out = out + (" on bus '%s'" % (self.bus_name))
        if self.pin_groups is not None:
            out = out + (" with pin groups '%s'" % (self.pin_groups))
        return out


class PowerObject(LoggingObject):
    def __init__(self, gpio, logger, bus_name, object_path):
        super(PowerObject, self).__init__(logger, bus_name, object_path)
        self._gpio = gpio
        self._pmbuses = {}
        self._devices = {}
        self._device_cache = {}        
            

    def _get_device(self, name):
        """
        :type name: str
        :rtype: PMBus_Device
        """
        if not name in self._devices:
            raise UnknownDeviceException(name)

        device_record = self._devices[name]

        if name in self._device_cache:
            self.logger.debug("Device %s is initialized, returning from cache", device_record)
            return self._device_cache[name]
        else:
            self.logger.warn("Device %s not initialized", device_record)
            raise DeviceUninitializedException(device_record)        


    def _get_monitor(self, device_name, monitor_name):
        """
        :type device_name: str
        :type monitor_name: str
        :rtype: Monitor
        """
        dev = self._get_device(device_name)
        if not monitor_name in dev.monitors:
            raise NoSuchMonitorException(device_name, monitor_name)
        return dev.monitors[monitor_name]


    def _get_control(self, device_name, control_name):
        """
        :type device_name: str
        :type monitor_name: str
        :rtype: Control
        """
        dev = self._get_device(device_name)
        if not control_name in dev.controls:
            raise NoSuchControlException(device_name, control_name)
        return dev.controls[control_name]


    def _reset_drivers(self, devices):
        for dev in devices:
            if not dev in self._device_cache:
                # Device isn't initialized, nothing to do here
                continue
            
            device_record = self._devices[dev]
            if device_record.is_i2c():
                pmbus = self._pmbuses[device_record.bus_name].bus
                pmbus.deregister_device(device_record.address)

            del self._device_cache[dev]


    def add_bus(self, name, bus, alert_pins=[]):
        """
        Adds a bus to be managed
        :type name: str
        :type bus: PMBus
        """
        self.logger.debug("Adding bus '%s'", name)
        if name in self._pmbuses:
            raise DuplicateBusException(name)
        self._pmbuses[name] = PmbusRecord(name, bus, alert_pins)

    
    def add_i2c_device(self, name, device_class, bus_name, address):
        """
        Adds an i2c device
        :type name: str
        :type device_class: type
        :type bus_name: str
        :type address: int
        """
        # FIXME: This should be specializing the regular add device
        # function. We might need a way to pass args/kwargs to constructors
        # in the future
        if name in self._devices:
            raise DuplicateDeviceException(name)

        if not bus_name in self._pmbuses:
            raise UnknownBusException(bus_name)

        pmbus_devices = self._pmbuses[bus_name].devices
        if address in pmbus_devices:
            raise DuplicateAddressException(bus_name, address)
        
        pmbus_devices[address] = name

        # Record if this is a polled device
        if issubclass(device_class, PMBus_Polled_Device):
            self._pmbuses[bus_name].polled_devices.append(address)

        record = DeviceRecord(name, device_class, bus_name=bus_name, address=address)
        self.logger.debug("Adding I2C device %s", record)
        self._devices[name] = record

    
    def add_gpio_device(self, name, device_class, pin_groups):
        """
        Adds a device
        :type name: str
        :type device_class: type
        """
        if name in self._devices:
            raise DuplicateDeviceException(name)

        record = DeviceRecord(name, device_class, pin_groups=pin_groups)
        self.logger.debug("Adding GPIO device %s", record)
        self._devices[name] = record


    def add_device(self, name, device_class):
        """
        Adds a device
        :type name: str
        :type device_class: type
        """
        if name in self._devices:
            raise DuplicateDeviceException(name)

        record = DeviceRecord(name, device_class)
        self.logger.debug("Adding device %s", record)
        self._devices[name] = record

    
    @dbus.service.method(
        dbus_interface=POWER_INTERFACE_NAME,
        in_signature="s",
        out_signature=""
    )
    def init_driver(self, device_name):
        """
        Initializes/instantiates and probes a device given by a string.
        Attempts to probe PMBus devices and throws an exception on failure.
        This must be called before the device can be used.
        :type device_name: str
        """
        if not device_name in self._devices:
            raise UnknownDeviceException(device_name)

        device_record = self._devices[device_name]
        self.logger.info("Initializing driver for device %s", device_record)

        if device_name in self._device_cache:
            self.logger.warn("Device %s already initialized", device_record)
            raise DoubleInstantiationException(device_record)
        
        if device_record.is_i2c():
            pmbus = self._pmbuses[device_record.bus_name].bus
            try:
                # FIXME: Some i2c devices have constructors that can fail
                # because they try and access the bus. This shouldn't be the case
                dev = device_record.dev_class(pmbus, device_record.address)
            except Exception as e:
                self.logger.error("Failed to instantiate device %s (%s)", device_record, e)
                # Depending on where the instantiation failed, the device
                # might already be registered with the bus, so we deregister it
                pmbus.deregister_device(device_record.address)
                raise DeviceInstantiationException(device_record)

            if (dev.probe()):
                self.logger.debug("Successfully probed device %s", device_record)
            else:
                self.logger.error("Failed to probe device %s", device_record)
                # Deregister the device from the bus
                pmbus.deregister_device(device_record.address)
                raise DeviceProbeException(device_record)
        elif device_record.is_gpio():
            try:
                # FIXME: Make sure that GPIO device constructors can't fail
                dev = device_record.dev_class(self._gpio, device_record.pin_groups)
            except Exception as e:
                self.logger.error("Failed to instantiate device %s (%s)", device_record, e)
                raise DeviceInstantiationException(device_record)
        else:
            try:
                # FIXME: Also make sure that other constructors can't fail
                dev = device_record.dev_class()
            except Exception as e:
                self.logger.error("Failed to instantiate device %s (%s)", device_record, e)
                raise DeviceInstantiationException(device_record)

        self._device_cache[device_name] = dev


    @dbus.service.method(
        dbus_interface=POWER_INTERFACE_NAME,
        in_signature="",
        out_signature=""
    )
    def reset_all_drivers(self):
        """
        Resets the drivers for all devices
        """
        self.logger.info("Resetting drivers for all devices")
        self._reset_drivers(self._devices.keys())


    @dbus.service.method(
        dbus_interface=POWER_INTERFACE_NAME,
        in_signature="s",
        out_signature=""
    )
    def reset_bus_drivers(self, bus_name):
        """
        Resets all drivers for the devices on a specific bus
        :type bus_name: str
        """
        if not bus_name in self._pmbuses:
            raise UnknownBusException(bus_name)

        self.logger.info("Resetting drivers for all devices on bus '%s'", bus_name)
        pmbus_devices = self._pmbuses[bus_name].devices
        self._reset_drivers(pmbus_devices.values())


    @dbus.service.method(
        dbus_interface=POWER_INTERFACE_NAME,
        in_signature="s",
        out_signature=""
    )
    def reset_driver(self, device_name):
        """
        Resets device/driver given by a string
        :param device_name: str
        """
        if not device_name in self._devices:
            raise UnknownDeviceException(device_name)

        self.logger.info("Resetting driver for device '%s'", device_name)
        self._reset_drivers([device_name])


    @dbus.service.method(
        dbus_interface=POWER_INTERFACE_NAME,
        in_signature="sv",
        out_signature="v"
    )
    def device_read(self, device_name, command):
        """
        :type device_name: str
        :type command: str
        """
        self.logger.debug("Reading '%s' of device '%s'", command, device_name)
        dev = self._get_device(device_name)
        value = dev.read(from_dbus(command))
        self.logger.debug("'%s' of device '%s': %s", command, device_name, value)
        return to_dbus(value)


    @dbus.service.method(
        dbus_interface=POWER_INTERFACE_NAME,
        in_signature="svv",
        out_signature=""
    )
    def device_write(self, device_name, command, value):
        """
        :type device_name: str
        :type command: str
        """
        self.logger.info("Writing '%s' to '%s' of device '%s'", value, command, device_name)
        dev = self._get_device(device_name)
        dev.write(from_dbus(command), from_dbus(value))


    @dbus.service.method(
        dbus_interface=POWER_INTERFACE_NAME,
        in_signature="sav",
        out_signature=""
    )
    def device_configure(self, device_name, config):
        self.logger.info("Writing raw configuration to device '%s'", device_name)
        dev = self._get_device(device_name)
        dev.configure(from_dbus(config))


    @dbus.service.method(
        dbus_interface=POWER_INTERFACE_NAME,
        in_signature="s",
        out_signature="a(vs)"
    )
    def get_device_status(self, device_name):
        """
        Get the status of a device
        :type device_name: str
        :rtype: [(str, str)]
        """
        self.logger.debug("Reading status of device '%s'", device_name)
        dev = self._get_device(device_name)
        status = dev.read_status()
        self.logger.debug("'%s' status: %s", device_name, status)
        return to_dbus(status)


    @dbus.service.method(
        dbus_interface=POWER_INTERFACE_NAME,
        in_signature="s",
        out_signature=""
    )
    def clear_device_faults(self, device_name):
        """
        Clears the faults of a device
        :type device_name: str
        """
        self.logger.info("Clearing faults of device '%s'", device_name)
        dev = self._get_device(device_name)
        dev.clear_faults()


    @dbus.service.method(
        dbus_interface=POWER_MONITOR_INTERFACE_NAME,
        in_signature="ss",
        out_signature=""
    )
    def enable_device_monitor(self, device_name, monitor_name):
        """
        Enables a monitor of a device
        :type device_name: str
        :type monitor_name: str
        """
        self.logger.info(
            "Enabling monitor '%s' of device '%s'",
            monitor_name,
            device_name
        )
        mon = self._get_monitor(device_name, monitor_name)
        mon.enable()


    @dbus.service.method(
        dbus_interface=POWER_MONITOR_INTERFACE_NAME,
        in_signature="ss",
        out_signature=""
    )
    def disable_device_monitor(self, device_name, monitor_name):
        """
        Disables a device monitor
        :type device_name: str
        :type monitor_name: str
        """
        self.logger.info(
            "Disabling monitor '%s' of device '%s'",
            monitor_name,
            device_name
        )
        mon = self._get_monitor(device_name, monitor_name)
        mon.disable()


    @dbus.service.method(
        dbus_interface=POWER_MONITOR_INTERFACE_NAME,
        in_signature="ss",
        out_signature="d"
    )
    def read_device_monitor(self, device_name, monitor_name):
        """
        Reads the value from a monitor of a device
        :type device_name: str
        :type monitor_name: str
        :rtype: float
        """
        self.logger.debug(
            "Reading monitor '%s' of device '%s'",
            monitor_name,
            device_name
        )
        mon = self._get_monitor(device_name, monitor_name)
        if not mon.is_enabled():
            self.logger.warn(
                "Reading disabled monitor '%s' of device '%s'",
                monitor_name,
                device_name
            )
        return mon.read()
    
    
    def _handle_alert_for_bus(self, bus_name, pin_name):
        if not bus_name in self._pmbuses:
            self.logger.error("Alert '%s' received on unknown bus '%s'", pin_name, bus_name)
            return

        pmbus_record = self._pmbuses[bus_name]

        self.logger.debug("Alert '%s', received on bus '%s'", pin_name, bus_name)

        address = ALERT_ADDRESS_UNKNOWN
        try:
            address = pmbus_record.bus.alert_response()
        except OSError as e:
            if (e.errno == errno.ENXIO):
                if self._gpio.get_value(pin_name):
                    # Couldn't get alert address but alert pin isn't low anymore
                    # Happens when we check for futher alerts too fast
                    # Shouldn't according to SMBus standard but you know...
                    self.logger.debug("No alert response and alert wasn't asserted anymore")
                    return
                else:
                    # Probably a device that doesn't respond to ARA, try polling
                    address = ALERT_ADDRESS_POLL           
            else:
                self.logger.debug("Failed to get alert address (%s)", e)  
        except Exception as e:
            self.logger.debug("Failed to get alert address (%s)", e)

        if address == ALERT_ADDRESS_POLL:
            self.logger.debug("Checking polled devices for bus '%s'", bus_name)
            for d_addr in pmbus_record.polled_devices:
                device_name = pmbus_record.devices[d_addr]

                alerts = []
                try:
                    alerts = self.get_device_alerts(device_name)
                except DeviceUninitializedException:
                    # Driver not initialized so assume it's not this one
                    # Without querying it we can't know anyway
                    self.logger.debug("Device '%s' was not initialized when polling for alerts.", device_name)
                except Exception as e:
                    self.logger.error("Failed to get alerts for device '%s' (%s)", device_name, e)

                if alerts:
                    self.logger.debug("Polled device '%s' has alert flags set: %s", device_name, alerts)
                    address = d_addr
                    # Get only the new alerts
                    dev = self._get_device(device_name)
                    alerts = dev.query_unacknowleged_alert()
                    if alerts:
                        self.logger.debug("Polled device '%s' has unacknowledged alerts: %s", device_name, alerts)
                        # Acknowledge the alert so we know which ones we've already seen
                        # XXX: Some devices (e.g. MAX15301) won't let go of the alert line so we'll be stuck polling
                        # ARA/polled devices until the problem is fixed
                        dev.acknowledge_alerts(alerts)

                        # Send the alert out
                        self.device_alert(device_name, to_dbus(alerts))
                        # Handle only one alert per call to alert handler
                        # Keeps the polling semantics in line with the normal ARA ones
                        break
            
            # If we haven't found an actual address, we don't know who alerted us
            if address == ALERT_ADDRESS_POLL:
                self.unknown_device_alert(bus_name, ALERT_ADDRESS_UNKNOWN)
        elif address in pmbus_record.devices:
            # ARA gave us an address and we know what it is
            device_name = pmbus_record.devices[address]
            
            alerts = []
            try:
                alerts = self.get_device_alerts(device_name)
            except Exception as e:
                self.logger.error("Failed to get alerts for device '%s' (%s)", device_name, e)

            self.device_alert(device_name, to_dbus(alerts))
        else:
            # We don't know the address or no device is associated with it
            self.unknown_device_alert(bus_name, address)

        # Wait a bit and check if there are more devices with an alert in case the alert is still enabled then
        # XXX: We can possibly lower the 5ms delay now that we have pull-ups enabled in the FPGA
        run_deferred(5, lambda: self._poll_alert_for_bus(bus_name, pin_name) if pin_name in pmbus_record.alert_signals else None)


    def _poll_alert_for_bus(self, bus_name, pin_name):
        if not self._gpio.get_value(pin_name):
            self._handle_alert_for_bus(bus_name, pin_name)

    
    def _enable_alert_pin(self, pmbus_record, alert_pin):
        if alert_pin in pmbus_record.alert_signals:
            # Alert is already enabled
            self.logger.debug("Alert from pin '%s' for bus '%s' already enabled", alert_pin, pmbus_record.name)
            return

        signal_match = self._gpio.connect_to_signal(
            "value_low",
            lambda p: self._handle_alert_for_bus(pmbus_record.name, p),
            arg0=alert_pin
        )
        pmbus_record.alert_signals[alert_pin] = signal_match
        self.logger.info("Enabled alerts from pin '%s' for bus '%s'", alert_pin, pmbus_record.name)
        
        # Check if there is an alert already pending
        run_deferred(0, lambda: self._poll_alert_for_bus(pmbus_record.name, alert_pin))


    def _disable_alert_pin(self, pmbus_record, alert_pin):
        if not alert_pin in pmbus_record.alert_signals:
            # Alert is not enabled
            self.logger.debug("Alert from pin '%s' for bus '%s' wasn't enabled", alert_pin, pmbus_record.name)
            return

        signal_match = pmbus_record.alert_signals[alert_pin]
        signal_match.remove()
        del pmbus_record.alert_signals[alert_pin]
        self.logger.info("Disabled alerts from pin '%s' for bus '%s'", alert_pin, pmbus_record.name)


    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="ss",
        out_signature=""
    )
    def enable_bus_alert(self, bus_name, alert_pin):
        if not bus_name in self._pmbuses:
            raise UnknownBusException(bus_name)

        pmbus_record = self._pmbuses[bus_name]
        if alert_pin not in pmbus_record.alert_pins:
            raise InvalidAlertPinException(bus_name, alert_pin)

        self._enable_alert_pin(pmbus_record, alert_pin)


    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="s",
        out_signature=""
    )
    def enable_bus_alerts(self, bus_name):
        if not bus_name in self._pmbuses:
            raise UnknownBusException(bus_name)

        pmbus_record = self._pmbuses[bus_name]
        for pin in pmbus_record.alert_pins:
            self._enable_alert_pin(pmbus_record, pin)

    
    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="ss",
        out_signature=""
    )
    def disable_bus_alert(self, bus_name, alert_pin):
        if not bus_name in self._pmbuses:
            raise UnknownBusException(bus_name)

        pmbus_record = self._pmbuses[bus_name]
        if alert_pin not in pmbus_record.alert_pins:
            raise InvalidAlertPinException(bus_name, alert_pin)

        self._disable_alert_pin(pmbus_record, alert_pin)


    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="s",
        out_signature=""
    )
    def disable_bus_alerts(self, bus_name):
        if not bus_name in self._pmbuses:
            raise UnknownBusException(bus_name)

        pmbus_record = self._pmbuses[bus_name]
        for pin in pmbus_record.alert_pins:
            self._disable_alert_pin(pmbus_record, pin)

    
    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="ss",
        out_signature="as"
    )
    def list_comparators(self, device_name, monitor_name):
        """
        Lists comparators available for a given monitor.
        :type device_name: str
        :type monitor_name: str
        :rtype: [str]
        """
        mon = self._get_monitor(device_name, monitor_name)
        return mon.list_comparators()

    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="sss",
        out_signature="d"
    )
    def get_device_threshold(self, device_name, monitor_name, threshold_name):
        """
        Gets a threshold of a monitor of a device
        :type device_name: str
        :type monitor_name: str
        :type threshold_name: str
        :rtype: float
        """
        self.logger.debug(
            "Getting threshold '%s' of monitor '%s' of device '%s'",
            threshold_name,
            monitor_name,
            device_name
        )
        mon = self._get_monitor(device_name, monitor_name)
        return mon.get_threshold(threshold_name)


    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="sssd",
        out_signature=""
    )
    def set_device_threshold(self, device_name, monitor_name, threshold_name, value):
        """
        Sets a threshold of a monitor of a device
        :type device_name: str
        :type monitor_name: str
        :type threshold_name: str
        :type value: float
        """
        self.logger.info(
            "Setting threshold '%s' of monitor '%s' of device '%s'",
            threshold_name,
            monitor_name,
            device_name
        )
        mon = self._get_monitor(device_name, monitor_name)
        mon.set_threshold(threshold_name, value)


    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="sss",
        out_signature=""
    )
    def enable_device_alert(self, device_name, monitor_name, threshold_name):
        """
        Enable an alert for a threshold
        :type device_name: str
        :type monitor_name: str
        :type threshold_name: str
        """
        self.logger.info(
            "Enabling alert for '%s' of monitor '%s' of device '%s'",
            threshold_name,
            monitor_name,
            device_name
        )
        mon = self._get_monitor(device_name, monitor_name)
        mon.enable_alert(threshold_name)


    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="sss",
        out_signature=""
    )
    def disable_device_alert(self, device_name, monitor_name, threshold_name):
        """
        Enable an alert for a threshold
        :type device_name: str
        :type monitor_name: str
        :type threshold_name: str
        """
        self.logger.info(
            "Enabling alert for '%s' of monitor '%s' of device '%s'",
            threshold_name,
            monitor_name,
            device_name
        )
        mon = self._get_monitor(device_name, monitor_name)
        mon.enable_alert(threshold_name)


    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="s",
        out_signature="a(ss)"
    )
    def get_device_alerts(self, device_name):
        """
        Get the alerts for a device
        :type address: str
        :rtype: [(str, str)]
        """
        self.logger.debug("Querying alerts of device '%s'", device_name)
        dev = self._get_device(device_name)
        alerts = dev.query_alert()
        self.logger.debug("'%s alerts: %s", device_name, alerts)
        return alerts


    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="sss",
        out_signature=""
    )
    def clear_device_alert(self, device_name, monitor_name, threshold_name):
        """
        Clear an alert for a threshold
        :type device_name: str
        :type monitor_name: str
        :type threshold_name: str
        """
        self.logger.info(
            "Clearing alert for '%s' of monitor '%s' of device '%s'",
            threshold_name,
            monitor_name,
            device_name
        )
        mon = self._get_monitor(device_name, monitor_name)
        mon.clear_alert(threshold_name)


    @dbus.service.method(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        in_signature="s",
        out_signature=""
    )
    def clear_device_alerts(self, device_name):
        """
        Clear all alerts on the given device.
        :type device_name: str
        """
        self.logger.info(
            "Clearing all alerts for device '%s'",
            device_name
        )
        dev = self._get_device(device_name)
        dev.clear_faults()


    @dbus.service.signal(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        signature="sa(ss)"
    )
    def device_alert(self, device_name, alerts):
        self.logger.warn("Alert from device '%s': %s", device_name, from_dbus(alerts))


    @dbus.service.signal(
        dbus_interface=POWER_ALERT_INTERFACE_NAME,
        signature="sv"
    )
    def unknown_device_alert(self, bus_name, address):
        if address == ALERT_ADDRESS_UNKNOWN:
            self.logger.error("Alert from from unknown address on bus '%s'", bus_name)
        else:
            self.logger.error("Alert from unknown device at address 0x%02x on bus '%s'", address, bus_name)


    @dbus.service.method(
        dbus_interface=POWER_CONTROL_INTERFACE_NAME,
        in_signature="ss",
        out_signature=""
    )
    def enable_device_control(self, device_name, control_name):
        """
        Enables a control of a device
        :type device_name: str
        :type control_name: str
        """
        self.logger.info(
            "Enabling control '%s' of device '%s'",
            control_name,
            device_name
        )
        ctrl = self._get_control(device_name, control_name)
        return ctrl.enable()


    @dbus.service.method(
        dbus_interface=POWER_CONTROL_INTERFACE_NAME,
        in_signature="ss",
        out_signature=""
    )
    def disable_device_control(self, device_name, control_name):
        """
        Disables a control of a device
        :type device_name: str
        :type control_name: str
        """
        self.logger.info(
            "Disabling control '%s' of device '%s'",
            control_name,
            device_name
        )
        ctrl = self._get_control(device_name, control_name)
        return ctrl.disable()


    @dbus.service.method(
        dbus_interface=POWER_CONTROL_INTERFACE_NAME,
        in_signature="ss",
        out_signature="v"
    )
    def get_device_control(self, device_name, control_name):
        """
        Get the current value of a control of a device
        :type device_name: str
        :type control_name: str
        :rtype: float
        """
        self.logger.debug(
            "Get control '%s' of device '%s'",
            control_name,
            device_name
        )
        ctrl = self._get_control(device_name, control_name)
        return to_dbus(ctrl.get())


    @dbus.service.method(
        dbus_interface=POWER_CONTROL_INTERFACE_NAME,
        in_signature="ssv",
        out_signature=""
    )
    def set_device_control(self, device_name, control_name, value):
        """
        Set the value of a control of a device
        :type device_name: str
        :type control_name: str
        :type value: float
        """
        self.logger.info(
            "Set control '%s' of device '%s'",
            control_name,
            device_name
        )
        ctrl = self._get_control(device_name, control_name)
        return ctrl.set(from_dbus(value))
