import dbus
import dbus.service

from .logging import LoggingObject

FAULT_INTERFACE_NAME = "systems.enzian.Fault"

class FaultObject(LoggingObject):
    def __init__(self, logger, bus_name, object_path, gpio, power, scram_faults, scram_handler):
        super(FaultObject, self).__init__(logger, bus_name, object_path)
        self.gpio= gpio
        self.power= power
        self.scram_masked= True
        self.scram_faults = scram_faults
        self.scram_handler = scram_handler

    @dbus.service.method(
        dbus_interface=FAULT_INTERFACE_NAME,
        in_signature="",
        out_signature=""
    )
    def mask_scram(self):
        self.logger.info("SCRAM response masked.")
        self.scram_masked= True

    @dbus.service.method(
        dbus_interface=FAULT_INTERFACE_NAME,
        in_signature="",
        out_signature=""
    )
    def unmask_scram(self):
        self.logger.info("SCRAM response unmasked.")
        self.scram_masked= False

    def fault_handler(self, pin_name):
        """FAULT is asserted by the temperature and fan controller for either
        a CPU/FPGA critical temperature fault or a failed fan.  It triggers an
        emergency shutdown."""
        if not self.scram_masked:
            # Scram before logging - time is of the essence.
            self.scram_handler(self.logger, self.gpio, self.power)
            self.mask_scram()

        self.logger.debug("Fault on pin %s", pin_name)

    def alert_handler(self, device_name, alerts):
        # A subset of faults can be set to cause a scram.
        for comparator, fault in alerts:
            if ((device_name, comparator, fault) in self.scram_faults and
                not self.scram_masked):
                self.scram_handler(self.logger, self.gpio, self.power)
                self.mask_scram()

        self.logger.debug("Alerts on device %s: %s", device_name, alerts)

    def unknown_alert_handler(self, bus_name, address):
        # Alerts from unknown sources always scram.
        if not self.scram_masked:
            self.scram_handler(self.logger, self.gpio, self.power)
            self.mask_scram()

        self.logger.debug("Unknown alert on bus %s", bus_name)
