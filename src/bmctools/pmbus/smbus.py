from . import i2cbus as I2C

class SMBus_Exception(I2C.I2CBus_Exception):
    pass

class SMBus_Protocol_Error(SMBus_Exception):
    """An error in the SMBus protocol e.g. unexpected number of bytes
    received."""
    pass

### SMBus Transactions

class Transaction(object):
    """Each SMBus command has an associated read and/or write transaction.
    These are not necessarily the same e.g. SMBALERT_MASK with Write
    Word/Block Read Process Call.  See Table 29 in Appendix I of the PMBus
    spec.
    
    Also, each transaction may or may not specify a size: Block read/write and
    process call transactions may be of variable length.  The subclasses of
    `Transaction` implement these."""
    pass

class Write(Transaction):
    """Write Byte or Write Word"""
    def __init__(self, length=None):
        self.length= length

    def execute(self, smbus, address, command, tx_data):
        if self.length != len(tx_data):
            raise SMBus_Protocol_Error(
                "SMBus write (0x%X,0x%X): Expected %dB, got %dB" %
                (address, command, self.length, len(tx_data)))

        if self.length == 0:
            return smbus.send_byte(address, bytearray([command]))
        elif self.length == 1:
            return smbus.write_byte(address, command, tx_data)
        else:
            return smbus.write_word(address, command, tx_data)

class BlockWrite(Transaction):
    """Block Write, with fixed or variable length"""
    def __init__(self, length=None, compliant=True):
        self.length= length
        self.compliant= compliant

    def execute(self, smbus, address, command, tx_data):
        if not self.length is None and self.length != len(tx_data):
            raise SMBus_Protocol_Error(
                "SMBus block write (0x%X,0x%X): Expected %dB, got %dB" %
                (address, command, self.length, len(tx_data)))

        if self.compliant:
            return smbus.write_block(address, command, tx_data)
        else:
            return smbus.write_block_noncompliant(address, command, tx_data)

class Read(Transaction):
    """Read Byte or Read Word"""
    def __init__(self, length=None):
        self.length= length

    def execute(self, smbus, address, command, tx_data):
        if self.length == 0:
            return smbus.receive_byte(address)
        elif self.length == 1:
            return smbus.read_byte(address, command)
        else:
            return smbus.read_word(address, command)

class BlockRead(Transaction):
    """Block Read, with fixed or variable length"""
    def __init__(self, length=None, compliant=True):
        assert compliant or not length is None, \
            "Non-compliant block reads must specify the block length."
        self.length= length
        self.compliant= compliant

    def execute(self, smbus, address, command, tx_data):
        if self.compliant:
            rx_data= smbus.read_block(address, command)
        else:
            rx_data= smbus.read_block_noncompliant(address, command,
                                                   self.length)

        if not self.length is None and self.length != len(rx_data):
            raise SMBus_Protocol_Error(
                "SMBus block read (0x%X,0x%X): Expected %dB, got %dB" %
                (address, command, self.length, len(rx_data)))

        return rx_data

class Call(Transaction):
    """Block Read or Block Write Process Call.  TX and/or RX length may be
    variable (`None`)."""
    def __init__(self, tx_length=None, rx_length=None):
        self.tx_length= tx_length
        self.rx_length= rx_length

    def execute(self, smbus, address, command, tx_data):
        if not self.tx_length is None and self.tx_length != len(tx_data):
            raise SMBus_Protocol_Error(
                "SMBus call (0x%X,0x%X): " \
                "Expected %dB arguments, got %dB" %
                (address, command, self.tx_length, len(tx_data)))

        rx_data= smbus.process_call_block(address, command, tx_data)

        if not self.rx_length is None and self.rx_length != len(rx_data):
            raise SMBus_Protocol_Error(
                "SMBus call (0x%X,0x%X): Expected %dB, got %dB" %
                (address, command, self.rx_length, len(tx_data)))

        return rx_data

# SMBus reserved addresses (Appendix C, Table 17), excluding those already
# reserved in the I2C specification.
smbus_rsvd_by_name = {
    "SMBUS_HOST"       : 0x08,
    "BATTERY_CHARGER"  : 0x09,
    "BATTERY_SELECTOR" : 0x0a,
    "BATTERY"          : 0x0b,
    "ALERT_RESPONSE"   : 0x0c,
    "PMBUS_ZONE_READ"  : 0x28,
    "LCD_CONTRAST"     : 0x2c,
    "CCFL_BACKLIGHT"   : 0x2d,
    "PMBUS_ZONE_WRITE" : 0x37,
    "PCMCIA_0"         : 0x40,
    "PCMCIA_1"         : 0x41,
    "PCMCIA_2"         : 0x42,
    "PCMCIA_3"         : 0x43,
    "VGA"              : 0x44,
    "PROTOTYPE_0"      : 0x48,
    "PROTOTYPE_1"      : 0x49,
    "PROTOTYPE_2"      : 0x4a,
    "PROTOTYPE_3"      : 0x4b,
    "SMBUS_DEFAULT"    : 0x61
}
smbus_rsvd_addr = frozenset(smbus_rsvd_by_name.values())

# Exclude non-device addresses
smbus_probe_exclude = \
    frozenset([smbus_rsvd_by_name[n] for n in [ \
        "SMBUS_HOST",
        "ALERT_RESPONSE",
        "PMBUS_ZONE_READ",
        "PMBUS_ZONE_WRITE"
        ]])

class SMBus(I2C.I2CBus):
    def __init__(self, *args, **kwargs):
        super(SMBus, self).__init__(*args, **kwargs)

    def quick_cmd(self, address, read):
        """
        Quick command (S6.5.1)
        :type address: int
        :type read: bool
        """
        raise SMBus_Exception("unimplemented")

    def send_byte(self, address, data):
        """
        Send byte (S6.5.2)
        :type address: int
        :rtype: data bytearray
        """
        raise SMBus_Exception("unimplemented")

    def receive_byte(self, address):
        """
        Receive byte (S6.5.3)
        :type address: int
        :rtype: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def write_byte(self, address, command, data):
        """
        Write byte (S6.5.4)
        :type address: int
        :type command: int
        :type data: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def write_word(self, address, command, data):
        """
        Write word (S6.5.4)
        :type address: int
        :type command: int
        :type data: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def read_byte(self, address, command):
        """
        Read byte (S6.5.5)
        :type address: int
        :type command: int
        :rtype: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def read_word(self, address, command):
        """
        Read byte (S6.5.5)
        :type address: int
        :type command: int
        :rtype: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def process_call(self, address, command, data):
        """
        Process call (S6.5.6)
        :type address: int
        :type command: int
        :type data: bytearray
        :rtype: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def write_block(self, address, command, data):
        """
        Block write (S6.5.7)
        :type address: int
        :type command: int
        :type data: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def write_block_noncompliant(self, address, command, data):
        """
        Non compliant block write that doesn't transfer the
        data length
        :type address: int
        :type command: int
        :type data: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def read_block(self, address, command):
        """
        Block read (S6.5.7)
        :type address: int
        :type command: int
        :rtype: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def read_block_noncompliant(self, address, command, length):
        """
        Non compliant block read that takes the data length as
        parameter instead of reading it from the bus
        :type address: int
        :type command: int
        :type length: int
        :rtype: bytearray
        """
        raise SMBus_Exception("unimplemented")

    def process_call_block(self, address, command, data):
        """
        Block Write-Block Read Process Call(S6.5.8)
        :type address: int
        :type command: int
        :type data: bytearray
        :rtype: bytearray
        """
        raise SMBus_Exception("unimplemented")
    
    def alert_response(self):
        """
        SMBus Alert Reponse (A.2)

        Issues a read to the SMBus Alert Reponse Address (0x0C), to which the
        device pulling SMBALERT low must respond with its address.

        :return: Alerting device address (or None)
        :rtype: int
        """
        # Address is 7 most significant bits of received byte
        byte = self.receive_byte(smbus_rsvd_by_name["ALERT_RESPONSE"])[0]
        return (byte >> 1)


class SMBus_Device(I2C.I2CBus_Device):
    """A device at a given address on the given SMBus."""

    def __init__(self, *args, **kwargs):
        super(SMBus_Device, self).__init__(*args, **kwargs)

    def __str__(self):
        return "SMBus device at (7b) 0x%X" % self.address
