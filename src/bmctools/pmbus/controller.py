from copy import copy

from . import variables as V

class Controller(object):
    _monitor_specs= {}
    _monitor_overrides= {}
    _monitor_aliases= {}
    monitors= {}

    _control_specs= {}
    _control_overrides= {}
    _control_aliases= {}
    controls= {}

    def __init__(self, configure=False, extra_cfg={}, *args, **kwargs):
        super(Controller, self).__init__(*args, **kwargs)

        # Prevent monitors and controls being shared between instances
        # XXX: Any subclass that modifies these fields before calling our constructor
        # needs to do the same!!
        # FIXME: This is brittle, think about changing the design pattern
        # to factory based objects
        self.monitors = copy(self.monitors)
        self.controls = copy(self.controls)

        # Initialise all prespecified monitors
        for name in self._monitor_specs:
            m_kwargs= copy(self._monitor_specs[name])

            if name in self._monitor_overrides:
                override = self._monitor_overrides[name]
                m_kwargs.update(override)

            self.monitors[name]= V.Monitor(name, self, **m_kwargs)

        # Initialise all prespecified controls
        for name in self._control_specs:
            m_kwargs= copy(self._control_specs[name])

            if name in self._control_overrides:
                override = self._control_overrides[name]
                m_kwargs.update(override)

            self.controls[name]= V.Control(name, self, **m_kwargs)

        for (alias, name) in self._monitor_aliases.items():
            self.monitors[alias]= self.monitors[name]
            self.monitors[alias].name = alias

        for (alias, name) in self._control_aliases.items():
            self.controls[alias]= self.controls[name]
            self.controls[alias].name = alias

        if configure: self.configure(**extra_cfg)

    def configure(self, *args, **kwargs):
        raise Exception("Unimplemented base class method")

    def sample_interval(self, name):
        """Return the current sampling interval for the given variable."""
        return self.monitors[name].interval

    def is_monitor_enabled(self, name):
        raise Exception("Unimplemented base class method")

    def enable_monitor(self, name):
        raise Exception("Unimplemented base class method")

    def disable_monitor(self, name):
        raise Exception("Unimplemented base class method")

    def set_threshold(self, M, cmp_type, threshold):
        raise Exception("Unimplemented base class method")

    def get_threshold(self, M, cmp_type):
        raise Exception("Unimplemented base class method")

    def read_alert_mask(self):
        raise Exception("Unimplemented base class method")

    def enable_alert(self, M, cmp_type):
        raise Exception("Unimplemented base class method")

    def disable_alert(self, M, cmp_type):
        raise Exception("Unimplemented base class method")

    def clear_alert(self, M, cmp_type):
        raise Exception("Unimplemented base class method")

    def disable_all_alerts(self):
        raise Exception("Unimplemented base class method")

    def query_alert(self):
        raise Exception("Unimplemented base class method")
