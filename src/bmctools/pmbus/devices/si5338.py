import time

from .. import i2cbus as I2C
from .. import formats as F

class SI5338(I2C.I2CBus_RegisterDevice):
    """Silicon Labs Si5338

    I2C-Programmable Any-Frequency, Any-Output Quad Clock Generator

    References are to Si5338 Datasheet Rev. 1.6 12/15, and Si5338 Reference
    Manual, Rev 1.3 8/14.
    """

    _reg_width= 1

    # Only fields needed for programming are defined - all others are to be
    # loaded from a generated register map.
    _parameters = [
        I2C.Field("Dev_Config2",
                  [(2, 5, 0)],
                  F.Unsigned(6),
                  tags= {
                    "desc" : "Bits 5:0 represent the last two digits of the"
                             " base part number: \"38\" for Si5338.  See"
                             " \"10.6.1. Example Part Number for Device ID"
                             " Registers\" on page 55 for complete part"
                             " number example.",
                  }
        ),
        I2C.Field("FCAL_OVRD",
                  [(47, 1, 0), (46, 7, 0), (45, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "Override Frequency Calibration for the VCO.",
                  }
        ),
        I2C.Field("R47_MAGIC",
                  [(47, 7, 2)],
                  F.Tabular("u6", {
                    0x05 : "Programmed",
                  }),
                  tags= {
                    "desc" : "Must write 000101b to these bits if the device"
                             " is not factory programmed.",
                  }
        ),
        I2C.Field("FCAL_OVRD_EN",
                  [(49, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "FCAL Override Enable.  0: Do not use FCAL value"
                             " in registers 45,46,47.  1: Use FCAL value in"
                             " registers 45,46,47 Once a part is programmed"
                             " and calibrated (FCAL), this bit must be set."
                             " See Si5338 data sheet for more information.",
                  }
        ),
        I2C.Field("PLL_LOL",
                  [(218, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "PLL Loss of Lock (LOL).  Asserts when the two"
                             " PFD inputs have a frequency difference > 1000"
                             " ppm.  This bit is held high during a POR_reset"
                             " until the PLL has locked. This bit will not"
                             " chatter while the PLL is locking. PLL_LOL does"
                             " not assert when the external input reference"
                             " clock is lost. When PLL_LOL asserts, the part"
                             " will automatically try to re-acquire to the"
                             " input clock. See Register 241[7].",
                  },
                  writeable=False,
                  volatile=True
        ),
        I2C.Field("LOS_FDBK",
                  [(218, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Loss of Signal on Feedback Clock from IN5,6 or"
                             " IN4.",
                  },
                  writeable=False,
                  volatile=True
        ),
        I2C.Field("LOS_CLKIN",
                  [(218, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "Loss of Signal on Input Clock from IN1,2 or"
                             " IN3.",
                  },
                  writeable=False,
                  volatile=True
        ),
        I2C.Field("SYS_CAL",
                  [(218, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "Device Calibration in Process.",
                  },
                  writeable=False,
                  volatile=True
        ),
        # XXX - The datasheet forbids a RMW to this register, but instead
        # insists that all other bits must be set to zero on every write.  The
        # actual field is in bit 2.
        I2C.Field("MS_RESET",
                  [(226, 7, 0)],
                  F.Tabular("u8", {
                    0x04 : True,
                    0x00 : False
                  }),
                  tags= {
                    "desc" : "Multisynth Master Reset.  This reset will"
                             " disable all clock outputs, reset all"
                             " Multisynth blocks, and then enable all the"
                             " clock outputs. Retains device configuration"
                             " stored in RAM. Do not use read-modify-write"
                             " procedure to perform soft reset.  Instead,"
                             " write reg242 = 0x04 or 0x00. All Multisynth"
                             " blocks will remain in reset until a 0 is"
                             " written to this bit.",
                  },
                  readable=False,
                  volatile=True
        ),
        I2C.Field("OEB_ALL",
                  [(230, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "Output Enable Low for All Clock Outputs"
                             " 0: All output clocks are enabled, OEB_3,2,1,0"
                             " can still disable each clock.  1: All output"
                             " clocks are disabled regardless of the state"
                             " of OEB_3,2,1,0.",
                  }
        ),
        I2C.Field("FCAL",
                  [(237, 1, 0), (236, 7, 0), (235, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "Frequency Calibration for the VCO.",
                  }
        ),
        I2C.Field("DIS_LOL",
                  [(241, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "When asserted, the PLL_LOL status in register"
                             " 218 is prevented from asserting.",
                  }
        ),
        # (241, 6, 0) = write 0x65 after configuration
        I2C.Field("R241_MAGIC",
                  [(241, 6, 0)],
                  F.Tabular("u7", {
                    0x65 : "Programmed",
                  }),
                  tags= {
                    "desc" : "On a non-factory-programmed device this"
                             " register must be set to 0x65.  On a factory"
                             " programmed device, this register must stay"
                             " 0x65. See the I2C Programming Procedure in the"
                             " Si5338 data sheet for when to write this"
                             " register.",
                  }
        ),
        # XXX - The datasheet forbids a RMW to this register, but instead
        # insists that all other bits must be set to zero on every write.  The
        # actual field is in bit 1.
        I2C.Field("SOFT_RESET",
                  [(246, 7, 0)],
                  F.Tabular("u8", {
                    0x02 : True,
                    0x00 : False
                  }),
                  tags= {
                    "desc" : "Soft Reset.  This reset will disable all clock"
                             " outputs, then re-acquire the PLL to the input"
                             " clock and then enable all the clock outputs."
                             " Retains device configuration stored in RAM. Do"
                             " not use read-modify-write procedure to perform"
                             " soft reset. Instead, write reg246=0x02,"
                             " regardless of the current value of this bit."
                             " Reading this bit after a soft reset will"
                             " return a 1.",
                  },
                  readable=False,
                  volatile=True
        ),
    ]

    def __init__(self, bus, address, *args, **kwargs):
        super(SI5338, self).__init__(bus, address, *args, **kwargs)

    def probe(self):
        try:
            part= self.read("Dev_Config2")
            if part == 38:
                return True
            else:
                self.dev_logger.warning("Unrecognised part number: %d", part)
                return False
        except I2C.I2CBus_Exception as e:
            self.dev_logger.error("Failed to read registers.")
            raise e

    def clear_faults(self):
        pass

    def configure(self, registers, timeout=1.0):
        """Load a configuration supplied as a list (iterable) of register
        writes (register #, value, mask), as generated by Clockbuilder Pro.
        The list must include page-change commands, where required.
        
        Follows figure 9 in the datasheet."""

        # Disable outputs.
        self.write("OEB_ALL", True)

        # Pause LOL (loss of lock signalling).
        self.write("DIS_LOL", True)

        # Load register map.
        for reg_num, value, mask in registers:
            if mask == 0x00:
                # Ignore non-writable registers.
                continue
            elif mask == 0xFF:
                # Write the whole register.
                self.write_reg_raw(reg_num, bytearray([value]))
            else:
                # Read-modify-write.
                raw= self.read_reg_raw(reg_num)[0]
                raw&= ~mask
                raw|= value & mask
                self.write_reg_raw(reg_num, bytearray([raw]))

        # Validate input clock status.
        t_start= time.clock()
        # XXX - Assumes we're not using the feedback clock.
        while self.read("LOS_CLKIN"):
            t_now= time.clock()
            if t_now - t_start > timeout:
                self.dev_logger.error("Timeout waiting for LOS_CLKIN to"
                                      " deassert.")
                return

        # Configure PLL for locking
        self.write("FCAL_OVRD_EN", False)

        # Initiate locking of PLL
        self.write("SOFT_RESET", True)

        time.sleep(25e-3)

        # Restart LOL
        self.write("DIS_LOL", False)
        self.write("R241_MAGIC", "Programmed")

        # Wait for PLL to lock.
        while self.read("PLL_LOL") or self.read("SYS_CAL"):
            t_now= time.clock()
            if t_now - t_start > timeout:
                self.dev_logger.error("Timeout waiting for PLL_LOL and"
                                      " SYS_CAL to deassert.")
                return

        # Copy FCAL values to active registers
        raw= self.read("FCAL")
        self.write("FCAL_OVRD", raw)
        self.write("R47_MAGIC", "Programmed")

        # Set PLL to use FCAL values
        self.write("FCAL_OVRD_EN", True)

        # Reset MultiSynth
        self.write("MS_RESET", True)
        time.sleep(1e-3)
        self.write("MS_RESET", False)

        # Enable outputs
        self.write("OEB_ALL", False)
