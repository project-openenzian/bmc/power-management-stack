import time

from .. import i2cbus as I2C

class SI5395(I2C.I2CBus_RegisterDevice):
    """ Si5395/94/92
        12-Channel, Any-Frequency, Any-Output Jitter Attenuator/
        Clock Multiplier with Ultra-Low Jitter
    """

    _reg_width= 1

    def __init__(self, bus, address, *args, **kwargs):
        super(SI5395, self).__init__(bus, address, *args, **kwargs)

    def probe(self):
        try:
            self.write_reg_raw(0x01, [0x00])
            id2 = self.read_reg_raw(2)
            id3 = self.read_reg_raw(3)
            if id2[0] == 0x95 and id3[0] == 0x53:
                return True
            return False

        except I2C.I2CBus_Exception as e:
            self.dev_logger.warning("Failed to read registers.")
            raise e

    def clear_faults(self):
        pass

    def configure(self, registers):
        """Load the configuration."""
        self.write_reg_raw(0x01, [0x0b])
        self.write_reg_raw(0x24, [0xc0])
        self.write_reg_raw(0x25, [0x00])
        self.write_reg_raw(0x01, [0x05])
        self.write_reg_raw(0x40, [0x01])
        current_page = 5
        for (address, value) in registers:
            page = int(address / 256)
            if (page != current_page):
                self.write_reg_raw(0x01, [page])
                current_page = page
            self.write_reg_raw(address % 256, [value])
