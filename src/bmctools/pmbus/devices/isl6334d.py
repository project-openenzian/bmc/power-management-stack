from .. import controller as C
from .. import variables as V

def _convert_voltage_to_binary(value):
    """
    Converts a floating point voltage to byte encoding
    and thresholds it (see table 3 in datasheet).

    The ISL's voltage range is 0.5V - 1.6V everything outside the
    range turns the regulator off.

    :type value: float
    """
    steps = (1.6125 - value) / 0.00625
    steps = int(round(steps))
    if steps < 0:
        steps = 0
    elif steps > 0xff:
        steps = 0xff
    return steps

def _convert_binary_to_voltage(value):
    """
    Does the inverse of the above

    :type value: int
    """
    voltage = 1.6125 - (value * 0.00625)
    if voltage < 0.5:
        voltage = 0
    elif voltage > 1.6:
        voltage = 0
    return voltage

class ISLVoltageControl(V.Control):
    def __init__(self, name, gpio, pin_group):
        super(ISLVoltageControl, self).__init__(
            name, None, enable={}, disable={})
        self._gpio = gpio
        self._pin_group = pin_group

    def set(self, value):
        voltage = _convert_voltage_to_binary(value)
        self._gpio.set_value(self._pin_group, voltage)

    def get(self):
        binary = self._gpio.get_value(self._pin_group)
        return _convert_binary_to_voltage(binary)

    def state(self):
        return {
            "value" : self.get()
        }    


class ISL6334D(C.Controller):
    """
    ISL6334D VR11.1, 4-Phase PWM Controller with Phase Dropping, Droop Disabled and Load
    Current Monitoring Features
    """
    def __init__(self, gpio, pin_groups):
        super(ISL6334D, self).__init__(configure=False)

        self.controls["VID"] = ISLVoltageControl("VID", gpio, pin_groups["VID"])

    def configure(self, extra_cfg):
        pass

    def clear_faults(self):
        pass
