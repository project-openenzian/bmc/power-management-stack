from .. import formats as F
from .. import i2cbus as I2C
from .. import smbus as SMB
from .. import variables as V
from .. import controller as C

class ISPPAC(SMB.SMBus_Device,I2C.I2CBus_RegisterDevice,C.Controller):
    """ ispPAC-POWR1220AT8
        In-System Programmable Power Supply
        Monitoring, Sequencing and Margining Controller
    """
    _reg_width= 1

    _parameters = [
        I2C.Field("VMON_STATUS",
            [(0x02, 7, 0), (0x01, 7, 0), (0x00, 7, 0)],
            F.Flags(
                "b1" * 24, [
                "VMON12B", "VMON12A",
                "VMON11B", "VMON11A",
                "VMON10B", "VMON10A",
                "VMON9B",  "VMON9A",
                "VMON8B",  "VMON8A",
                "VMON7B",  "VMON7A",
                "VMON6B",  "VMON6A",
                "VMON5B",  "VMON5A",
                "VMON4B",  "VMON4A",
                "VMON3B",  "VMON3A",
                "VMON2B",  "VMON2A",
                "VMON1B",  "VMON1A"
            ]),
            tags= {
                "desc" : "VMON comparator status",
                "section" : "VMON Status Registers",
                "persistent_cfg" : False,
            },
            writeable=False,
            volatile=True
        ),
        I2C.Field("OUTPUT_STATUS",
            [(0x05, 3, 0), (0x04, 7, 0), (0x03, 7, 0)],
            F.Flags("b1" * 20, [
                "OUT20", "OUT19", "OUT18", "OUT17",
                "OUT16", "OUT15", "OUT14", "OUT13",
                "OUT12", "OUT11", "OUT10", "OUT9",
                "OUT8",  "OUT7",  "OUT6",  "OUT5",
                "HVOUT4", "HVOUT2", "HVOUT3", "HVOUT1",
            ]),
            tags= {
                "desc" : "Output status",
                "section" : "Output Status Registers",
                "persistent_cfg" : False,
            },
            writeable=False,
            volatile=True
        ),
        I2C.Field("INPUT_STATUS",
            [(0x06, 5, 0)],
            F.Flags("b1" * 6, [
                "IN6", "IN5", "IN4", "IN3", "IN2", "IN1"
            ]),
            tags= {
                "desc" : "Input status",
                "section" : "Inputs",
                "persistent_cfg" : False,
            },
            writeable = False,
            volatile =True
        ),
        I2C.Field("ADC_VALUE",
            [(0x08, 7, 0), (0x07, 7, 0)],
            F.Bitfield("r12p3r1", [ "VALUE", "DONE" ], field_fmt={
                "VALUE" : F.UnsignedScaled(12, 2e-3),
                "DONE"  : F.Boolean 
            }),
            writeable=False,
            volatile=True,
            tags = {
                "desc" : "Raw value converted by ADC, only valid if DONE is"
                         " set",
                "section" : "ADC",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("ADC_MUX",
            [(0x09, 4, 0)],
            F.Bitfield("r1r4", [ "ATTEN", "SEL" ], field_fmt={
                "ATTEN" : F.Boolean,
                "SEL" : F.Tabular("u4", {
                    0 : "VMON1",
                    1 : "VMON2",
                    2 : "VMON3",
                    3 : "VMON4",
                    4 : "VMON5",
                    5 : "VMON6",
                    6 : "VMON7",
                    7 : "VMON8",
                    8 : "VMON9",
                    9 : "VMON10",
                    10 : "VMON11",
                    11 : "VMON12",
                    12 : "VCCA",
                    13 : "VCCINP"
                }),
            }),
            tags= {
                "desc" : """ADC Input Multiplexer, writing starts conversion,
                only issue new command once adc_value.DONE is set.
                
                ATTEN sets the ADC input attentuator to give either an LSB of
                2mV (False, FSR 2.048V) or 6mV (True, FSR 6.144V)""",
                "section" : "ADC",
                "persistent_cfg" : False,
            }
        ),
        I2C.Field("UES",
            [(0x0D, 7, 0), (0x0C, 7, 0), (0x0B, 7, 0), (0x0A, 7, 0)],
            F.Bitfield("r32", [ "UES" ]),
            writeable=False,
            tags={
                "section" : "User Electronic Signature",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT20",
            [(0x10, 3, 3)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT20.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT19",
            [(0x10, 2, 2)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT19.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT18",
            [(0x10, 1, 1)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT18.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT17",
            [(0x10, 0, 0)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT17.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT16",
            [(0x0F, 7, 7)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT16.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT15",
            [(0x0F, 6, 6)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT15.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT14",
            [(0x0F, 5, 5)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT14.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT13",
            [(0x0F, 4, 4)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT13.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT12",
            [(0x0F, 3, 3)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT12.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT11",
            [(0x0F, 2, 2)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT11.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT10",
            [(0x0F, 1, 1)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT10.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT9",
            [(0x0F, 0, 0)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT9.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT8",
            [(0x0E, 7, 7)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT8.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT7",
            [(0x0E, 6, 6)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT7.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT6",
            [(0x0E, 5, 5)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT6.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_OUT5",
            [(0x0E, 4, 4)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets OUT5.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_HVOUT4",
            [(0x0E, 3, 3)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets HVOUT3.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_HVOUT3",
            [(0x0E, 2, 2)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets HVOUT3.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_HVOUT2",
            [(0x0E, 1, 1)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets HVOUT2.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("GP_HVOUT1",
            [(0x0E, 0, 0)],
            F.Boolean,
            volatile=True,
            tags={
                "desc" : "Manually sets HVOUT1.",
                "persistent_cfg" : False
            }
        ),
        I2C.Field("INPUT_VALUE",
            [(0x11, 5, 1)],
            F.Flags("b1" * 5, [
                "IN6", "IN5", "IN4", "IN3", "IN2"
            ]),
            tags= {
                "desc" : "Input status",
                "section" : "Inputs",
                "persistent_cfg" : False,
            }
        ),
        I2C.Register(0x12, "RESET",
            F.Boolean,
            readable=False,
            tags={
                "desc" : "Writing any value to this register resets the device",
                "persistent_cfg" : False 
            }
        ),
        # Reading via this parameter will poll until DONE is asserted or
        # timeout exceeded (default 0s).
        I2C.Polled("ADC_POLLED",
            "ADC_VALUE",
            "VALUE",
            "DONE",
            True
        ),
        # These present a demultiplexed view of the ADC inputs.  The _ATT
        # variants read with attenuator enabled (6mV resolution).
        I2C.Multiplexed("READ_VMON1",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON1" } )]
        ),
        I2C.Multiplexed("READ_VMON2",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON2" } )]
        ),
        I2C.Multiplexed("READ_VMON3",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON3" } )]
        ),
        I2C.Multiplexed("READ_VMON4",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON4" } )]
        ),
        I2C.Multiplexed("READ_VMON5",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON5" } )]
        ),
        I2C.Multiplexed("READ_VMON6",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON6" } )]
        ),
        I2C.Multiplexed("READ_VMON7",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON7" } )]
        ),
        I2C.Multiplexed("READ_VMON8",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON8" } )]
        ),
        I2C.Multiplexed("READ_VMON9",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON9" } )]
        ),
        I2C.Multiplexed("READ_VMON10",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON10" } )]
        ),
        I2C.Multiplexed("READ_VMON11",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON11" } )]
        ),
        I2C.Multiplexed("READ_VMON12",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VMON12" } )]
        ),
        I2C.Multiplexed("READ_VCCA",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VCCA" } )]
        ),
        I2C.Multiplexed("READ_VCCINP",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : False, "SEL" : "VCCINP" } )]
        ),
        I2C.Multiplexed("READ_VMON1_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON1" } )]
        ),
        I2C.Multiplexed("READ_VMON2_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON2" } )]
        ),
        I2C.Multiplexed("READ_VMON3_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON3" } )]
        ),
        I2C.Multiplexed("READ_VMON4_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON4" } )]
        ),
        I2C.Multiplexed("READ_VMON5_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON5" } )]
        ),
        I2C.Multiplexed("READ_VMON6_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON6" } )]
        ),
        I2C.Multiplexed("READ_VMON7_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON7" } )]
        ),
        I2C.Multiplexed("READ_VMON8_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON8" } )]
        ),
        I2C.Multiplexed("READ_VMON9_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON9" } )]
        ),
        I2C.Multiplexed("READ_VMON10_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON10" } )]
        ),
        I2C.Multiplexed("READ_VMON11_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON11" } )]
        ),
        I2C.Multiplexed("READ_VMON12_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VMON12" } )]
        ),
        I2C.Multiplexed("READ_VCCA_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VCCA" } )]
        ),
        I2C.Multiplexed("READ_VCCINP_ATT",
            "ADC_POLLED",
            [("ADC_MUX", { "ATTEN" : True, "SEL" : "VCCINP" } )]
        ),
        # TODO: Trim values
    ]

    _monitor_specs = {
        "VMON1" : {
            "key" : "READ_VMON1",
            "quantity" : "VOLTAGE"
        },
        "VMON2" : {
            "key" : "READ_VMON2",
            "quantity" : "VOLTAGE"
        },
        "VMON3" : {
            "key" : "READ_VMON3",
            "quantity" : "VOLTAGE"
        },
        "VMON4" : {
            "key" : "READ_VMON4",
            "quantity" : "VOLTAGE"
        },
        "VMON5" : {
            "key" : "READ_VMON5",
            "quantity" : "VOLTAGE"
        },
        "VMON6" : {
            "key" : "READ_VMON6",
            "quantity" : "VOLTAGE"
        },
        "VMON7" : {
            "key" : "READ_VMON7",
            "quantity" : "VOLTAGE"
        },
        "VMON8" : {
            "key" : "READ_VMON8",
            "quantity" : "VOLTAGE"
        },
        "VMON9" : {
            "key" : "READ_VMON9",
            "quantity" : "VOLTAGE"
        },
        "VMON10" : {
            "key" : "READ_VMON10",
            "quantity" : "VOLTAGE"
        },
        "VMON11" : {
            "key" : "READ_VMON11",
            "quantity" : "VOLTAGE"
        },
        "VMON12" : {
            "key" : "READ_VMON12",
            "quantity" : "VOLTAGE"
        },
        "VCCA" : {
            "key" : "READ_VCCA",
            "quantity" : "VOLTAGE"
        },
        "VCCINP" : {
            "key" : "READ_VCCINP",
            "quantity" : "VOLTAGE"
        },
        "VMON1_ATT" : {
            "key" : "READ_VMON1_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON2_ATT" : {
            "key" : "READ_VMON2_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON3_ATT" : {
            "key" : "READ_VMON3_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON4_ATT" : {
            "key" : "READ_VMON4_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON5_ATT" : {
            "key" : "READ_VMON5_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON6_ATT" : {
            "key" : "READ_VMON6_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON7_ATT" : {
            "key" : "READ_VMON7_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON8_ATT" : {
            "key" : "READ_VMON8_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON9_ATT" : {
            "key" : "READ_VMON9_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON10_ATT" : {
            "key" : "READ_VMON10_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON11_ATT" : {
            "key" : "READ_VMON11_ATT",
            "quantity" : "VOLTAGE"
        },
        "VMON12_ATT" : {
            "key" : "READ_VMON12_ATT",
            "quantity" : "VOLTAGE"
        },
        "VCCA_ATT" : {
            "key" : "READ_VCCA_ATT",
            "quantity" : "VOLTAGE"
        },
        "VCCINP_ATT" : {
            "key" : "READ_VCCINP_ATT",
            "quantity" : "VOLTAGE"
        },
    }

    _control_specs = {
        "HVOUT1" : {
            "command" : "GP_HVOUT1",
            "quantity" : "BOOLEAN"
        },
        "HVOUT2" : {
            "command" : "GP_HVOUT2",
            "quantity" : "BOOLEAN"
        },
        "HVOUT3" : {
            "command" : "GP_HVOUT3",
            "quantity" : "BOOLEAN"
        },
        "HVOUT4" : {
            "command" : "GP_HVOUT4",
            "quantity" : "BOOLEAN"
        },
        "OUT5" : {
            "command" : "GP_OUT5",
            "quantity" : "BOOLEAN"
        },
        "OUT6" : {
            "command" : "GP_OUT6",
            "quantity" : "BOOLEAN"
        },
        "OUT7" : {
            "command" : "GP_OUT7",
            "quantity" : "BOOLEAN"
        },
        "OUT8" : {
            "command" : "GP_OUT8",
            "quantity" : "BOOLEAN"
        },
        "OUT9" : {
            "command" : "GP_OUT9",
            "quantity" : "BOOLEAN"
        },
        "OUT10" : {
            "command" : "GP_OUT10",
            "quantity" : "BOOLEAN"
        },
        "OUT11" : {
            "command" : "GP_OUT11",
            "quantity" : "BOOLEAN"
        },
        "OUT12" : {
            "command" : "GP_OUT12",
            "quantity" : "BOOLEAN"
        },
        "OUT13" : {
            "command" : "GP_OUT13",
            "quantity" : "BOOLEAN"
        },
        "OUT14" : {
            "command" : "GP_OUT14",
            "quantity" : "BOOLEAN"
        },
        "OUT15" : {
            "command" : "GP_OUT15",
            "quantity" : "BOOLEAN"
        },
        "OUT16" : {
            "command" : "GP_OUT16",
            "quantity" : "BOOLEAN"
        },
        "OUT17" : {
            "command" : "GP_OUT17",
            "quantity" : "BOOLEAN"
        },
        "OUT18" : {
            "command" : "GP_OUT18",
            "quantity" : "BOOLEAN"
        },
        "OUT19" : {
            "command" : "GP_OUT19",
            "quantity" : "BOOLEAN"
        },
        "OUT20" : {
            "command" : "GP_OUT20",
            "quantity" : "BOOLEAN"
        },
    }

    def __init__(self, bus, address, *args, **kwargs):
        super(ISPPAC, self).__init__(bus, address, *args, **kwargs)

    def probe(self):
        # TODO: We could read the USE here
        return True

    def configure(self, extra_cfg):
        # Apply any additional configuration
        for name, value in extra_cfg.items():
            self.write(name, value)

    def is_monitor_enabled(self, name):
        return True

    def enable_monitor(self, name):
        pass 

    def disable_monitor(self, name):
        pass 

    def set_threshold(self, M, cmp_type, threshold):
        pass

    def get_threshold(self, M, cmp_type):
        return None

    def clear_faults(self):
        pass
