from .. import formats as F
from .. import pmbus_formats as PF
from .. import pmbus_commands as PC
from .. import i2cbus as I2C
from .. import smbus as SMB
from .. import pmbus as PMB
from .. import variables as V
from .. import controller as C

sec_level_table = {
    0x0 : frozenset(),
    0x1 : frozenset(["USER_CONF"]),
    0x2 : frozenset(["USER_CONF", "MANUF_CONF"]),
}

known_ids = set([
    "MAX15301",
    "MAX15301AA02",
])

class MAX15301(PMB.PMBus_Polled_Device):
    """A Maxim MAX15301 InTune Automatically Compensated Digital PoL
    Controller with Driver and PMBus Telemetry.

    References are to the MAX15301 datasheet revision 4 (document number
    19 6267, November 2013) and MAX15301 PMBus Command Set User's Guide
    revision 1 (October 2014).

    Limitations: The MAX15301 does not respond to the ARA."""

    _supported_commands = [
        "OPERATION", "ON_OFF_CONFIG", "CLEAR_FAULTS", "WRITE_PROTECT",
        "STORE_DEFAULT_ALL", "RESTORE_DEFAULT_ALL", "STORE_USER_ALL",
        "RESTORE_USER_ALL", "CAPABILITY", "VOUT_MODE", "VOUT_COMMAND",
        "VOUT_TRIM", "VOUT_CAL_OFFSET", "VOUT_MAX", "VOUT_MARGIN_HIGH",
        "VOUT_MARGIN_LOW", "VOUT_TRANSITION_RATE", "VOUT_DROOP",
        "FREQUENCY_SWITCH", "VIN_ON", "VIN_OFF", "INTERLEAVE", "IOUT_CAL_GAIN",
        "IOUT_CAL_OFFSET", "VOUT_OV_FAULT_LIMIT", "VOUT_OV_FAULT_RESPONSE",
        "VOUT_UV_FAULT_LIMIT", "VOUT_UV_FAULT_RESPONSE", "IOUT_OC_FAULT_LIMIT",
        "IOUT_OC_FAULT_RESPONSE", "OT_FAULT_LIMIT", "OT_FAULT_RESPONSE",
        "OT_WARN_LIMIT", "VIN_OV_FAULT_LIMIT", "VIN_OV_FAULT_RESPONSE",
        "VIN_UV_FAULT_LIMIT", "VIN_UV_FAULT_RESPONSE", "POWER_GOOD_ON",
        "POWER_GOOD_OFF", "TON_DELAY", "TON_RISE", "TOFF_DELAY", "TOFF_FALL",
        "STATUS_BYTE", "STATUS_WORD", "STATUS_VOUT", "STATUS_IOUT",
        "STATUS_INPUT", "STATUS_TEMPERATURE", "STATUS_CML", "READ_VIN",
        "READ_VOUT", "READ_IOUT", "READ_TEMPERATURE_1", "READ_TEMPERATURE_2",
        "READ_DUTY_CYCLE", "READ_FREQUENCY", "PMBUS_REVISION", "MFR_ID",
        "MFR_MODEL", "MFR_REVISION", "MFR_LOCATION", "MFR_DATE", "MFR_SERIAL",
        "IC_DEVICE_ID", "IC_DEVICE_REV"
    ]

    _command_overrides = {
        "VOUT_TRANSITION_RATE" : { "fmt" : PF.Linear11(), },
        "VOUT_DROOP"           : { "fmt" : PF.Linear11(), },
        "VIN_ON"               : { "fmt" : PF.Linear11(), },
        "VIN_OFF"              : { "fmt" : PF.Linear11(), },
        "FREQUENCY_SWITCH"     : { "fmt" : PF.Linear11(), },
        "TON_DELAY"            : { "fmt" : PF.Linear11(), },
        "TON_RISE"             : { "fmt" : PF.Linear11(), },
        "TOFF_DELAY"           : { "fmt" : PF.Linear11(), },
        "TOFF_FALL"            : { "fmt" : PF.Linear11(), },
        "IOUT_OC_FAULT_LIMIT"  : { "fmt" : PF.Linear11(), },
        "OT_FAULT_LIMIT"       : { "fmt" : PF.Linear11(), },
        "OT_WARN_LIMIT"        : { "fmt" : PF.Linear11(), },
        "OT_WARN_LIMIT"        : { "fmt" : PF.Linear11(), },
        "VIN_OV_FAULT_LIMIT"   : { "fmt" : PF.Linear11(), },
        "VIN_UV_FAULT_LIMIT"   : { "fmt" : PF.Linear11(), },
        "READ_VIN"             : { "fmt" : PF.Linear11(), },
        "READ_IOUT"            : { "fmt" : PF.Linear11(), },
        "READ_TEMPERATURE_1"   : { "fmt" : PF.Linear11(), },
        "READ_TEMPERATURE_2"   : { "fmt" : PF.Linear11(), },
        "READ_DUTY_CYCLE"      : { "fmt" : PF.Linear11(), },
        "READ_FREQUENCY"       : { "fmt" : PF.Linear11(), },
        "IOUT_CAL_GAIN"        : { "fmt" : PF.Linear11(), },
        "IOUT_CAL_OFFSET"      : { "fmt" : PF.Linear11(), },
        "STATUS_BYTE"          : { "writeable" : False},
        "STATUS_WORD"          : { "writeable" : False},
        "STATUS_VOUT"          : { "writeable" : False},
        "STATUS_IOUT"          : { "writeable" : False},
        "STATUS_INPUT"         : { "writeable" : False},
        "STATUS_TEMPERATURE"   : { "writeable" : False},
        "STATUS_CML"           : { "writeable" : False},
    }

    _mfr_commands= [
        PC.PRM(0xD0, "ADAPTIVE_MODE",
            F.Bitfield(
               "p3b1p2b1b1p1b1b1b1b1b1b1b1",
               ["FIRST_ENABLE_ONLY", "ADAPT_POST_RAMP", "ADAPT_CONTINUOUS",
                "UPDATE_FLC", "UPDATE_FZ", "UPDATE_ZLC", "RESET_GAINS",
                "WRITEFLASH", "GAIN_CALC", "FAST_GAINS"]
            ),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xD3, "FEEDBACK_EFFORT",
            PF.Linear11(),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xD5, "LOOP_CONFIG",
            F.Bitfield(
                "p6b1b1p1b1p1b1b1p3",
                ["NOGAINCALC", "PIDMODE", "GCTRLTABLEEN", "AGDEN", "NEGDUTYEN"]
            ),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xDB, "COMP_MODEL",
            F.Bitfield(
                "r16r16r16",
                ["FLC_FSW", "FZ_FSW", "ZLC"], {
                    "FLC_FSW"      : PF.Linear11(),
                    "FZ_FSW"       : PF.Linear11(),
                    "ZLC"          : PF.Linear11(),
                }
            ),
            block=True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xE0, "MANUF_CONF",
            F.Flags("b1" * 256, range(256)),
            block=True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xE1, "MANUF_LOCK",
            F.Raw(16),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xE2, "MANUF_PASSWD",
            F.Raw(16)
        ),
        PC.PRM(0xE3, "USER_CONF",
            F.Flags("b1" * 256, range(256)),
            block=True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xE4, "USER_LOCK",
            F.Raw(16),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xE5, "USER_PASSWD",
            F.Raw(16)
        ),
        PC.PRM(0xE6, "SECURITY_LEVEL",
            F.Tabular("p6u2", sec_level_table),
            writeable=False,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xE7, "DEADTIME_GCTRL",
            F.Bitfield(
                "s16s16s16s16u8u8u8u8u8u8u8u8u8u16",
                ["FIXEDDTR", "FIXEDDTF", "TDR", "TDF", "LXDAC", "GCTRL0",
                 "GCTRL1", "GCTRL2", "GCTRL3", "GCTRL4", "GCTRL5", "GCTRL6",
                 "GCTRL7", "GCTRLDAC"]
            ),
            block=True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xE8, "ZETAP",
            PF.Linear11(),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.CMD(0xEA, "RESTORE_MAXIM_ALL",
            PC.RPC()
        ),
        PC.PRM(0xF8, "EXT_TEMP_CAL",
            F.Bitfield(
                "r16r16",
                ["m", "b"], {
                    "m"            : PF.Linear11(),
                    "b"            : PF.Linear11(),
                }
            ),
            block=True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
    ]

    _monitor_aliases= {
        "T_INTERNAL" : "TEMPERATURE_1",
        "T_EXTERNAL" : "TEMPERATURE_2",
    }

    def __init__(self, *args, **kwargs):
        super(MAX15301, self).__init__(*args, **kwargs)

    def probe(self):
        try:
            dev_id= self.read("IC_DEVICE_ID").decode()
            if dev_id in known_ids:
                return True
            else:
                self.dev_logger.warning("Unrecognised IC_DEVICE_ID: \"%s\"",
                                     dev_id)
                return False

        except I2C.I2CBus_Exception as e:
            self.dev_logger.warning("Failed to read IC_DEVICE_ID.")
            raise e

    def configure(self, extra_cfg):
        """Apply default configuration."""

        self.clear_faults()

        # Apply any additional configuration
        for name, value in extra_cfg.items():
            self.write(name, value)

    # The MAX15301 has no alert mask.
    def read_alert_mask(self):
        return frozenset()

    def enable_alert(self, M, cmp_type):
        pass

    def disable_alert(self, M, cmp_type):
        raise V.MonitorError("Alerts cannot be disabled on this device.")

    def disable_all_alerts(self, M, cmp_type):
        raise V.MonitorError("Alerts cannot be disabled on this device.")
