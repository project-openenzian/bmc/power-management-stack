"""TI INA226 High-Side or Low-Side Measurement, Bidirectional Current and
Power Monitor with I2C-Compatible Interface"""

import math

from .. import formats as F
from .. import i2cbus as I2C
from .. import smbus as SMB
from .. import variables as V
from .. import controller as C

# Number of averages
cfg_AVG_table = {
    0 :    1,
    1 :    4,
    2 :   16,
    3 :   64,
    4 :  128,
    5 :  256,
    6 :  512,
    7 : 1024
}

# Bus/Shunt voltage conversion time
cfg_CT_table = {
    0 : 140.0e-6,
    1 : 204.0e-6,
    2 : 332.0e-6,
    3 : 588.0e-6,
    4 : 1.100e-3,
    5 : 2.116e-3,
    6 : 4.156e-3,
    7 : 8.244e-4
}

def compute_I_lsb(**kwargs):
    coeff= kwargs["coeff"]
    try:
        cal= coeff["CALIBRATION"]
        R_shunt= coeff["R_SHUNT"]
    except KeyError as e:
        raise F.ParameterRequest(e.args[0])

    if R_shunt is None: raise F.FormatException("R_shunt not set.")

    if cal == 0:
        raise F.FormatException("Calibration value is 0, cannot compute I_lsb")

    # 0.00512 = 2.5e-6 * 2048
    return 0.00512 / (cal * R_shunt)

def compute_P_lsb(**kwargs):
    coeff= kwargs["coeff"]
    try:
        I_lsb= coeff["I_LSB"]
    except KeyError as e:
        raise F.ParameterRequest(e.args[0])

    # Per S7.6.4, P_lsb (in W) = 25 * I_lsb (in A)
    return 25 * I_lsb

fmt_shunt_voltage= F.SignedScaled(16, 2.50e-6) # 2.5uV LSB
fmt_bus_voltage=   F.SignedScaled(16, 1.25e-3) # 1.25mV LSB
fmt_current=       F.SignedScaled(16, "I_LSB") # Determined by CALIBRATION
fmt_power=         F.SignedScaled(16, "P_LSB") # Determined by CALIBRATION

class INA226(SMB.SMBus_Device,I2C.I2CBus_RegisterDevice,C.Controller):
    _reg_width= 2 # Bytes
    _LSB_first= False

    _parameters = [
        I2C.Register(0x00, "CONFIGURATION",
            F.Bitfield(
                "b1p3r3r3r3b1b1b1",
                ["RST", "AVG", "VBUSCT", "VSHCT",
                 "MODE_CTS", "MODE_BUS", "MODE_SHNT"], {
                    "AVG"    : F.Tabular("u3", cfg_AVG_table),
                    "VBUSCT" : F.Tabular("u3", cfg_CT_table),
                    "VSHCT"  : F.Tabular("u3", cfg_CT_table),
                }
            )
        ),
        I2C.Register(0x01, "SHUNT_VOLTAGE",
            fmt_shunt_voltage,
            writeable=False,
            volatile=True
        ),
        I2C.Register(0x02, "BUS_VOLTAGE",
            fmt_bus_voltage,
            writeable=False,
            volatile=True
        ),
        I2C.Register(0x03, "POWER",
            fmt_power,
            writeable=False,
            volatile=True
        ),
        I2C.Register(0x04, "CURRENT",
            fmt_current,
            writeable=False,
            volatile=True
        ),
        I2C.Register(0x05, "CALIBRATION",
            F.Signed(16)
        ),
        I2C.Register(0x06, "MASK",
            F.Flags(
                "b1b1b1b1b1b1p5b1b1b1b1b1",
                ["SOL", "SUL", "BOL", "BUL", "POL", "CNVR",
                 "AFF", "CVRF", "OVF", "APOL", "LEN"]
            ),
            volatile=True
        ),
        I2C.Register(0x07, "ALERT_LIMIT",
            F.Alternate(["ALERT_MONITOR"], {
                "VOLTAGE" : fmt_bus_voltage,
                "CURRENT" : fmt_current,
                "POWER"   : fmt_power,
                "NONE"    : F.Raw(16),
            })
        ),
        I2C.Register(0xFE, "MFR_ID",
            F.Raw(),
            writeable=False
        ),
        I2C.Register(0xFF, "DIE_ID",
            F.Raw(),
            writeable=False
        ),
        I2C.Runtime("R_SHUNT"),
        I2C.Computed("I_LSB",
            compute_I_lsb,
            ["CALIBRATION", "R_SHUNT"]
        ),
        I2C.Computed("P_LSB",
            compute_P_lsb,
            ["I_LSB"]
        ),
        I2C.Runtime("ALERT_MONITOR", default="NONE"),
    ]

    _monitor_specs = {
        "VOLTAGE" : {
            "key" : "BUS_VOLTAGE",
            "quantity" : "VOLTAGE",
            "comparators" : {
                "WARN_HIGH" : "BOL",
                "WARN_LOW" : "BUL",
            }
        },
        "POWER" : {
            "key" : "POWER",
            "quantity" : "POWER",
            "comparators" : {
                "WARN_HIGH" : "POL",
            }
        },
        "CURRENT" : {
            "key" : "CURRENT",
            "quantity" : "CURRENT",
            "comparators" : {
                "WARN_HIGH" : "SOL",
                "WARN_LOW" : "SUL",
            }
        },
    }

    def __init__(self, bus, address, R_shunt=None, target_I_lsb=None,
                 **kwargs):
        super(INA226, self).__init__(bus, address, **kwargs)

        # If R_shunt is not specified, only voltages can be properly
        # interpreted.
        if not R_shunt is None:
            self.write("R_SHUNT", R_shunt)

            if target_I_lsb == None:
                # If R_shunt is specified, but no target resolution given,
                # load the current device calibration.
                self.load_calibration()
            else:
                # Otherwise, use the target LSB to calibrate the device.
                self.calibrate(target_I_lsb)

    def probe(self):
        try:
            mfr_id= self.read("MFR_ID").decode()
            die_id= self.read("DIE_ID")
            if mfr_id == "TI" and list(die_id) == [0x22, 0x60]:
                return True
            else:
                self.dev_logger.warning(
                    "Unrecognised MFR_ID.DIE_ID: %s.%s", mfr_id, die_id)
                return False

        except I2C.I2CBus_Exception as e:
            self.dev_logger.warning("Failed to read MFR_ID & DIE_ID.")
            raise e

    def is_calibrated(self):
        return (not self.read("R_SHUNT") is None) and \
               self.read("CALIBRATION") != 0

    def calibrate(self, target_I_lsb, R_shunt=None):
        """Calculate the required calibration register value, given the
        desired current resolution (I_lsb) and the known shunt resistance
        using equation 1 (S7.5) of the datasheet, and write it to the device.

        The INA226 calculates the value in the current register as
        (ShuntVoltage * Calibration) / 2048 (equation 3).  A shunt register
        value of 1 (2.5uV) represents a current of (2.5uV / R_shunt) e.g.
        2.5mA for R_shunt = 1mOhm.  A calibration value of 2048 would thus
        give a current register value of 1 (one LSB) for 2.5mA, and 5120 would
        give an LSB of 1mA.
        """

        if not R_shunt is None: self.write("R_SHUNT", R_shunt)

        # 0.00512 = 2.5e-6 * 2048
        cal= int(round(0.00512 / (target_I_lsb * R_shunt)))
        true_I_lsb= 0.00512 / (cal * R_shunt)

        if cal < 1 or cal > 32767:
            raise I2C.I2CBus_ConfigError(
                "target_I_lsb out of range [%.3eA,%.3eA]" %
                (0.00512 / (32767 * R_shunt), 0.00512 / (1 * R_shunt))
            )

        if true_I_lsb != target_I_lsb:
            self.dev_logger.warning("Target I_lsb of %.12eA rounded to %.12eA",
                target_I_lsb, true_I_lsb)

        # Write the device register
        self.write("CALIBRATION", cal)

    def configure(self, extra_cfg):
        """Reconfigure the device, including optional parameters."""

        # Disable all alerts, alert is active-low, enable latch (alert remains
        # asserted until read).
        self.write("MASK", set(["LEN"]))

        # Default to no averaging, 1.1ms conversion time, continuous
        # conversion of both voltage and current.
        cfg= {
            "RST" : False,
            "AVG" : 1,
            "VBUSCT" : 1.1e-3,
            "VSHCT" : 1.1e-3,
            "MODE_CTS" : True,
            "MODE_BUS" : True,
            "MODE_SHNT" : True,
        }
        # Allow the caller to override conversion time and averaging.
        if "AVG" in extra_cfg: cfg["AVG"]= extra_cfg["AVG"]
        if "VBUSCT" in extra_cfg: cfg["VBUSCT"]= extra_cfg["VBUSCT"]
        if "VSHCT" in extra_cfg: cfg["VSHCT"]= extra_cfg["VSHCT"]
        self.write("CONFIGURATION", cfg)

        self.calibrate(extra_cfg["TARGET_I_LSB"], extra_cfg["R_SHUNT"])

    @staticmethod
    def _ina226_sample_interval(cfg):
        """Calculate the current sampling interval (same for VBUS and
        VSHUNT)."""
        bus_time= cfg["VBUSCT"]
        shunt_time= cfg["VSHCT"]
        averages= cfg["AVG"]
        return (bus_time + shunt_time) * averages

    def sample_interval(self, name, cfg=None):
        if cfg is None:
            cfg= self.read("CONFIGURATION")
        return INA226._ina226_sample_interval(cfg)

    def is_monitor_enabled(self, M):
        cfg= self.read("CONFIGURATION")
        if M.name == "VOLTAGE":
            return cfg["MODE_BUS"] and cfg["MODE_CTS"]
        elif M.name == "CURRENT":
            return cfg["MODE_SHNT"] and cfg["MODE_CTS"] and \
                   self.is_calibrated()
        elif M.name == "POWER":
            return cfg["MODE_BUS"] and cfg["MODE_SHNT"] and \
                   cfg["MODE_CTS"] and self.is_calibrated()
        else:
            raise V.MonitorError("Unknown monitor: %s" % M.name)

    def enable_monitor(self, M):
        cfg= self.read("CONFIGURATION")
        if M.name == "VOLTAGE":
            cfg["MODE_BUS"]= True
            cfg["MODE_CTS"]= True
        elif M.name == "CURRENT":
            cfg["MODE_SHNT"]= True
            cfg["MODE_CTS"]= True
        elif M.name == "POWER":
            cfg["MODE_BUS"]= True
            cfg["MODE_SHNT"]= True
            cfg["MODE_CTS"]= True
        else:
            raise V.MonitorError("Unknown monitor: %s" % M.name)
        self.write("CONFIGURATION", cfg)

    def disable_monitor(self, M):
        cfg= self.read("CONFIGURATION")
        if M.name == "VOLTAGE":
            cfg["MODE_BUS"]= False
        elif M.name == "CURRENT":
            cfg["MODE_SHNT"]= False
        elif M.name == "POWER":
            # Leave V & I monitors running.
            pass
        else:
            raise V.MonitorError("Unknown monitor: %s" % M.name)
        self.write("CONFIGURATION", cfg)

    def set_threshold(self, M, cmp_type, threshold):
        current_alert_monitor= self.read("ALERT_MONITOR")

        # There's only one actual comparator, so ensure that no other alerts
        # are enabled before updating the threshold.
        alerts= self.read_alert_mask()
        for (v_name, c_typ) in alerts:
            if v_name != M.name or c_typ != cmp_type:
                self.dev_logger.error("Setting threshold for %s.%s would " \
                                      "affect enabled alert %s.%s",
                                      M.name, cmp_type, v_name, c_typ)
                raise V.MonitorIncompatible()

            if v_name != current_alert_monitor:
                self.dev_logger.error("Config error: alert %s.%s enabled"
                                      " while ALERT_MONITOR=%s",
                                      v_name, c_typ, current_alert_monitor)
                raise V.MonitorIncompatible()

        if (M.name == "CURRENT" or M.name == "POWER") and \
           not self.is_calibrated():
            self.dev_logger.error(
                "Cannot set %s threshold without calibration", M.name)
            return V.MonitorError("Not calibrated.")

        # Record which monitor is driving the alert (i.e. which format
        # ALERT_LIMIT is written with).  We've checked that no alerts are
        # enabled if this differs from current_alert_monitor.
        self.write("ALERT_MONITOR", M.name)

        # Write it.
        self.write("ALERT_LIMIT", threshold)

    def get_threshold(self, M, cmp_type):
        # Only return a threshold encoded for this monitor.
        current_alert_monitor= self.read("ALERT_MONITOR")
        if M.name != current_alert_monitor:
            self.dev_logger.error("Current value of ALERT_LIMIT was"
                                  " written for monitor %s (tried to read"
                                  " as %s)", current_alert_monitor, M.name)
            raise V.MonitorIncompatible()

        if (M.name == "CURRENT" or M.name == "POWER") and \
           not self.is_calibrated():
            self.dev_logger.error("Cannot interpret %s threshold "
                                  "without calibration", M.name)
            return V.MonitorError("Not calibrated.")

        return self.read("ALERT_LIMIT")

    def read_alert_mask(self, mask=None):
        if mask is None: mask= self.read("MASK")
        alerts= []
        if "SOL" in mask: alerts.append(("CURRENT", "WARN_HIGH"))
        if "SUL" in mask: alerts.append(("CURRENT", "WARN_LOW"))
        if "BOL" in mask: alerts.append(("VOLTAGE", "WARN_HIGH"))
        if "BUL" in mask: alerts.append(("VOLTAGE", "WARN_LOW"))
        if "POL" in mask: alerts.append(("POWER", "WARN_HIGH"))
        return alerts

    def enable_alert(self, M, cmp_type):
        alerts= self.read_alert_mask()
        if (M.name, cmp_type) in alerts:
            # Log and ignore an already-enabled alert.
            self.dev_logger.warning("Alert %s.%s already enabled, ignoring.",
                                    M.name, cmp_type)
            return
        elif len(alerts) > 0:
            # Only one alert can be enabled at a time.
            self.dev_logger.warning("Cannot enable %s.%s.  "
                                    "Already enabled: %s.",
                                    M.name, cmp_type, alerts)
            raise V.MonitorError("Cannot enable two alerts at once.")

        mask= self.read("MASK")
        if M.name == "CURRENT" and cmp_type == "WARN_LOW":
            mask.add("SUL")
        elif M.name == "CURRENT" and cmp_type == "WARN_HIGH":
            mask.add("SOL")
        elif M.name == "VOLTAGE" and cmp_type == "WARN_LOW":
            mask.add("BUL")
        elif M.name == "VOLTAGE" and cmp_type == "WARN_HIGH":
            mask.add("BOL")
        elif M.name == "POWER" and cmp_type == "WARN_HIGH":
            mask.add("POL")
        else:
            assert False, "Unknown alert type."
        mask.discard("CNVR")
        self.write("MASK", mask)

    def disable_alert(self, M, cmp_type):
        alerts= self.read_alert_mask()
        if not (M.name, cmp_type) in alerts:
            # Log and ignore an already-disabled alert.
            self.dev_logger.warning("Alert %s.%s already disabled, ignoring.",
                                    M.name, cmp_type)
            return

        mask= self.read("MASK")
        if M.name == "CURRENT" and cmp_type == "WARN_LOW":
            mask.discard("SUL")
        elif M.name == "CURRENT" and cmp_type == "WARN_HIGH":
            mask.discard("SOL")
        if M.name == "VOLTAGE" and cmp_type == "WARN_LOW":
            mask.discard("BUL")
        elif M.name == "VOLTAGE" and cmp_type == "WARN_HIGH":
            mask.discard("BOL")
        elif M.name == "POWER" and cmp_type == "WARN_HIGH":
            mask.discard("POL")
        else:
            assert False, "Unknown alert type."
        mask.discard("CNVR")
        self.write("MASK", mask)

    def disable_all_alerts(self):
        mask= self.read("MASK")
        mask.difference_update(["SUL", "SOL", "BUL", "BOL", "POL", "CNVR"])
        self.write("MASK", mask)

    def query_alert(self):
        mask= self.read("MASK")
        if "AFF" in mask:
            # The Alert Function Flag signals that an alert is active.
            alerts= self.read_alert_mask(mask=mask)
            # The alert bits are priority-encoded, so the first enabled one
            # should be the culprit.
            return alerts[:1]
        else:
            return None

    def clear_faults(self):
        pass
