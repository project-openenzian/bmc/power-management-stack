"""Infineon (IR) IR3581 (Baxter) PMIC

6-Phase buck controller, intended for use as a CPU/GPU regulator.
"""

from .. import formats as F
from .. import pmbus_formats as PF
from .. import i2cbus as I2C
from .. import smbus as SMB
from .. import pmbus_commands as PC
from .. import pmbus as PMB
from .. import variables as V
from .. import controller as C

class IR3581_Loop(PMB.PMBus_Device):
    _supported_commands= [
        "OPERATION", "ON_OFF_CONFIG", "CLEAR_FAULTS", "WRITE_PROTECT",
        "RESTORE_DEFAULT_ALL", "CAPABILITY", "SMBALERT_MASK", "VOUT_MODE",
        "VOUT_COMMAND", "VOUT_TRIM", "VOUT_MAX", "VOUT_MARGIN_HIGH",
        "VOUT_MARGIN_LOW", "VOUT_TRANSITION_RATE", "VOUT_DROOP",
        "VOUT_SCALE_LOOP", "FREQUENCY_SWITCH", "VIN_ON", "VIN_OFF",
        "IOUT_CAL_OFFSET", "VOUT_OV_FAULT_LIMIT", "VOUT_OV_FAULT_RESPONSE",
        "VOUT_OV_WARN_LIMIT", "VOUT_UV_WARN_LIMIT", "VOUT_UV_FAULT_LIMIT",
        "VOUT_UV_FAULT_RESPONSE", "IOUT_OC_FAULT_LIMIT",
        "IOUT_OC_FAULT_RESPONSE", "IOUT_OC_WARN_LIMIT", "OT_FAULT_LIMIT",
        "OT_FAULT_RESPONSE", "OT_WARN_LIMIT", "VIN_OV_FAULT_LIMIT",
        "VIN_OV_FAULT_RESPONSE", "VIN_UV_WARN_LIMIT", "IIN_OC_WARN_LIMIT",
        "POWER_GOOD_ON", "POWER_GOOD_OFF", "TON_DELAY", "TON_RISE",
        "TON_MAX_FAULT_LIMIT", "TON_MAX_FAULT_LIMIT", "TOFF_DELAY",
        "TOFF_FALL", "STATUS_BYTE", "STATUS_WORD", "STATUS_VOUT",
        "STATUS_IOUT", "STATUS_INPUT", "STATUS_TEMPERATURE", "STATUS_CML",
        "STATUS_MFR_SPECIFIC", "READ_VIN", "READ_IIN", "READ_VOUT",
        "READ_IOUT", "READ_TEMPERATURE_1", "READ_TEMPERATURE_2",
        "READ_DUTY_CYCLE", "READ_POUT", "READ_PIN", "PMBUS_REVISION", "MFR_ID",
        "MFR_MODEL", "MFR_REVISION", "MFR_DATE", "IC_DEVICE_ID",
        "IC_DEVICE_REV",
    ]

    _mfr_commands = [
        # Get/Set I2C registers
        PC.CMD(0xD0, "MFR_READ_REG",
            None,
            PC.RPC(F.Unsigned(8), F.Unsigned(8))
        ),
        PC.CMD(0xD1, "MFR_WRITE_REG",
            PC.RPC(F.Bitfield("u8u8", ["ADDRESS", "VALUE"]))
        ),
        # Get/Set I2C interface address
        PC.PRM(0xD6, "MFR_I2C_ADDRESS",
            F.Unsigned(8)
        )
    ]

    _command_overrides= {
        "VOUT_TRANSITION_RATE" : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp=-4),
        },
        "VOUT_DROOP"          : {
            "fmt" : PF.Linear11(max_exp=-7, min_exp=-7),
        },
        "VOUT_SCALE_LOOP"     : {
            "fmt" : PF.Linear11(max_exp=-3, min_exp=-3),
            "writeable" : False,
        },
        "FREQUENCY_SWITCH"     : {
            "fmt" : PF.Linear11(max_exp= 1, min_exp= 0),
        },
        "VIN_ON"              : {
            "fmt" : PF.Linear11(max_exp=-1, min_exp=-1),
        },
        "VIN_OFF"             : {
            "fmt" : PF.Linear11(max_exp=-1, min_exp=-1),
        },
        "IOUT_CAL_OFFSET"     : {
            "fmt" : PF.Linear11(max_exp=-2, min_exp=-2),
        },
        "IOUT_OC_FAULT_LIMIT" : {
            "fmt" : PF.Linear11(max_exp=-1, min_exp=-1),
        },
        "IOUT_OC_WARN_LIMIT"  : {
            "fmt" : PF.Linear11(max_exp=-1, min_exp=-1),
        },
        "OT_FAULT_LIMIT"      : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "OT_WARN_LIMIT"       : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "VIN_OV_FAULT_LIMIT"  : {
            "fmt" : PF.Linear11(max_exp=-4, min_exp=-4),
        },
        "VIN_UV_WARN_LIMIT"   : {
            "fmt" : PF.Linear11(max_exp=-4, min_exp=-4),
        },
        "IIN_OC_WARN_LIMIT"   : {
            "fmt" : PF.Linear11(max_exp=-1, min_exp=-1),
        },
        "TON_DELAY"           : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "TON_RISE"            : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "TON_MAX_FAULT_LIMIT" : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "TOFF_DELAY"          : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "TOFF_FALL"           : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "READ_VIN"            : {
            "fmt" : PF.Linear11(),
        },
        "READ_IIN"            : {
            "fmt" : PF.Linear11(),
        },
        "READ_IOUT"           : {
            "fmt" : PF.Linear11(),
        },
        "READ_TEMPERATURE_1"  : {
            "fmt" : PF.Linear11(),
        },
        "READ_TEMPERATURE_2"  : {
            "fmt" : PF.Linear11(),
        },
        "READ_DUTY_CYCLE"     : {
            "fmt" : PF.Direct(),
        },
        "READ_POUT"           : {
            "fmt" : PF.Linear11(),
        },
        "READ_PIN"            : {
            "fmt" : PF.Linear11(),
        },
        "VOUT_OV_FAULT_LIMIT" : {
            "writeable" : False,
        },
        "VOUT_UV_FAULT_LIMIT" : {
            "writeable" : False,
        },
        "STATUS_BYTE"         : {
            "writeable" : False,
        },
        "STATUS_WORD"         : {
            "writeable" : False,
        },
        "STATUS_VOUT"         : {
            "writeable" : False,
        },
        "STATUS_IOUT"         : {
            "writeable" : False,
        },
        "STATUS_INPUT"        : {
            "writeable" : False,
        },
        "STATUS_TEMPERATURE"  : {
            "writeable" : False,
        },
        "STATUS_CML"          : {
            "writeable" : False,
        },
        "STATUS_MFR_SPECIFIC" : {
            "fmt" : F.Flags(
                        "p5b1b1b1",
                        ["DRIVER_FAULT", "UNPOPULATED_PHASE", "EXTERNAL_OT"]
                    ),
            "writeable" : False,
        }
    }

    _fixed_values = {
        "COEFFICIENTS" : {
            "READ_DUTY_CYCLE" : {True : { "m" : 1, "b" : 0, "R" : 0 }},
        }
    }

    _monitor_aliases= {
        "T_POWERSTAGE" : "TEMPERATURE_1",
    }

    def __init__(self, bus, address):
        super(IR3581_Loop, self).__init__(bus, address)

    def probe(self):
        try:
            mfr_id= self.read("MFR_ID").decode()
            if mfr_id != "IR":
                self.dev_logger.warning("Unrecognised MFR_ID: \"%s\"", mfr_id)
                return False

            dev_id= self.read("MFR_MODEL")
            if len(dev_id) == 0 or dev_id[0] != 0x48:
                self.dev_logger.warning("Unrecognised MFR_MODEL")
                return False

            return True
        except I2C.I2CBus_Exception:
            self.dev_logger.warning("Failed to read MFR_ID & MFR_MODEL.")
            return False

    def configure(self, extra_cfg):
        self.clear_faults()

        # Apply any additional configuration
        for name, value in extra_cfg.items():
            self.write(name, value)

loop_X_rll_table= {
     0 : -1.00,
     1 : -0.85,
     2 : -0.75,
     3 : -0.50,
     4 : -0.40,
     5 : -0.35,
     6 : -0.30,
     7 : -0.25,
     8 : -0.20,
     9 : -0.15,
    10 : -0.10,
    11 : -0.05,
    12 :  0.00,
    13 :  0.05,
    14 :  0.10,
    15 :  0.15,
    16 :  0.20,
    17 :  0.25,
    18 :  0.30,
    19 :  0.35,
    20 :  0.40,
    21 :  0.45,
    22 :  0.50,
    23 :  0.55,
    24 :  0.60,
    25 :  0.65,
    26 :  0.70,
    27 :  0.75,
    28 :  0.80,
    29 :  0.80,
    30 :  0.80,
    31 :  0.80,
}

class IR3581(I2C.I2CBus_RegisterDevice):
    _reg_width= 1

    _parameters = [
        # 0x00-0x1D are manufacturer trim registers and not reprogrammable.
        # 0x00 .. 0x1F

        # 0x20-0x8C are user programmable registers
        # XXX - not listed in tool, no description in UN0026
        I2C.Field("low_power_on_disable",
                  [(0x20, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "MISSING",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("svid_glitch_eater_del",
                  [(0x20, 6, 5)],
                  F.Tabular("u2", {
                    0 : 1.2e-9,
                    1 : 2.4e-9,
                    2 : 3.3e-9,
                    3 : 5.0e-9,
                  }),
                  tags= {
                    "desc" : "The maximum glitch pulse width that will be"
                             " blocked for both SVID clock and data."
                             " 00-1.2ns. 01-2.4ns. 10-3.3ns. 11-5ns.",
                    "section" : "Noise Rejection Utilities",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("svid_del_skew",
                  [(0x20, 4, 3)],
                  F.Tabular("u2", {
                    0 : 0.56-9,
                    1 : 1.8e-9,
                    2 : 2.7e-9,
                    3 : 4.5e-9,
                  }),
                  tags= {
                    "desc" : "Extra delay added to the SVID clock line"
                             " before it reaches the core. 00-0.56ns."
                             " 01-1.8ns. 10-2.7ns. 11-4.5ns.",
                    "section" : "Noise Rejection Utilities",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("svid_filter_corner",
                  [(0x20, 2, 1)],
                  F.Tabular("u2", {
                    0 : 0.1e-9,
                    1 : 1.4e-9,
                    2 : 2.44e-9,
                    3 : 4.5e-9,
                  }),
                  tags= {
                    "desc" : "Time constant of the RC filter on both the"
                             " SVID clock and data signals. 00 -time"
                             " constant=0.1ns. 01- time constant=1.4ns."
                             " 10-time constant=2.44ns. 11-time"
                             " constant=4.5ns.",
                    "section" : "Noise Rejection Utilities",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("svid_tco_delay_mode",
                  [(0x20, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable adding the glitch-eater delay to the"
                             " SVID data time to clock out (TCO). 0-data"
                             " is clocked out on the pre-glitch-eater-clock."
                             " 1-data is clocked out on the"
                             " post-glitcher-eater-clock.",
                    "section" : "Noise Rejection Utilities",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists this as current_limiting_en
        I2C.Field("add_delay_to_i2c",
                  [(0x21, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "If set to 1, the transmit side will have a"
                             " hold time of 300ns/30ns minimum.",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("i2c_address",
                  [(0x21, 6, 0)],
                  F.Unsigned(7),
                  tags= {
                    "desc" : "The chip I2C address. In test mode, the"
                             " chip also accepts a default value of 0x0A."
                             " locked by the I2C address lock.",
                    "section" : "I2C/PMBus/SVID Address",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_svid_local_addr",
                  [(0x22, 7, 4)],
                  F.Unsigned(4),
                  tags= {
                    "desc" : "Sets the SVID Address for Loop 2.",
                    "section" : "I2C/PMBus/SVID Address",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_svid_local_addr",
                  [(0x22, 3, 0)],
                  F.Unsigned(4),
                  tags= {
                    "desc" : "Sets the SVID Address for Loop 1.",
                    "section" : "I2C/PMBus/SVID Address",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("chip_enable_polarity",
                  [(0x23, 7, 7)],
                  F.Tabular("u1", { 0 : "ACT_HIGH", 1 : "ACT_LOW" }),
                  tags= {
                    "desc" : "the polarity of the chip enable signal. 0"
                             " - chip enable is active high. 1 - chip"
                             " enable is active low.",
                    "section" : "Pin Mode",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("device_personalities",
                  [(0x23, 6, 5)],
                  F.Tabular("u2", {
                    0 : "INTEL",
                    1 : "PVID GPU",
                    2 : "PWM GPU",
                  }),
                  tags= {
                    "desc" : "00 Intel mode. 01 PVID GPU - 6.25mV VID"
                             " Steps. 10 NVIDIA PWM GPU - 5mV VID Steps."
                             " 11 Reserved.",
                    "section" : "Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loops_configuration",
                  [(0x23, 4, 0)],
                  F.Tabular("u5", {
                       # Loop1, Loop2
                     0 : (8, 0),
                     1 : (7, 0),
                     2 : (6, 0),
                     3 : (5, 0),
                     4 : (4, 0),
                     5 : (3, 0),
                     6 : (2, 0),
                     7 : (1, 0),
                     8 : (6, 1),
                     9 : (5, 1),
                    10 : (4, 1),
                    11 : (3, 1),
                    12 : (2, 1),
                    13 : (1, 1),
                    14 : (6, 2),
                    15 : (5, 2),
                    16 : (4, 2),
                    17 : (3, 2),
                    18 : (2, 2),
                    19 : (1, 2),
                  }),
                  tags= {
                    "desc" : "Loops configuration. 0. 8+0, 1. 7+0, 2. 6+0,"
                             " 3. 5+0, 4. 4+0, 5. 3+0, 6. 2+0, 7. 1+0, 8."
                             " 6+1, 9. 5+1, 10. 4+1, 11. 3+1, 12. 2+1, 13."
                             " 1+1, 14. 6+2, 15. 5+2, 16. 4+2, 17. 3+2,"
                             " 18. 2+2, 19. 1+2. All other combinations"
                             " reserved.",
                    "section" : "Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_icc_max",
                  [(0x24, 7, 0)],
                  F.Unsigned(8),
                  tags= {
                    "desc" : "The maximum total Icc the platform supports"
                             " for loop 1. Values are in 1A resolution."
                             " Minimum valid value is 10A.",
                    "section" : "Faults OCP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_icc_max",
                  [(0x25, 7, 0)],
                  F.Unsigned(8),
                  tags= {
                    "desc" : "The maximum total Icc the platform supports"
                             " for loop 2. Values are in 1A resolution."
                             " Minimum valid value is 10A.",
                    "section" : "Faults OCP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_vboot",
                  [(0x26, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The Vboot voltage during start-up of loop 1."
                             " In IBM mode, the values of 0x00, 0x01 and"
                             " 0xFF are not allowed. 0V can be set by using"
                             " the value of 0xFE. Values are in VID"
                             " resolution.",
                    "section" : "Startup",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_vboot",
                  [(0x27, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The Vboot voltage during start-up of loop 2."
                             " In IBM mode, the values of 0x00, 0x01 and"
                             " 0xFF are not allowed. 0V can be set by using"
                             " the value of 0xFE. Values are in VID"
                             " resolution.  In MPOL mode these bits"
                             " represent the delay between loop 1 start-up"
                             " and loop 2 start-up (resolution=2.66 us,"
                             " range =[0, 680 us]).",
                    "section" : "Startup",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_phase_1_thresh",
                  [(0x28, 7, 4)],
                  F.UnsignedScaled(4, 2.0),
                  tags= {
                    "desc" : "The current threshold for loop 1, above which"
                             " it is 2 phase operation. 2A/code.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_phase_2_delta",
                  [(0x28, 3, 0)],
                  F.UnsignedScaled(4, 2.0),
                  tags= {
                    "desc" : "Value when added to loop_1_phase1_thresh"
                             " gives loop_1_phase2_thresh, the current"
                             " threshold above which it is 3 phase"
                             " operation. 2A/code.Note: When LSPBAL is"
                             " enabled, this register will be set to 0"
                             " and cannot be changed until LSPBAL is"
                             " disabled.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_phase_3_delta",
                  [(0x29, 7, 4)],
                  F.UnsignedScaled(4, 2.0),
                  tags= {
                    "desc" : "Value when added to loop_1_phase2_thresh"
                             " gives loop_1_phase3_thresh, the current"
                             " threshold above which it is 4 phase"
                             " operation. 2A/code.Note: When LSPBAL is"
                             " enabled, this register will be set to 0"
                             " and cannot be changed until LSPBAL is"
                             " disabled.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_phase_4_delta",
                  [(0x29, 3, 0)],
                  F.UnsignedScaled(4, 2.0),
                  tags= {
                    "desc" : "Value when added to loop_1_phase3_thresh"
                             " gives loop_1_phase4_thresh, the current"
                             " threshold above which it is 5 phase"
                             " operation. 2A/code.Note: When LSPBAL is"
                             " enabled, this register will be set to 0"
                             " and cannot be changed until LSPBAL is"
                             " disabled.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_phase_5_delta",
                  [(0x2A, 7, 4)],
                  F.UnsignedScaled(4, 2.0),
                  tags= {
                    "desc" : "Value when added to loop_1_phase4_thresh"
                             " gives loop_1_phase5_thresh, the current"
                             " threshold above which it is 6 phase"
                             " operation. 2A/code.Note: When LSPBAL is"
                             " enabled, this register will be set to 0"
                             " and cannot be changed until LSPBAL is"
                             " disabled.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_phase_6_delta",
                  [(0x2A, 3, 0)],
                  F.UnsignedScaled(4, 2.0),
                  tags= {
                    "desc" : "Value, when added to loop_1_phase5/6_thresh"
                             " gives loop_1_phase6/7_thresh, the current"
                             " threshold above which it is 7/8 phase"
                             " operation. 2A/code.Note: When LSPBAL is"
                             " enabled, this register will be set to 0"
                             " and cannot be changed until LSPBAL is"
                             " disabled.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 gives the following:
        #I2C.Field("loop_1_phase_7_delta",
                  #[(0x2B, 7, 4)],
                  #F.Raw(),
                  #tags= {
                    #"desc" : "",
                    #"section" : "",
                  #}
        #),
        I2C.Field("svid_pmbus_vout_sel",
                  [(0x2B, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "0=SVID, 1=PMBus.",
                    "section" : "Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("start_min_pulse_width",
                  [(0x2B, 6, 4)],
                  F.UnsignedScaled(3, 10e-9, 30e-9),
                  tags= {
                    "desc" : "The minimum pulse width for the pwm signal"
                             " when Vout < 250 mV. Width = (30 +"
                             " loop_1_min_pulse_width*10) ns.",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_phase_1_thresh",
                  [(0x2B, 3, 0)],
                  F.UnsignedScaled(4, 2.0),
                  tags= {
                    "desc" : "The current threshold for loop 2, above which"
                             " it is 2 phase operation. 2A/code.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ph1_gain",
                  [(0x2C, 7, 4)],
                  F.UnsignedScaled(4, 1.0/64),
                  tags= {
                    "desc" : "This register is used for the phase current"
                             " imbalance feature. Phase 1 will have"
                             " 1/(1-(1/64)*ph1_gain) more current than"
                             " a phase with no gain. ph1_gain is always"
                             " positive.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ph2_gain",
                  [(0x2C, 3, 0)],
                  F.UnsignedScaled(4, 1.0/64),
                  tags= {
                    "desc" : "This register is used for the phase current"
                             " imbalance feature. Phase 2 will have"
                             " 1/(1-(1/64)*ph2_gain) more current than"
                             " a phase with no gain. ph2_gain is always"
                             " positive.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ph3_gain",
                  [(0x2D, 7, 4)],
                  F.UnsignedScaled(4, 1.0/64),
                  tags= {
                    "desc" : "This register is used for the phase current"
                             " imbalance feature. Phase 3 will have"
                             " 1/(1-(1/64)*ph3_gain) more current than"
                             " a phase with no gain. ph3_gain is always"
                             " positive.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ph4_gain",
                  [(0x2D, 3, 0)],
                  F.UnsignedScaled(4, 1.0/64),
                  tags= {
                    "desc" : "This register is used for the phase current"
                             " imbalance feature. Phase 4 will have"
                             " 1/(1-(1/64)*ph4_gain) more current than"
                             " a phase with no gain. ph4_gain is always"
                             " positive.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ph5_gain",
                  [(0x2E, 7, 4)],
                  F.UnsignedScaled(4, 1.0/64),
                  tags= {
                    "desc" : "This register is used for the phase current"
                             " imbalance feature. Phase 5 will have"
                             " 1/(1-(1/64)*ph5_gain) more current than"
                             " a phase with no gain. ph5_gain is always"
                             " positive.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ph6_gain",
                  [(0x2E, 3, 0)],
                  F.UnsignedScaled(4, 1.0/64),
                  tags= {
                    "desc" : "This register is used for the phase current"
                             " imbalance feature. Phase 6 will have"
                             " 1/(1-(1/64)*ph6_gain) more current than"
                             " a phase with no gain. ph6_gain is always"
                             " positive.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ph7_gain",
                  [(0x2F, 7, 4)],
                  F.UnsignedScaled(4, 1.0/64),
                  tags= {
                    "desc" : "This register is used for the phase current"
                             " imbalance feature. Phase 7 will have"
                             " 1/(1-(1/64)*ph7_gain) more current than"
                             " a phase with no gain. ph7_gain is always"
                             " positive.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ph8_gain",
                  [(0x2F, 3, 0)],
                  F.UnsignedScaled(4, 1.0/64),
                  tags= {
                    "desc" : "This register is used for the phase current"
                             " imbalance feature. Phase 8 will have"
                             " 1/(1-(1/64)*ph8_gain) more current than"
                             " a phase with no gain. ph8_gain is always"
                             " positive.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        # Datasheet lists these as (0x30, 7, 3) and (0x30, 4, 0).
        # XXX - not listed in tool, no description in UN0026
        I2C.Field("vgd_min",
                  [(0x30, 7, 4)],
                  F.Raw(),
                  tags= {
                    "desc" : "MISSING",
                    "section" : "Var Gate Drive",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_force_off_time",
                  [(0x31, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Forces a minimum off time of ~60ns between"
                             " pulses of a phase.",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists pwm_moving_avg_en at 0x31[6]
        # XXX - UN0026 lists this as 0x31[5]
        I2C.Field("loop_1_force_off_time",
                  [(0x31, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Forces a minimum off time of ~60ns between"
                             " pulses of a phase.",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists this as 0x31[4:0]
        I2C.Field("loop_1_sw_period",
                  [(0x31, 5, 0)],
                  F.SemiLog(mantissa_bits=4, exponent_bits=2, scale=20.833e-9),
                  tags= {
                    "desc" : "VR switching period. Quasi log scale. swp"
                             " = (16+loop_1_sw_period[3:0]) *"
                             " 2^loop_1_sw_period[5:4] * 20.833 ns.",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists r_vgd at 0x32[7:5]
        # XXX - UN0026 lists this as 0x32[4:0]
        I2C.Field("loop_2_sw_period",
                  [(0x32, 5, 0)],
                  F.SemiLog(mantissa_bits=4, exponent_bits=2, scale=20.833e-9),
                  tags= {
                    "desc" : "VR switching period. Quasi log scale. swp ="
                             " (16+loop_2_sw_period[3:0]) *"
                             " 2^loop_2_sw_period[5:4] * 20.833 ns.",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_ll_reg",
                  [(0x33, 7, 0)],
                  F.UnsignedScaled(8, 25e-6),
                  tags= {
                    "desc" : "Register that denotes the loadline slope for"
                             " loop 1. 0.025*value mohm.",
                    "section" : "Loadline",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_ll_reg",
                  [(0x34, 7, 0)],
                  F.UnsignedScaled(8, 25e-6),
                  tags= {
                    "desc" : "Register that denotes the loadline slope for"
                             " loop 2. 0.050*value mohm.",
                    "section" : "Loadline",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_vid_offset",
                  [(0x35, 7, 4)],
                  F.Signed(4),
                  tags= {
                    "desc" : "High resolution offset for loop 1. -/+(DAC"
                             " res*value)mV. Represented by a 2's"
                             " compliment number between +8 to -7 (n+1).",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_vid_offset",
                  [(0x35, 3, 0)],
                  F.Signed(4),
                  tags= {
                    "desc" : "High resolution offset for loop 2. -/+(DAC"
                             " res*value)mV. Represented by a 2's"
                             " compliment number between +8 to -7 (n+1).",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_dutyc_adj",
                  [(0x36, 7, 4)],
                  F.UnsignedScaled(4, 1.0/256),
                  tags= {
                    "desc" : "This register is used to adjust(reduce) the"
                             " measured dutycycle for loop 2 to compensate"
                             " for a non ideal driver.  Unit 1/256 (or"
                             " ~0.4%). Only changes input current"
                             " calculation.",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("delay_mode",
                  [(0x36, 3, 3)],
                  F.Tabular("u1", {
                    0 : "LOOP_READY",
                    1 : "PAD",
                  }),
                  tags= {
                    "desc" : "The delay start point: 0 - Delay from loop"
                             " ready. 1 - Delay from pad. NOTE: If"
                             " sequence_mode is logic 0, this MUST be set"
                             " to 1.  enable_delay_time is not valid if"
                             " sequence_mode=0 and delay_mode=0",
                    "section" : "Startup",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("enable_delay_time",
                  [(0x36, 2, 0)],
                  F.Tabular("u3", {
                    0 : 0,
                    1 : 0.25e-3,
                    2 : 0.50e-3,
                    3 : 1.00e-3,
                    4 : 2.50e-3,
                    5 : 5.00e-3,
                    6 : 10.0e-3,
                  }),
                  tags= {
                    "desc" : "Delay time.  0=0ms, 1=0.25ms, 2=0.5ms, 3=1ms,"
                             "4=2.5ms, 5=5ms, 6=10ms, 7=Reserved.",
                    "section" : "Startup",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_db_duration",
                  [(0x37, 7, 5)],
                  F.UnsignedScaled(3, 666e-9, 625e-9),
                  tags= {
                    "desc" : "Maximum duration of diode braking ="
                             "db_duration * 666ns + 625ns.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_dvid_bb_delay",
                  [(0x37, 4, 3)],
                  F.UnsignedScaled(2, 64e-6/96.0, 127e-6/96),
                  tags= {
                    "desc" : "This determines how long body braking is"
                             " disabled after an active DVID event."
                             " Delay - ((reg_value+1)*64+63)/96 us",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x37, 2, 2
        I2C.Field("loop_1_tsense_method",
                  [(0x37, 1, 0)],
                  F.Tabular("u2", {
                    0 : "NTC",
                    2 : "IR3555",
                  }),
                  tags= {
                    "desc" : "temperature sense method. 0 = NTC, 1 ="
                             " Reserved, 2 = IR3555, 3 = Reserved.",
                    "section" : "Faults OTP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_db_duration",
                  [(0x38, 7, 5)],
                  F.UnsignedScaled(3, 666e-9, 625e-9),
                  tags= {
                    "desc" : "Maximum duration of diode braking ="
                             " db_duration * 666ns + 625ns.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_dvid_bb_delay",
                  [(0x38, 4, 3)],
                  F.UnsignedScaled(2, 64e-6/96.0, 127e-6/96),
                  tags= {
                    "desc" : "This determines how long body braking is"
                             " disabled after an active DVID event. Delay"
                             " - ((reg_value+1)*64+63)/96 us",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("dis_sm_alert",
                  [(0x38, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "1=VR does not support SM_ALERT. 0=otherwise.",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_tsense_method",
                  [(0x38, 1, 0)],
                  F.Tabular("u2", {
                    0 : "NTC",
                    2 : "IR3555", # Desc says Kineo, tool says IR3555
                  }),
                  tags= {
                    "desc" : "temperature sense method. 0 = NTC, 1 = diode,"
                             " 2 = Kineo, 3 = Disable.",
                    "section" : "Faults OTP",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists loop_1_k_vref at 0x39[7:0]
        # 0x39, 7, 4
        I2C.Field("loop_1_interleave_order",
                  [(0x39, 3, 0)],
                  F.Unsigned(4),
                  tags= {
                    "desc" : "loop 1 interleave order. Used in Sync function.",
                    "section" : "Sync",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists loop_2_k_vref at 0x3A[7:0]
        # 0x3A, 7, 4
        I2C.Field("loop_2_interleave_order",
                  [(0x3A, 3, 0)],
                  F.Unsigned(4),
                  tags= {
                    "desc" : "loop 1 interleave order. Used in Sync function.",
                    "section" : "Sync",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_sr_slow",
                  [(0x3B, 7, 6)],
                  F.Tabular("u2", {
                    0 : 1.0/2,
                    1 : 1.0/4,
                    2 : 1.0/8,
                    3 : 1.0/16,
                  }),
                  tags= {
                    "desc" : "Control for loop 1 to slow the ramp rate by a"
                             " factor. 0 - 1/2, 1 - 1/4, 2 - 1/8, 3 - 1/16.",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_kp",
                  [(0x3B, 5, 0)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=4,
                            exponent_bias=-13),
                  tags= {
                    "desc" : "Single-phase proportional coefficient for"
                             " loop 1. Value = (4+loop_1_kp[1:0]) *"
                             " 2^(loop_1_kp[5:2]-13).",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("i2c_take_addr_from_ext",
                  [(0x3C, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Defines if the address is NVM only or NVM"
                             " + offset from ext. resistor. 1 - NVM +"
                             " ext resistor. 0 - NVM only.",
                    "section" : "I2C/PMBus/SVID Address",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("svid_take_addr_from_ext",
                  [(0x3C, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Defines if the address is NVM only or NVM +"
                             " offset from ext. resistor. 1 - NVM +"
                             " ext resistor. 0 - NVM only.",
                    "section" : "I2C/PMBus/SVID Address",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_ki",
                  [(0x3C, 5, 0)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=4,
                            exponent_bias=-23),
                  tags= {
                    "desc" : "Single-phase integration coefficient for loop"
                             " 1. Value = (4+loop_1_ki[1:0]) *"
                             " 2^(loop_1_ki[5:2]-23).  ",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_sr_slow",
                  [(0x3D, 7, 6)],
                  F.Tabular("u2", {
                    0 : 1.0/2,
                    1 : 1.0/4,
                    2 : 1.0/8,
                    3 : 1.0/16,
                  }),
                  tags= {
                    "desc" : "Control for loop 2 to slow the ramp rate by a"
                             " factor. 0 - 1/2, 1 - 1/4, 2 - 1/8, 3 - 1/16.",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_kd",
                  [(0x3D, 5, 0)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=4,
                            exponent_bias=-10),
                  tags= {
                    "desc" : "Single-phase differentiation coefficient for"
                             " loop 1. Value = (4+loop_1_kd[1:0])"
                             " * 2^(loop_1_kd[5:2]-10).",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_kpole1",
                  [(0x3E, 7, 4)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=2,
                            exponent_bias=-9),
                  tags= {
                    "desc" : "Single-phase pole1 coefficient for loop 1."
                             " c = (4+loop_x_kpole1[1:0]) *"
                             " 2^(loop_x_kpole1[3:2]-9). Bandwidth ="
                             " (c * 48e6)/(pi * (4 - 4*c - c^2)^0.5).",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_kpole2",
                  [(0x3E, 3, 0)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=2,
                            exponent_bias=-8),
                  tags= {
                    "desc" : "Single-phase pole2 coefficient for loop 1."
                             " c = (4+loop_x_kpole2[1:0]) *"
                             " 2^(loop_x_kpole2[3:2]-8). Bandwidth ="
                             " (c * 24e6)/(pi * (4 - 4*c - c^2)^0.5).",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool, no description in UN0026
        I2C.Field("delay_ocp",
                  [(0x3F, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "MISSING",
                    "section" : "AMD SVI",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool, no description in UN0026
        I2C.Field("vgd_invert",
                  [(0x3F, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "MISSING",
                    "section" : "Var Gate Drive",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_kp",
                  [(0x3F, 5, 0)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=4,
                            exponent_bias=-13),
                  tags= {
                    "desc" : "Single-phase proportional coefficient for loop"
                             " 2. Value = (4+loop_2_kp[1:0]) *"
                             " 2^(loop_2_kp[5:2]-13).",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_phase_mult",
                  [(0x40, 7, 6)],
                  F.Tabular("u2", {
                    0 : 1,
                    1 : 2,
                    2 : 1,
                    3 : 4,
                  }),
                  tags= {
                    "desc" : "0,2 - no multiplication, 1 - phase doubling,"
                             " 3 - phase quadrupling. If set to 3, PWM"
                             " changes to Active trilevel mode. Effective"
                             " switching period is independent of"
                             " phase_mult. Not used in asus mode.",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_ki",
                  [(0x40, 5, 0)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=4,
                            exponent_bias=-23),
                  tags= {
                    "desc" : "Single-phase integration coefficient for loop"
                             " 2. Value = (4+loop_2_ki[1:0]) *"
                             " 2^(loop_2_ki[5:2]-23).",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("salert_polarity",
                  [(0x41, 7, 6)],
                  F.Tabular("u2", {
                    0 : "Open Drain Active Low",
                    1 : "Open Drain Active High",
                    2 : "CMOS Active High",
                    3 : "CMOS Active Low",
                  }),
                  tags= {
                    "desc" : "the polarity of the salert. 0 - Open Drain"
                             " Active Low. 1 - Open Drain Active high."
                             " 2 - CMOS Active high. 3 - CMOS Active low.",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_kd",
                  [(0x41, 5, 0)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=4,
                            exponent_bias=-10),
                  tags= {
                    "desc" : "Single-phase differentiation coefficient for"
                             " loop 2. Value = (4+loop_2_kd[1:0]) *"
                             " 2^(loop_2_kd[5:2]-10).",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_kpole1",
                  [(0x42, 7, 4)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=2,
                            exponent_bias=-9),
                  tags= {
                    "desc" : "Single-phase pole1 coefficient for loop 2. c"
                             " = (4+loop_x_kpole1[1:0]) *"
                             " 2^(loop_x_kpole1[3:2]-9). Bandwidth = (c"
                             " * 48e6)/(pi * (4 - 4*c - c^2)^0.5).",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_kpole2",
                  [(0x42, 3, 0)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=2,
                            exponent_bias=-8),
                  tags= {
                    "desc" : "Single-phase pole2 coefficient for loop 2. c"
                             " = (4+loop_x_kpole2[1:0]) *"
                             " 2^(loop_x_kpole2[3:2]-8). Bandwidth = (c"
                             " * 24e6)/(pi * (4 - 4*c - c^2)^0.5).",
                    "section" : "PID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vr_hot_mode",
                  [(0x43, 7, 5)],
                  F.Tabular("u3", {
                    0 : "VR_HOT",
                    1 : "VR_HOT+Icritical",
                    2 : "VR_HOT+OCP",
                  }),
                  tags= {
                    "desc" : "The functionality of the VR_HOT pin. 0 -"
                             " vr_hot. 1 - vr_hot or Icritical. 2 -"
                             " vr_hot or OCP. All others reserved.",
                    "section" : "Pin Mode",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ovtp_thresh",
                  [(0x43, 4, 0)],
                  F.Unsigned(5),
                  tags= {
                    "desc" : "Over temperature protection (OTP) threshold"
                             " above VR_HOT setting. If the temperature"
                             " exceeds setting, the VR shuts down. Values"
                             " are in 1 degree C resolution.",
                    "section" : "Faults OTP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("temp_max",
                  [(0x44, 7, 2)],
                  F.UnsignedScaled(6, 1, 64),
                  tags= {
                    "desc" : "The maximum temperature that the platform"
                             " supports and the level that VR-hot asserts."
                             " This value should be added to 64C. Values"
                             " are in 1 degree C resolution.",
                    "section" : "Faults OTP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ocp_mode",
                  [(0x44, 1, 0)],
                  F.Tabular("u2", {
                    0 : "Shutdown immediately",
                    1 : "Shutdown after 7 tries",
                    2 : "Continuous retry",
                    3 : "Shutdown after 2 tries (startup), immediately"
                        " otherwise",
                  }),
                  tags= {
                    "desc" : "0=shutdown immediately, 1=shutdown after 7"
                             " tries, 2=continuous hiccup, 3=shutdown"
                             " after 2 tries at startup/shutdown"
                             " immediately during operation.",
                    "section" : "Faults OCP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ovp_thresh",
                  [(0x45, 7, 5)],
                  F.UnsignedScaled(3, 50e-3, 50e-3),
                  tags= {
                    "desc" : "0=50mV 1=100mV 2=150mV 3=200mV 4=250mV"
                             " 5=300mV 6=350mV 7=400mV.",
                    "section" : "Faults OVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_ocp_thr",
                  [(0x45, 4, 0)],
                  F.UnsignedScaled(5, 2.0),
                  tags= {
                    "desc" : "The per-phase overcurrent threshold (the"
                             " number of phases scale during phase"
                             " multiplication). Values are in 2A resolution.",
                    "section" : "Faults OCP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("uvp_thresh",
                  [(0x46, 7, 5)],
                  F.UnsignedScaled(3, 50e-3, 50e-3),
                  tags= {
                    "desc" : "0=50mV 1=100mV 2=150mV 3=200mV 4=250mV"
                             " 5=300mV 6=350mV 7=400mV.",
                    "section" : "Faults UVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_ocp_thr",
                  [(0x46, 4, 0)],
                  F.UnsignedScaled(5, 2.0),
                  tags= {
                    "desc" : "The per-phase overcurrent threshold (the"
                             " number of phases scale during phase"
                             " multiplication). Values are in 2A resolution.",
                    "section" : "Faults OCP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_de_thresh",
                  [(0x47, 7, 5)],
                  F.UnsignedScaled(5, 3e-3),
                  tags= {
                    "desc" : "Error threshold to start a pulse during diode"
                             " emulation for loop 1. The resolution is 3mv.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_de_pw",
                  [(0x47, 4, 2)],
                  F.Tabular("u3", {
                    0 : 160e-9,
                    1 : 187e-9,
                    2 : 213e-9,
                    3 : 267e-9,
                    4 : 320e-9,
                    5 : 573e-9,
                    6 : 427e-9,
                    7 : 533e-9,
                  }),
                  tags= {
                    "desc" : "Fixed pulse width 'on' time during diode"
                             " emulation for loop 1.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("sequence_mode",
                  [(0x47, 1, 0)],
                  F.Tabular("u2", {
                    0 : "Together",
                    1 : "Loop 1 then Loop 2",
                    2 : "Loop 2 then Loop 1",
                  }),
                  tags= {
                    "desc" : "Sequencing the loop startups. 0 - Both loops"
                             " start together. 1 - Loop 2 follows loop 1."
                             " 2 - Loop 1 follows loop 2. 3 - Reserved.",
                    "section" : "Startup",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_de_thresh",
                  [(0x48, 7, 5)],
                  F.UnsignedScaled(5, 3e-3),
                  tags= {
                    "desc" : "Error threshold to start a pulse during"
                             " diode emulation for loop 2. The resolution"
                             " is 3mv.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_de_pw",
                  [(0x48, 4, 2)],
                  F.Tabular("u3", {
                    0 : 160e-9,
                    1 : 187e-9,
                    2 : 213e-9,
                    3 : 267e-9,
                    4 : 320e-9,
                    5 : 573e-9,
                    6 : 427e-9,
                    7 : 533e-9,
                  }),
                  tags= {
                    "desc" : "Fixed pulse width 'on' time during diode"
                             " emulation for loop 2.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_auto_ps_mode",
                  [(0x48, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "Logic 1 enables automatic power state mode"
                             " on loop 1.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_auto_ps_mode",
                  [(0x48, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "Logic 1 enables automatic power state mode"
                             " on loop 2.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("pec_mode",
                  [(0x49, 7, 6)],
                  F.Tabular("u2", {
                    0 : "ACK PEC and non-PEC",
                    1 : "ACK only non-PEC",
                    2 : "ACK only PEC",
                  }),
                  tags= {
                    "desc" : "Sets the pec mode. 0 - supported both"
                             " according to the master. 1 - support only"
                             " none PEC. 2 - support only PEC packets."
                             " 3 - Reserved.",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_p_active_psi",
                  [(0x49, 5, 5)],
                  F.UnsignedScaled(1, 1, 1),
                  tags= {
                    "desc" : "Replaces p_active in PS1 mode for loop 1.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_p_active_min",
                  [(0x49, 4, 4)],
                  F.UnsignedScaled(1, 1, 1),
                  tags= {
                    "desc" : "Personality bit which determines the minimum"
                             " number of phases during PS0 with auto phase"
                             " shedding. 0 - one phase. 1 - two phases.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_p_active_register",
                  [(0x49, 3, 1)],
                  F.UnsignedScaled(3, 1, 1),
                  tags= {
                    "desc" : "maximum number of phases on loop 1 ="
                             " loop_1_p_active_register+1.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_p_active_register",
                  [(0x49, 0, 0)],
                  F.UnsignedScaled(3, 1, 1),
                  tags= {
                    "desc" : "maximum number of phases on loop 2 ="
                             " loop_2_p_active_register+1.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool.
        I2C.Field("vin1_div_mode",
                  [(0x4A, 7, 7)],
                  F.Tabular("u1", {
                    0 : 14.0,
                    1 : 22.0,
                  }),
                  tags= {
                    "desc" : "The external resistor ratio. 0 - 1/14. 1 -"
                             " 1/22.  OVP threshold is 0 - 14.5V and in"
                             " 1 23.5V rst:0",
                    "section" : "Faults",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_ll_en",
                  [(0x4A, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Load line enable on loop 1.",
                    "section" : "Loadline",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_ll_en",
                  [(0x4A, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Load line enable on loop 2.",
                    "section" : "Loadline",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool, no description in UN0026
        I2C.Field("vr_settled_at_0",
                  [(0x4A, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "MISSING",
                    "section" : "SVID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool, no description in UN0026
        I2C.Field("auto_alert",
                  [(0x4A, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "MISSING",
                    "section" : "VR12",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool, no description in UN0026
        I2C.Field("amd_cmd_mode",
                  [(0x4A, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "MISSING",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool, no description in UN0026
        I2C.Field("pad_output_mode",
                  [(0x4A, 1, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "MISSING",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_sw_period_skew",
                  [(0x4B, 7, 5)],
                  F.Tabular("u3", {
                    0 : "1",
                    1 : "1 + (I- 8)/32",
                    2 : "1 + (I-12)/32",
                    3 : "1 + (I-16)/32",
                    4 : "1",
                    5 : "1 - (I- 8)/64",
                    6 : "1 - (I-12)/64",
                    7 : "1 - (I-16)/64",
                  }),
                  tags= {
                    "desc" : "Scale switching period, based on per phase"
                             " current. 0,4-no change. 1-(1 + (I-8)/32),"
                             " 2-(1 + (I-12)/32), 3-(1 + (I-16)/32),"
                             " 5-(1 - (I-8)/64), 6-(1 - (I-12)/64),"
                             " 7-(1 - (I-16)/64). saturated to range"
                             " [0.5, 2].",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_current_offset",
                  [(0x4B, 4, 0)],
                  F.SignedScaled(5, 125e-3),
                  tags= {
                    "desc" : "Per phase offset to the current measurement"
                             " on loop 1 in 0.125A resolution. Value in"
                             " 2's complement.",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_low_pbal_en",
                  [(0x4C, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable phase balancing on loop 1. 0 -"
                             " Disable. 1 - Enable.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_low_pbal_en",
                  [(0x4C, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable phase balancing on loop 2. 0 -"
                             " Disable. 1 - Enable.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vcpu_high_release_en",
                  [(0x4C, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable the release of the low-side FETs if"
                             " the output voltage is below 0.5V after a"
                             " VCPU high fault. 0 - Don't release. 1 - Release.",
                    "section" : "Faults OVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_current_offset",
                  [(0x4C, 4, 0)],
                  F.SignedScaled(5, 125e-3),
                  tags= {
                    "desc" : "Per phase offset to the current measurement"
                             " on loop 2 in 0.125A resolution. Value in"
                             " 2's complement.",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - format
        I2C.Field("loop_1_vmax",
                  [(0x4D, 7, 4)],
                  F.Raw(),
                  tags= {
                    "desc" : "This sets the maximum voltage for loop 1."
                             " Setting for DAC resolution of 5mV (0.645V"
                             " + value*120mV). Setting for DAC resolution"
                             " of 6.25mV (0.80625V + value*112.5mV)."
                             " Setting for DAC resolution of 10mV (1.29 V"
                             " + value*240mV).",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - format
        I2C.Field("loop_2_vmax",
                  [(0x4D, 3, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "This sets the maximum voltage for loop 2."
                             " Setting for DAC resolution of 5mV (0.645V"
                             " + value*120mV). Setting for DAC resolution"
                             " of 6.25mV (0.80625V + value*112.5mV)."
                             " Setting for DAC resolution of 10mV (1.29 V"
                             " + value*240mV).",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vin1_uv_hth",
                  [(0x4E, 7, 0)],
                  F.UnsignedScaled(8, 1.0/16),
                  tags= {
                    "desc" : "Vin should be above this value for the"
                             " under-voltage flag to 0. Resolution is"
                             " 1/16V, independent of vin1_div_mode. ",
                    "section" : "Faults VIN",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vin1_uv_lth",
                  [(0x4F, 7, 0)],
                  F.UnsignedScaled(8, 1.0/16),
                  tags= {
                    "desc" : "Vin should be below this value for the"
                             " under-voltage flag to 1. Resolution: 1/16V,"
                             " independent of vin1_div_mode.",
                    "section" : "Faults VIN",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vin2_uv_hth",
                  [(0x50, 7, 0)],
                  F.UnsignedScaled(8, 1.0/16),
                  tags= {
                    "desc" : "Vin should be above this value for the"
                             " under-voltage flag to 0. Resolution is"
                             " 1/16V, independent of vin2_div_mode. ",
                    "section" : "Faults VIN",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vin2_uv_lth",
                  [(0x51, 7, 0)],
                  F.UnsignedScaled(8, 1.0/16),
                  tags= {
                    "desc" : "Vin should be below this value for the"
                             " under-voltage flag to 1. Resolution: 1/16V,"
                             " independent of vin2_div_mode.",
                    "section" : "Faults VIN",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("gpu_main_vid_value_0",
                  [(0x52, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The VID assigned for a 0 value on the main"
                             " loop in GPU mode. In Intel mode, this"
                             " register changes to {vr125_alert_on_startup,"
                             " second_enable_pin_select,"
                             " vr125_disable_second_loop,"
                             " vr125_vboot_value[1:0], vr125_translate,"
                             " vr125_loop_2_addr_active, vr125_mode_nvm}."
                             " in nvdia pwm mode if the pwm interface is"
                             " chosen bit[0] is the VID resolution."
                             " 0  5mv. 1  10mv.",
                    "section" : "GPU",
                    "mode" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vr125_alert_on_startup",
                  [(0x52, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Select ALERT# setting on startup. 0 = Do not"
                             " set ALERT# on startup. 1 = Set ALERT# on"
                             " startup.",
                    "section" : "Intel Alert Configuration",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("second_enable_pin_select",
                  [(0x52, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable loop 2 to have an Enable pin. Must"
                             " be set to 0 if using CFP Fault. NOTE: when"
                             " this feature is enabled pad_output_mode has"
                             " to be open drain. Only valid with Second"
                             " enable pin select = 0.",
                    "section" : "Pin Mode",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vr125_disable_second_loop",
                  [(0x52, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Selects Loop 2 Output. 0 = Output voltage on"
                             " Loop 2. 1 = Do not output voltage on Loop 2.",
                    "section" : "INTEL Configuration",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vr125_vboot_value",
                  [(0x52, 4, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Selects Vboot voltage. Translate Vboot must"
                             " be set to 1. 0 = 0.0V. 1 = 1.65V. 2 = 1.70V."
                             " 3 = 1.75V",
                    "section" : "INTEL Configuration",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vr125_translate",
                  [(0x52, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "Selects VR12.5 boot voltage from Vboot Value"
                             " instead of Vboot register. 0 = Take Vboot"
                             " from Vboot register. 1 = Select Vboot"
                             " through Vboot Value",
                    "section" : "INTEL Configuration",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vr125_loop_2_addr_active",
                  [(0x52, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "Selects Loop 2 Communication. 0 = Disable"
                             " Loop 2 SVID Address. 1 = Enable Loop 2"
                             " SVID Address.",
                    "section" : "I2C/PMBus/SVID Address",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vr125_mode_nvm",
                  [(0x52, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enables VR12.5 mode. 0 = VR12. 1 = VR12.5.",
                    "section" : "INTEL Mode",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists this as 0x54
        I2C.Field("gpu_main_vid_value_2",
                  [(0x53, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The VID assigned for a 2 value on the main"
                             " loop in GPU mode.",
                    "section" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists this as 0x53
        I2C.Field("gpu_main_vid_value_1",
                  [(0x54, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The VID assigned for a 1 value on the main"
                             " loop in GPU mode.In Intel mode, this"
                             " register changes to"
                             " {Reserved[2:0],ps4_dvid_or_decay,"
                             " ps4_single_multi_out, ps4_enable,"
                             " vr126_mode, alert_on_0v}. dvid = 0;"
                             " single = 0; disable PS4 = 0; VR12.6 = 1;"
                             " don't alert = 0.",
                    "section" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ps4_dvid_or_decay",
                  [(0x54, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable decay to 0V with PS4 command."
                             " 0 = DVID. 1 = Decay.",
                    "section" : "INTEL Configuration",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ps4_single_multi_out",
                  [(0x54, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable Wake from PS4 on Target SVID ADDR"
                             " only. 0 = Wake on any SVID Clock pulse."
                             " 1 = Wake only on an SetVID or SetPS 0/1/2/3"
                             " command directed at the target loop.",
                    "section" : "INTEL Configuration",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ps4_enable",
                  [(0x54, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "0 = PS4 is disabled. 1 = PS4 is enabled.",
                    "section" : "INTEL Configuration",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - Not listed by tool or by UN0026
        I2C.Field("vr126_mode",
                  [(0x54, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "VR12.6",
                    "section" : "INTEL Configuration",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("alert_on_0v",
                  [(0x54, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "SVID_ALERT will be issued when the output"
                             " reaches 0V",
                    "section" : "Intel Alert Configuration",
                    "mode" : "INTEL",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("gpu_main_vid_value_3",
                  [(0x55, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The VID assigned for a 3 value on the main"
                             " loop in GPU mode.",
                    "section" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists this as gpu_second_vid_value_0
        I2C.Field("gpu_main_vid_value_4",
                  [(0x56, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The VID assigned for a 4 value on the main"
                             " loop in GPU mode.",
                    "section" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool, no description in UN0026
        I2C.Field("vid_decay_mode",
                  [(0x57, 7, 7)],
                  F.Raw(),
                  tags= {
                    "desc" : "MISSING",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_c_comp_ovp_en",
                  [(0x57, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable the common compartor. 1 - use the"
                             " common comparator for OVP. 0 - use the"
                             " ADC for OVP (248 mV).",
                    "section" : "Faults OVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_c_comp_uvp_en",
                  [(0x57, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable the common compartor. 1 - use the"
                             " common comparator for UVP. 0 - use the ADC"
                             " for UVP (248 mV).",
                    "section" : "Faults UVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_c_comp_ovp_en",
                  [(0x57, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable the common compartor. 1 - use the"
                             " common comparator for OVP. 0 - use the ADC"
                             " for OVP (248 mV).",
                    "section" : "Faults OVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_c_comp_uvp_en",
                  [(0x57, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable the common compartor. 1 - use the"
                             " common comparator for UVP. 0 - use the ADC"
                             " for UVP (248 mV).",
                    "section" : "Faults UVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("enable_thresh",
                  [(0x57, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "0=choose ttl levels, 1=choose vtt levels.",
                    "section" : "Pin Mode",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("usr_sync_enable",
                  [(0x57, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "Determines PWM modulation scheme and"
                             " ability to Synchronize to external clock."
                             " 1=single edge modulation, can sync."
                             " 0=dual edge modulation, can't sync. A 0"
                             " trim_sync_enable overrides this and"
                             " disables sync. When sync is diabled, the"
                             " in_range signal to the PLL is 0.",
                    "section" : "Sync",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("svid_bw",
                  [(0x57, 0, 0)],
                  F.Tabular("u1", {
                    0 : 102.0,
                    1 : 879.0,
                  }),
                  tags= {
                    "desc" : "svid bandwidth for current_ratio. Bandwidths"
                             " = [102, 879]Hz for values 0 and 1."
                             " Equivalent Intel av. windows (time"
                             " constants) are [1554, 181] us.",
                    "section" : "INTEL Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("i2c_high_speed_mode",
                  [(0x58, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable working with hold time for high speed"
                             " I2C. 0 - support normal mode and fast mode"
                             " (100 - 400 KHz). 1 - support high speed I2C"
                             " (1MHz).",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("clear_gamer_with_0_v_enable",
                  [(0x58, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable clear of the gamer command when the"
                             " CPU issue a 0V command. 0 - Disable. 1"
                             " - Enable.",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_ni_thresh",
                  [(0x58, 5, 0)],
                  F.UnsignedScaled(6, 250e-3),
                  tags= {
                    "desc" : "Total current threshold below which it is"
                             " assumed that the inductor current has a"
                             " negative component. Resolution 0.25 A.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("disable_vcpu_high_with_no_vin",
                  [(0x59, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Disable VCPU high when input voltage is not"
                             " present. 0 - Enable VCPU High all the time."
                             " 1 - Disable VCPU High when input voltage"
                             " is not present",
                    "section" : "Faults OVP",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool.
        I2C.Field("vin2_div_mode",
                  [(0x59, 6, 6)],
                  F.Tabular("u1", {
                    0 : 14.0,
                    1 : 22.0,
                  }),
                  tags= {
                    "desc" : "The external resistor ratio. 0 - 1/14. 1 -"
                             " 1/22.  OVP threshold is 0 - 14.5V and in"
                             " 1 23.5V rst:0",
                    "section" : "Faults",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_ni_thresh",
                  [(0x59, 5, 0)],
                  F.UnsignedScaled(6, 250e-3),
                  tags= {
                    "desc" : "Total current threshold below which it is"
                             " assumed that the inductor current has a"
                             " negative component. Resolution 0.25 A.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_off_time_adj",
                  [(0x5A, 7, 4)],
                  F.UnsignedScaled(4, 60e-9),
                  tags= {
                    "desc" : "The value subtracts from the diode emulate"
                             " off time for loop 1, with a resolution of"
                             " 60 nsec.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_off_time_adj",
                  [(0x5A, 3, 0)],
                  F.UnsignedScaled(4, 60e-9),
                  tags= {
                    "desc" : "The value subtracts from the diode emulate"
                             " off time for loop 2, with a resolution of"
                             " 60 nsec.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("vboot_trans_enable",
                  [(0x5B, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Translate the VID select pin to a vboot"
                             " value.  Note: should only be set in GPU"
                             " mode. For this feature to work loop 1 vboot"
                             " should be set to 0x78 then the loop1 vboot"
                             " voltages will be as follows: 0 - 0.9V."
                             " 1 - 1V. 2 - 1.1V. 3 - 1.2V. rst:0",
                    "section" : "Vboot/Vmax",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x5B, 6, 6
        I2C.Field("loop_1_temp_offset",
                  [(0x5B, 5, 0)],
                  F.Signed(6),
                  tags= {
                    "desc" : "An offset for the temperature measurement on"
                             " loop 1. This is a 2's complement number"
                             " with 1 degree C resolution.",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("en_ovp_on_disable",
                  [(0x5C, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable OVP when the chip is disabled. 0 -"
                             " Disable. 1 - Enable.",
                    "section" : "Faults OVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("second_enable_from_pin",
                  [(0x5C, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable loop 2 to have an Enable pin. Must be" 
                             " set to 0 if using CFP Fault. NOTE: when this"
                             " feature is enabled pad_output_mode has to"
                             " be open drain. Only valid with Second"
                             " enable pin select = 0.",
                    "section" : "Pin Mode",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_temp_offset",
                  [(0x5C, 5, 0)],
                  F.Signed(6),
                  tags= {
                    "desc" : "An offset for the temperature measurement on"
                             " loop 2. This is a 2's complement number with"
                             " 1 degree C resolution.",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_i_gain",
                  [(0x5D, 7, 6)],
                  F.Tabular("u2", {
                    0 : 2,
                    1 : 4,
                    2 : 8,
                  }),
                  tags= {
                    "desc" : "Controls the ISense gain for loop 1. 0 ->"
                             " 2, 1 -> 4, 2 -> 8.  All other combinations"
                             " reserved.",
                    "section" : "Current Sense",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("avp_en_4uvp_loop_1",
                  [(0x5D, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Excludes the loadline term on Verror for"
                             " comparator UVP on loop 1.",
                    "section" : "Faults UVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_slow_iph_max",
                  [(0x5D, 4, 0)],
                  F.UnsignedScaled(5, 2.0),
                  tags= {
                    "desc" : "The per phase over-current threshold for the"
                             " heavily filtered loop current reading on"
                             " loop 1. Values are in 2A resolution. Slow"
                             " ocp is disabled when value is 0.",
                    "section" : "Faults OCP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_i_gain",
                  [(0x5E, 7, 6)],
                  F.Tabular("u2", {
                    0 : 2,
                    1 : 4,
                    2 : 8,
                  }),
                  tags= {
                    "desc" : "Controls the ISense gain for loop 2. 0 ->"
                             " 2, 1 -> 4, 2 -> 8.  All other combinations"
                             " reserved.",
                    "section" : "Current Sense",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("avp_en_4uvp_loop_2",
                  [(0x5E, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Excludes the loadline term on Verror for"
                             " comparator UVP on loop 2.",
                    "section" : "Faults UVP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_slow_iph_max",
                  [(0x5E, 4, 0)],
                  F.UnsignedScaled(5, 2.0),
                  tags= {
                    "desc" : "The per phase over-current threshold for the"
                             " heavily filtered loop current reading on"
                             " loop 2. Values are in 2A resolution. Slow"
                             " ocp is disabled when value is 0.",
                    "section" : "Faults OCP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("usr_password",
                  [(0x60, 7, 0), (0x5F, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The the 16 bit password for the trim section.",
                    "section" : "Security",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("write_protect_mode",
                  [(0x61, 7, 6)],
                  F.Tabular("u2", {
                    0 : "password",
                    1 : "PIN",
                    2 : "PIN and password",
                    3 : "lock forever",
                  }),
                  tags= {
                    "desc" : "Write protect mode. 0 - Password. 1 - Pin."
                             " 2 - Pin and Password. 3 - Lock For Ever.",
                    "section" : "Security",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("read_protect_mode",
                  [(0x61, 5, 4)],
                  F.Tabular("u2", {
                    0 : "password",
                    1 : "PIN",
                    2 : "PIN and password",
                    3 : "lock forever",
                  }),
                  tags= {
                    "desc" : "Read protect mode. 0 - Password. 1 - Pin."
                             " 2 - Pin and Password. 3 - Lock For Ever.",
                    "section" : "Security",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("write_protect_section",
                  [(0x61, 3, 2)],
                  F.Tabular("u2", {
                    0 : "none",
                    1 : "configuration",
                    3 : "all",
                  }),
                  tags= {
                    "desc" : "Chose the section that is write protected. 0"
                             " - No Protection. 1 - Protect Configuration."
                             " 2 - Reserved. 3 - Protect All.",
                    "section" : "Security",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("read_protect_section",
                  [(0x61, 1, 0)],
                  F.Tabular("u2", {
                    0 : "none",
                    1 : "configuration",
                    2 : "all but telemetry",
                    3 : "all",
                  }),
                  tags= {
                    "desc" : "Chose the section that is read protected."
                             " 0 - No Protection. 1 - Protect Configuration."
                             " 2 - Protect All but Telemetry. 3 -"
                             " Protect All.",
                    "section" : "Security",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_le_th",
                  [(0x62, 7, 4)],
                  F.UnsignedScaled(4, 4e-3),
                  tags= {
                    "desc" : "error threshold to go from PS1/PS2 mode to"
                             " full phase count. 4 mV Q.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_le_th",
                  [(0x62, 3, 0)],
                  F.UnsignedScaled(4, 4e-3),
                  tags= {
                    "desc" : "error threshold to go from PS1/PS2 mode to"
                             " full phase count. 4 mV Q.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_v_lift",
                  [(0x63, 7, 4)],
                  F.UnsignedScaled(4, 2e-3),
                  tags= {
                    "desc" : "Added voltage offset during load oscillation."
                             "2 mV Q.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_v_lift",
                  [(0x63, 3, 0)],
                  F.UnsignedScaled(4, 2e-3),
                  tags= {
                    "desc" : "Added voltage offset during load oscillation."
                             " 2 mV Q.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_err_lth",
                  [(0x64, 7, 4)],
                  F.UnsignedScaled(4, 4e-3),
                  tags= {
                    "desc" : "Overshoot threshold beyond which PWM pulses"
                             " are not issued. A value of 0 disables the"
                             " feature. 4 mV Q.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_err_lth",
                  [(0x64, 3, 0)],
                  F.UnsignedScaled(4, 4e-3),
                  tags= {
                    "desc" : "Overshoot threshold beyond which PWM"
                             " pulses are not issued. A value of 0"
                             " disables the feature. 4 mV Q.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_fc_hth",
                  [(0x65, 7, 4)],
                  F.UnsignedScaled(4, 4e-3),
                  tags= {
                    "desc" : "undershoot threshold when ATA will start. 4"
                             " mV Q.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_fc_hth",
                  [(0x65, 3, 0)],
                  F.UnsignedScaled(4, 4e-3),
                  tags= {
                    "desc" : "Undershoot threshold when ATA will start."
                             " 4 mV Q.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_phase_mult",
                  [(0x66, 7, 6)],
                  F.Tabular("u2", {
                    0 : 1,
                    1 : 2,
                    2 : 1,
                    3 : 4,
                  }),
                  tags= {
                    "desc" : "0,2 - no multiplication, 1 - phase doubling,"
                             " 3 - phase quadrupling. If set to 3, PWM"
                             " changes to Active trilevel mode. Effective"
                             " switching period is independent of phase_mult.",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists this as 0x66[5:4]
        I2C.Field("min_pulse_width",
                  [(0x66, 5, 3)],
                  F.UnsignedScaled(3, 10e-9, 20e-9),
                  tags= {
                    "desc" : "The minimum pulse width for the pwm signal"
                             " = (20 + loop_1_min_pulse_width*10) ns.",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool.
        I2C.Field("psi_pin_mode",
                  [(0x66, 2, 0)],
                  F.Tabular("u3", {
                    0 : "Disabled",
                    1 : "Listen",
                    2 : "Loop1",
                    3 : "Loop2",
                    4 : "Both",
                  }),
                  tags= {
                    "desc" : "The definition is 0=PSI# and driver faults"
                             " are disabled. 1=Listens for Driver Fault"
                             " Only.  Shutdown both loops if fault occurs."
                             " 2=Assert PSI# when Loop1 goes to PSI mode."
                             " Shutdown Loop1 if fault occurs. 3=Assert"
                             " PSI# when Loop2 goes to PSI mode. Shutdown"
                             " Loop2 if fault occurs. 4=Assert PSI# when"
                             " Loop1 and Loop2 go to PSI mode.  Shutdown"
                             " both loops if fault occurs. 5,6,7=Reserved."
                             " rst:000",
                    "section" : "Control",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_fc_p",
                  [(0x67, 7, 3)],
                  F.UnsignedScaled(5, 0.25),
                  tags= {
                      "desc" : "ATA proportional term (0 disable ATA)."
                               " Resolution is 2^-2.",
                      "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        # UN0026 register map and PowIRCenter tool disagree here: per the tool
        # these are 7:3 and 2:0.  Per the doc 7:4 and 3:0.
        I2C.Field("loop_1_fc_slope_th",
                  [(0x67, 2, 0)],
                  F.UnsignedScaled(3, 12e3),
                  tags= {
                      "desc" : "Slope threshold when ATA will start."
                               "12 mV/us Q.",
                      "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_fc_p",
                  [(0x68, 7, 3)],
                  F.UnsignedScaled(5, 0.25),
                  tags= {
                      "desc" : "ATA proportional term (0 disable ATA)."
                               " Resolution is 2^-2.",
                      "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_fc_slope_th",
                  [(0x68, 2, 0)],
                  F.UnsignedScaled(3, 12e3),
                  tags= {
                    "desc" : "slope threshold when ATA will start. 12"
                             " mV/us Q.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_fc_d",
                  [(0x69, 7, 4)],
                  F.UnsignedScaled(4, 8),
                  tags= {
                    "desc" : "ATA differential term. Resolution is 2^3.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_fc_d",
                  [(0x69, 3, 0)],
                  F.UnsignedScaled(4, 8),
                  tags= {
                    "desc" : "ATA differential term. Resolution is 2^3.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_hspb_fth",
                  [(0x6A, 7, 4)],
                  F.UnsignedScaled(4, 46.9e3),
                  tags= {
                    "desc" : "load oscillation frequency below which high"
                             " speed phase balance is disabled. Q level"
                             " is 46.9 KHz.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_hspb_fth",
                  [(0x6A, 3, 0)],
                  F.UnsignedScaled(4, 46.9e3),
                  tags= {
                    "desc" : "load oscillation frequency below which high"
                             " speed phase balance is disabled. Q level is"
                             " 46.9 KHz.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_hspb_hth",
                  [(0x6B, 7, 4)],
                  F.UnsignedScaled(4, 4e-3),
                  tags= {
                    "desc" : "If error exceeds this window, high speed"
                             " phase balance starts. 4 mV Q.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_hspb_hth",
                  [(0x6B, 3, 0)],
                  F.UnsignedScaled(4, 4e-3),
                  tags= {
                    "desc" : "If error exceeds this window, high speed"
                             " phase balance starts. 4 mV Q.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        #I2C.Field("gpr_usr3",
                  #[(0x6C, 7, 2)],
                  #F.Raw(),
                  #tags= {
                    #"desc" : "",
                    #"section" : "",
                  #}
        #),
        # 0x6C 7
        I2C.Field("ocp_cfp",
                  [(0x6C, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enables over current contribution to"
                             " catastrophic fault. 1=enable, 0=disable.",
                    "section" : "Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("current_limiting_en",
                  [(0x6C, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enables fold back current limiting response"
                             " during OC event.",
                    "section" : "Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x6C 4
        I2C.Field("loop_1_pwm_en_ats",
                  [(0x6C, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable active trilevel drivers for loop 1. 0"
                             " - Disable. 1 - Enable. Can be overriden by"
                             " trim_pwm_en_ats.",
                    "section" : "Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_pwm_en_ats",
                  [(0x6C, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable active trilevel drivers for loop 2. 0"
                             " - Disable. 1 - Enable. Can be overriden by"
                             " trim_pwm_en_ats.",
                    "section" : "Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_hspb_enable",
                  [(0x6C, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "1=enable high speed phase current balance.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_hspb_enable",
                  [(0x6C, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "1=enable high speed phase current balance.",
                    "section" : "Phase Balance",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("loop_1_half_q",
                  [(0x6D, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Half ADC Q offset for voltage error. 0 -"
                             " No offset rst:1",
                    "section" : "Control",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_pmbus_addr",
                  [(0x6D, 6, 0)],
                  F.Unsigned(7),
                  tags= {
                    "desc" : "The pmbus address the chip will react to."
                             " 0 disables the interface.",
                    "section" : "I2C/PMBus/SVID Address",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("loop_2_half_q",
                  [(0x6E, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Half ADC Q offset for voltage error. 0 -"
                             " No offset rst:1",
                    "section" : "Control",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_pmbus_addr",
                  [(0x6E, 6, 0)],
                  F.Unsigned(7),
                  tags= {
                    "desc" : "The pmbus address the chip will react to."
                             " 0 disables the interface.",
                    "section" : "I2C/PMBus/SVID Address",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("loop_1_max_current_support",
                  [(0x6F, 7, 4)],
                  F.UnsignedScaled(4, 32),
                  tags= {
                    "desc" : "Maximum current supported by loop 1. Q ="
                             " 32 A.  rst:0000",
                    "section" : "PMBus",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("loop_2_max_current_support",
                  [(0x6F, 3, 0)],
                  F.UnsignedScaled(4, 32),
                  tags= {
                    "desc" : "Maximum current supported by loop 1. Q ="
                             " 32 A.  rst:0000",
                    "section" : "PMBus",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("alert_on_slew_rate_mode",
                  [(0x70, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Sets alert according to slew rate and not"
                             " according to DAC. Only valid with 'DVID"
                             " Alert Mode'=1. 0=according to DAC."
                             " 1=according to slew rate.",
                    "section" : "Intel Alert Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vaux_en",
                  [(0x70, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable observation of Vaux (instead of Temp2)."
                             " The voltage thresholds with hysterisis for"
                             " Vaux are 0.586V and 0.664V.",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("second_tsen_en_reg",
                  [(0x70, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable for second temperature sense."
                             " 0=disable, 1=enable.",
                    "section" : "Faults OTP",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_diode_brake",
                  [(0x70, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "During load release, enable diode braking"
                             " for loop 1.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("ps3_isense_shutdown_disable",
                  [(0x70, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "When the value is set to 0  isense will be"
                             " shutdown during ps3 mode. When the value"
                             " is set to 1 - isense will Not be shutdown"
                             " during ps3 mode",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_fc_shape",
                  [(0x70, 2, 0)],
                  F.UnsignedScaled(3, 0.03),
                  tags= {
                    "desc" : "ATA response non-linear shaping term. (approx"
                             " resolution is 3%)",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x71, 7, 5
        I2C.Field("loop_2_diode_brake",
                  [(0x71, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "During load release, enable diode braking"
                             " for loop 2.",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("decay_alert_mode",
                  [(0x71, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "The SVID alert behavior in case of a SetVID"
                             " Decay. 0 - don't set the alert. 1 - set"
                             " the alert",
                    "section" : "Intel Alert Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_fc_shape",
                  [(0x71, 2, 0)],
                  F.UnsignedScaled(3, 0.03),
                  tags= {
                    "desc" : "ATA response non-linear shaping term."
                             " (approx resolution is 3%)",
                    "section" : "ATA",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("always_decay_down",
                  [(0x72, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "0=don't automatically decay. 1=automatically"
                             " decay when commanded to go to a lower"
                             " voltage. rst:0",
                    "section" : "VR12",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("application_mode",
                  [(0x72, 6, 6)],
                  F.Alternate(("device_personalities",), {
                    "INTEL" : F.Tabular("u1", {
                        0 : "CPU",
                        1 : "MPOL",
                    }),
                    "PVID GPU" : F.Tabular("u1", {
                        1 : "PVID GPU",
                    }),
                    "PWM GPU" : F.Tabular("u1", {
                        0 : "PWM GPU",
                    }),
                  }),
                  tags= {
                    "desc" : "Intel Mode: 0=CPU, 1=MPOL. PVID GPU:"
                             " 0=Reserved. 1=PVID GPU. NVIDIA PWM GPU:"
                             " 0=PWM GPU. 1=Reserved.",
                    "section" : "Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("interface_mode",
                  [(0x72, 5, 5)],
                  F.Tabular("u1", {
                     0 : "SVI1.0",
                     1 : "SVI2.0",
                  }),
                  tags= {
                    "desc" : "0=SVI2.0, 1=SVI1.0. Sets SVI pins to either"
                             " CMOS(SVI2.0) or Open-Drain(SVI1.x). rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_iscale",
                  [(0x72, 4, 0), (0x73, 7, 0)],
                  F.UnsignedScaled(13, 1.0/2048),
                  tags= {
                    "desc" : "Scales the Iout reading.",
                    "section" : "Current Sense",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("populated_check_nvm",
                  [(0x74, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable to check which rail are populated and"
                             " set the number of rail accordingly. rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("svi2_mode",
                  [(0x74, 6, 6)],
                  F.Tabular("u1", {
                     0 : "SVI1.0",
                     1 : "SVI2.0",
                  }),
                  tags= {
                    "desc" : "Defines the mode that the SVI2 works in for"
                             " AMD.  0=SVI1.0, 1=SVI2.0. *Note: The state"
                             " of SVT pin voltage will override setting"
                             " when VR is enabled.  Also, this bit MUST"
                             " be set to 0 for Intel mode so that SVID"
                             " timeout works. rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("ack_support",
                  [(0x74, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "choose to support ack in AMD mode. 0=not"
                             " supported, 1=supported. rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_iscale",
                  [(0x74, 4, 0), (0x75, 7, 0)],
                  F.UnsignedScaled(13, 1.0/2048),
                  tags= {
                    "desc" : "Scales the Iout reading.",
                    "section" : "Current Sense",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_c",
                  [(0x76, 7, 0)],
                  F.UnsignedScaled(8, 50e-6),
                  tags= {
                    "desc" : "Load capacitance. Unit 50 uF. Used in dvid"
                             " compensation.",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_ll_bw",
                  [(0x77, 7, 4)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=2,
                            exponent_bias=-9),
                  tags= {
                    "desc" : "Load line bandwidth. Value ="
                             " (4+loop_x_ll_bw[1:0]) *"
                             " 2^(loop_x_ll_bw[3:2]-9). BW ="
                             " (c * 48e6)/(pi * (4 - 4*c - c^2)^0.5). (4'd8)",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_ll_bw",
                  [(0x77, 3, 0)],
                  F.SemiLog(mantissa_bits=2, exponent_bits=2,
                            exponent_bias=-9),
                  tags= {
                    "desc" : "Load line bandwidth. Value ="
                             " (4+loop_x_ll_bw[1:0]) *"
                             " 2^(loop_x_ll_bw[3:2]-9). BW ="
                             " (c * 48e6)/(pi * (4 - 4*c - c^2)^0.5). (4'd8)",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - format
        I2C.Field("loop_1_sr_fast",
                  [(0x78, 7, 4)],
                  F.Raw(),
                  tags= {
                    "desc" : "Controls the slew rate for the VR on loop 1."
                             " 0 - 10 mV/uS. 1 - 15 mV/uS. 2 - 20 mV/uS."
                             " 3 - 25 mV/uS. 4 - 30 mV/us. 5 - 35 mV/uS."
                             " 6 - 40 mV/uS. 7 - 45 mV/uS. 8 - 50 mV/uS."
                             " 9 - 55 mV/us. 10 - 60 mV/uS. 11 - 65 mV/uS."
                             " 12 - 70 mV/uS. 13 - 80 mV/uS. 14 - 85 mV/us."
                             " 15 - 95 mV/us. 6.25 mV DAC resolution:1 -"
                             " 17 mV/us. 2 - 23 mV/us. 3 - 28 mV/us. 4 -"
                             " 34 mV/us. 5 - 40 mV/us. 6 - 45 mV/us.  7 -"
                             " 52 mV/us. 8 - 52 mV/us. 9 - 62 mV/us. 10 -"
                             " 62 mV/us. 11 - 78 mV/us. 12 - 78 mV/us. 13 -"
                             " 78 mV/us. 14 - 78 mV/us. 15 - 101 mV/us",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - format
        I2C.Field("loop_2_sr_fast",
                  [(0x78, 3, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "Controls the slew rate for the VR on loop 2."
                             " 0 - 10 mV/uS. 1 - 15 mV/uS. 2 - 20 mV/uS."
                             " 3 - 25 mV/uS. 4 - 30 mV/us. 5 - 35 mV/uS."
                             " 6 - 40 mV/uS. 7 - 45 mV/uS. 8 - 50 mV/uS."
                             " 9 - 55 mV/us. 10 - 60 mV/uS. 11 - 65 mV/uS."
                             " 12 - 70 mV/uS. 13 - 80 mV/uS. 14 - 85 mV/us."
                             " 15 - 95 mV/us. 6.25 mV DAC resolution:1 -"
                             " 17 mV/us. 2 - 23 mV/us. 3 - 28 mV/us. 4 -"
                             " 34 mV/us. 5 - 40 mV/us. 6 - 45 mV/us.  7 -"
                             " 52 mV/us. 8 - 52 mV/us. 9 - 62 mV/us. 10 -"
                             " 62 mV/us. 11 - 78 mV/us. 12 - 78 mV/us. 13 -"
                             " 78 mV/us. 14 - 78 mV/us. 15 - 101 mV/us",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists this as gpr_second_vid_value_1
        I2C.Field("gpu_main_vid_value_6",
                  [(0x79, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The VID value for external VID pins set to 6"
                             " in GPU mode",
                    "section" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_iin_fixed_offset",
                  [(0x7A, 7, 4)],
                  F.UnsignedScaled(4, 1.0/32),
                  tags= {
                    "desc" : "A fixed offset to adjust the estimated input"
                             " current for loop 1. This is a fixed"
                             " component of the adjustment. Always"
                             " positive with a resolution of 1/32 A."
                             " rst:0000",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_iin_per_phase_offset",
                  [(0x7A, 3, 0)],
                  F.UnsignedScaled(4, 1.0/128),
                  tags= {
                    "desc" : "A per-phase offset to adjust the estimated"
                             " input current for loop 1. This is the"
                             " per-phase component of the adjustment."
                             " Always positive. Assumption: Prior to"
                             " adding these offsets, our estimated input"
                             " current is always lower than the correct"
                             " amount. Resolution = 1/128 A. rst:0000",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_iin_fixed_offset",
                  [(0x7B, 7, 4)],
                  F.UnsignedScaled(4, 1.0/32),
                  tags= {
                    "desc" : "A fixed offset to adjust the estimated input"
                             " current for loop 2. This is a fixed"
                             " component of the adjustment. Always"
                             " positive with a resolution of 1/32 A. rst:0000",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_iin_per_phase_offset",
                  [(0x7B, 3, 0)],
                  F.UnsignedScaled(4, 1.0/128),
                  tags= {
                    "desc" : "A per-phase offset to adjust the estimated"
                             " input current for loop 2. This is the"
                             " per-phase component of the adjustment."
                             " Always positive. Assumption: Prior to adding"
                             " these offsets, our estimated input current"
                             " is always lower than the correct amount."
                             " Resolution = 1/128 A. rst:0000 ",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_c",
                  [(0x7C, 7, 0)],
                  F.UnsignedScaled(8, 50e-6),
                  tags= {
                    "desc" : "Load capacitance. Unit 50 uF. Used in dvid"
                             " compensation.",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_vout_offset",
                  [(0x7D, 7, 0)],
                  F.Signed(8),
                  tags= {
                    "desc" : "Sets the offset in VID steps for Intel VR12"
                             " for loop 1. Bit 7 is the sign bit.",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_vout_offset",
                  [(0x7E, 7, 0)],
                  F.Signed(8),
                  tags= {
                    "desc" : "Sets the offset in VID steps for Intel VR12"
                             " for loop 2. Bit 7 is the sign bit",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x7F, 7, 7
        I2C.Field("vin2_en",
                  [(0x7F, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable second input voltage. 1=enable."
                             " 0=disable.",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("cfp_en",
                  [(0x7F, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "0=Disable CFP pin. 1=Enables CFP pin. Must be"
                             " set to 0 if using Loop 2 Enable pin.",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("dig_reg_clamp_disable",
                  [(0x7F, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "Bit to disable load detect/clamp circuit"
                             " which adds current sink capability to the"
                             " 1.8V digital regulator. 1=disable. rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("vgd_en_ats",
                  [(0x7F, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable active trilevel driver for variable"
                             " gate drive. 0 - Disable. 1 - Enable. rst:0",
                    "section" : "PWM",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("telemetry_bw",
                  [(0x7F, 2, 0)],
                  F.Tabular("u3", {
                    0 : 0.69,
                    1 : 1.39,
                    2 : 2.78,
                    3 : 5.55,
                    4 : 11.1,
                    5 : 22.2,
                    6 : 44.6,
                    7 : 89.5,
                  }),
                  tags= {
                    "desc" : "telemetry bandwidth for input currents,"
                             " output currents, input voltages, output"
                             " voltages and temperatures. Bandwidths ="
                             " [0.69, 1.39, 2.78, 5.55, 11.1, 22.2, 44.6,"
                             " 89.5]Hz for values 0 through 7.",
                    "section" : "Telemetry",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_loadline_trim",
                  [(0x80, 7, 4)],
                  F.SignedScaled(4, 0.05),
                  tags= {
                    "desc" : "2s complement loadline trim for loop1."
                             " Resolution is 5%.",
                    "section" : "Loadline",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_loadline_trim",
                  [(0x80, 3, 0)],
                  F.SignedScaled(4, 0.05),
                  tags= {
                    "desc" : "2s complement loadline trim for loop2."
                             " Resolution is 5%.",
                    "section" : "Loadline",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("individual_mode",
                  [(0x81, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "In case of overcurrent or a VCPU low event,"
                             " both loops should react. 0 - Both loops"
                             " react. 1 - Only the faulted loop reacts.",
                    "section" : "Faults OCP",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("tsense_gain_adjust",
                  [(0x81, 6, 0)],
                  F.UnsignedScaled(7, 1.0/256),
                  tags= {
                    "desc" : "Used when the diode/PNP is selected for"
                             " adjusting the gain factor. Gain = (1.0 -"
                             " tsense_gain_adjust/256). Scales Kelvin"
                             " temperature to adjust for analog gain."
                             " rst:0000000",
                    "section" : "Control",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("pmbus_mfr_id",
                  [(0x82, 7, 0), (0x83, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "PMBus MFR_ID",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("pmbus_mfr_date",
                  [(0x84, 7, 0), (0x85, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "PMBus MFR_DATE",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("pmbus_mfr_model",
                  [(0x86, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "PMBus MFR_MODEL",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("pmbus_mfr_revision",
                  [(0x87, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "PMBus MFR_REVISION",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_de_x2",
                  [(0x88, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "1=doubles ON/OFF times for diode emulation."
                             " Used when using large L & C.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_de_x2",
                  [(0x88, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "1=doubles ON/OFF times for diode emulation."
                             " Used when using large L & C.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x88, 5, 5
        # XXX - not listed in tool
        I2C.Field("en_inmode",
                  [(0x88, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "enable inmode pin. 0=disable. 1=enable. rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("reduce_dvid_speed",
                  [(0x88, 3, 2)],
                  F.Tabular("u2", {
                    0 : 1.0,
                    1 : 1.0/2,
                    2 : 1.0/4,
                    3 : 1.0/8,
                  }),
                  tags= {
                    "desc" : "Reduce programmed DVID speed. 0 = speed as"
                             " programmed. 1 = speed/2. 2 = speed/4."
                             " 3= speed/8.",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x88, 1, 0
        # XXX - Tool *also* lists this as gpu_main_vid_value_5
        I2C.Field("gpr_1",
                  [(0x89, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "customer register 1.",
                    "section" : "GPR",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - Tool *also* lists this as gpu_main_vid_value_7
        I2C.Field("gpr_2",
                  [(0x8A, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "customer register 2.",
                    "section" : "GPR",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_dvid_delay",
                  [(0x8B, 7, 4)],
                  F.UnsignedScaled(4, 1.33e-6, 1.33e-6),
                  tags= {
                    "desc" : "The time delay from when the loop 1 DAC is"
                             " stable to the check of the target voltage."
                             " This Register setting will also affect"
                             " VR_Ready at start-up and SVID Alert timing"
                             " when dvid_alert_mode is 0. Resolution in"
                             " 1.33usec, 0 mean 1.33usec.",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_dvid_delay",
                  [(0x8B, 3, 0)],
                  F.UnsignedScaled(4, 1.33e-6, 1.33e-6),
                  tags= {
                    "desc" : "The time delay from when the loop 2 DAC is"
                             " stable to the check of the target voltage."
                             " This Register setting will also affect"
                             " VR_Ready at start-up and SVID Alert timing"
                             " when dvid_alert_mode is 0. Resolution in"
                             " 1.33usec, 0 mean 1.33usec.",
                    "section" : "DVID",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x8C, 7, 4
        # XXX - UN0026 lists this at 0x30[3:0]
        I2C.Field("loop_1_dutyc_adj",
                  [(0x8C, 3, 0)],
                  F.UnsignedScaled(4, 1.0/256),
                  tags= {
                    "desc" : "This register is used to adjust(reduce) the"
                             " measured dutycycle for loop 1 to compensate"
                             " for a non ideal driver. Unit 1/256 (or"
                             " ~0.4%). Only changes input current"
                             " calculation.",
                    "section" : "Offset",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x8D .. 0x8F
        # 0x90-0xA3 are manufacturer programmable registers
        I2C.Field("vin1_high_en",
                  [(0x90, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable Vin1 high fault. rst:1",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vin1_low_en",
                  [(0x90, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable Vin1 low fault. rst:1",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x90, 5, 5
        I2C.Field("pi_fault_en",
                  [(0x90, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable phase current fault. If any phase"
                             " has too high/low current, the loop is"
                             " shutdown. rst:0",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("gpu_loop_1_dvid_speed",
                  [(0x90, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "The DVID speed in GPU mode for loop 1."
                             " This applies anytime the VID is changed."
                             " 0 - Fast  DVID. 1 - Slow DVID. rst:0",
                    "section" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("gpu_loop_2_dvid_speed",
                  [(0x90, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "The DVID speed in GPU mode for loop 2."
                             "This applies anytime the VID is changed."
                             "0 - Fast DVID. 1 - Slow DVID. rst:0",
                    "section" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_gpu_start_dvid",
                  [(0x90, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 enables the DVID on loop 1 in"
                             " GPU mode. 0 - Don't DVID (go to VBOOT)."
                             " 1 - DVID. rst:0",
                    "section" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_gpu_start_dvid",
                  [(0x90, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 enables the DVID on loop 2 in"
                             " GPU mode. 0 - Don't DVID (go to VBOOT)."
                             " 1 - DVID. rst:0",
                    "section" : "GPU",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x91, 7, 7
        # XXX - not listed in tool
        I2C.Field("loop_1_ps_gamer_enable",
                  [(0x91, 5, 4)],
                  F.Unsigned(2),
                  tags= {
                    "desc" : "A logic 1 overrides the PS in gamer mode"
                             " on Loop 1. 0 - Don't override the PS in"
                             " gamer mode. 1 - Override the PS in gamer"
                             " mode. rst:0",
                    "section" : "Gamer",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists this as 0x91[3:2]
        # XXX - not listed in tool
        I2C.Field("loop_1_manual_gamer_ps",
                  [(0x91, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Force the loop 1 PS to the value in this"
                             " register in gamer mode rst:00",
                    "section" : "Gamer",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - UN0026 lists this as 0x91[1:1]
        I2C.Field("loop_1_manual_ll_scale",
                  [(0x91, 2, 1)],
                  F.Tabular("u2", {
                    0 : 0.0,
                    1 : 0.2,
                    2 : 0.4,
                    3 : 1.0,
                  }),
                  tags= {
                    "desc" : "allow loadline change when in gamer mode, 0"
                             " no change, 1 -  -20%, 2 -  -40%, 3"
                             " disable loadline.",
                    "section" : "Loadline",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_manual_type",
                  [(0x91, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "the type of the manual vid. 0  Fixed Voltage."
                             " 1  Fixed offset.",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - format
        I2C.Field("loop_1_manual_vid",
                  [(0x92, 7, 0), (0x91, 6, 6)],
                  F.Unsigned(9),
                  tags= {
                    "desc" : "The VID setting for loop 1 in manual mode.",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("loop_2_ps_gamer_enable",
                  [(0x93, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 overrides the PS in gamer mode"
                             " on Loop 2. 0 - Don't override the PS in"
                             " gamer mode. 1 - Override the PS in gamer"
                             " mode. rst:0",
                    "section" : "Gamer",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("loop_2_manual_gamer_ps",
                  [(0x93, 5, 4)],
                  F.Raw(),
                  tags= {
                    "desc" : "Force the loop 1 PS to the value in this"
                             " register in gamer mode rst:00",
                    "section" : "Gamer",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_manual_ll_scale",
                  [(0x93, 3, 2)],
                  F.Tabular("u2", {
                    0 : 0.0,
                    1 : 0.2,
                    2 : 0.4,
                    3 : 1.0,
                  }),
                  tags= {
                    "desc" : "allow loadline change when in gamer mode, 0"
                             " no change, 1 -  -20%, 2 -  -40%, 3"
                             " disable loadline.",
                    "section" : "Loadline",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("decay_error_check_mode",
                  [(0x93, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "ADC error used. 0=from filter, 1=no filter."
                             " rst:1",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_manual_type",
                  [(0x93, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "the type of the manual vid. 0  Fixed Voltage."
                             " 1  Fixed offset.",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - format
        I2C.Field("loop_2_manual_vid",
                  [(0x94, 7, 0), (0x93, 7, 7)],
                  F.Unsigned(9),
                  tags= {
                    "desc" : "The VID setting for loop 2 in manual mode.",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vin2_high_en",
                  [(0x95, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable Vin2 high fault.",
                    "section" : "Fault Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vcpu_low_en",
                  [(0x95, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable VCPU low fault.",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("oc_en",
                  [(0x95, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable overcurrent (OCP) fault.",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vin2_low_en",
                  [(0x95, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable Vin2 low fault.",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("v3_low_en",
                  [(0x95, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable V3 low fault.",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("vcpu_high_en",
                  [(0x95, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable VCPU high fault.",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("driver_fault_en",
                  [(0x95, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "Driver faults are enabled when set to 1. rst:1",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("otp_en",
                  [(0x95, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable over-temperature protection (OTP).",
                    "section" : "Faults Enable",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("timeout_value",
                  [(0x96, 7, 0)],
                  F.UnsignedScaled(8, 8.0 / 96e6),
                  tags= {
                    "desc" : "This value multiplied by 8*(96Mhz clock"
                             " cycle) = 83.34 ns resolution, in which the"
                             " SVID communcation link will be reset"
                             " (including SVT in AMD mode) if the SVID"
                             " clock remains high. The SVID master can hold"
                             " the clock high for this long to restart the"
                             " SVID link. If set to 0.5usec (6) and the"
                             " chip is in test mode the timeout value is"
                             " 20usec. For SVID timeout to work in Intel"
                             " mode, the svi2_mode bit must be set to 0."
                             " rst:",
                    "section" : "VR12",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - format
        I2C.Field("loop_1_vout_max",
                  [(0x97, 7, 0)],
                  F.Unsigned(8),
                  tags= {
                    "desc" : "The maximum VID that the VR accepts on the"
                             " SVID interface for loop 1.",
                    "section" : "INTEL Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - format
        I2C.Field("loop_2_vout_max",
                  [(0x98, 7, 0)],
                  F.Unsigned(8),
                  tags= {
                    "desc" : "The maximum VID that the VR accepts on the"
                             " SVID interface for loop 2.",
                    "section" : "INTEL Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x99, 7, 3
        I2C.Field("otp_clock_enable",
                  [(0x99, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable MTP programming clock.",
                    "section" : "MTP Programming",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x99, 2, 0
        # 0x9A, 7, 2
        I2C.Field("loop_1_lock_vid",
                  [(0x9A, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 locks the SVID interface from"
                             " changing the current VID and PS setting"
                             " for loop 1. This gets unlocked if the CPU"
                             " sends SetPS(0) or cycles the enable. 0"
                             " = unlock. 1 = lock.",
                    "section" : "INTEL Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_vr_ready_0v",
                  [(0x9A, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "This bit determines the behavior of Loop 1"
                             " VR_Ready when a SetVID=00h is issued. 0"
                             " - shutdown. 1 - stays ready",
                    "section" : "INTEL Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x9B, 7, 2
        I2C.Field("loop_2_lock_vid",
                  [(0x9B, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 locks the SVID interface from"
                             " changing the current VID and PS setting for"
                             " loop 2. This gets unlocked if the CPU sends"
                             " SetPS(0) or cycles the enable. 0 = unlock."
                             " 1 = lock.",
                    "section" : "INTEL Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_2_vr_ready_0v",
                  [(0x9B, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "This bit determines the behavior of Loop 2"
                             " VR_Ready when a SetVID=00h is issued. 0"
                             " - shutdown. 1 - stays ready",
                    "section" : "INTEL Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x9C, 7, 3
        I2C.Field("loop_2_ps_override",
                  [(0x9C, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 overrides the PS in manual mode on"
                             " Loop 2. 0 - Don't override the PS. 1 -"
                             " Override the PS",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("loop_2_manual_ps",
                  [(0x9C, 1, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "Manually force the loop 2 PS to the value in"
                             " this register. rst:00",
                    "section" : "Manual",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x9D, 7, 4
        I2C.Field("fix_ovp_thresh",
                  [(0x9D, 3, 2)],
                  F.Tabular("u2", {
                    0 : 2.5,
                    1 : 1.2,
                    2 : 1.275,
                    3 : 1.35,
                  }),
                  tags= {
                    "desc" : "Sets the Fixed Over-Voltage Protection"
                             " (Fixed-OVP) threshold. 0=2.5V, 1=1.2V,"
                             " 2=1.275V, 3=1.35V",
                    "section" : "Faults OVP",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("scale_speed",
                  [(0x9D, 1, 1)],
                  F.Tabular("u1", {
                    0 : 1.25e-6,
                    1 : 10.0e-6
                  }),
                  tags= {
                    "desc" : "Selects the update rate to change the dynamic"
                             " loadline by 5%. 0=1.25 uS 1=10 uS rst:0",
                    "section" : "Manual",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("disable_ps_in_dvid",
                  [(0x9D, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "If set to 1 ps command during dvid will be"
                             " rejected. If set to 0 PS command is accepted"
                             " during dvid in vr12.5 and rejected in vr12.",
                    "section" : "INTEL Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("slow_end_dvid_enable",
                  [(0x9E, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 enables a slower rate at the end of"
                             " a DVID (1/2 at 3 and 4 steps before the end"
                             " and 1/4 at 2 and 1 step from the end). rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("v3_lps_disable",
                  [(0x9E, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Disables the resistor divider for the V33"
                             " supply so that only during the v33 sampling"
                             " interval the resistor divider logic is on,"
                             " other times it is 0.  When we want the V33"
                             " divider always present set this bit to 1."
                             " rst:0",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("loop_1_ps_override",
                  [(0x9E, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 overrides the PS in manual mode on"
                             " Loop 2. 0 - Don't override the PS 1 -"
                             " Override the PS",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("loop_1_manual_ps",
                  [(0x9E, 4, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Manually force the loop 1 PS to the value in"
                             " this register. rst:00",
                    "section" : "Manual",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("enab_svi_c_d_pullup",
                  [(0x9E, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "A 1 enables a ~50K pull-up on the SVI clock"
                             " and data signals. Improves sensing of boot"
                             " voltage code. rst:1",
                    "section" : "Telemetry",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("enab_svi_t_pullup",
                  [(0x9E, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "A 1 enables a ~50K pull-up on the SVI"
                             " telemetry signal. Required to sense if SVT"
                             " line is pulled LOW. rst:1",
                    "section" : "Telemetry",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("enable_vboot_100u",
                  [(0x9E, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "1=enables the current source to external"
                             " resistor to detect vboot. 0=current source"
                             " disabled, vboot determined using external"
                             " voltage. rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("enable_short_detect",
                  [(0x9F, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "If high, if the DAC waits at 2 for the loop"
                             " to catch up for more than 128 us, vcpu_low"
                             " is declared.",
                    "section" : "Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x9F, 6
        I2C.Field("comp_pwr_dn_disable",
                  [(0x9F, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "vsense comparators are powered down during"
                             " ps3. 1 = disables this feature.",
                    "section" : "Power Saving",
                    "persistent_cfg" : True,
                  }
        ),
        # 0x9F, 4, 3
        I2C.Field("disable_vcpu_low_on_start_up",
                  [(0x9F, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "when set the vcpu low fault is disabled"
                             " during startup.",
                    "section" : "Faults UVP",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("hyst_enable",
                  [(0x9F, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 enables hysteresis on the ICC max"
                             " and temp max indicators to be used for SVID"
                             " alert. rst:1",
                    "section" : "VR12",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("svid_power_state_read_mode",
                  [(0x9F, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "this bit sets the svid power state mode. 0 -"
                             " read the VR power state after all overrides."
                             " 1 - read the cpu commanded power state"
                             " (M-2/Nashoba mode).  rst:1",
                    "section" : "VR12",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("en_half_drv_pad_1",
                  [(0xA0, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "When set to high, the SVT pad will only use"
                             " half of the driver strength. rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("en_half_drv_pad_2",
                  [(0xA0, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "When set to high, the SVC pad will only use"
                             " half of the driver strength. rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("en_half_drv_pad_3",
                  [(0xA0, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "When set to high, the SVD pad will only use"
                             " half of the driver strength. rst:0",
                    "section" : "Personality",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("dont_stop_dac",
                  [(0xA0, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "Prevent the DAC to stop at 15mv until the"
                             " loop catch up. Must be 1 to avoid long wait"
                             " times before start. 0 - Enable DAC stop. 1"
                             " - Prevent DAC stop.  rst:1",
                    "section" : "VR12",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("dvid_alert_mode",
                  [(0xA0, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable setting the alert when the DAC get to"
                              " the target voltage instead of the output"
                              " voltage reaching the target.. 0 - Disable."
                              " 1 - Enable.",
                    "section" : "Intel Alert Configuration",
                    "persistent_cfg" : True,
                  }
        ),
        I2C.Field("dont_restart_gamer_on_0_v",
                  [(0xA0, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "For this bit to be active,"
                             " clear_gamer_with_0_v_enable must be set to"
                             " 0. 0 = when 0V command is received, the"
                             " output goes to 0V and will return to the"
                             " gamer voltage when a non-0V command is"
                             " received. 1 = Will ignore the 0V command and"
                             " remain at the gamer voltage.",
                    "section" : "Manual VID",
                    "persistent_cfg" : True,
                  }
        ),
        # XXX - not listed in tool
        I2C.Field("v12_clear_register_enable",
                  [(0xA0, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "Reset all the read write register on the SVID"
                             " interface when the 12V is low. 0 - Disable."
                             " 1 - Enable. rst:1",
                    "section" : "VR12",
                    "persistent_cfg" : True,
                  }
        ),
        # 0xA0, 0, 0
        # 0xA1
        I2C.Field("pmbus_timeout_value",
                  [(0xA2, 7, 0)],
                  F.UnsignedScaled(8, 1.365e-3),
                  tags= {
                    "desc" : "For debug only. 0=disable timeout. Q=1.365ms.",
                    "section" : "I2C/PMBus Settings",
                    "persistent_cfg" : True,
                  }
        ),
        # 0xA3 .. 0xAF
        # 0xB0-0xC5 are read-only registers
        I2C.Field("loop_1_svid",
                  [(0xB0, 7, 0)],
                  F.Unsigned(8),
                  tags= {
                    "desc" : "This is the Intel/AMD SVID data for loop 1.",
                    "section" : "VR12",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("loop_2_svid",
                  [(0xB1, 7, 0)],
                  F.Unsigned(8),
                  tags= {
                    "desc" : "This is the Intel/AMD SVID data for loop 2.",
                    "section" : "VR12",
                  },
                  writeable= False,
                  volatile=True
        ),
        # XXX - not listed in tool
        I2C.Field("pxi_filt",
                  [(0xB2, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "phase current for the phase indicated by"
                             " pxi_sel.",
                    "section" : "Telemetry",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xB3, 7, 7
        I2C.Field("power_ok",
                  [(0xB3, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Status of the power ok signal to the chip.",
                    "section" : "Status",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("loop_1_chip_enable",
                  [(0xB3, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Chip enable signal for loop 1.",
                    "section" : "Status",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("loop_2_chip_enable",
                  [(0xB3, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "Chip enable signal for loop 2.",
                    "section" : "Status",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xB3, 3, 0
        # XXX - format
        I2C.Field("muxed_ro_reg1",
                  [(0xB4, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "This is a muxed register controlled by"
                             " ro_sel1[4:0] register. The signals muxed are:"
                             "0. vin1_supply[7:0] Q = 1/8 V\n"
                             "1. vin2_supply[7:0] Q = 1/8 V\n"
                             "2. v33_supply[7:0] Q = 5/256 V\n"
                             "3. vaux_supply[7:0] Q = 1.25/(256*divider"
                               " ratio) V\n"
                             "4. loop_1_vcpu[7:0] Q = 1/64 V\n"
                             "5. loop_2_vcpu[7:0] Q = 1/64 V\n"
                             "6. loop_1_output_current[7:0] Q = 2A\n"
                             "7. loop_2_output_current[7:0] Q = 1/2A\n"
                             "8. temp1[7:0] Q = 1C\n"
                             "9. temp2[7:0] Q = 1C\n"
                             "10. loop_1_curr_ratio[7:0] ratio w.r.t icc_max"
                                " Q = 1/256\n"
                             "11. loop_2_curr_ratio[7:0] ratio w.r.t icc_max"
                                " Q = 1/256\n"
                             "12. loop_1_input_i[7:0] input current Q ="
                                " 1/8A\n"
                             "13. loop_2_input_i[7:0] input current Q ="
                                " 1/16A\n"
                             "14. loop_1_i_startup_ref_code[7:0]: reference"
                                " code for output current for loop1.\n"
                             "15. loop_2_i_startup_ref_code[7:0]: reference"
                                " code for output current for loop2.\n"
                             "16. loop_2_ps_mode[3:0], loop_1_ps_mode[3:0]:"
                                " The power state s for the 2 loops. 0 -"
                                " PS0, 1 - PS1, 2 - PS2, 3 - PS3.\n"
                             "17. loop_1_fail_code_nonstick[7:0]: over_temp,"
                                " over_current, vcpu_high, vcpu_low,"
                                " v12_low, v3_low, phase_fault,"
                                " loop_1_slow_over_current.  Cleared on"
                                " reset.\n"
                             "18. loop_2_fail_code_nonstick[7:0]: over_temp,"
                                " over_current, vcpu_high, vcpu_low,"
                                " v12_low, v3_low, phase_fault,"
                                " loop_2_slow_over_current.  Cleared on"
                                " reset.\n"
                             "19. loop_1_fail_code_sticky[7:0]: over_temp,"
                                " over_current, vcpu_high, vcpu_low,"
                                " v12_low, v3_low, phase_fault,"
                                " loop_1_slow_over_current.  Cleared on"
                                " reset and clear_sticky_codes.\n"
                             "20. loop_2_fail_code_sticky[7:0]: over_temp,"
                                " over_current, vcpu_high, vcpu_low,"
                                " v12_low, v3_low, phase_fault,"
                                " loop_2_slow_over_current.  Cleared on"
                                " reset and clear_sticky_codes.\n"
                             "21. pin_value[7:0]: Delivers the value of the"
                                " pads during dft_mode.\n"
                             "22. adc_saddr_2[3:0], adc_saddr_1[3:0]: ADC"
                                " reading of the SADDR2, SADDR1.\n"
                             "23. pi_fault[7:0]: phase current fault"
                                " indication, 1 bit per phase. Indicates"
                                " that the phase current is too high/low.\n"
                             "24. Reserved\n"
                             "25. Reserved\n"
                             "26. Reserved\n"
                             "27. Reserved\n"
                             "28. vin_status = 1'd0, loop_1_ext_tsen,"
                                " loop_2_ext_tsen, vaux_low, vin1_high,"
                                " vin1_low, vin2_high, vin2_low: vinx high"
                                " threshold is dependent on vinx_div_mode."
                                " 0=14.5V, 1=23.5V. The"
                                " vinx_uv_hth/vinx_uv_lth combination"
                                " determines vinx_Low.\n"
                             "29. 2'd0, loop_1_phase_mult_analog[1:0],"
                                " subtraction_ref[3:0].\n"
                             "30. 3'd0, loop_1_ll_scale[4:0].\n"
                             "31. 3'd0, loop_2_ll_scale[4:0].",
                    "section" : "Telemetry",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xB5, 7, 2
        I2C.Field("usr_crc_flag",
                  [(0xB5, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "OTP CRC flags for usr section.",
                    "section" : "MTP Programming",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("mfr_crc_flag",
                  [(0xB5, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "OTP CRC flags for mfr section.",
                    "section" : "MTP Programming",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("loop_1_max_cond",
                  [(0xB6, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "One or more phase currents in loop1 is too"
                             " high.",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("loop_1_min_cond",
                  [(0xB6, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "One or more phase currents in loop1 is too low.",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("otp_trim_ptr",
                  [(0xB6, 5, 3)],
                  F.Unsigned(3),
                  tags= {
                    "desc" : "The entry that the trim section was read"
                             " from. 7 - Null trim section.",
                    "section" : "MTP Programming",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("otp_mfr_ptr",
                  [(0xB6, 2, 0)],
                  F.Unsigned(3),
                  tags= {
                    "desc" : "The entry that the mfr section was read from."
                             " 7 - Null mfr section.",
                    "section" : "MTP Programming",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("loop_2_max_cond",
                  [(0xB7, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "One or more phase currents in loop2 is too"
                             " high.",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("loop_2_min_cond",
                  [(0xB7, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "One or more phase currents in loop2 is too low.",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xB7, 5, 5
        I2C.Field("current_ro_sel1",
                  [(0xB7, 4, 0)],
                  F.Unsigned(5),
                  tags= {
                    "desc" : "The current state of the pointer that points"
                             " to 1 of 32 ready only values in muxed_ro_reg1.",
                    "section" : "Telemetry",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xB8, 7, 4
        I2C.Field("otp_usr_ptr",
                  [(0xB8, 3, 0)],
                  F.Unsigned(4),
                  tags= {
                    "desc" : "The entry that the user section was read from."
                             " 15 - Null usr section",
                    "section" : "MTP Programming",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xB9
        # 0xBA, 7, 6
        I2C.Field("l1_p_active_target",
                  [(0xBA, 5, 3)],
                  F.UnsignedScaled(3, 1, 1),
                  tags= {
                    "desc" : "The target number of phases in loop 1 is equal"
                             " to l1_p_active_target+1.",
                    "section" : "Power Saving",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("l2_p_active_target",
                  [(0xBA, 2, 2)],
                  F.UnsignedScaled(1, 1, 1),
                  tags= {
                    "desc" : "The target number of phases in loop 2 is equal"
                             " to l2_p_active_target+1.",
                    "section" : "Power Saving",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("loop_1_vid_decode",
                  [(0xBA, 1, 1), (0xBB, 7, 0)],
                  F.Unsigned(9),
                  tags= {
                    "desc" : "Loop 1 decoded vid[8:0], with vid_offset.",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("loop_2_vid_decode",
                  [(0xBA, 0, 0), (0xBC, 7, 0)],
                  F.Unsigned(9),
                  tags= {
                    "desc" : "Loop 2 decoded vid[8:0], with vid_offset.",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xBD
        I2C.Field("svid_data_in_byte",
                  [(0xBE, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "Shows snapshot of SVID/AMD SVI2 byte sequence."
                             " dig_tb_sel will select between bytes 1,2,3.",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        I2C.Field("usr_crc",
                  [(0xBF, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "CRC code for the user section. (1+x^1+x^2+x^8).",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xC0
        # XXX - not listed in tool, format unknown
        I2C.Field("vboot_val",
                  [(0xC1, 7, 4)],
                  F.Raw(),
                  tags= {
                    "desc" : "value of the vboot determined from external"
                             " resistor.",
                    "section" : "DFT",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xC1, 3, 0
        # 0xC2
        I2C.Field("pxi_adj_cnt",
                  [(0xC3, 7, 1)],
                  F.SignedScaled(7, 0.001),
                  tags= {
                    "desc" : "low speed phase balance adjust count for the"
                             " phase indicated by pxi_sel. It is signed 2s"
                             " complement. It is added to the dutycycle."
                             " LSB is 0.1% of switching period.",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        # XXX - format
        I2C.Field("pxi_code_ref",
                  [(0xC3, 0, 0), (0xC4, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "The offset or code reference for the current"
                             " of the phase indicated by pxi_sel.",
                    "section" : "Debug",
                  },
                  writeable= False,
                  volatile=True
        ),
        # XXX - format
        I2C.Field("minmax_val",
                  [(0xC5, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "Value of the minimum or maximum value of the"
                             " quantity selected by minmax_sel[4:0].  Units"
                             " of the quantities: loop_1_current: 2 A/bit,"
                             " loop_2_current: 0.5 A/bit,"
                             " loop_1_input_current: 1/8 A/bit ,"
                             " loop_2_input_current: 1/16 A/bit,"
                             " loop_x_output_voltage: 1/64 V/bit,"
                             " input_voltage: 1/8 V/bit, tempx: 1 C/bit.",
                    "section" : "Telemetry",
                  },
                  writeable= False,
                  volatile=True
        ),
        # 0xC6 .. CF
        # 0xCE-0xF9 are volatile running-configuration registers
        I2C.Field("otp_command",
                  [(0xD0, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "Command to the OTP manager. rst:00000000",
                    "section" : "MTP Programming",
                  },
                  volatile= True
        ),
        # 0xD1 .. 0xD7
        # 0xD8, 7, 4
        I2C.Field("soft_reset",
                  [(0xD8, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "When set to 1, a soft reset occurs on the"
                             " chip and the OTP is reloaded. This bit clears"
                             " automatically. rst:0",
                    "section" : "Personality",
                  },
                  volatile= True
        ),
        # 0xD8, 2, 0
        # 0xD9 .. 0xDD
        # 0xDE, 7, 6
        I2C.Field("auto_increment",
                  [(0xDE, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "1=pointer for muxed_ro_reg1 auto-increments"
                             " with every read. 0=pointer is ro_sel1. rst:0",
                    "section" : "Telemetry",
                  },
                  volatile= True
        ),
        I2C.Field("ro_sel1",
                  [(0xDE, 4, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "muxes 1 of 32 read only registers into"
                             " muxed_ro_reg1. A pointer auto-increments for"
                             " subsequent reads. rst:00000",
                    "section" : "Telemetry",
                  },
                  volatile= True
        ),
        # 0xDF .. 0xE0
        I2C.Field("loop_1_mfr_status_clear",
                  [(0xE1, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 clears mfr_status for loop 1. rst:0",
                    "section" : "Faults",
                  },
                  readable= False,
        ),
        I2C.Field("loop_2_mfr_status_clear",
                  [(0xE1, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 clears mfr_status for loop 2. rst:0",
                    "section" : "Faults",
                  },
                  readable= False,
        ),
        I2C.Field("loop_1_temp_status_clear",
                  [(0xE1, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 clears temp_status for loop 2. rst:0",
                    "section" : "Faults",
                  },
                  readable= False,
        ),
        I2C.Field("loop_2_temp_status_clear",
                  [(0xE1, 4, 4)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 clears temp_status for loop 2. rst:0",
                    "section" : "Faults",
                  },
                  readable= False,
        ),
        I2C.Field("loop_1_status_clear",
                  [(0xE1, 3, 3)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 clears status_word for loop 1. rst:0",
                    "section" : "Faults",
                  },
                  readable= False,
        ),
        I2C.Field("loop_2_status_clear",
                  [(0xE1, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 clears status_word for loop 2. rst:0",
                    "section" : "Faults",
                  },
                  readable= False,
        ),
        I2C.Field("loop_1_clear_sticky_codes",
                  [(0xE1, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 clears the sticky failure codes for"
                             " loop 1. rst:0",
                    "section" : "Faults",
                  },
                  readable= False,
        ),
        I2C.Field("loop_2_clear_sticky_codes",
                  [(0xE1, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 clears the sticky failure codes for"
                             " loop 2. rst:0",
                    "section" : "Faults",
                  },
                  readable= False,
        ),
        # 0xE2, 7, 3
        I2C.Field("pxi_sel",
                  [(0xE2, 2, 0)],
                  F.Unsigned(3),
                  tags= {
                    "desc" : "Selects the phase for which the user needs"
                             " reference code/phase current. rst:000",
                    "section" : "Telemetry",
                  },
        ),
        # 0xE2, 7, 5
        # XXX - format
        I2C.Field("minmax_sel",
                  [(0xE3, 4, 0)],
                  F.Unsigned(5),
                  tags= {
                    "desc" : "Select for minimum or maximum quantity.\n"
                             "0: loop_1_current_min;\n"
                             "1: loop_1_current_max;\n"
                             "2: loop_2_current_min;\n"
                             "3: loop_2_current_max;\n"
                             "4: loop_1_input_current_min;\n"
                             "5: loop_1_input_current_max;\n"
                             "6: loop_2_input_current_min;\n"
                             "7: loop_2_input_current_max;\n"
                             "8: loop_1_output_voltage_min;\n"
                             "9: loop_1_output_voltage_max;\n"
                             "10: loop_2_output_voltage_min;\n"
                             "11: loop_2_output_voltage_max;\n"
                             "12: input_voltage_min;\n"
                             "13: input_voltage_max;\n"
                             "14: temp1_min;\n"
                             "15: temp1_max;\n"
                             "16: temp2_min;\n"
                             "default: temp2_max;\n"
                             "rst:00000",
                    "section" : "Telemetry",
                  },
        ),
        # 0xE4 .. 0xE5
        # 0xE6, 7, 3
        I2C.Field("vmax_lock",
                  [(0xE6, 2, 2)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 locks the maximum output voltage!",
                   "section" : "Manual VID",
                  },
        ),
        I2C.Field("i2c_lock",
                  [(0xE6, 1, 1)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 locks i2c_address and i2c_enable"
                             " registers!",
                    "section" : "I2C/PMBus/SVID Address",
                  },
        ),
        I2C.Field("address_lock",
                  [(0xE6, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 locks the SMBus and SVID address"
                             " registers!",
                    "section" : "I2C/PMBus/SVID Address",
                  },
        ),
        # 0xE7, 7, 1
        # XXX - not listed in tool.
        I2C.Field("ir_lock",
                  [(0xE7, 0, 0)],
                  F.Boolean,
                  tags= {
                    "desc" : "A logic 1 locks the trim registers. rst:1",
                    "section" : "Personality",
                  },
        ),
        # 0xE8 .. 0xE9
        # 0xEA, 7, 5
        I2C.Field("loop_1_rll",
                  [(0xEA, 4, 0)],
                  F.Tabular("u5", loop_X_rll_table),
                  tags= {
                    "desc" : "adjust to loadline for loop 1.",
                    "section" : "Loadline",
                  },
        ),
        I2C.Field("clear_phase_faults",
                  [(0xEB, 7, 7)],
                  F.Boolean,
                  tags= {
                    "desc" : "Clear all phase current faults for the 2"
                             " loops.  rst:0",
                    "section" : "Debug",
                  },
        ),
        I2C.Field("loop_1_rll_override",
                  [(0xEB, 6, 6)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable the loop_1_rll registers.",
                    "section" : "Loadline",
                  },
        ),
        I2C.Field("loop_2_rll_override",
                  [(0xEB, 5, 5)],
                  F.Boolean,
                  tags= {
                    "desc" : "Enable the loop_2_rll registers.",
                    "section" : "Loadline",
                  },
        ),
        I2C.Field("loop_2_rll",
                  [(0xEB, 4, 0)],
                  F.Tabular("u5", loop_X_rll_table),
                  tags= {
                    "desc" : "adjust to loadline for loop 2.",
                    "section" : "Loadline",
                  },
        ),
        I2C.Field("usr_try_password",
                  [(0xED, 7, 0), (0xEC, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "16 bit password for the usr section."
                             " rst:00000000",
                    "section" : "Security",
                  },
        ),
        I2C.Field("trim_try_password",
                  [(0xEF, 7, 0), (0xEE, 7, 0)],
                  F.Raw(),
                  tags= {
                    "desc" : "16 bit password for the trim section."
                             " rst:00000000",
                    "section" : "Security",
                  },
        ),
        # 0xF0 .. 0xF9
        I2C.Field("silicon_ver",
                  [(0xFA, 7, 0)],
                  F.Unsigned(8),
                  tags= {
                    "desc" : "The silicon version",
                    "section" : "Personality",
                  },
                  writeable= False,
                  volatile= False
        ),
        I2C.Field("product_id",
                  [(0xFB, 7, 0)],
                  F.Unsigned(8),
                  tags= {
                    "desc" : "The product ID. IR3590 0x2C, IR3595 0x27-0x2A.",
                    "section" : "Personality",
                  },
                  writeable= False,
                  volatile= False
        ),
        I2C.Field("mfr_id",
                  [(0xFC, 7, 0), (0xFD, 7, 0)],
                  F.ASCII(),
                  tags= {
                    "desc" : "The ASCII code for the manufacturer's ID. IR"
                             " = International Rectifier (0x4952)",
                    "section" : "Personality",
                  },
                  writeable= False,
                  volatile= False
        ),
        # 0xFE .. 0xFF
    ]

    def __init__(self, bus, address, skip_loops=True, *args, **kwargs):
        super(IR3581, self).__init__(bus, address, *args, **kwargs)

        if not skip_loops:
            loop1_addr= self.read('loop_1_pmbus_addr')
            loop2_addr= self.read('loop_2_pmbus_addr')

            self.loop1= IR3581_Loop(bus, loop1_addr)
            self.dev_logger.info("IR3581 loop 1 PMBus interface at 0x%02x",
                                loop1_addr)
            self.loop2= IR3581_Loop(bus, loop2_addr)
            self.dev_logger.info("IR3581 loop 2 PMBus interface at 0x%02x",
                                loop2_addr)

    def probe(self):
        try:
            silicon_ver= self.read('silicon_ver')
            product_id=  self.read('product_id')
            mfr_id=      self.read('mfr_id')

            if mfr_id != "IR":
                self.dev_logger.warning("Manufacturer ID mismatch. "
                    "Got \"%s\", expected \"IR\"." % mfr_id)
                return False

            if product_id != 0x48:
                self.dev_logger.warning("Product ID mismatch. Got %d, "
                    "expected %d (IR3581)." % (product_id, 0x48))
                return False

            self.dev_logger.info("IR3581 I2C interface at 0x%02x, "
                "silicon version %d" % (self.address, silicon_ver))
            return True
        except I2C.I2CBus_Exception:
            self.dev_logger.warning("Failed to read device registers.")
            return False

    def clear_faults(self):
        pass

    def configure(self, cfg):
        # Updating the PMBus address requires special treatment.
        self.write('address_lock', False)
        self.write('loop_1_pmbus_addr', cfg["LOOP_1_PMBUS_ADDR"])
        self.write('loop_2_pmbus_addr', cfg["LOOP_2_PMBUS_ADDR"])
        self.write('address_lock', True)

        # Everything else is written as raw address/value pairs.
        for (address, value, mask) in cfg["RAW_REGISTERS"]:
          self.write_reg_raw(address, [value])

    def verify(self, registers):
        for (address, value, mask) in registers:
          rv = self.read_reg_raw(address)[0]
          assert rv & mask == value & mask

class IR3581_Dummy(I2C.I2CBus_RegisterDummy,IR3581):
    def __init__(self, bus, address, cfg_file=None, *args, **kwargs):
        initial_values= {}
        if not cfg_file is None:
            for line in open(cfg_file):
                _reg, _val, _mask= line.split(' ')
                R= int(_reg, 16)
                V= int(_val, 16)
                M= int(_mask, 16)

                initial_values[R]= bytearray([V & M])

        super(IR3581_Dummy, self).__init__(bus, address,
                                           initial_values=initial_values,
                                           *args, **kwargs)
