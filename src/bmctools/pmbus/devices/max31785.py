from time import sleep

from .. import i2cbus as I2C
from .. import smbus as SMB
from .. import pmbus as PMB
from .. import formats as F
from .. import pmbus_formats as PF
from .. import pmbus_commands as PC
from .. import controller as C
from .. import variables as V

# PWM frequency in Hz, encoded in 3b.  Table 26.
pwm_freq_table = {
    0 : 30.0,
    1 : 50.0,
    2 : 100.0,
    3 : 150.0,
    4 : None,
    5 : None,
    6 : None,
    7 : 25e3
}

# Temperature hysteresis in degrees C.  Table 26.
hys_table = {
    0 : 2.0,
    1 : 4.0,
    2 : 6.0,
    3 : 8.0,
}

# PWM update rate in ms, PWM step size in % and 40%-100% ramp time in s,
# encoded in 3b.  Table 26.
ramp_rate_table = {
    0 : (1000.0, 1.0, 60.0),
    1 : (1000.0, 2.0, 30.0),
    2 : (1000.0, 3.0, 20.0),
    3 : ( 200.0, 1.0, 12.0),
    4 : ( 200.0, 2.0,  6.0),
    5 : ( 200.0, 3.0,  4.0),
    6 : ( 200.0, 4.0,  3.0),
    7 : ( 200.0, 5.0,  2.4)
}

# Fan spinup revolutions.  Table 26.
spin_table = {
    0 : 0,
    1 : 2,
    2 : 4,
    3 : 8,
}

# Fan health thresholds.  Table 22.
health_threshold_table = {
    0 : (10.0, 15.0, 15.0),
    1 : (10.0, 20.0, 20.0),
    2 : (15.0, 20.0, 20.0),
    3 : (15.0, 25.0, 25.0)
}

fan_health_table = {
    0 : "RED",
    1 : "ORANGE",
    2 : "YELLOW",
    3 : "GREEN"
}

mfr_status_fmt = \
    F.Flags("p1b1b1b1p4", ["OT_WARN", "OT_FAULT", "WATCHDOG"])

def check_lut(lut):
    if len(lut) != 8:
        raise PC.PMBus_Config_Error("Fan LUT must have 8 entries.")

    temp, speed, unit= lut[0]
    for temp_1, speed_1, unit_1 in lut[1:]:
        if temp_1 < temp:
            raise PC.PMBus_Config_Error("Temperature steps not monotonic")
        if speed_1 < speed:
            raise PC.PMBus_Config_Error("Speed steps not monotonic")
        if unit_1 != unit:
            raise PC.PMBus_Config_Error("Mixed RPM and PWM")

coeff_V=     {"m" :     1, "b" : 0, "R" : 0}
coeff_Scale= {"m" : 32767, "b" : 0, "R" : 0}
coeff_T=     {"m" :     1, "b" : 0, "R" : 2}
coeff_RPM=   {"m" :     1, "b" : 0, "R" : 0}
coeff_Duty=  {"m" :     1, "b" : 0, "R" : 2}

# Fan command format, either RPM or Duty Cycle (%).  This is determined by the
# settings for Fan 1 on the appropriate page.
fan_fmt= PF.FanControl(
    pwm_fmt = PF.Direct(m = coeff_Duty["m"],
                        b = coeff_Duty["b"],
                        R = coeff_Duty["R"]),
    rpm_fmt = PF.Direct(m = coeff_RPM["m"],
                        b = coeff_RPM["b"],
                        R = coeff_RPM["R"]),
    ctrl_reg = ("FAN_CONFIG_1_2", None),
    rpm_field = "FANA_RPM"
)

# Fan comparator (alert) format: percentage of commanded value for RPM
# control, RPM for PWM control i.e. inverted w.r.t. fan_fmt.
fan_cmp_fmt= PF.FanControl(
    pwm_fmt = PF.Direct(m = coeff_RPM["m"],
                        b = coeff_RPM["b"],
                        R = coeff_RPM["R"]),
    rpm_fmt = PF.Direct(m = coeff_Duty["m"],
                        b = coeff_Duty["b"],
                        R = coeff_Duty["R"]),
    ctrl_reg = ("FAN_CONFIG_1_2", None),
    rpm_field = "FANA_RPM"
)

# The OT_WARN and OT_FAULT flags are in STATUS_MFR_SPECIFIC, rather than
# STATUS_TEMPERATURE.
temp_comparators= {
    "FAULT_HIGH" : (
        "OT_FAULT_LIMIT",
        ("STATUS_MFR_SPECIFIC", "OT_FAULT")
    ),
    "WARN_HIGH"  : (
        "OT_WARN_LIMIT",
        ("STATUS_MFR_SPECIFIC", "OT_WARN")
    )
}

# There is no PMBus-standard location for fan fault thresholds.
fan_comparators= {
    "FAULT_LOW"  : (
        "MFR_FAN_FAULT_LIMIT",
        ("STATUS_FANS_1_2", "FAN_1_FAULT")
    ),
    "WARN_LOW"   : (
        "MFR_FAN_WARN_LIMIT",
        ("STATUS_FANS_1_2", "FAN_1_WARNING")
    )
}

class MAX31785(PMB.PMBus_Device):
    """A Maxim MAX31785 6-Channel Intelligent Fan Controller.

    References are to the MAX31785 datasheet revision 3 (document number
    19-5703, August 2012)."""

    _supported_commands = [
        "PAGE", "CLEAR_FAULTS", "WRITE_PROTECT", "STORE_DEFAULT_ALL",
        "RESTORE_DEFAULT_ALL", "CAPABILITY", "VOUT_MODE", "VOUT_SCALE_MONITOR",
        "FAN_CONFIG_1_2", "FAN_COMMAND_1", "VOUT_OV_FAULT_LIMIT",
        "VOUT_OV_WARN_LIMIT", "VOUT_UV_WARN_LIMIT", "VOUT_UV_FAULT_LIMIT",
        "OT_FAULT_LIMIT", "OT_WARN_LIMIT", "STATUS_BYTE", "STATUS_WORD",
        "STATUS_VOUT", "STATUS_CML", "STATUS_MFR_SPECIFIC", "STATUS_FANS_1_2",
        "READ_VOUT", "READ_TEMPERATURE_1", "READ_FAN_SPEED_1",
        "PMBUS_REVISION", "MFR_ID", "MFR_MODEL", "MFR_REVISION",
    ]

    _command_overrides = {
        "VOUT_MODE"              : {
            "writeable" : False,
        },
        "VOUT_SCALE_MONITOR"     : {
            "pages" : PC.page_range(17,22),
            "paged" : True,
        },
        "FAN_CONFIG_1_2"         : {
            "pages" : PC.page_range(0,5),
            "paged" : True,
        },
        "FAN_COMMAND_1"          : {
            "fmt"   : fan_fmt,
            "pages" : PC.page_range(0,5),
            "paged" : True,
        },
        "VOUT_OV_FAULT_LIMIT"    : {
            "pages" : PC.page_range(17,22),
            "paged" : True,
        },
        "VOUT_OV_WARN_LIMIT"     : {
            "pages" : PC.page_range(17,22),
            "paged" : True,
        },
        "VOUT_UV_WARN_LIMIT"     : {
            "pages" : PC.page_range(17,22),
            "paged" : True,
        },
        "VOUT_UV_FAULT_LIMIT"    : {
            "pages" : PC.page_range(17,22),
            "paged" : True,
        },
        "OT_FAULT_LIMIT"         : {
            "pages" : PC.page_range(6,16),
            "paged" : True,
        },
        "OT_WARN_LIMIT"          : {
            "pages" : PC.page_range(6,16),
            "paged" : True,
        },
        "STATUS_BYTE"            : {
            "writeable" : False,
        },
        "STATUS_WORD"            : {
            "writeable" : False,
        },
        "STATUS_VOUT"            : {
            "pages" : PC.page_range(17,22),
            "paged" : True,
            "writeable" : False,
        },
        "STATUS_CML"             : {
            "writeable" : False,
        },
        "STATUS_MFR_SPECIFIC"    : {
            "pages" : PC.page_range(6,16),
            "paged" : True,
            "writeable" : False,
            "fmt" : mfr_status_fmt,
        },
        "STATUS_FANS_1_2"        : {
            "pages" : PC.page_range(0,5),
            "paged" : True,
            "writeable" : False,
        },
        "READ_VOUT"              : {
            "pages" : PC.page_range(17,22),
            "paged" : True,
        },
        "READ_TEMPERATURE_1"     : {
            "pages" : PC.page_range(6,16),
            "paged" : True,
        },
        "READ_FAN_SPEED_1"       : {
            "pages" : PC.page_range(0,5),
            "paged" : True,
        },
        # These are read byte/read word, rather than read block (as in the
        # PMBus specification).
        "MFR_ID"                 : {
            "fmt"   : F.Raw(8),
            "block" : False,
        },
        "MFR_MODEL"              : {
            "fmt"   : F.Raw(8),
            "block" : False,
        },
        "MFR_REVISION"           : {
            "fmt"   : F.Raw(16),
            "block" : False,
        },
        # The MAX31785 violates the PMBus and SMBus specifications by not
        # transmitting the length before the block data.
        "MFR_LOCATION"           : {
            "fmt"       : F.Raw(64),
            "compliant" : False,
        },
        "MFR_DATE"               : {
            "fmt"       : F.Raw(64),
            "compliant" : False,
        },
        "MFR_SERIAL"             : {
            "fmt"       : F.Raw(64),
            "compliant" : False,
        },
    }

    _mfr_commands = [
        PC.PRM(0xD1, "MFR_MODE",
            F.Bitfield(
                "b1b1b1p1b1p3r2b1b1b1b1b1b1",
                ["FORCE_NV_FAULT_LOG", "CLEAR_NV_FAULT_LOG", "ALERT",
                 "SOFT_RESET", "FAN_HEALTH_CRITERIA", "ADC5_ENABLE",
                 "ADC4_ENABLE", "ADC3_ENABLE", "ADC2_ENABLE", "ADC1_ENABLE",
                 "ADC0_ENABLE"], {
                    "FAN_HEALTH_CRITERIA" :
                        F.Tabular("u2", health_threshold_table),
                }
            ),
            # The force/clear log bits are reset by the device when operation completes
            volatile=True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xD4, "MFR_VOUT_PEAK",
            PF.Direct(),
            volatile=True,
            pages= PC.page_range(17,22),
            paged= True
        ),
        PC.PRM(0xD6, "MFR_TEMPERATURE_PEAK",
            PF.Direct(),
            volatile=True,
            pages= PC.page_range(6,16),
            paged= True
        ),
        PC.PRM(0xD7, "MFR_VOUT_MIN",
            PF.Direct(),
            volatile=True,
            pages= PC.page_range(17,22),
            paged= True
        ),
        # A 254B nonvolatile fault log from the MAX31785.
        #
        # All coefficients are hardcoded to ensure that the log can be read
        # without reference to an active device, and that its interpretation
        # does not change.
        #
        # See Table 24, p38.
        PC.PRM(0xD9, "MFR_FAULT_RESPONSE",
            F.Bitfield(
                "b1b1b1p2b1b1b1",
                ["NV_LOG", "NV_LOG_OV", "UV_LOG_FILTER", "FAULT_PIN_ENABLE_OV",
                 "FAULT_PIN_ENABLE", "FAULT_PIN_MONITOR"]
            ),
            pages= PC.page_range(0,22),
            paged= True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xDC, "MFR_NV_FAULT_LOG",
            F.Bitfield(
                # Note that this has to be in MSB first order
                "u8r176r96r96r96r96r768p8u8p32r96"
                "r176r96r48r96r48r16r8r8u32u16u8p8",
                 ["LOG_VALID", "READ_TEMPERATURE_1", "MFR_FAN_PWM_AVG",
                  "MFR_FAN_RUN_TIME", "MFR_READ_FAN_PWM", "READ_FAN_SPEED_1",
                  "READ_VOUT", "VOLTAGE_INDEX", "MFR_VOUT_MIN",
                  "MFR_TEMPERATURE_PEAK", "MFR_VOUT_PEAK", "STATUS_FANS_1_2",
                  "STATUS_MFR_SPECIFIC", "STATUS_VOUT", "STATUS_WORD",
                  "STATUS_CML", "STATUS_BYTE", "MFR_TIME_COUNT",
                  "FAULT_LOG_COUNT", "FAULT_LOG_INDEX"], {
                    "STATUS_BYTE" : PC.command_by_name["STATUS_BYTE"].fmt,
                    "STATUS_CML" : PC.command_by_name["STATUS_CML"].fmt,
                    "STATUS_WORD" : PC.command_by_name["STATUS_WORD"].fmt,
                    "STATUS_VOUT" : F.Array(
                        6, "r8", PC.command_by_name["STATUS_VOUT"].fmt
                    ),
                    "STATUS_MFR_SPECIFIC" : F.Array(
                        12, "r8", mfr_status_fmt
                    ),
                    "STATUS_FANS_1_2" : F.Array(
                        6, "r8", PC.command_by_name["STATUS_FANS_1_2"].fmt
                    ),
                    "MFR_VOUT_PEAK": F.Array(
                        6, "r16", PF.Direct(**coeff_V)
                    ),
                    "MFR_TEMPERATURE_PEAK": F.Array(
                        11, "r16", PF.Direct(**coeff_T)
                    ),
                    "MFR_VOUT_MIN": F.Array(
                        6, "r16", PF.Direct(**coeff_V)
                    ),
                    "READ_VOUT" : F.Array(
                        8, "r96", F.Array(
                            6, "r16", PF.Direct(**coeff_V)
                        )
                    ),
                    "READ_FAN_SPEED_1" : F.Array(
                        6, "r16", PF.Direct(**coeff_RPM)
                    ),
                    "MFR_READ_FAN_PWM" : F.Array(
                        6, "r16", PF.Direct(**coeff_Duty))
                    ,
                    "MFR_FAN_RUN_TIME" : F.Array(
                        6, "r16", F.Unsigned(16)
                    ),
                    "MFR_FAN_PWM_AVG" : F.Array(
                        6, "r16", PF.Direct(**coeff_Duty)
                    ),
                    "READ_TEMPERATURE_1" : F.Array(
                        11, "r16", PF.Direct(**coeff_T)
                    ),
                }
            ),
            writeable=False,
            volatile=True,
            block=True,
            compliant=False
        ),
        PC.PRM(0xDD, "MFR_TIME_COUNT",
            F.Unsigned(32),
            volatile=True,
            block=True,
            compliant=False
        ),
        PC.PRM(0xF0, "MFR_TEMP_SENSOR_CONFIG",
            F.Bitfield(
                "b1u5p4b1b1b1b1b1b1",
                ["ENABLE", "OFFSET", "FAN5", "FAN4", "FAN3", "FAN2", "FAN1",
                 "FAN0"]
            ),
            pages= PC.page_range(6,16),
            paged= True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xF1, "MFR_FAN_CONFIG",
            F.Bitfield(
                "r3b1r2b1b1r3b1b1b1r2",
                ["FREQ", "DUAL_TACH", "HYS", "TSFO", "TACHO", "RAMP", "HEALTH",
                 "ROTOR_HI_LO", "ROTOR", "SPIN"], {
                    "FREQ" : F.Tabular("u3", pwm_freq_table),
                    "HYS"  : F.Tabular("u2", hys_table),
                    "RAMP" : F.Tabular("u3", ramp_rate_table),
                    "SPIN" : F.Tabular("u2", spin_table),
                }
            ),
            pages= PC.page_range(0,6),
            paged= True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xF2, "MFR_FAN_LUT",
            F.Array(
                8, "r32", F.Bitfield(
                    "r16r16", [ "SPEED_STEP", "TEMP_STEP" ], {
                        "TEMP_STEP" : PF.Direct(m = coeff_T["m"],
                                                          b = coeff_T["b"],
                                                          R = coeff_T["R"]),
                        "SPEED_STEP" : fan_fmt,
                    }
                )
            ),
            block=True,
            compliant=False,
            pages= PC.page_range(0,6),
            paged= True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xF3, "MFR_READ_FAN_PWM",
            PF.Direct(),
            writeable= False,
            volatile= True,
            pages= PC.page_range(0,6),
            paged= True
        ),
        PC.PRM(0xF5, "MFR_FAN_FAULT_LIMIT",
            fan_cmp_fmt,
            pages= PC.page_range(0,6),
            paged= True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xF6, "MFR_FAN_WARN_LIMIT",
            fan_cmp_fmt,
            pages= PC.page_range(0,6),
            paged= True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xF7, "MFR_FAN_RUN_TIME",
            F.Unsigned(16),
            pages= PC.page_range(0,6),
            volatile= True,
            paged= True
        ),
        PC.PRM(0xF8, "MFR_FAN_PWM_AVG",
            PF.Direct(),
            pages= PC.page_range(0,6),
            volatile= True,
            paged= True
        ),
        PC.PRM(0xF9, "MFR_FAN_PWM2RPM",
            F.Bitfield(
                "r16r16r16r16",
                [100, 80, 60, 40], {
                    100 : PF.Direct(),
                     80 : PF.Direct(),
                     60 : PF.Direct(),
                     40 : PF.Direct(),
                }
            ),
            block=True,
            compliant=False,
            pages= PC.page_range(0,6),
            paged= True,
            tags= {
              "persistent_cfg" : True,
            }
        ),
    ]

    _fixed_values = {
        "COEFFICIENTS" : {
            "VOUT_OV_FAULT_LIMIT"  : {True : coeff_V,     False : coeff_V},
            "VOUT_OV_WARN_LIMIT"   : {True : coeff_V,     False : coeff_V},
            "VOUT_UV_WARN_LIMIT"   : {True : coeff_V,     False : coeff_V},
            "VOUT_UV_FAULT_LIMIT"  : {True : coeff_V,     False : coeff_V},
            "READ_VOUT"            : {True : coeff_V},
            "MFR_VOUT_PEAK"        : {True : coeff_V,     False : coeff_V},
            "MFR_VOUT_MIN"         : {True : coeff_V,     False : coeff_V},
            "VOUT_SCALE_MONITOR"   : {True : coeff_Scale, False : coeff_Scale},
            "OT_FAULT_LIMIT"       : {True : coeff_T,     False : coeff_T},
            "OT_WARN_LIMIT"        : {True : coeff_T,     False : coeff_T},
            "READ_TEMPERATURE_1"   : {True : coeff_T},
            "MFR_TEMPERATURE_PEAK" : {True : coeff_T,     False : coeff_T},
            "READ_FAN_SPEED_1"     : {True : coeff_RPM},
            "READ_FAN_PWM"         : {True : coeff_Duty},
            "MFR_FAN_PWM_AVG"      : {True : coeff_Duty},
            "MFR_READ_FAN_PWM"     : {True : coeff_Duty,  False : coeff_Duty},
            "MFR_FAN_PWM2RPM"      : {True : coeff_RPM,   False : coeff_RPM},
        },
        "QUERY" : {
            "VOUT_SCALE_MONITOR"  : {
                True  : {"FORMAT" : "DIRECT"},
                False : {"FORMAT" : "DIRECT"}
            },
            "VOUT_OV_FAULT_LIMIT" : {
                True  : {"FORMAT" : "DIRECT"},
                False : {"FORMAT" : "DIRECT"}
            },
            "VOUT_OV_WARN_LIMIT"  : {
                True  : {"FORMAT" : "DIRECT"},
                False : {"FORMAT" : "DIRECT"}
            },
            "VOUT_UV_FAULT_LIMIT" : {
                True  : {"FORMAT" : "DIRECT"},
                False : {"FORMAT" : "DIRECT"}
            },
            "VOUT_UV_WARN_LIMIT"  : {
                True  : {"FORMAT" : "DIRECT"},
                False : {"FORMAT" : "DIRECT"}
            },
            "OT_FAULT_LIMIT"      : {
                True  : {"FORMAT" : "DIRECT"},
                False : {"FORMAT" : "DIRECT"}
            },
            "OT_WARN_LIMIT"       : {
                True  : {"FORMAT" : "DIRECT"},
                False : {"FORMAT" : "DIRECT"}
            },
            "READ_TEMPERATURE_1"  : {
                True  : {"FORMAT" : "DIRECT"},
                False : {"FORMAT" : "DIRECT"}
            },
            "READ_FAN_SPEED_1"    : {
                True  : {"FORMAT" : "DIRECT"},
                False : {"FORMAT" : "DIRECT"}
            },
        }
    }

    _monitor_specs = {
        "FAN_0_PWM": {
            "key" : ("MFR_READ_FAN_PWM", 0),
            "comparators" : fan_comparators
        },
        "FAN_1_PWM": {
            "key" : ("MFR_READ_FAN_PWM", 1),
            "comparators" : fan_comparators
        },
        "FAN_2_PWM": {
            "key" : ("MFR_READ_FAN_PWM", 2),
            "comparators" : fan_comparators
        },
        "FAN_3_PWM": {
            "key" : ("MFR_READ_FAN_PWM", 3),
            "comparators" : fan_comparators
        },
        "FAN_4_PWM": {
            "key" : ("MFR_READ_FAN_PWM", 4),
            "comparators" : fan_comparators
        },
        "FAN_5_PWM": {
            "key" : ("MFR_READ_FAN_PWM", 5),
            "comparators" : fan_comparators
        },
    }

    _monitor_overrides = {
        ("TEMPERATURE_1",  6) : {
            "conflicts" : [("VOUT", 17)],
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1",  7) : {
            "conflicts" : [("VOUT", 18)],
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1",  8) : {
            "conflicts" : [("VOUT", 19)],
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1",  9) : {
            "conflicts" : [("VOUT", 20)],
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1", 10) : {
            "conflicts" : [("VOUT", 21)],
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1", 11) : {
            "conflicts" : [("VOUT", 22)],
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1", 12) : {
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1", 13) : {
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1", 14) : {
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1", 15) : {
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("TEMPERATURE_1", 16) : {
            "interval"  : 1.0,
            "comparators" : temp_comparators,
        },
        ("FAN_SPEED_1", 0) : {
            "interval"  : 1.0,
            "comparators" : fan_comparators,
        },
        ("FAN_SPEED_1", 1) : {
            "interval"  : 1.0,
            "comparators" : fan_comparators,
        },
        ("FAN_SPEED_1", 2) : {
            "interval"  : 1.0,
            "comparators" : fan_comparators,
        },
        ("FAN_SPEED_1", 3) : {
            "interval"  : 1.0,
            "comparators" : fan_comparators,
        },
        ("FAN_SPEED_1", 4) : {
            "interval"  : 1.0,
            "comparators" : fan_comparators,
        },
        ("FAN_SPEED_1", 5) : {
            "interval"  : 1.0,
            "comparators" : fan_comparators,
        },
        ("VOUT",          17) : {
            "conflicts" : [("TEMPERATURE_1",  6)],
            "interval"  : 10e-3,
        },
        ("VOUT",          18)  : {
            "conflicts" : [("TEMPERATURE_1",  7)],
            "interval"  : 10e-3,
        },
        ("VOUT",          19)  : {
            "conflicts" : [("TEMPERATURE_1",  8)],
            "interval"  : 10e-3,
        },
        ("VOUT",          20)  : {
            "conflicts" : [("TEMPERATURE_1",  9)],
            "interval"  : 10e-3,
        },
        ("VOUT",          21)  : {
            "conflicts" : [("TEMPERATURE_1", 10)],
            "interval"  : 10e-3,
        },
        ("VOUT",          22)  : {
            "conflicts" : [("TEMPERATURE_1", 11)],
            "interval"  : 10e-3,
        },
    }

    _monitor_aliases = {
        "FAN_0_RPM"  : ("FAN_SPEED_1",    0),
        "FAN_1_RPM"  : ("FAN_SPEED_1",    1),
        "FAN_2_RPM"  : ("FAN_SPEED_1",    2),
        "FAN_3_RPM"  : ("FAN_SPEED_1",    3),
        "FAN_4_RPM"  : ("FAN_SPEED_1",    4),
        "FAN_5_RPM"  : ("FAN_SPEED_1",    5),
        "T_DIODE_0"  : ("TEMPERATURE_1",  6),
        "T_DIODE_1"  : ("TEMPERATURE_1",  7),
        "T_DIODE_2"  : ("TEMPERATURE_1",  8),
        "T_DIODE_3"  : ("TEMPERATURE_1",  9),
        "T_DIODE_4"  : ("TEMPERATURE_1", 10),
        "T_DIODE_5"  : ("TEMPERATURE_1", 11),
        "T_INTERNAL" : ("TEMPERATURE_1", 12),
        "T_I2C_0"    : ("TEMPERATURE_1", 13),
        "T_I2C_1"    : ("TEMPERATURE_1", 14),
        "T_I2C_2"    : ("TEMPERATURE_1", 15),
        "T_I2C_3"    : ("TEMPERATURE_1", 16),
        "ADC_0"      : ("VOUT",          17),
        "ADC_1"      : ("VOUT",          18),
        "ADC_2"      : ("VOUT",          19),
        "ADC_3"      : ("VOUT",          20),
        "ADC_4"      : ("VOUT",          21),
        "ADC_5"      : ("VOUT",          22),
    }

    _fans= frozenset([
        "FAN_0_RPM", "FAN_0_PWM",
        "FAN_1_RPM", "FAN_1_PWM",
        "FAN_2_RPM", "FAN_2_PWM",
        "FAN_3_RPM", "FAN_3_PWM",
        "FAN_4_RPM", "FAN_4_PWM",
        "FAN_5_RPM", "FAN_5_PWM"
    ])
    _temps= frozenset(["T_DIODE_0", "T_DIODE_1", "T_DIODE_2",
                       "T_DIODE_3", "T_DIODE_4", "T_DIODE_5", "T_INTERNAL",
                       "T_I2C_0", "T_I2C_1", "T_I2C_2", "T_I2C_3"])
    _adcs= frozenset(["ADC_0", "ADC_1", "ADC_2", "ADC_3", "ADC_4", "ADC_5"])
    _adc_fields= {
        "ADC_0" : ("T_DIODE_0", "ADC0_ENABLE"),
        "ADC_1" : ("T_DIODE_1", "ADC1_ENABLE"),
        "ADC_2" : ("T_DIODE_2", "ADC2_ENABLE"),
        "ADC_3" : ("T_DIODE_3", "ADC3_ENABLE"),
        "ADC_4" : ("T_DIODE_4", "ADC4_ENABLE"),
        "ADC_5" : ("T_DIODE_5", "ADC5_ENABLE")
    }

    _control_overrides = {
        "FAN_0_PWM" : { "monitor" : "FAN_0_PWM" },
        "FAN_1_PWM" : { "monitor" : "FAN_1_PWM" },
        "FAN_2_PWM" : { "monitor" : "FAN_2_PWM" },
        "FAN_3_PWM" : { "monitor" : "FAN_3_PWM" },
        "FAN_4_PWM" : { "monitor" : "FAN_4_PWM" },
        "FAN_5_PWM" : { "monitor" : "FAN_5_PWM" }
    }

    _control_aliases = {
        "FAN_0_PWM" : ("FAN_PWM_1",    0),
        "FAN_0_RPM" : ("FAN_RPM_1",    0),
        "FAN_1_PWM" : ("FAN_PWM_1",    1),
        "FAN_1_RPM" : ("FAN_RPM_1",    1),
        "FAN_2_PWM" : ("FAN_PWM_1",    2),
        "FAN_2_RPM" : ("FAN_RPM_1",    2),
        "FAN_3_PWM" : ("FAN_PWM_1",    3),
        "FAN_3_RPM" : ("FAN_RPM_1",    3),
        "FAN_4_PWM" : ("FAN_PWM_1",    4),
        "FAN_4_RPM" : ("FAN_RPM_1",    4),
        "FAN_5_PWM" : ("FAN_PWM_1",    5),
        "FAN_5_RPM" : ("FAN_RPM_1",    5)
    }

    def __init__(self, *args, **kwargs):
        super(MAX31785, self).__init__(*args, **kwargs)

    def probe(self):
        try:
            mfr_id= self.read("MFR_ID").decode()
            mfr_model= self.read("MFR_MODEL").decode()
            if mfr_id != "M" or mfr_model != "S":
                self.dev_logger.warning(
                    "Unrecognised MFR_ID.MFR_MODEL: %s.%s", mfr_id, mfr_model)
                return False

            mfr_revision= self.read("MFR_REVISION")
            self.dev_logger.info(
                "MAX31785 revision 0x%02x%02x at 0x%02X",
                mfr_revision[0], mfr_revision[1], self.address)
            return True

        except I2C.I2CBus_Exception as e:
            self.dev_logger.warning("Failed to read device registers.")
            raise e

    def configure(self, extra_cfg):
        """Apply default configuration."""

        # Enable SMBALERT, disable all ADCs.
        mfr_mode= self.read("MFR_MODE")
        mfr_mode["ALERT"]= True
        mfr_mode["ADC5_ENABLE"]= False
        mfr_mode["ADC4_ENABLE"]= False
        mfr_mode["ADC3_ENABLE"]= False
        mfr_mode["ADC2_ENABLE"]= False
        mfr_mode["ADC1_ENABLE"]= False
        mfr_mode["ADC0_ENABLE"]= False
        self.write("MFR_MODE", mfr_mode)
        sleep(0.25)
        # "After this command is sent, another command should not be sent for
        # at least 250ms" - p.35

        # Disable logging, and fault pin assertion/monitoring.  Enable filter
        # for UV/OV events.
        for p in range(0,23):
            mfr_fr= self.read(("MFR_FAULT_RESPONSE", p))
            mfr_fr["NV_LOG"]= False
            if 17 <= p <= 22:
                mfr_fr["NV_LOG_OV"]= False
                mfr_fr["UV_OV_FILTER"]= True
                mfr_fr["FAULT_PIN_ENABLE_OV"]= False
            mfr_fr["FAULT_PIN_ENABLE"]= False
            if 0 <= p <= 5:
                mfr_fr["FAULT_PIN_MONITOR"]= False
            self.write(("MFR_FAULT_RESPONSE", p), mfr_fr)

        # Set fan defaults.
        for p in range(0,6):
            # Disabled, PWM control, 2 pulses per revolution
            fc= self.read(("FAN_CONFIG_1_2", p))
            fc["FANA_ENABLE"]= False
            fc["FANA_RPM"]= False
            fc["FANA_PULSES_PER_REV"]= 2
            self.write(("FAN_CONFIG_1_2", p), fc)

            mfr_fc= self.read(("MFR_FAN_CONFIG", p))
            # 25kHz PWM
            mfr_fc["FREQ"]= 25e3
            mfr_fc["DUAL_TACH"]= False
            # Don't ramp to 100% on fault
            mfr_fc["TSFO"]= False
            mfr_fc["TACHO"]= False
            # Ramp 10%/sec
            mfr_fc["RAMP"]= (200.0, 2.0, 6.0)
            mfr_fc["HEALTH"]= False
            # Use tacho input
            mfr_fc["ROTOR"]= False
            # Two revolution spinup
            mfr_fc["SPIN"]= 2
            self.write(("MFR_FAN_CONFIG", p), mfr_fc)

        # Set temp sensor defaults.
        for p in range(6,17):
            mfr_tsc= self.read(("MFR_TEMP_SENSOR_CONFIG", p))
            mfr_tsc["ENABLE"]= False
            mfr_tsc["OFFSET"]= 0
            # Don't use this to control fans.
            mfr_tsc["FAN5"]= False
            mfr_tsc["FAN4"]= False
            mfr_tsc["FAN3"]= False
            mfr_tsc["FAN2"]= False
            mfr_tsc["FAN1"]= False
            mfr_tsc["FAN0"]= False
            self.write(("MFR_TEMP_SENSOR_CONFIG", p), mfr_tsc)

        # Set ADC scale factor to 1.0
        for p in range(18,23):
            self.write(("VOUT_SCALE_MONITOR", p), 1.0)

        # Apply any additional configuration
        for name, value in extra_cfg.items():
            self.write(name, value)

    def is_monitor_enabled(self, M):
        if M.name in self._fans:
            return True # Always enabled
        elif M.name in self._temps:
            page= M.get_page()
            return self.read(("MFR_TEMP_SENSOR_CONFIG", page))["ENABLE"]
        elif M.name in self._adcs:
            (t_name, en_field)= self._adc_fields[M.name]
            p_temp= self.monitors[t_name].page
            en_adc= self.read("MFR_MODE")[en_field]
            en_temp= self.read(("MFR_TEMP_SENSOR_CONFIG", p_temp))["ENABLE"]
            # Temp sense overrides ADC
            return en_adc and not en_temp
        else:
            raise V.MonitorError("Unknown monitor: %s" % (M.name))

    def enable_monitor(self, M):
        if M.name in self._fans:
            pass # Always enabled
        elif M.name in self._temps:
            page= M.get_page()
            cfg= self.read(("MFR_TEMP_SENSOR_CONFIG", page))
            cfg["ENABLE"]= True
            self.write(("MFR_TEMP_SENSOR_CONFIG", page), cfg)
        elif M.name in self._adcs:
            en_field= self._adc_fields[M.name][1]
            mfr_mode= self.read("MFR_MODE")
            mfr_mode[en_field]= True
            self.write("MFR_MODE", mfr_mode)
        else:
            raise V.MonitorError("Unknown monitor: %s" % (M.name))

    def disable_monitor(self, M):
        if M.name in self._fans:
            pass # Always enabled
        elif M.name in self._temps:
            page= M.get_page()
            cfg= self.read(("MFR_TEMP_SENSOR_CONFIG", page))
            cfg["ENABLE"]= False
            self.write(("MFR_TEMP_SENSOR_CONFIG", page), cfg)
        elif M.name in self._adcs:
            en_field= self._adc_fields[M.name][1]
            mfr_mode= self.read("MFR_MODE")
            mfr_mode[en_field]= False
            self.write("MFR_MODE", mfr_mode)
        else:
            raise V.MonitorError("Unknown monitor: %s" % (M.name))

    # The MAX31785 has no alert mask.
    def read_alert_mask(self):
        alerts= []
        for M in self.monitors: alerts+= M.get_comparators()
        return alerts

    def enable_alert(self, M, cmp_type):
        pass

    def disable_alert(self, M, cmp_type):
        raise V.MonitorError("Alerts cannot be disabled on this device.")
