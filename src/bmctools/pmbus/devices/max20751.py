from .. import i2cbus as I2C
from .. import smbus as SMB
from .. import pmbus as PMB
from .. import formats as F
from .. import pmbus_formats as PF
from .. import pmbus_commands as PC
from .. import variables as V
from .. import controller as C

fsw_table = {
    0x0 : 300e3,
    0x1 : 350e3,
    0x2 : 400e3,
    0x3 : 450e3,
    0x4 : 500e3,
    0x5 : 600e3,
    0x6 : 700e3,
    0x7 : 800e3,
}

slew_rate_table = {
    0x0 : 1.25e3,
    0x1 : 2.5e3,
    0x2 : 5e3,
    0x3 : 0.5e3,
}

ocr_gain_table = {
    0x0 : 0,
    0x1 : 4,
    0x2 : 8,
    0x3 : 10,
}

ocs_ton_table = {
    0x0 : 0.65e-6,
    0x1 : 1.30e-6,
    0x2 : 1.90e-6,
    0x3 : 2.75e-6,
}

vout_fine_table = {
    0x0 :  3.75e-3,
    0x1 :  2.50e-3,
    0x2 :  1.25e-3,
    0x3 :  0.00,
    0x4 : -1.25e-3,
    0x5 : -2.50e-3,
    0x6 : -3.75e-3,
    0x7 : -5.00e-3,
}

fault_log_fmt = F.Flags(
    "b1b1b1b1p1b1p1b1",
    ["SLAVE_FAULT", "WDOF", "OCP_CORE", "VIN_UV", "OVP_UMB_CORE", "OVP_CORE"]
)

class MAX20751(PMB.PMBus_Device):
    """A Maxim MAX20751 Multiphase Master with PMBus Interface
    and Internal Buck Converter.

    References are to the MAX20751 datasheet revision 2 (document number
    19-7080, March 2015)."""

    _supported_commands = [
        "OPERATION", "ON_OFF_CONFIG", "CLEAR_FAULTS", "RESTORE_DEFAULT_ALL",
        "STORE_USER_ALL", "RESTORE_USER_ALL", "CAPABILITY", "QUERY",
        "SMBALERT_MASK", "VOUT_MODE", "VOUT_COMMAND", "VOUT_MAX",
        "VOUT_MARGIN_HIGH", "VOUT_MARGIN_LOW", "IOUT_CAL_GAIN",
        "IOUT_CAL_OFFSET", "VOUT_OV_WARN_LIMIT", "VOUT_UV_WARN_LIMIT",
        "VOUT_UV_FAULT_LIMIT", "VOUT_UV_FAULT_RESPONSE",
        "IOUT_OC_FAULT_RESPONSE", "IOUT_OC_WARN_LIMIT", "OT_FAULT_LIMIT",
        "OT_FAULT_RESPONSE", "OT_WARN_LIMIT", "UT_WARN_LIMIT",
        "VIN_OV_FAULT_LIMIT", "VIN_OV_FAULT_RESPONSE", "VIN_OV_WARN_LIMIT",
        "VIN_UV_WARN_LIMIT", "VIN_UV_FAULT_LIMIT", "VIN_UV_FAULT_RESPONSE",
        "POWER_GOOD_ON", "POWER_GOOD_OFF", "TON_DELAY", "TON_MAX_FAULT_LIMIT",
        "TON_MAX_FAULT_RESPONSE", "TOFF_DELAY", "STATUS_BYTE", "STATUS_WORD",
        "STATUS_VOUT", "STATUS_IOUT", "STATUS_INPUT", "STATUS_TEMPERATURE",
        "STATUS_CML", "STATUS_MFR_SPECIFIC", "READ_VIN", "READ_VOUT",
        "READ_IOUT", "READ_TEMPERATURE_1", "READ_POUT", "PMBUS_REVISION",
        "MFR_ID", "MFR_MODEL", "MFR_REVISION", "MFR_SERIAL"
    ]

    _command_overrides = {
        "VOUT_MODE"           : { "writeable" : False,   },
        "IOUT_CAL_OFFSET"     : { "fmt" : PF.Direct(),   },
        "IOUT_CAL_GAIN"       : { "fmt" : PF.Direct(),   },
        "IOUT_OC_WARN_LIMIT"  : {
            "fmt" : PF.Linear11(max_exp=-1, min_exp=-1),
        },
        "OT_FAULT_LIMIT"      : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "OT_WARN_LIMIT"       : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "UT_WARN_LIMIT"       : {
            "fmt" : PF.Linear11(max_exp= 0, min_exp= 0),
        },
        "VIN_OV_FAULT_LIMIT"  : {
            "fmt" : PF.Linear11(max_exp=-5, min_exp=-5),
        },
        "VIN_OV_WARN_LIMIT"   : {
            "fmt" : PF.Linear11(max_exp=-5, min_exp=-5),
        },
        "VIN_UV_WARN_LIMIT"   : {
            "fmt" : PF.Linear11(max_exp=-5, min_exp=-5),
        },
        "VIN_UV_FAULT_LIMIT"  : {
            "fmt" : PF.Linear11(max_exp=-5, min_exp=-5),
        },
        "TON_DELAY"           : { "fmt" : PF.Direct(),   },
        "TON_MAX_FAULT_LIMIT" : { "fmt" : PF.Direct(),   },
        "TOFF_DELAY"          : { "fmt" : PF.Direct(),   },
        "READ_VIN"            : { "fmt" : PF.Linear11(), },
        "READ_IOUT"           : { "fmt" : PF.Linear11(), },
        "READ_TEMPERATURE_1"  : { "fmt" : PF.Linear11(), },
        "READ_POUT"           : { "fmt" : PF.Linear11(), },
    }

    _mfr_commands = [
        PC.PRM(0xD1, "VIN_RATIO",
            PF.Linear11(),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xD6, "FSW",
            F.Tabular("p5u3", fsw_table),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xD7, "HARDWARE_FLAGS",
            F.Flags(
                "p7b1b1b1b1b1b1b1b1b1",
                ["VDDH_UVLOB", "SENSE_P_OPEN_CORE", "RREF", "R_MRAMP", "WDOF",
                 "SLAVE_FAULT", "SLAVE_POPULATION", "SLAVE_STARTUP", "PWM_OPEN"]
            ),
            writeable=False,
            volatile=True
        ),
        PC.CMD(0xDD, "STORE_USER_ALL_NUM",
            PC.RPC(F.Singleton("p5u3", "SNO"))
        ),
        PC.PRM(0xE2, "FAULT_LOG1",
            fault_log_fmt,
            writeable=False,
            volatile=True
        ),
        PC.PRM(0xE3, "FAULT_LOG2",
            fault_log_fmt,
            writeable=False,
            volatile=True
        ),
        PC.PRM(0xE4, "FAULT_LOG3",
            fault_log_fmt,
            writeable=False,
            volatile=True
        ),
        PC.PRM(0xE5, "FAULT_LOG4",
            fault_log_fmt,
            writeable=False,
            volatile=True
        ),
        PC.PRM(0xE6, "FAULT_LOG5",
            fault_log_fmt,
            writeable=False,
            volatile=True
        ),
        PC.CMD(0xE7, "CLEAR_FAULT_LOG",
            PC.RPC(F.Singleton("p7b1", "CLEAR"))
        ),
        PC.PRM(0xE8, "FIRMWARE_REVISION",
            F.Unsigned(8),
            writeable=False
        ),
        PC.PRM(0xEC, "VOUT_COMMAND_FINE",
            F.Tabular("p5u3", vout_fine_table),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xED, "VIN_CAL_OFFSET",
            PF.Direct(),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xEF, "SLEW_RATE",
            F.Tabular("p6u2", slew_rate_table),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xF1, "OCR_GAIN",
            F.Tabular("p6u2", ocr_gain_table),
            tags= {
              "persistent_cfg" : True,
            }
        ),
        PC.PRM(0xF2, "OCS_TON",
            F.Tabular("p6u2", ocs_ton_table),
            tags= {
              "persistent_cfg" : True,
            }
        ),
    ]

    _fixed_values= {
        "COEFFICIENTS" : {
            "IOUT_CAL_GAIN"       : {
                False : { "m" : 2, "b" : 0, "R" : 0 },
                True  : { "m" : 2, "b" : 0, "R" : 0 }
            },
            "IOUT_CAL_OFFSET"     : {
                False : { "m" : 2, "b" : 0, "R" : 0 },
                True  : { "m" : 2, "b" : 0, "R" : 0 }
            },
            "TON_DELAY"           : {
                False : { "m" : 500, "b" : 0x800, "R" : 0 },
                True  : { "m" : 500, "b" : 0x800, "R" : 0 }
            },
            "TON_MAX_FAULT_LIMIT" : {
                False : { "m" : 500, "b" : 0x800, "R" : 0 },
                True  : { "m" : 500, "b" : 0x800, "R" : 0 }
            },
            "TOFF_DELAY"          : {
                False : { "m" : 500, "b" : 0x800, "R" : 0 },
                True  : { "m" : 500, "b" : 0x800, "R" : 0 }
            },
            "VIN_CAL_OFFSET"      : {
                False : { "m" : 32, "b" : 0, "R" : 0 },
                True  : { "m" : 32, "b" : 0, "R" : 0 }
            },
        },
    }

    _monitor_aliases= {
        "T_POWERSTAGE" : "TEMPERATURE_1",
    }

    def __init__(self, *args, **kwargs):
        super(MAX20751, self).__init__(*args, **kwargs)

    def probe(self):
        try:
            mfr_id= self.read("MFR_ID").decode()
            if mfr_id != "VT":
                self.dev_logger.warning("Unrecognised MFR_ID: \"%s\"", mfr_id)
                return False

            mfr_model= self.read("MFR_MODEL")
            if len(mfr_model) != 1 or mfr_model[0] != 0x01:
                self.dev_logger.warning("Unrecognised MFR_MODEL: \"%s\"",
                                        mfr_model)
                return False

            return True
        except I2C.I2CBus_Exception:
            self.dev_logger.warning("Failed to read MFR_ID & MFR_MODEL.")
            return False

    def configure(self, extra_cfg):
        on_off_config = self.read('ON_OFF_CONFIG')
        if 'PIN_CTRL' in extra_cfg: on_off_config['PIN_CTRL'] = cfg['PIN_CTRL']
        if 'REG_CTRL' in extra_cfg: on_off_config['REG_CTRL'] = cfg['REG_CTRL']
        self.write('ON_OFF_CONFIG', on_off_config)

        # Apply any additional configuration
        for name, value in extra_cfg.items():
            self.write(name, value)
