from . import i2cbus
from . import smbus
from . import whitebait_smbus
from . import whitebait_i2cbus as wb_i2c
from . import pmbus

class Whitebait_PMBus(whitebait_smbus.Whitebait_SMBus,pmbus.PMBus):
    """A PMBus object is an extension of an SMBus object, definining a few
    additional protocol elements, and a standard set of commands.

    Section references are to the PMBus Specification, revision 1.2 (2010).
    """

    def __init__(self, *args, **kwargs):
        super(Whitebait_PMBus, self).__init__(*args, **kwargs)

    def group_command(self, commands):
        """Group Command (S5.2.3)"""

        P= []
        i= 0
        for (address, cmd_code, data) in commands:
            self.tr_logger.debug("group_command(%d,0x%02X,0x%02X,0x%s)", i,
                                 address, cmd_code,
                                 "".join("%02X" % x for x in data),
                                 extra={ "protocol" : "PMBus" })
            P+= [wb_i2c.PAddress(address), wb_i2c.PWrite([cmd_code] + data)]
        P.append(wb_i2c.PDone())

        return self.execute(P)

    def write_word_extended(self, address, command, data, ext_code=0xFF):
        """Extended Command: Write Byte/Word (S5.3.4/S5.2.5)
        
        Defaults to the PMBus extended command set (extension code 0xFF).
        Use write_word_mfr() for manufacturer-specific commands (extension
        code 0xFE)."""
        assert 1 <= len(data) <= 2

        if len(data) == 1:
            self.tr_logger.debug("write_byte_extended" \
                                 "(0x%02X,0x%02X,0x%02X,0x%02X)",
                                 address, ext_code, command, data[0],
                                 extra={ "protocol" : "PMBus" })
        else:
            self.tr_logger.debug("write_word_extended" \
                                 "(0x%02X,0x%02X,0x%02X,0x%02X%02X)",
                                 address, ext_code, command, data[0], data[1],
                                 extra={ "protocol" : "PMBus" })

        self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite([ext_code, command] + data),
            wb_i2c.PDone()
        ])

    def read_word_extended(self, address, command, N=2, ext_code=0xFF):
        """Extended Command: Read Byte/Word (S5.3.4/S5.2.5)
        
        Defaults to the PMBus extended command set (extension code 0xFF).
        Use read_word_mfr() for manufacturer-specific commands (extension
        code 0xFE)."""
        assert 1 <= N <= 2

        if N == 1:
            self.tr_logger.debug("read_byte_extended(0x%02X,0x%02X,0x%02X)",
                                 address, ext_code, command,
                                 extra={ "protocol" : "PMBus" })
        else:
            self.tr_logger.debug("read_word_extended(0x%02X,0x%02X,0x%02X)",
                                 address, ext_code, command,
                                 extra={ "protocol" : "PMBus" })

        return self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite([ext_code, command]),
            wb_i2c.PRead(N),
            wb_i2c.PDone()
        ])
