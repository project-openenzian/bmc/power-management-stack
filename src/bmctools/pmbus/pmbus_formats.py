"""PMBus-Specific Data Formats"""

import math
from .. import bitstruct

from . import formats as F

### The LINEAR formats - non-standard floating point

class Linear(F.DataFormat):
    """Superclass for both Linear11 and Linear16.

    Packed floating-point representation, with signed mantissa and signed,
    unbiased exponent.
    """

    # Exponent bits, mantissa bits
    mbits= None
    ebits= None

    # Minimum & maximum exponent
    min_exp= None
    max_exp= None

    def __init__(self, mbits, ebits, *args, **kwargs):
        self.mbits= mbits
        self.ebits= ebits

        if "min_exp" in kwargs:
            self.min_exp= kwargs["min_exp"]
        else:
           self.min_exp= -(2 ** (self.ebits - 1))

        if "max_exp" in kwargs:
            self.max_exp= kwargs["max_exp"]
        else:
            self.max_exp= 2 ** (self.ebits - 1) - 1

        super(Linear, self).__init__(*args, **kwargs)

    def least_exp(self):
        return self.min_exp

    def greatest_exp(self):
        return self.max_exp

    def least_mantissa(self):
        return -(2 ** (self.mbits - 1))

    def greatest_mantissa(self):
        return 2 ** (self.mbits - 1) - 1

    def least(self, N=None):
        """Least representable value."""
        if N is None: N= self.greatest_exp()

        return float(self.least_mantissa() * (2 ** N))

    def upper_bound(self, N=None):
        """Least upper bound of representable values."""
        if N is None: N= self.greatest_exp()

        return (self.greatest_mantissa() + 0.5) * (2 ** N)

    def interpret(self, **kwargs):
        """Use the formula of S7.1 to convert a value (`Y`) in LINEAR format
        to a floating-point value."""

        raw_fields= kwargs["raw_fields"]

        if "N" in raw_fields:
            # For Linear11, N is encoded alongside Y
            N= raw_fields["N"]
        else:
            # For Linear16, it's in the global coefficients (VOUT_MODE)
            try:
                coeff= kwargs["coeff"]
                N= coeff["VOUT_MODE"]["N"]
            except KeyError:
                raise F.ParameterRequest("VOUT_MODE")

        return raw_fields["Y"] * (2.0 ** N)

    def encode(self, **kwargs):
        """Use the formula of S7.1 to encode a floating-point value (`X`) in
        LINEAR format."""

        value= kwargs["value"]

        if self.mbits is None or self.ebits is None:
            raise Exception("Can't encode an incomplete LINEAR format.")

        if value < self.least() or value >= self.upper_bound():
            raise F.FormatOutOfRange("Value %.12e out of range [%.12e,%.12e)" %
                (value, self.least(), self.upper_bound()))

        # Map literal 0 directly
        if value == 0.0: return { "Y": 0, "N": self.least_exp() }

        # Get the (IEEE754) mantissa and exponent
        m, e= math.frexp(value)

        # math.frexp() returns -2^n as -0.5 * 2^(n+1), rather than -1.0 * 2^n,
        # which is representable with a signed mantissa.
        if m == -0.5: m, e= -1.0, e-1

        # Bias the exponent such that mantissa range [-1,1) maps to
        # [-2^(mbits-1), 2^(mbits-1) - 1).
        M= m * 2.0**(self.mbits - 1)
        E= e - (self.mbits - 1)

        # Round mantissa to nearest integer
        rM= int(round(M))

        # The range [-2^(mbits-1) - 0.5, 2^(mbits-1)) i.e. the top 1/2 LSB,
        # will round to 2^(mbits-1), which is not representable in the
        # mantissa.  In this case, increase the exponent by one.
        if rM == 2 ** (self.mbits - 1):
            M/= 2.0
            rM= int(round(M))
            E+= 1

        if E < self.least_exp():
            # We have an underflow, construct a denormalised number.
            dE= self.least_exp() - E

            if dE < self.mbits:
                # If there are enough significant bits remaining, shift the
                # mantissa right such that E is the lowest possible exponent.
                rM>>= dE
            else:
                # Otherwise, we have a total underflow, and end up with 0.
                rM= 0
            E= self.least_exp()

        # Round mantissa to nearest integer
        return { "Y": rM, "N": E }

class Linear11(Linear,F.Bitfield):
    """5-bit exponent, 11-bit mantissa, packed together."""

    def __init__(self, *args, **kwargs):
        super(Linear11, self).__init__(11, 5, "s5s11", ["N", "Y"],
                                       *args, **kwargs)

class Linear16(Linear,F.Bitfield):
    """16-bit mantissa, plus separately-supplied 5-bit exponent
    
    Exponent must be supplied as field "N" in keyword argument "coeff".
    """
    def __init__(self, *args, **kwargs):
        super(Linear16, self).__init__(16, 5, "s16", ["Y"],
                                       *args, **kwargs)

    def encode(self, **kwargs):
        """For Linear16, N is fixed and taken from VOUT_MODE."""

        value= kwargs["value"]

        try:
            coeff= kwargs["coeff"]
            N= coeff["VOUT_MODE"]["N"]
        except KeyError:
            raise F.ParameterRequest("VOUT_MODE")

        return { "Y" : int(round(value / (2.0 ** N))) }

### VID formats - affine with 0.0 forced to 0

def VR12():
    """Intel VR12 VID"""
    return F.UnsignedScaled(16, 5e-3, 245e-3, limit=0xFF, hard_zero=True)

def VR125():
    """Intel VR12.5 VID"""
    return F.UnsignedScaled(16, 10e-3, 490e-3, limit=0xFF, hard_zero=True)

def GPU625():
    """6.25mV step GPU VID"""
    return F.UnsignedScaled(16, 6.25e-3, 306.25e-3, limit=0xFF, hard_zero=True)

### The DIRECT format - affine with logarithmic scale factors

class Direct(F.Bitfield):
    """'Direct-formatted scalar value."""

    def __init__(self, m=None, b=None, R=None, *args, **kwargs):
        self.m= m
        self.b= b
        self.R= R
        super(Direct, self).__init__("s16", ["Y"], *args, **kwargs)

    def gather_coeff(self, **kwargs):
        if self.m is None:
            try:
                coeff= kwargs["coeff"]
                m= coeff["COEFFICIENTS"]["m"]
            except KeyError:
                raise F.ParameterRequest("COEFFICIENTS")
        else:
            m= self.m

        if self.b is None:
            try:
                coeff= kwargs["coeff"]
                b= coeff["COEFFICIENTS"]["b"]
            except KeyError:
                raise F.ParameterRequest("COEFFICIENTS")
        else:
            b= self.b

        if self.R is None:
            try:
                coeff= kwargs["coeff"]
                R= coeff["COEFFICIENTS"]["R"]
            except KeyError:
                raise F.ParameterRequest("COEFFICIENTS")
        else:
            R= self.R

        return (m, b, R)

    def interpret(self, **kwargs):
        """Use the formula of S7.2.1 to convert a value (`Y`) in DIRECT format
        to a floating-point value."""

        Y= kwargs["raw_fields"]["Y"]
        m, b, R= self.gather_coeff(**kwargs)
        return (float(Y) * (10.0**-R) - b) / m

    def encode(self, **kwargs):
        """Use the formula of S7.2.2 to encode a floating-point value (`X`) in
        DIRECT format."""

        X= kwargs["value"]
        m, b, R= self.gather_coeff(**kwargs)
        return { "Y": int((m * X + b) * 10.0**R) }

### Context-dependent formats

def Vout():
    """Any format for valid VOUT commands, selected according to the value of
    field "MODE" in argument "coeff"."""

    return F.Alternate(("VOUT_MODE", "MODE"), {
        "LINEAR" : Linear16(),
        # XXX - I haven't managed to find any documentation on the mapping
        # from VID_CODE to VID type.  The only point of reference so far is
        # the MAX20751 which implements VR12.  This hardcoding should be
        # removed.
        "VID"    : VR12(),
        "DIRECT" : Direct(),
    })

def FanControl(pwm_fmt, rpm_fmt, ctrl_reg, rpm_field):
    """RPM or PWM fan control (and telemetry) format.

    FAN_COMMAND_n is interpreted as either RPM or PWM duty cycle (%),
    depending on the setting of the FAN_CONTROL_a_b registers."""

    return F.Alternate((ctrl_reg, rpm_field), {
        True  : rpm_fmt,
        False : pwm_fmt
        })
