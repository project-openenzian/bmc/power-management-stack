from . import i2cbus

class MPSSE_I2CBus(object):
    """An 'MPSSE_I2CBus' object wraps an object representing an I2C host
    interface, and expects an interface compatible with that provided by the
    `mpsse` module (from libmpsse).

    The I2C bus must be configured to send data MSB-first.

    Section references are to the I2C-bus specification and user manual,
    revision 6 (2014) - UM10204.
    """

    i2c_bus= None

    def __init__(self, i2c_bus):
        self.i2c_bus= i2c_bus

    def probe(self, address):
        """Probe for an I2C device at `address`.
        
        Sends a 0-byte write and checks for an acknowledgement."""
        self.i2c_bus.Start()
        self.i2c_bus.Write(chr(i2cbus.write_addr(address)))
        self.i2c_bus.Stop()
        return self.i2c_bus.GetAck() == 0

    def output_int(self, n):
        """Send an integer as a single byte, expecting an acknowledgement.

        @param n int"""

        assert 0 <= n <= 255

        self.i2c_bus.Write(chr(n))
        if self.i2c_bus.GetAck() == 1:
            raise i2cbus.I2CBus_NACK()

    def output_bytearray(self, data):
        """Send the given array, byte by byte.

        @param data bytearray"""
        for x in data: self.output_int(x)

    def input_int(self, ack=True):
        """Receive a byte as an integer and, optionally, acknowledge it.

        @return int"""
        if ack: self.i2c_bus.SendAcks()
        else:   self.i2c_bus.SendNacks()
        return ord(self.i2c_bus.Read(1))

    def input_bytearray(self, n):
        """Receive a stream of bytes as a bytearray.  Send a NACK on the last
        byte to signal the device that we're done (per S3.1.6, point 5).

        @param n int
        @return bytearray"""
        if n > 1:
            self.i2c_bus.SendAcks()
            data_0= bytearray(self.i2c_bus.Read(n - 1))
        else:
            data_0= bytearray([])
        # The last byte received must be NACK-ed
        data_1= self.input_int(ack=False)
        self.i2c_bus.SendAcks()

        return data_0 + bytearray([data_1])

    def bus_clear(self):
        """Generate 9 clock pulses with SDA low, to return any devices in an
        indeterminate state to a known one."""
        self.i2c_bus.Start()
        self.i2c_bus.Write(chr(0))
        self.i2c_bus.Stop()

    # Read/Write register operations are not part of the I2C specification,
    # but are extremely common, thus implemented in the base class.

    def write_reg8_single(self, address, n):
        """Write the single register (no register address) of a very simple
        device."""
        try:
            self.i2c_bus.Start()
            self.output_bytearray(self, [i2cbus.write_addr(address), n])
        finally:
            self.i2c_bus.Stop()

    def read_reg8_single(self, address):
        """Read the single register (no register address) of a very simple
        device."""
        try:
            self.i2c_bus.Start()
            self.output_bytearray([i2cbus.read_addr(address)])
            data= self.input_bytearray(1)
        finally:
            self.i2c_bus.Stop()

        return data

    def write_reg8(self, address, reg, n):
        """Write an 8-bit register at an 8-bit address."""
        try:
            self.i2c_bus.Start()
            self.output_bytearray(self, [i2cbus.write_addr(address), reg, n])
        finally:
            self.i2c_bus.Stop()

    def read_reg8(self, address, reg):
        """Read an 8-bit register at an 8-bit address."""
        try:
            self.i2c_bus.Start()
            self.output_bytearray([i2cbus.write_addr(address), reg])
            self.i2c_bus.Start() # Repeated start
            self.output_bytearray([i2cbus.read_addr(address)])
            data= self.input_bytearray(1)
        finally:
            self.i2c_bus.Stop()

        return data
