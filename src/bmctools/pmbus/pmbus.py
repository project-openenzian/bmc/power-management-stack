from copy import copy
from time import sleep
import logging
import math

from . import formats as F
from . import pmbus_formats as PF
from . import pmbus_commands as PC
from . import i2cbus as I2C
from . import smbus as SMB
from . import controller as C
from . import variables as V

class PMBus(SMB.SMBus):
    """A PMBus object is an extension of an SMBus object, definining a few
    additional protocol elements, and a standard set of commands.

    This base class must be subclassed to implement group_command(),
    write_word_extended() and read_word_extended() in a transport-specific
    manner.

    Section references are to the PMBus Specification, revision 1.2 (2010).
    """

    def __init__(self, *args, **kwargs):
        super(PMBus, self).__init__(*args, **kwargs)

    def group_command(self, commands):
        """Group Command (S5.2.3)"""

        raise PC.PMBus_Command_Error("unimplemented")

    def write_word_extended(self, address, command, data, ext_code=0xFF):
        """Extended Command: Write Byte/Word (S5.3.4/S5.2.5)
        
        Defaults to the PMBus extended command set (extension code 0xFF).
        Use write_word_mfr() for manufacturer-specific commands (extension
        code 0xFE)."""

        raise PC.PMBus_Command_Error("unimplemented")

    def write_word_mfr(self, address, command, data):
        """Manufacturer Extended Command: Write Byte/Word (S5.3.4/S5.2.5)"""
        self.write_word_extended(address, command, data, ext_code=0xFE)

    def read_word_extended(self, address, command, N=2, ext_code=0xFF):
        """Extended Command: Read Byte/Word (S5.3.4/S5.2.5)
        
        Defaults to the PMBus extended command set (extension code 0xFF).
        Use read_word_mfr() for manufacturer-specific commands (extension
        code 0xFE)."""

        raise PC.PMBus_Command_Error("unimplemented")

    def read_word_mfr(self, address, command, N=2):
        """Manufacturer Extended Command: Read Byte/Word (S5.3.4/S5.2.5)"""
        return self.read_word_extended(address, command, N, ext_code=0xFE)

    def read_byte_extended(self, address, command, ext_code=0xFF):
        return self.read_word_extended(address, command, 1, ext_code)

    def read_byte_mfr(self, address, command):
        return self.read_byte_extended(address, command, ext_code=0xFE)

class PMBus_Device(SMB.SMBus_Device,C.Controller):
    """A device at a given address on the given PMBus.
    
    Internal fields are overridden to add manufacturer-specific commands and
    to handle quirks."""

    # PMBus-standard commands supported by the device
    _supported_commands= []
    # Manufacturer-specific commands, not in the PMBus spec.
    _mfr_commands= []
    # Entries here overrides properties of commands (e.g. writeable),
    # including those of _mfr_commands.
    _command_overrides= {}
    # Fixed results for commands not implemented by the device (e.g.
    # Fixed results for commands not implemented by the device (e.g.
    # COEFFICIENTS).
    _fixed_values= {}

    # Delay in s before first transaction retry.
    retry_delay= 0.1

    # PMBus transactions are LSB-first
    _LSB_first= True

    def __init__(self, *args, **kwargs):
        # We are going to modify the monitor and control fields of C.Controller
        # before calling its constructor so we copy the class attributes here
        # FIXME: See comment in C.Controllers constructor
        self.monitors = copy(self.monitors)
        self.controls = copy(self.controls)

        # From the fields overriden by our subclass (specifying supported
        # PMBus commands), generate a list of parameters for the I2CBus_Device
        # initialiser.
        raw_parameters= \
            [PC.command_by_name[name] for name in self._supported_commands] + \
            self._mfr_commands

        # Build a map from CMD code to CMD name for all supported commands.
        # Used in the handling reads from fixed parameters.
        self._name_by_code= dict((P.code, P.name) for P in raw_parameters)

        # Apply parameter overrides.
        for i in range(len(raw_parameters)):
            name= raw_parameters[i].name
            if name in self._command_overrides:
                # If a command is overridden, make a copy and modify that.
                p_args, p_kwargs= raw_parameters[i].get_args()
                # Replace the original keyword arguments.
                p_kwargs.update(self._command_overrides[name])
                # Create a new object with the new arguments.
                CLS= raw_parameters[i].__class__
                raw_parameters[i]= CLS(*p_args, **p_kwargs)

        self._paged_commands= set()
        self._command_pages= dict()
        self._alert_status_cmds= set()
        self._alert_map= {}

        # Construct the list.
        self._parameters= []
        page_wrapped_commands = []
        for P in raw_parameters:
            if P.paged:
                # For paged parameters, add paged instances
                self._parameters+= [PC.Paged(P, p) for p in P.pages]
                page_wrapped_commands.append(P)
                self._paged_commands.add(P.name)
            else:
                self._parameters.append(P)

            self._command_pages[P.name]= P.pages

        # Add default monitors for all supported commands.
        for name in PC.standard_monitors:
            std_mon= PC.standard_monitors[name]
            m_kwargs= dict()

            # Skip unsupported commands
            # XXX - We can do this here because we don't support
            # overriding "key"
            if not (std_mon["key"] is None or
                    std_mon["key"] in self._supported_commands):
                continue
            m_kwargs["key"]= std_mon["key"]
            m_kwargs["quantity"]= std_mon["quantity"]

            # Remove unsupported comparators.
            # XXX - We can do this here because we assume overrides
            # only contain supported comparators
            m_kwargs["comparators"]= dict()
            for (cmp_type, cmp_data) in std_mon["comparators"].items():
                cmd_name= cmp_data[0]
                if cmd_name is None or cmd_name in self._supported_commands:
                   m_kwargs["comparators"][cmp_type]= cmp_data

            if m_kwargs["key"] is None or \
               not m_kwargs["key"] in self._paged_commands:
                # Non-paged monitor
                # XXX - logger not yet available
                #self.dev_logger.debug("Adding default PMBus monitor %s", name)

                if name in self._monitor_overrides:
                    override = self._monitor_overrides[name]
                    if "key" in override:
                        raise PC.PMBus_Config_Error("Overriding montitor read command not supported")
                    m_kwargs.update(override)
                
                # If there is no read command, and no comparators, skip it.
                if m_kwargs["key"] is None and len(m_kwargs["comparators"]) == 0:
                    continue

                # Add a mapping from alert flag to comparator name
                for cmp_type in m_kwargs["comparators"]:
                    flag_path= m_kwargs["comparators"][cmp_type][1]
                    if not flag_path is None:
                        status_cmd= flag_path[0]
                        self._alert_status_cmds.add(status_cmd)
                        self._alert_map[flag_path]= (name, cmp_type)

                # Create a monitor object
                self.monitors[name]= V.Monitor(name, self, **m_kwargs)
            else:
                # Paged monitor
                pages= self._command_pages[m_kwargs["key"]]
                for p in pages:
                    # XXX - logger not yet available
                    #self.dev_logger.debug(
                        #"Adding paged PMBus monitor (%s,%d)", name, p)

                    p_kwargs= copy(m_kwargs)

                    if (name, p) in self._monitor_overrides:
                        override = self._monitor_overrides[(name, p)]
                        if "key" in override:
                            raise PC.PMBus_Config_Error("Overriding montitor read command not supported")
                        p_kwargs.update(override)

                    # Tag read parameter with page.
                    p_kwargs["key"]= (p_kwargs["key"], p)

                    # Tag comparators with page.
                    tagged = {}
                    for cmp_type in p_kwargs["comparators"]:
                        threshold_cmd, flag_path= \
                            p_kwargs["comparators"][cmp_type]

                        if not flag_path is None:
                            status_cmd, flag= flag_path
                            flag_path= ((status_cmd, p), flag)
                            self._alert_status_cmds.add((status_cmd, p))
                            self._alert_map[flag_path]= ((name, p), cmp_type)

                        if threshold_cmd is None:
                            tagged[cmp_type]= \
                            (None, flag_path)
                        else:
                            tagged[cmp_type]= \
                                ((threshold_cmd, p), flag_path)
                    p_kwargs["comparators"] = tagged

                    self.monitors[(name, p)]= V.Monitor((name, p), self, **p_kwargs)

        # Add default controls for all supported commands.
        for name in PC.standard_controls:
            c_kwargs= copy(PC.standard_controls[name])

            # Skip unsupported commands
            # XXX - We can do this here because we don't support
            # overriding "command"
            if not c_kwargs["command"] in self._supported_commands:
                continue

            # Remove unsupported commands
            # XXX - We can do this here because we assume overrides
            # only contain supported commands
            for k in ["offset", "rate"]:
                if not k in c_kwargs: continue
                if not c_kwargs[k] in self._supported_commands:
                    del(c_kwargs[k])
            for k in ["margin", "limit"]:
                if not k in c_kwargs: continue
                low_key, high_key= c_kwargs[k]
                if not low_key in self._supported_commands: low_key= None
                if not high_key in self._supported_commands: high_key= None
                if low_key is None and high_key is None:
                    del(c_kwargs[k])
                else:
                    c_kwargs[k]= (low_key, high_key)
            for k in ["enable", "disable"]:
                if not k in c_kwargs: continue
                supported = []
                for path, value in c_kwargs[k]:
                    if path[0] in self._supported_commands:
                        supported.append((path, value))
                if len(supported) == 0:
                    del(c_kwargs[k])
                else:
                    c_kwargs[k] = supported

            if not c_kwargs["command"] in self._paged_commands:
                # Non-paged control
                if name in self._control_overrides:
                    override = self._control_overrides[name]
                    if "command" in override:
                        raise PC.PMBus_Config_Error("Overriding control command not supported")
                    c_kwargs.update(override)

                # Create a control object
                self.controls[name]= V.Control(name, self, **c_kwargs)
            else:
                # Paged control
                pages = self._command_pages[c_kwargs["command"]]
                for p in pages:
                    p_kwargs = copy(c_kwargs)

                    if (name, p) in self._control_overrides:
                        override = self._control_overrides[(name, p)]
                        if "command" in override:
                            raise PC.PMBus_Config_Error("Overriding control command not supported")
                        p_kwargs.update(override)

                    # Tag commands with page
                    p_kwargs["command"] = (p_kwargs["command"], p)
                    for k in ["offset", "rate"]:
                        if k in p_kwargs:
                            p_kwargs[k] = (p_kwargs[k], p)
                    for k in ["margin", "limit"]:
                        if not k in p_kwargs: continue
                        low_key, high_key= p_kwargs[k]
                        if not low_key is None:
                            low_key = (low_key, p)
                        if not high_key is None:
                            high_key = (high_key, p)
                        p_kwargs[k]= (low_key, high_key)
                    for k in ["enable", "disable"]:
                        if not k in p_kwargs: continue
                        tagged = []
                        for path, value in p_kwargs[k]:
                            tagged_path = (((path[0], p),) + path[1:])
                            tagged.append((tagged_path, value))
                        p_kwargs[k] = tagged

                    # Tag monitor with page
                    if "monitor" in p_kwargs:
                        p_kwargs["monitor"] = (p_kwargs["monitor"], p)

                    # Create a control object
                    self.controls[(name, p)]= V.Control((name, p), self, **p_kwargs)

        super(PMBus_Device, self).__init__(*args, **kwargs)

        # Mark all paged commands as dependent of the page command
        for P in page_wrapped_commands:
            self.add_derived("PAGE", P)

    def read(self, name, **kwargs):
        """Read the raw result of the given command and interpret it according
        to its associated format."""

        # Parameter fetches may request a paged value, with page unspecified
        # (None), as this is set at the format level.  In this case, the page
        # of the command for which the parameter is needed is passed as
        # keyword argument PAGE.
        try:
            base_name, page= name
            if page is None:
                page= kwargs["PAGE"]
                name= (base_name, page)
                self.dev_logger.debug("Directing read from %s to page %d",
                                      base_name, page)
        except KeyError:
            raise PC.PMBus_Command_Error("No PAGE for command %s" % base_name)
        except ValueError:
            # Not a tuple
            pass

        # If there are hardcoded values for this parameter.
        if name in self._fixed_values:
            table= self._fixed_values[name]
            # Get the command code.
            cmd_code= kwargs["CODE"]
            # Map it to a name.
            cmd_name= self._name_by_code[cmd_code]
            # Is this a read?
            read= kwargs["READ"]
            # If there's a hardcoded entry for the transaction in question
            # (command code + read/write), return it.
            if cmd_name in table and read in table[cmd_name]:
                self.dev_logger.debug("Returning hardcoded %s for %s, READ=%d",
                                      name, cmd_name, read)
                return table[cmd_name][read]
            # Otherwise fall through and read from the device.

        return super(PMBus_Device, self).read(name, **kwargs)

    # STATUS_x commands for which SMBALERT_MASK is applicable.  Note that
    # STATUS_BYTE and STATUS_WORD are composed of bits taken from these
    # underlying registers, and are thus masked transitively.
    #
    # See Figure 26, p77.
    status_commands = set([
        "STATUS_VOUT",
        "STATUS_IOUT",
        "STATUS_INPUT",
        "STATUS_TEMPERATURE",
        "STATUS_CML",
        "STATUS_OTHER",
        "STATUS_MFR_SPECIFIC",
        "STATUS_FANS_1_2",
        "STATUS_FANS_3_4",
    ])

    def read_smbalert_mask(self, cmd_name):
        """The SMBALERT command takes one parameter: the command code for
        which we want to check the mask bit."""

        if not cmd_name in self.status_commands:
            raise PC.PMBus_Command_Error(
                "SMBALERT_MASK on non-status command %s" % cmd_name)

        # Get the command whose mask we want to read
        cmd= self._parameter_map[cmd_name]

        # Get the mask value, and interpret it (by default as a set of flags,
        # using the format of the queried command).
        raw= self.read("SMBALERT_MASK", CODE=cmd.code)
        return cmd.unpack(self, raw=raw)

    def write_smbalert_mask(self, cmd_name, mask):
        """Update the alert mask for the given status command."""
        if not cmd_name in self.status_commands:
            raise PC.PMBus_Command_Error(
                "SMBALERT_MASK on non-status command %s" % cmd_name)

        cmd= self._parameter_map[cmd_name]
        raw= cmd.pack(self, value=mask)
        self.write("SMBALERT_MASK", CODE=cmd.code, MASK=raw)

    def read_smbalert_mask_all(self):
        """Read SMBALERT_MASK for all STATUS_x commands."""
        results= {}
        for cmd_name in self.status_commands:
            results[cmd_name]= self.read_smbalert_mask(cmd_name)
        return results

    #def disable_all_alerts(self):
        #for status_cmd in self.status_commands:
            #fmt= self.get_format(status_cmd)
            #mask= set(fmt.names)
            #self.write_smbalert_mask(mask)

    def read_status_tagged(self, cmd):
        flags= set()

        if cmd in self._paged_commands:
            # Read STATUS_x on all supported pages
            for p in self._command_pages[cmd]:
                # Tag flags with register name and page
                flags.update({((cmd,p),f) for f in self.read((cmd, p))})
        elif cmd in self._supported_commands:
            # Tag with the register name
            flags.update({(cmd,f) for f in self.read(cmd)})
        
        # XXX: We silently swallow any status reads if the command is not supported
        # Think about whether this is the right approach
        return flags

    def read_status(self):
        """Recursively read the status words to enumerate all status flags."""
        # Base register
        flags= self.read_status_tagged("STATUS_WORD")

        # Some flags are the 'OR' of a secondary register
        if ("STATUS_WORD", "TEMPERATURE") in flags:
            flags.update(self.read_status_tagged("STATUS_TEMPERATURE"))
        if ("STATUS_WORD", "CML") in flags:
            flags.update(self.read_status_tagged("STATUS_CML"))
        if ("STATUS_WORD", "VOUT") in flags:
            flags.update(self.read_status_tagged("STATUS_VOUT"))
        if ("STATUS_WORD", "IOUT/POUT") in flags:
            flags.update(self.read_status_tagged("STATUS_IOUT"))
        if ("STATUS_WORD", "INPUT") in flags:
            flags.update(self.read_status_tagged("STATUS_INPUT"))
        if ("STATUS_WORD", "MFR_SPECIFIC") in flags:
            flags.update(self.read_status_tagged("STATUS_MFR_SPECIFIC"))
        if ("STATUS_WORD", "FANS") in flags:
            flags.update(self.read_status_tagged("STATUS_FANS_1_2"))
            flags.update(self.read_status_tagged("STATUS_FANS_3_4"))
        if ("STATUS_WORD", "OTHER") in flags:
            flags.update(self.read_status_tagged("STATUS_OTHER"))

        return flags

    def clear_fault(self, fault):
        status_cmd, flag_name= fault
        self.write(status_cmd, set([flag_name]))

    def clear_faults(self):
        self.write("CLEAR_FAULTS")

    def set_threshold(self, M, cmp_type, threshold):
        """Set comparator threshold."""
        comparator = M.get_comparator(cmp_type)
        threshold_cmd= comparator[0]
        if threshold_cmd is None:
            raise PC.PMBus_Command_Error("Not threshold command for this comparator")
        self.write(threshold_cmd, threshold)

    def get_threshold(self, M, cmp_type):
        """Get comparator threshold."""
        comparator = M.get_comparator(cmp_type)
        threshold_cmd= comparator[0]
        if threshold_cmd is None:
            raise PC.PMBus_Command_Error("Not threshold command for this comparator")
        return self.read(threshold_cmd)

    def read_alert_mask(self):
        """Return the set of masked (disabled) alerts."""
        flags= set()
        for cmd in self._alert_status_cmds:
            # XXX - Check if this works for a paged SMBALERT_MASK
            mask= self.read_smbalert_mask(cmd)
            flags.update({(cmd, E) for E in mask})

        return {self._alert_map[A] for A in flags if A in self._alert_map}

    def enable_alert(self, M, cmp_type):
        """Enable an alert by clearing its SMBALERT_MASK bit."""
        flag_path= M.get_comparator(cmp_type)[1]
        status_cmd, flag_name= flag_path

        mask= self.read_smbalert_mask(status_cmd)
        mask.discard(flag_name)
        self.write_smbalert_mask(status_cmd, mask)

    def disable_alert(self, M, cmp_type):
        """Disable an alert by setting its SMBALERT_MASK bit."""
        flag_path= M.get_comparator(cmp_type)[1]
        status_cmd, flag_name= flag_path

        mask= self.read_smbalert_mask(status_cmd)
        mask.add(flag_name)
        self.write_smbalert_mask(status_cmd, mask)

    def clear_alert(self, M, cmp_type):
        """Clear a comparator alert (may reassert immediately)."""
        flag_path= M.get_comparator(cmp_type)[1]
        self.clear_fault(flag_path)

    def query_alert(self):
        """List all comparators currently asserting fault or warning."""

        flags= self.read_status()
        alerts = {self._alert_map[f] for f in flags if f in self._alert_map}
        return {(self.monitors[m].name, a) for m, a in alerts}

    def is_monitor_enabled(self, M):
        # Always enabled
        return True

    def enable_monitor(self, M):
        pass

    def disable_monitor(self, M):
        raise V.MonitorError("Cannot disable monitors on this device.")

    def store_default_all(self):
        self.write("STORE_DEFAULT_ALL")
        sleep(1.0)

    def restore_default_all(self):
        self.write("RESTORE_DEFAULT_ALL")
        sleep(1.0)

    def __str__(self):
        return "PMBus device at (7b) 0x%X" % self.address

    #def dump_paged(self):
        #"""Perform a read transaction for all commands in `commands`. If the
        #command differs between pages (access_paged), execute the command on
        #each page. Returns a dict of (cmd,page) to value """
        # dump the access_common items. Use the first range
        #res = {}
        #common_cmds = [c for (c,d) in self.command_pages.items() \
                #if d.access == CommandPageDesc.access_common]
        #paged_cmds = [c for (c,d) in self.command_pages.items() \
                #if d.access == CommandPageDesc.access_paged]
        #is_paging = len(paged_cmds) == 0

        #if not is_paging:
            #return {(key,-1) : val for key,val in \
                    #self.dump(None, common_cmds).items()}
        #else:
            #for cmd in common_cmds:
                #page = min(self.command_pages[cmd].range)
                #res.update({(key,page) : val for key,val in \
                    #self.dump(None, [cmd]).items()})
            #for cmd in common_cmds:
                #for page in self.command_pages[cmd].range:
                    #res.update({(key,page) : val for key,val in \
                        #self.dump(None, [cmd]).items()})
        #return res

    #def dump(self, page=None, commands=None):
        #"""Perform a read transaction for all commands in `commands`, on page
        #`page`.  Return a dictionary containing the results."""
        #results= {}
        #if commands is None:
            #commands = self.command_pages # all commands
        #if not page is None:
            #init_page= self.get_page()
            #self.set_page(page)

        #for c in commands:
            #command_code, write_transaction, read_transaction= \
                #self.lookup_command(c)

            # First dump all simple read (i.e. non-Call) commands.
            #if not isinstance(read_transaction, SMB.Read) and \
               #not isinstance(read_transaction, SMB.BlockRead):
                #continue

            # Skip commands not on this page.
            #if not page is None and not page in self.command_pages[c].range:
                #continue

            # Slow devices may start NACKing after a number of back-to-back
            # reads.  Retry with exponential backoff.
            #results[c]= self.read(c, tries=4)

        # If QUERY is available, append the results.
        #if "QUERY" in commands:
            #results["QUERY"]= self.query_all()

        # If SMBALERT_MASK is available, append the results.
        #if "SMBALERT_MASK" in commands:
            #results["SMBALERT_MASK"]= self.read_smbalert_mask_all()

        #if not page is None:
            #self.set_page(init_page)
        #return results


class PMBus_Polled_Device(PMBus_Device):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._acknowledged_alerts = set()

    def acknowledge_alerts(self, alerts):
        self._acknowledged_alerts.update(alerts)

    def query_unacknowleged_alert(self):
        alerts = self.query_alert()
        return alerts.difference(self._acknowledged_alerts)

    def clear_fault(self, fault):
        if fault in self._alert_map:
            m, a = self._alert_map[fault]
            alert = (self.monitors[m], a)
            if alert in self._acknowledged_alerts:
                self._acknowledged_alerts.remove(alert)
        return super().clear_fault(fault)

    def clear_faults(self):
        self._acknowledged_alerts = set()
        return super().clear_faults()
