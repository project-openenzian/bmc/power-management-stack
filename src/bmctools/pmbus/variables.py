from . import i2cbus as I2C

class MonitorError(I2C.I2CBus_Exception):
    pass

class MonitorIncompatible(I2C.I2CBus_Exception):
    pass

class Monitor(object):
    def __init__(self, name, dev, key=None, quantity=None, comparators={},
                 conflicts=[], interval=None):
        self.name= name
        self._dev= dev
        self._key= key
        self._quantity= quantity
        self._comparators= comparators
        self._conflicts= conflicts
        self._interval= interval

    def is_enabled(self):
        return self._dev.is_monitor_enabled(self)

    def enable(self):
        return self._dev.enable_monitor(self)

    def disable(self):
        return self._dev.disable_monitor(self)

    def read(self):
        if self._key is None:
            raise MonitorError("No read command for monitor %s" % self.name)

        return self._dev.read(self._key)

    def get_page(self):
        try:
            return self._key[1]
        except TypeError:
            return None

    def list_comparators(self):
        return self._comparators.keys()

    def get_comparators(self):
        return self._comparators

    def get_comparator(self, cmp_type):
        try:
            return self._comparators[cmp_type]
        except KeyError:
            raise MonitorError("No comparator for %s on %s" %
                               (cmp_type, self.name))

    def set_threshold(self, cmp_type, value):
        self._dev.set_threshold(self, cmp_type, value)

    def get_threshold(self, cmp_type):
        return self._dev.get_threshold(self, cmp_type)

    def enable_alert(self, cmp_type):
        self._dev.enable_alert(self, cmp_type)

    def disable_alert(self, cmp_type):
        self._dev.disable_alert(self, cmp_type)

    def clear_alert(self, cmp_type):
        self._dev.clear_alert(self, cmp_type)

class ControlError(I2C.I2CBus_Exception):
    pass

def set_all(dev, kvs):
    for key, value in kvs: dev.update(key, value)

class Control(object):
    def __init__(self, name, dev, command=None, quantity=None, conflicts=[],
                 monitor=None, enable=None, disable=None, offset=None,
                 rate=None, margin=None, limit=None):
        self.name= name
        self._dev= dev
        self._command= command
        self._quantity= quantity
        self._conflicts= conflicts
        self._monitor= monitor
        self._enable= enable
        self._disable= disable
        self._offset= offset
        self._rate= rate
        self._margin= margin
        self._limit= limit

    def is_enabled(self):
        if self._enable is None:
            raise ControlError("Control %s cannot be enabled.")
        for key, value in self._enable:
            if self._dev.resolve(key) != value: return False
        return True

    def enable(self):
        if self._enable is None:
            raise ControlError("Control %s cannot be enabled.")
        for key, value in self._enable:
            self._dev.update(key, value)

    def disable(self):
        if self._disable is None:
            raise ControlError("Control %s cannot be disabled.")
        for key, value in self._disable:
            self._dev.update(key, value)

    def set(self, value):
        self._dev.write(self._command, value)

    def get(self):
        return self._dev.read(self._command)

    def monitor(self):
        if self._monitor is None: return None
        else: return self._dev.monitors[self._monitor]

    def state(self):
        S= {}
        S["command"]= self._dev.read(self._command)
        if not self._offset is None: S["offset"]= self._dev.read(self._offset)
        if not self._rate is None: S["rate"]= self._dev.read(self._rate)
        if not self._margin is None:
            low_key, high_key= self._margin
            if low_key is None: low= None
            else: low= self._dev.read(low_key)
            if high_key is None: high= None
            else: high= self._dev.read(high_key)
            S["margin"]= (low, high)
        if not self._limit is None:
            low_key, high_key= self._limit
            if low_key is None: low= None
            else: low= self._dev.read(low_key)
            if high_key is None: high= None
            else: high= self._dev.read(high_key)
            S["limit"]= (low, high)
        if not self._enable is None:
            S["enabled"]= self.is_enabled()
        return S
