from . import i2cbus
from . import mpsse_i2cbus
from . import smbus

class MPSSE_SMBus(mpsse_i2cbus.MPSSE_I2CBus,smbus.SMBus):
    """An 'SMBus' object wraps an object representing an I2C interface, and
    expects an interface compatible with that provided by the `mpsse` module
    (from libmpsse).  The `SMBus` object provides SMBus operations as
    primitives.

    The I2C bus must be configured to send data MSB-first.  PEC (packet error
    checking) and ARP are not (yet) implemented.

    Section references are to the SMBus Specification, revision 3.1 (2018).
    """

    def __init__(self, *args, **kwargs):
        super(MPSSE_SMBus, self).__init__(*args, **kwargs)

    def scan(self, start=0x00, end=0x7F, exclude=set()):
        return super(MPSSE_SMBus, self).scan(self, start, end,
                           set.union(exclude, smbus.smbus_probe_exclude))

    def quick_cmd(self, address, read):
        """Quick command (S6.5.1) - R/W bit is the payload

        @param address int
        @param read bool"""
        if read: x= i2cbus.read_addr(address)
        else:    x= i2cbus.write_addr(address)

        try:
            self.i2c_bus.Start()
            self.output_int(x)
        finally:
            self.i2c_bus.Stop()

    def send_byte(self, address, value):
        """Send Byte (S6.5.2)

        @param address int
        @param value bytearray"""

        try:
            self.i2c_bus.Start()
            self.output_bytearray(
                bytearray([i2cbus.write_addr(address)]) + value)
        finally:
            self.i2c_bus.Stop()

    def receive_byte(self, address):
        """Receive Byte (S6.5.3)

        @param address int
        @return bytearray"""

        try:
            self.i2c_bus.Start()
            self.output_int(i2cbus.read_addr(address))
            data= self.input_bytearray(1)
        finally:
            self.i2c_bus.Stop()

        return data

    def write_byte(self, address, command, data):
        """Calls `write_word()`

        @param address int
        @param command int
        @param data bytearray
        @return bytearray"""
        return self.write_word(address, command, data)

    def write_word(self, address, command, data):
        """Write Byte/Word (S6.5.4)

        @param address int
        @param command int
        @param data bytearray"""
        assert len(data) <= 2

        try:
            self.i2c_bus.Start()
            self.output_bytearray(
                bytearray([i2cbus.write_addr(address), command]) + data)
        finally:
            self.i2c_bus.Stop()

    def read_byte(self, address, command):
        """Calls `read_word()` with N=1.

        @param address int
        @param command int
        @return bytearray"""
        return self.read_word(address, command, 1)

    def read_word(self, address, command, N=2):
        """Read Byte/Word (S6.5.5)

        @param address int
        @param command int
        @param N int
        @return bytearray"""
        assert N <= 2
        try:
            self.i2c_bus.Start()
            self.output_bytearray(
                bytearray([i2cbus.write_addr(address), command]))
            # Repeated start
            self.i2c_bus.Start()
            self.output_int(i2cbus.read_addr(address))
            data= self.input_bytearray(N)
        finally:
            self.i2c_bus.Stop()

        return data

    def process_call(self, address, command, tx_data):
        """Process Call (S6.5.6)

        @param address int
        @param command int
        @param tx_data bytearray
        @return bytearray"""
        assert len(tx_data) == 2

        try:
            self.i2c_bus.Start()
            self.output_bytearray(
                bytearray([i2cbus.write_addr(address), command]) + tx_data)
            # Repeated start
            self.i2c_bus.Start()
            self.output_int(i2cbus.read_addr(address))
            rx_data= self.input_bytearray(2)
        finally:
            self.i2c_bus.Stop()

        return rx_data

    def write_block(self, address, command, data):
        """Write Block (S6.5.7)

        @param address int
        @param command int
        @param data bytearray"""
        assert len(data) <= 255

        try:
            self.i2c_bus.Start()
            self.output_bytearray( \
                bytearray([i2cbus.write_addr(address), command,
                           len(data)]) + data)
        finally:
            self.i2c_bus.Stop()

    def write_block_noncompliant(self, address, command, data):
        """Write Block (S6.5.7)

        Performs a write block transaction, but does not transmit the block
        length.  This is needed for non-standard-compliant devices, such as
        the MAX31785.

        @param address int
        @param command int
        @param data bytearray"""
        assert len(data) <= 255

        try:
            self.i2c_bus.Start()
            self.output_bytearray( \
                bytearray([i2cbus.write_addr(address), command]) + data)
        finally:
            self.i2c_bus.Stop()

    def read_block(self, address, command):
        """Read Block (S6.5.7)

        @param address int
        @param command int
        @return dbytearray"""
        try:
            self.i2c_bus.Start()
            self.output_bytearray(
                bytearray([i2cbus.write_addr(address),command]))
            # Repeated start
            self.i2c_bus.Start()
            self.output_int(i2cbus.read_addr(address))
            N= self.input_int()
            data= self.input_bytearray(N)
        finally:
            self.i2c_bus.Stop()

        return data

    def read_block_noncompliant(self, address, command, N):
        """Read Block (S6.5.7)

        Performs a read block transaction, but takes the length of data to
        read as a parameter, rather than receiving it from the device.  This
        is needed for non-standard-compliant devices, such as the MAX31785.

        @param address int
        @param command int
        @return dbytearray"""
        try:
            self.i2c_bus.Start()
            self.output_bytearray(
                bytearray([i2cbus.write_addr(address), command]))
            # Repeated start
            self.i2c_bus.Start()
            self.output_int(i2cbus.read_addr(address))
            # The device *should* send N here, but doesn't.
            data= self.input_bytearray(N)
        finally:
            self.i2c_bus.Stop()

        return data

    def process_call_block(self, address, command, tx_data):
        """Block Write/Block Read Process Call (S6.5.8)

        @param address int
        @param command int
        @param tx_data bytearray
        @return bytearray"""
        assert len(tx_data) <= 255

        try:
            self.i2c_bus.Start()
            self.output_bytearray(
                bytearray([i2cbus.write_addr(address), command, len(tx_data)])
                    + tx_data)
            # Repeated start
            self.i2c_bus.Start()
            self.output_int(i2cbus.read_addr(address))
            N= self.input_int()
            rx_data= self.input_bytearray(N)
        finally:
            self.i2c_bus.Stop()

        return rx_data
