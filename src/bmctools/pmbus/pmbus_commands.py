from . import formats as F
from . import i2cbus as I2C
from . import smbus as SMB
from . import pmbus_formats as PF

def page_range(start, end): return frozenset(range(start, end+1))
all_pages= page_range(0, 254)

### Exceptions

class PMBus_Config_Error(SMB.SMBus_Exception):
    """An error in device configuration or definition e.g. missing
    commands."""
    pass

class PMBus_Command_Error(SMB.SMBus_Exception):
    """Trying to use a nonexistant command, or a command on the wrong page."""
    pass

class PMBus_No_Device(SMB.SMBus_Exception):
    """Device probe failed, or not a PMBus device."""
    pass

class PMBus_Fault(SMB.SMBus_Exception):
    """A dynamic fault condition."""
    pass

### Command data formats defined in the PMBus specification

bus_speed_table = {
    0 : 100000,
    1 : 400000,
    2 : None,
    3 : None
    }

wp_table = {
    0x00 : "ALL",
    0x20 : "WP_OP_PG_ON_VOUT",
    0x40 : "WP_OP_PG",
    0x80 : "WP",
}

op_state_table = {
    0x0 : "HARD_OFF",
    0x1 : "SOFT_OFF",
    0x2 : "ON"
}

op_margin_table = {
    0x0 : "OFF",
    0x1 : "LOW",
    0x2 : "HIGH"
}

op_fault_table = {
    0x0 : "UNSET",
    0x1 : "IGNORE",
    0x2 : "ACT"
}

fault_rsp_table = {
    0x0 : "CONTINUE",
    0x1 : "DELAY_RETRY",
    0x2 : "SHUTDOWN_RETRY",
    0x3 : "DISABLE"
}

fault_retry_table = {
    0x0 : 0,
    0x1 : 1,
    0x2 : 2,
    0x3 : 3,
    0x4 : 4,
    0x5 : 5,
    0x6 : 6,
    0x7 : "FOREVER"
}

fault_response_fmt= F.Bitfield(
    "r2r3u3",
    ["RESPONSE", "RETRY", "DELAY"], {
        "RESPONSE" : F.Tabular("u2", fault_rsp_table),
        "RETRY"    : F.Tabular("u2", fault_retry_table),
    }
)

current_fault_rsp_table = {
    0x0 : "ILIMIT",
    0x1 : "ILIMIT_VMIN",
    0x2 : "DELAY_RETRY",
    0x3 : "SHUTDOWN_RETRY"
}

current_fault_response_fmt= F.Bitfield(
    "r2r3u3",
    ["RESPONSE", "RETRY", "DELAY"], {
        "RESPONSE" : F.Tabular("u2", current_fault_rsp_table),
        "RETRY"    : F.Tabular("u2", fault_retry_table),
    }
)

query_format_table = {
    0x0 : "LINEAR",
    0x1 : "S16",
    0x3 : "DIRECT",
    0x4 : "U8",
    0x5 : "VID",
    0x6 : "MANUFACTURER",
    0x7 : "NON_NUMERIC"
}

vout_mode_table = {
    0x0 : "LINEAR",
    0x1 : "VID",
    0x2 : "DIRECT",
}

ppr_table = {
    0x0 : 1,
    0x1 : 2,
    0x2 : 3,
    0x3 : 4,
}

fan_config_fmt = F.Bitfield(
    "b1b1r2b1b1r2",
    ["FANA_ENABLE", "FANA_RPM", "FANA_PULSES_PER_REV",
     "FANB_ENABLE", "FANB_RPM", "FANB_PULSES_PER_REV"], {
        "FANA_PULSES_PER_REV" : F.Tabular("u2", ppr_table),
        "FANB_PULSES_PER_REV" : F.Tabular("u2", ppr_table),
    }
)

# Some command formats are left to the manufacturer's discretion.  On devices
# that support it, the QUERY command gives the format.  On others, it must be
# hardcoded.
queried_fmt = F.Alternate(
    ("QUERY", "FORMAT"), {
        "LINEAR" : PF.Linear11(),
        "S16"    : F.Signed(16),
        "DIRECT" : PF.Direct(),
        "U8"     : F.Unsigned(8),
    }
)

mfr_efficiency_fmt= F.Bitfield(
    "u16u16u16u16u16u16u16",
    ["VOLTAGE", "LP_POWER", "LP_EFFICIENCY", "MP_POWER",
     "MP_EFFICIENCY", "HP_POWER", "HP_EFFICIENCY"],
    field_fmt= {
        "VOLTAGE"       : PF.Linear11(),
        "LP_POWER"      : PF.Linear11(),
        "LP_EFFICIENCY" : PF.Linear11(),
        "MP_POWER"      : PF.Linear11(),
        "MP_EFFICIENCY" : PF.Linear11(),
        "HP_POWER"      : PF.Linear11(),
        "HP_EFFICIENCY" : PF.Linear11(),
    }
)

### PMBus command types

class PRM(I2C.Parameter):
    """PRM (Parameter) is used for PMBus commands behave like registers: a
    (possibly compound) value with a consistent interpretation that can be
    read or written directly.  This class implements the i2cbus.Parameter
    interface, to expose these directly to the existing infrastructure."""

    def __init__(self, code, name, fmt=queried_fmt, block=False,
                 pages=all_pages, paged=False, compliant=True,
                 *args, **kwargs):
        """If unspecified, format is determined by QUERY."""
        self.block= block
        self.code= code
        self.pages= pages
        self.paged= paged
        self.compliant= compliant

        # Pass the command code to parameter read calls.
        self._param_fetch_kwargs= { "CODE" : code }

        bitlength= fmt.length()
        if bitlength is None:
            self.size= None
        else:
            if bitlength % 8 != 0:
                raise PMBus_Config_Error("PMBus payload must be a"
                                         " multiple of 8")
            self.size= bitlength / 8

        super(PRM, self).__init__(name, fmt, *args, **kwargs)

        if block:
            # All PMBus parameters must be readable.
            if self.readable:
                self.tr_read= SMB.BlockRead(self.size, compliant)
            if self.writeable:
                self.tr_write= SMB.BlockWrite(self.size, compliant)
        else:
            if self.size is None:
                raise PMBus_Config_Error("Non-block transaction with"
                                         " unspecified length.")

            if self.readable:
                self.tr_read= SMB.Read(self.size)
            if self.writeable:
                self.tr_write= SMB.Write(self.size)

    def get_args(self):
        args, kwargs= super(PRM, self).get_args()

        name, fmt= args

        kwargs["fmt"]= fmt
        kwargs["block"]= self.block
        kwargs["pages"]= self.pages
        kwargs["paged"]= self.paged
        kwargs["compliant"]= self.compliant

        return ([self.code, name], kwargs)

    # read() and write() are inherited from I2C.Parameter

    def read_raw(self, dev):
        """Execute the SMBus read transaction."""
        return self.tr_read.execute(dev.bus, dev.address, self.code, None)

    def write_raw(self, dev, tx_data):
        """Execute the SMBus write transaction."""
        self.tr_write.execute(dev.bus, dev.address, self.code, tx_data)

class CMD(object):
    """CMD represents a PMBus command that does not behave as a Parameter
    (PRM).  This includes commands with no associated value such as
    CLEAR_FAULTS, and more complex RPC-type commands such as QUERY and
    SMBALERT_MASK.  The read() and write() methods are compatible with
    Parameter(), but take an extra keyword argument (tx_value) in the case of
    read(), and return the received value in the case of write().
    
    The RPC class is used to represent the 'read' and 'write' transactions."""

    def __init__(self, code, name, tr_write, tr_read=None,
                 pages=all_pages, paged=False, tags={}):
        self.name= name
        self.code= code
        self.tr_write= tr_write
        self.tr_read= tr_read
        self.pages= pages
        self.paged= paged
        self.tags= tags

        # Pass the command code to parameter read calls.
        self._param_fetch_kwargs= { "CODE" : code }

    def execute(self, tr, is_read, dev, **kwargs):
        if tr.wr_fmt is None:
            tx_raw= bytearray()
        else:
            # This will fill construct the TX value from kwargs, and may throw
            # a ParameterRequest, to be caught by with_params().
            kwargs_pack= kwargs.copy()
            kwargs_pack["LSB_first"]= dev._LSB_first
            try:
                tx_raw= dev.with_params(self.name, is_read, tr.wr_fmt.pack,
                                        self._param_fetch_kwargs,
                                        **kwargs_pack)
            except F.MissingField as e:
                raise PMBus_Command_Error(
                    "Missing argument %s for command %s" % (e.args[0],
                                                            self.name))

        rx_raw= tr.execute(dev.bus, dev.address, self.code, tx_raw)

        if not tr.rd_fmt is None:
            # Unpack the result, again wrapping against ParameterRequest.
            kwargs_unpack= kwargs.copy()
            kwargs_unpack["raw"]= rx_raw
            kwargs_unpack["LSB_first"]= dev._LSB_first
            rx= dev.with_params(self.name, is_read, tr.rd_fmt.unpack,
                                self._param_fetch_kwargs, **kwargs_unpack)

            return rx

    def prepare(self, dev):
        pass

    def read(self, dev, **kwargs):
        if self.tr_read is None: raise PMBus_Command_Error(
            "No read transaction for command: %s" % self.name)

        return self.execute(self.tr_read, True, dev, **kwargs)

    def write(self, dev, value={}, **kwargs):
        if self.tr_write is None: raise PMBus_Command_Error(
            "No write transaction for command: %s" % self.name)

        kwargs["value"]= value
        return self.execute(self.tr_write, False, dev, **kwargs)

class Paged(object):
    """Wraps a PRM or CMD object, and represents a paged instance of that
    value."""

    def __init__(self, wrapped, page):
        self.page= page
        self.wrapped= wrapped
        self.name= (wrapped.name, page)

    def prepare(self, dev):
        self.wrapped.prepare(dev)

    def read(self, dev, *args, **kwargs):
        if dev.read("PAGE") != self.page: dev.write("PAGE", self.page)
        param_fetch_kwargs= { "PAGE" : self.page }
        return self.wrapped.read(dev, param_fetch_kwargs=param_fetch_kwargs,
                                 *args, **kwargs)

    def write(self, dev, *args, **kwargs):
        if dev.read("PAGE") != self.page: dev.write("PAGE", self.page)
        param_fetch_kwargs= { "PAGE" : self.page }
        self.wrapped.write(dev, param_fetch_kwargs=param_fetch_kwargs,
                           *args, **kwargs)

class RPC(object):
    """RPC represents either the read or write transaction of PMBus command
    (CMD).  Note that each of these may be an SMBus Call transaction i.e. an
    RPC in its own right, and that the effect of the 'read' and 'write' may be
    only loosely coupled.
    
    The SMBus transaction (including size) is selected according to the
    supplied format."""

    wr_fmt= None
    rd_fmt= None

    def __init__(self, wr_fmt=None, rd_fmt=None):
        self.wr_fmt= wr_fmt
        self.rd_fmt= rd_fmt

        if wr_fmt is None:
            wr_len= 0
        else:
            wr_bitlength= wr_fmt.length()
            if wr_bitlength is None:
                wr_len= None
            elif wr_bitlength % 8 != 0:
                raise PMBus_Config_Error(
                    "PMBus payload must be a multiple of 8")
            else:
                wr_len= wr_bitlength / 8

        if rd_fmt is None:
            # No read phase.
            if wr_len is None or wr_len > 2:
                # If the length is variable (None) or doesn't fit in a word,
                # it's a block write.
                self._tr= SMB.BlockWrite()
            else:
                # Otherwise it's a byte or word write.  This *could* be
                # invalidated if there are 1 or 2-word block write commands,
                # but I haven't seen any yet.
                self._tr= SMB.Write(wr_len)
        else:
            # Any RPC with a read phase is a block write, block read process
            # call.  PMBus doesn't use word-write process calls.

            rd_bitlength= rd_fmt.length()
            if rd_bitlength is None:
                rd_len= None
            elif rd_bitlength % 8 != 0:
                raise PMBus_Config_Error(
                    "PMBus payload must be a multiple of 8")
            else:
                rd_len= rd_bitlength / 8

            self._tr= SMB.Call(wr_len, rd_len)

        # A read-only RPC is a parameter.

    def execute(self, bus, address, command, tx_raw):
        """Execute the underlying SMBus transaction."""
        return self._tr.execute(bus, address, command, tx_raw)

### PMBus commands defined by the specification: Table 26, Appendix 1.
#
standard_commands= [
    PRM(0x00, "PAGE",
        F.Unsigned(8)
    ),
    PRM(0x01, "OPERATION",
        F.Bitfield(
            "r2r2r2p2",
            ["STATE", "MARGIN", "FAULT_RSP"], {
                "STATE"     : F.Tabular("u2", op_state_table),
                "MARGIN"    : F.Tabular("u2", op_margin_table),
                "FAULT_RSP" : F.Tabular("u2", op_fault_table),
            }
        ),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x02, "ON_OFF_CONFIG",
        F.Bitfield(
            "p3b1b1b1b1b1",
            ["MANUAL", "REG_CTRL", "PIN_CTRL", "PIN_ACT_HIGH", "PIN_IMM_OFF"]
        ),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    CMD(0x03, "CLEAR_FAULTS",
        RPC()
    ),
    PRM(0x04, "PHASE",
        F.Unsigned(8)
    ),
    #CMD(0x05, "PAGE_PLUS_WRITE",
        #RPC(None)
    #),
    #CMD(0x06, "PAGE_PLUS_READ",
        #None,
        #RPC(None,
            #None)
    #),
    # 0x07 - 0x0F are reserved
    PRM(0x10, "WRITE_PROTECT",
        F.Tabular("u8", wp_table)
    ),
    CMD(0x11, "STORE_DEFAULT_ALL",
        RPC()
    ),
    CMD(0x12, "RESTORE_DEFAULT_ALL",
        RPC()
    ),
    CMD(0x13, "STORE_DEFAULT_CODE",
        RPC(F.Unsigned(8))
    ),
    CMD(0x14, "RESTORE_DEFAULT_CODE",
        RPC(F.Unsigned(8))
    ),
    CMD(0x15, "STORE_USER_ALL",
        RPC()
    ),
    CMD(0x16, "RESTORE_USER_ALL",
        RPC()
    ),
    CMD(0x17, "STORE_USER_CODE",
        RPC(F.Unsigned(8))
    ),
    CMD(0x18, "RESTORE_USER_CODE",
        RPC(F.Unsigned(8))
    ),
    PRM(0x19, "CAPABILITY",
        F.Bitfield(
            "b1r2b1p4",
            ["PEC", "SPEED", "SMBALERT"], {
                "SPEED" : F.Tabular("u2", bus_speed_table)
            }
        ), writeable=False
    ),
    CMD(0x1A, "QUERY",
        None,
        RPC(F.Unsigned(8),
            F.Bitfield(
                "b1b1b1r3p2",
                ["SUPPORTED", "WRITE", "READ", "FORMAT"], {
                    "FORMAT" : F.Tabular("u3", query_format_table)
                }
            )
        )
    ),
    CMD(0x1B, "SMBALERT_MASK",
        RPC(F.Bitfield("r8u8", ["MASK", "CODE"])),
        RPC(F.Unsigned(8, "CODE"),
            F.Raw(8, "MASK"))
    ),
    # 0x1C - 0x1F are reserved
    PRM(0x20, "VOUT_MODE",
        F.TaggedUnion(
            "r3p5", "MODE", F.Tabular("u3", vout_mode_table), {
                "LINEAR" : F.Bitfield("p3s5", ["N"]),
                "VID"    : F.Bitfield("p3u5", ["VID_CODE"]),
                "DIRECT" : F.Bitfield("p8", [])
            }
        ),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x21, "VOUT_COMMAND",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x22, "VOUT_TRIM",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x23, "VOUT_CAL_OFFSET",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x24, "VOUT_MAX",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x25, "VOUT_MARGIN_HIGH",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x26, "VOUT_MARGIN_LOW",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x27, "VOUT_TRANSITION_RATE",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x28, "VOUT_DROOP",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x29, "VOUT_SCALE_LOOP",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x2A, "VOUT_SCALE_MONITOR",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    # 0x2B - 0x2F are reserved
    CMD(0x30, "COEFFICIENTS",
        None,
        RPC(F.Bitfield("u8p7b1", ["NAME", "READ"]),
            F.Bitfield("s8s16s16", ["R","b","m"]))
    ),
    PRM(0x31, "POUT_MAX",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x32, "MAX_DUTY",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x33, "FREQUENCY_SWITCH",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    # 0x34 is reserved
    PRM(0x35, "VIN_ON",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x36, "VIN_OFF",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x37, "INTERLEAVE",
        F.Bitfield(
            "p4u4u4u4",
            ["GROUP_ID", "NUMBER_IN_GROUP", "INTERLEAVE_ORDER"]
        ),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x38, "IOUT_CAL_GAIN",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x39, "IOUT_CAL_OFFSET",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x3A, "FAN_CONFIG_1_2",
        fan_config_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x3B, "FAN_COMMAND_1",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x3C, "FAN_COMMAND_2",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x3D, "FAN_CONFIG_3_4",
        fan_config_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x3E, "FAN_COMMAND_3",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x3F, "FAN_COMMAND_4",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x40, "VOUT_OV_FAULT_LIMIT",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x41, "VOUT_OV_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x42, "VOUT_OV_WARN_LIMIT",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x43, "VOUT_UV_WARN_LIMIT",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x44, "VOUT_UV_FAULT_LIMIT",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x45, "VOUT_UV_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x46, "IOUT_OC_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x47, "IOUT_OC_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x48, "IOUT_OC_LV_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x49, "IOUT_OC_LV_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x4A, "IOUT_OC_WARN_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x4B, "IOUT_UC_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x4C, "IOUT_UC_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    # 0x4D - 0x4E are reserved
    PRM(0x4F, "OT_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x50, "OT_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x51, "OT_WARN_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x52, "UT_WARN_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x53, "UT_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x54, "UT_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x55, "VIN_OV_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x56, "VIN_OV_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x57, "VIN_OV_WARN_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x58, "VIN_UV_WARN_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x59, "VIN_UV_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x5A, "VIN_UV_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x5B, "IIN_OC_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x5C, "IIN_OC_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x5D, "IIN_OC_WARN_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x5E, "POWER_GOOD_ON",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x5F, "POWER_GOOD_OFF",
        PF.Vout(),
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x60, "TON_DELAY",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x61, "TON_RISE",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x62, "TON_MAX_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x63, "TON_MAX_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x64, "TOFF_DELAY",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x65, "TOFF_FALL",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x66, "TOFF_MAX_WARN_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    # 0x67 is reserved
    PRM(0x68, "POUT_OP_FAULT_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x69, "POUT_OP_FAULT_RESPONSE",
        fault_response_fmt,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x6A, "POUT_OP_WARN_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x6B, "PIN_OP_WARN_LIMIT",
        tags= {
          "persistent_cfg" : True,
        }
    ),
    # 0x6C - 0x77 are reserved
    PRM(0x78, "STATUS_BYTE",
        F.Flags(
            "b1b1b1b1b1b1b1b1",
            ["BUSY", "OFF", "VOUT_OV", "IOUT_OC", "VIN_UV",
             "TEMPERATURE", "CML", "OTHER_7_1"]
        ),
        volatile=True
    ),
    PRM(0x79, "STATUS_WORD",
        F.Flags(
            "b1b1b1b1b1b1b1b1b1b1b1b1b1b1b1b1",
            ["VOUT", "IOUT/POUT", "INPUT", "MFR_SPECIFIC", "POWER_GOOD_LOW",
             "FANS", "STATUS_OTHER", "UNKNOWN", "BUSY", "OFF", "VOUT_OV",
             "IOUT_OC", "VIN_UV", "TEMPERATURE", "CML", "OTHER_7_1"]
        ),
        volatile=True
    ),
    PRM(0x7A, "STATUS_VOUT",
        F.Flags(
            "b1b1b1b1b1b1b1b1",
            ["VOUT_OV_FAULT", "VOUT_OV_WARNING", "VOUT_UV_WARNING",
             "VOUT_UV_FAULT", "VOUT_MAX", "TON_MAX_FAULT", "TOFF_MAX_WARNING",
             "VOUT_TRACKING"]
        ),
        volatile=True
    ),
    PRM(0x7B, "STATUS_IOUT",
        F.Flags(
            "b1b1b1b1b1b1b1b1",
            ["IOUT_OC_FAULT", "IOUT_OC_LV_FAULT", "IOUT_OC_WARNING",
             "IOUT_UC_FAULT", "CURRENT_SHARE", "POWER_LIMIT", "POUT_OP_FAULT",
             "POUT_OP_WARNING"]
        ),
        volatile=True
    ),
    PRM(0x7C, "STATUS_INPUT",
        F.Flags(
            "b1b1b1b1b1b1b1b1",
            ["VIN_OV_FAULT", "VIN_OV_WARNING", "VIN_UV_WARNING",
             "VIN_UV_FAULT", "VIN_INSUFFICIENT", "IIN_OC_FAULT",
             "IIN_OC_WARNING", "PIN_OP_WARNING"]
        ),
        volatile=True
    ),
    PRM(0x7D, "STATUS_TEMPERATURE",
        F.Flags(
            "b1b1b1b1p4",
            ["OT_FAULT", "OT_WARNING", "UT_WARNING", "UT_FAULT"]
        ),
        volatile=True
    ),
    PRM(0x7E, "STATUS_CML",
        F.Flags(
            "b1b1b1b1b1p1b1b1",
            ["CMD_INVALID", "DATA_INVALID", "PEC_FAILED", "MEM_FAULT",
             "PROC_FAULT", "OTHER_C", "OTHER_ML"]
        ),
        volatile=True
    ),
    PRM(0x7F, "STATUS_OTHER",
        F.Flags(
            "p1p1b1b1b1b1b1p1",
            ["FUSE_IN_A", "FUSE_IN_B", "OR_IN_A", "OR_IN_B", "OR_OUT"]
        ),
        volatile=True
    ),
    PRM(0x80, "STATUS_MFR_SPECIFIC",
        volatile=True
    ),
    PRM(0x81, "STATUS_FANS_1_2",
        F.Flags(
            "b1b1b1b1b1b1b1b1",
            ["FAN_1_FAULT", "FAN_2_FAULT", "FAN_1_WARNING", "FAN_2_WARNING",
             "FAN_1_OVERRIDE", "FAN_2_OVERRIDE", "AIRFLOW_FAULT",
             "AIRFLOW_WARNING"]
        ),
        volatile=True
    ),
    PRM(0x82, "STATUS_FANS_3_4",
        F.Flags(
            "b1b1b1b1b1b1p1p1",
            ["FAN_3_FAULT", "FAN_4_FAULT", "FAN_3_WARNING", "FAN_4_WARNING",
             "FAN_3_OVERRIDE", "FAN_4_OVERRIDE"]
        ),
        volatile=True
    ),
    # 0x83 - 0x85 are reserved
    PRM(0x86, "READ_EIN",
        F.Bitfield(
            "r16u8u24",
            ["ENERGY_COUNT", "ROLLOVER_COUNT", "SAMPLE_COUNT"],
            field_fmt= { "ENERGY_COUNT" : queried_fmt }
        ),
        writeable=False,
        volatile=True
    ),
    PRM(0x87, "READ_EOUT",
        F.Bitfield(
            "r16u8u24",
            ["ENERGY_COUNT", "ROLLOVER_COUNT", "SAMPLE_COUNT"],
            field_fmt= { "ENERGY_COUNT" : queried_fmt }
        ),
        writeable=False,
        volatile=True
    ),
    PRM(0x88, "READ_VIN",
        writeable=False,
        volatile=True
    ),
    PRM(0x89, "READ_IIN",
        writeable=False,
        volatile=True
    ),
    PRM(0x8A, "READ_VCAP",
        writeable=False,
        volatile=True
    ),
    PRM(0x8B, "READ_VOUT",
        PF.Vout(),
        writeable=False,
        volatile=True
    ),
    PRM(0x8C, "READ_IOUT",
        writeable=False,
        volatile=True
    ),
    PRM(0x8D, "READ_TEMPERATURE_1",
        writeable=False,
        volatile=True
    ),
    PRM(0x8E, "READ_TEMPERATURE_2",
        writeable=False,
        volatile=True
    ),
    PRM(0x8F, "READ_TEMPERATURE_3",
        writeable=False,
        volatile=True
    ),
    PRM(0x90, "READ_FAN_SPEED_1",
        writeable=False,
        volatile=True
    ),
    PRM(0x91, "READ_FAN_SPEED_2",
        writeable=False,
        volatile=True
    ),
    PRM(0x92, "READ_FAN_SPEED_3",
        writeable=False,
        volatile=True
    ),
    PRM(0x93, "READ_FAN_SPEED_4",
        writeable=False,
        volatile=True
    ),
    PRM(0x94, "READ_DUTY_CYCLE",
        writeable=False,
        volatile=True
    ),
    PRM(0x95, "READ_FREQUENCY",
        writeable=False,
        volatile=True
    ),
    PRM(0x96, "READ_POUT",
        writeable=False,
        volatile=True
    ),
    PRM(0x97, "READ_PIN",
        writeable=False,
        volatile=True
    ),
    PRM(0x98, "PMBUS_REVISION",
        F.Bitfield(
            "u4u4",
            ["PART1", "PART2"]
        ),
        writeable=False
    ),
    PRM(0x99, "MFR_ID",
        F.Raw(),
        block=True,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x9A, "MFR_MODEL",
        F.Raw(),
        block=True,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x9B, "MFR_REVISION",
        F.Raw(),
        block=True,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x9C, "MFR_LOCATION",
        F.Raw(),
        block=True,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x9D, "MFR_DATE",
        F.Raw(),
        block=True,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    PRM(0x9E, "MFR_SERIAL",
        F.Raw(),
        block=True,
        tags= {
          "persistent_cfg" : True,
        }
    ),
    #PRM(0x9F, "APP_PROFILE_SUPPORT"        None,             SMB.BlockRead()),
    PRM(0xA0, "MFR_VIN_MIN"),
    PRM(0xA1, "MFR_VIN_MAX"),
    PRM(0xA2, "MFR_IIN_MAX"),
    PRM(0xA3, "MFR_PIN_MAX"),
    PRM(0xA4, "MFR_VOUT_MIN"),
    PRM(0xA5, "MFR_VOUT_MAX"),
    PRM(0xA6, "MFR_IOUT_MAX"),
    PRM(0xA7, "MFR_POUT_MAX"),
    PRM(0xA8, "MFR_TAMBIENT_MAX"),
    PRM(0xA9, "MFR_TAMBIENT_MIN"),
    PRM(0xAA, "MFR_EFFICIENCY_LL",
        mfr_efficiency_fmt,
        writeable= False
    ),
    PRM(0xAB, "MFR_EFFICIENCY_HL",
        mfr_efficiency_fmt,
        writeable= False
    ),
    PRM(0xAC, "MFR_PIN_ACCURACY",
        F.UnsignedScaled(8, 0.001),
        writeable= False
    ),
    PRM(0xAD, "IC_DEVICE_ID",
        F.Raw(),
        writeable= False,
        block=True
    ),
    PRM(0xAE, "IC_DEVICE_REV",
        F.Raw(),
        writeable= False,
        block=True
    ),
    # 0xAF is reserved
    PRM(0xB0, "USER_DATA_00",
        F.Raw(),
        block=True
    ),
    PRM(0xB1, "USER_DATA_01",
        F.Raw(),
        block=True
    ),
    PRM(0xB2, "USER_DATA_02",
        F.Raw(),
        block=True
    ),
    PRM(0xB3, "USER_DATA_03",
        F.Raw(),
        block=True
    ),
    PRM(0xB4, "USER_DATA_04",
        F.Raw(),
        block=True
    ),
    PRM(0xB5, "USER_DATA_05",
        F.Raw(),
        block=True
    ),
    PRM(0xB6, "USER_DATA_06",
        F.Raw(),
        block=True
    ),
    PRM(0xB7, "USER_DATA_07",
        F.Raw(),
        block=True
    ),
    PRM(0xB8, "USER_DATA_08",
        F.Raw(),
        block=True
    ),
    PRM(0xB9, "USER_DATA_09",
        F.Raw(),
        block=True
    ),
    PRM(0xBA, "USER_DATA_10",
        F.Raw(),
        block=True
    ),
    PRM(0xBB, "USER_DATA_11",
        F.Raw(),
        block=True
    ),
    PRM(0xBC, "USER_DATA_12",
        F.Raw(),
        block=True
    ),
    PRM(0xBD, "USER_DATA_13",
        F.Raw(),
        block=True
    ),
    PRM(0xBE, "USER_DATA_14",
        F.Raw(),
        block=True
    ),
    PRM(0xBF, "USER_DATA_15",
        F.Raw(),
        block=True
    ),
    PRM(0xC0, "MFR_MAX_TEMP_1"),
    PRM(0xC1, "MFR_MAX_TEMP_2"),
    PRM(0xC2, "MFR_MAX_TEMP_3"),
    # 0xC3 - 0xCF are reserved
    # The manufacturer-specific range 0xD0-0xFD is overridden for particular
    # devices.
]
command_by_name= dict((cmd.name, cmd) for cmd in standard_commands)

standard_controls= {
    "VOUT" : {
        "command"  : "VOUT_COMMAND",
        "quantity" : "VOLTAGE",
        "offset"   : "VOUT_TRIM",
        "rate"     : "VOUT_TRANSITION_RATE",
        "margin"   : ("VOUT_MARGIN_LOW", "VOUT_MARGIN_HIGH"),
        "limit"    : (None, "VOUT_MAX"),
        "monitor"  : "VOUT",
        "enable"   : [(("OPERATION", "STATE"), "ON")],
        "disable"  : [(("OPERATION", "STATE"), "SOFT_OFF")],
    },
    "FAN_PWM_1" : {
        "command"   : "FAN_COMMAND_1",
        "quantity"  : "DUTY_CYCLE",
        "conflicts" : ["FAN_RPM_1"],
        "monitor"   : "FAN_SPEED_1",
        "enable"    : [
            (("FAN_CONFIG_1_2", "FANA_ENABLE"), True),
            (("FAN_CONFIG_1_2", "FANA_RPM"),    False),
        ],
        "disable"   : [
            (("FAN_CONFIG_1_2", "FANA_ENABLE"), False),
        ],
    },
    "FAN_PWM_2" : {
        "command"   : "FAN_COMMAND_2",
        "quantity"  : "DUTY_CYCLE",
        "conflicts" : ["FAN_RPM_2"],
        "monitor"   : "FAN_SPEED_2",
        "enable"    : [
            (("FAN_CONFIG_1_2", "FANB_ENABLE"), True),
            (("FAN_CONFIG_1_2", "FANB_RPM"),    False),
        ],
        "disable"   : [
            (("FAN_CONFIG_1_2", "FANB_ENABLE"), False),
        ],
    },
    "FAN_PWM_3" : {
        "command"   : "FAN_COMMAND_3",
        "quantity"  : "DUTY_CYCLE",
        "conflicts" : ["FAN_RPM_3"],
        "monitor"   : "FAN_SPEED_3",
        "enable"    : [
            (("FAN_CONFIG_3_4", "FANA_ENABLE"), True),
            (("FAN_CONFIG_3_4", "FANA_RPM"),    False),
        ],
        "disable"   : [
            (("FAN_CONFIG_3_4", "FANA_ENABLE"), False),
        ],
    },
    "FAN_PWM_4" : {
        "command"   : "FAN_COMMAND_4",
        "quantity"  : "DUTY_CYCLE",
        "conflicts" : ["FAN_RPM_4"],
        "monitor"   : "FAN_SPEED_4",
        "enable"    : [
            (("FAN_CONFIG_3_4", "FANB_ENABLE"), True),
            (("FAN_CONFIG_3_4", "FANB_RPM"),    False),
        ],
        "disable"   : [
            (("FAN_CONFIG_3_4", "FANB_ENABLE"), False),
        ],
    },
    "FAN_RPM_1" : {
        "command"   : "FAN_COMMAND_1",
        "quantity"  : "RPM",
        "conflicts" : ["FAN_PWM_1"],
        "monitor"   : "FAN_SPEED_1",
        "enable"    : [
            (("FAN_CONFIG_1_2", "FANA_ENABLE"), True),
            (("FAN_CONFIG_1_2", "FANA_RPM"),    True),
        ],
        "disable"   : [
            (("FAN_CONFIG_1_2", "FANA_ENABLE"), False),
        ],
    },
    "FAN_RPM_2" : {
        "command"   : "FAN_COMMAND_2",
        "quantity"  : "RPM",
        "conflicts" : ["FAN_PWM_2"],
        "monitor"   : "FAN_SPEED_2",
        "enable"    : [
            (("FAN_CONFIG_1_2", "FANB_ENABLE"), True),
            (("FAN_CONFIG_1_2", "FANB_RPM"),    True),
        ],
        "disable"   : [
            (("FAN_CONFIG_1_2", "FANB_ENABLE"), False),
        ],
    },
    "FAN_RPM_3" : {
        "command"   : "FAN_COMMAND_3",
        "quantity"  : "RPM",
        "conflicts" : ["FAN_PWM_3"],
        "monitor"   : "FAN_SPEED_3",
        "enable"    : [
            (("FAN_CONFIG_3_4", "FANA_ENABLE"), True),
            (("FAN_CONFIG_3_4", "FANA_RPM"),    True),
        ],
        "disable"   : [
            (("FAN_CONFIG_3_4", "FANA_ENABLE"), False),
        ],
    },
    "FAN_RPM_4" : {
        "command"   : "FAN_COMMAND_4",
        "quantity"  : "RPM",
        "conflicts" : ["FAN_PWM_4"],
        "monitor"   : "FAN_SPEED_4",
        "enable"    : [
            (("FAN_CONFIG_3_4", "FANB_ENABLE"), True),
            (("FAN_CONFIG_3_4", "FANB_RPM"),    True),
        ],
        "disable"   : [
            (("FAN_CONFIG_3_4", "FANB_ENABLE"), False),
        ],
    },
    "FREQUENCY_SWITCH" : {
        "command"  : "FREQUENCY_SWITCH",
        "quantity" : "FREQUENCY",
        "monitor"  : "FREQUENCY",
    }
}

standard_monitors= {
    "EIN" : {
        "key" : "READ_EIN",
        "quantity" : "ENERGY",
        "comparators" : { },
    },
    "EOUT" : {
        "key" : "READ_EOUT",
        "quantity" : "ENERGY",
        "comparators" : { },
    },
    "VIN" : {
        "key" : "READ_VIN",
        "quantity" : "VOLTAGE",
        "comparators" : {
            "FAULT_HIGH" : (
                "VIN_OV_FAULT_LIMIT",
                ("STATUS_INPUT", "VIN_OV_FAULT")
            ),
            "FAULT_LOW"  : (
                "VIN_UV_FAULT_LIMIT",
                ("STATUS_INPUT", "VIN_UV_FAULT")
            ),
            "WARN_HIGH"  : (
                "VIN_OV_WARN_LIMIT",
                ("STATUS_INPUT", "VIN_OV_WARNING")
            ),
            "WARN_LOW"   : (
                "VIN_UV_WARN_LIMIT",
                ("STATUS_INPUT", "VIN_UV_WARNING")
            ),
        },
    },
    "IIN" : {
        "key" : "READ_IIN",
        "quantity" : "CURRENT",
        "comparators" : {
            "FAULT_HIGH" : (
                "IIN_OC_FAULT_LIMIT",
                ("STATUS_INPUT", "IIN_OC_FAULT")
            ),
            "WARN_HIGH"  : (
                "IIN_OC_WARN_LIMIT",
                ("STATUS_INPUT", "IIN_OC_WARNING")
            ),
        },
    },
    "VCAP" : {
        "key" : "READ_VCAP",
        "quantity" : "VOLTAGE",
        "comparators" : { },
    },
    "VOUT" : {
        "key" : "READ_VOUT",
        "quantity" : "VOLTAGE",
        "comparators" : {
            "FAULT_HIGH" : (
                "VOUT_OV_FAULT_LIMIT",
                ("STATUS_VOUT", "VOUT_OV_FAULT")
            ),
            "FAULT_LOW"  : (
                "VOUT_UV_FAULT_LIMIT",
                ("STATUS_VOUT", "VOUT_UV_FAULT")
            ),
            "WARN_HIGH"  : (
                "VOUT_OV_WARN_LIMIT",
                ("STATUS_VOUT", "VOUT_OV_WARNING")
            ),
            "WARN_LOW"   : (
                "VOUT_UV_WARN_LIMIT",
                ("STATUS_VOUT", "VOUT_UV_WARNING")
            ),
            "FAULT_LOW_OC"  : (
                "IOUT_OC_LV_FAULT_LIMIT",
                ("STATUS_IOUT", "IOUT_OC_LV_FAULT")
            ),
        },
    },
    "IOUT" : {
        "key" : "READ_IOUT",
        "quantity" : "CURRENT",
        "comparators" : {
            "FAULT_HIGH" : (
                "IOUT_OC_FAULT_LIMIT",
                ("STATUS_IOUT", "IOUT_OC_FAULT")
            ),
            "FAULT_LOW"  : (
                "IOUT_UC_FAULT_LIMIT",
                ("STATUS_IOUT", "IOUT_UC_FAULT")
            ),
            "WARN_HIGH"  : (
                "IOUT_OC_WARN_LIMIT",
                ("STATUS_IOUT", "IOUT_OC_WARNING")
            ),
        },
    },
    "TEMPERATURE_1" : {
        "key" : "READ_TEMPERATURE_1",
        "quantity" : "TEMPERATURE",
        "comparators" : {
            "FAULT_HIGH" : (
                "OT_FAULT_LIMIT",
                ("STATUS_TEMPERATURE", "OT_FAULT")
            ),
            "FAULT_LOW"  : (
                "UT_FAULT_LIMIT",
                ("STATUS_TEMPERATURE", "UT_FAULT")
            ),
            "WARN_HIGH"  : (
                "OT_WARN_LIMIT",
                ("STATUS_TEMPERATURE", "OT_WARNING")
            ),
            "WARN_LOW"   : (
                "UT_WARN_LIMIT",
                ("STATUS_TEMPERATURE", "UT_WARNING")
            ),
        },
    },
    "TEMPERATURE_2" : {
        "key" : "READ_TEMPERATURE_2",
        "quantity" : "TEMPERATURE",
        "comparators" : {
            "FAULT_HIGH" : (
                "OT_FAULT_LIMIT",
                ("STATUS_TEMPERATURE", "OT_FAULT")
            ),
            "FAULT_LOW"  : (
                "UT_FAULT_LIMIT",
                ("STATUS_TEMPERATURE", "UT_FAULT")
            ),
            "WARN_HIGH"  : (
                "OT_WARN_LIMIT",
                ("STATUS_TEMPERATURE", "OT_WARNING")
            ),
            "WARN_LOW"   : (
                "UT_WARN_LIMIT",
                ("STATUS_TEMPERATURE", "UT_WARNING")
            ),
        },
    },
    "TEMPERATURE_3" : {
        "key" : "READ_TEMPERATURE_3",
        "quantity" : "TEMPERATURE",
        "comparators" : {
            "FAULT_HIGH" : (
                "OT_FAULT_LIMIT",
                ("STATUS_TEMPERATURE", "OT_FAULT")
            ),
            "FAULT_LOW"  : (
                "UT_FAULT_LIMIT",
                ("STATUS_TEMPERATURE", "UT_FAULT")
            ),
            "WARN_HIGH"  : (
                "OT_WARN_LIMIT",
                ("STATUS_TEMPERATURE", "OT_WARNING")
            ),
            "WARN_LOW"   : (
                "UT_WARN_LIMIT",
                ("STATUS_TEMPERATURE", "UT_WARNING")
            ),
        },
    },
    "FAN_SPEED_1" : {
        "key" : "READ_FAN_SPEED_1",
        "quantity" : "RPM",
        "comparators" : {
            "FAULT_LOW"  : (
                None,
                ("STATUS_FANS_1_2", "FAN_1_FAULT")
            ),
            "WARN_LOW"   : (
                None,
                ("STATUS_FANS_1_2", "FAN_1_WARNING")
            ),
        },
    },
    "FAN_SPEED_2" : {
        "key" : "READ_FAN_SPEED_2",
        "quantity" : "RPM",
        "comparators" : {
            "FAULT_LOW"  : (
                None,
                ("STATUS_FANS_1_2", "FAN_2_FAULT")
            ),
            "WARN_LOW"   : (
                None,
                ("STATUS_FANS_1_2", "FAN_2_WARNING")
            ),
        },
    },
    "FAN_SPEED_3" : {
        "key" : "READ_FAN_SPEED_3",
        "quantity" : "RPM",
        "comparators" : {
            "FAULT_LOW"  : (
                None,
                ("STATUS_FANS_3_4", "FAN_3_FAULT")
            ),
            "WARN_LOW"   : (
                None,
                ("STATUS_FANS_3_4", "FAN_3_WARNING")
            ),
        },
    },
    "FAN_SPEED_4" : {
        "key" : "READ_FAN_SPEED_4",
        "quantity" : "RPM",
        "comparators" : {
            "FAULT_LOW"  : (
                None,
                ("STATUS_FANS_3_4", "FAN_4_FAULT")
            ),
            "WARN_LOW"   : (
                None,
                ("STATUS_FANS_3_4", "FAN_4_WARNING")
            ),
        },
    },
    "DUTY_CYCLE" : {
        "key" : "READ_DUTY_CYCLE",
        "quantity" : "DUTY_CYCLE",
        "comparators" : { },
    },
    "FREQUENCY" : {
        "key" : "READ_FREQUENCY",
        "quantity" : "FREQUENCY",
        "comparators" : { },
    },
    "POUT" : {
        "key" : "READ_POUT",
        "quantity" : "FREQUENCY",
        "comparators" : {
            "FAULT_HIGH" : (
                "POUT_OP_FAULT_LIMIT",
                ("STATUS_IOUT", "POUT_OP_FAULT")
            ),
            "WARN_HIGH"  : (
                "POUT_OP_WARN_LIMIT",
                ("STATUS_IOUT", "POUT_OP_WARNING")
            ),
        },
    },
    "PIN" : {
        "key" : "READ_PIN",
        "quantity" : "POWER",
        "comparators" : {
            "WARN_HIGH"  : (
                "PIN_OP_WARN_LIMIT",
                ("STATUS_INPUT", "PIN_OP_WARNING")
            ),
        },
    },
    "TON" : {
        "key" : None,
        "quantity" : "TIME",
        "comparators" : {
            "FAULT_HIGH"  : (
                "TON_MAX_FAULT_LIMIT",
                ("STATUS_VOUT", "TON_MAX_FAULT")
            ),
        },
    },
    "TOFF" : {
        "key" : None,
        "quantity" : "TIME",
        "comparators" : {
            "WARN_HIGH"  : (
                "TOFF_MAX_WARN_LIMIT",
                ("STATUS_VOUT", "TOFF_MAX_WARNING")
            ),
        },
    },
}
