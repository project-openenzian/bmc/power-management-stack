import io
import Queue
import os
import os.path
import signal
import threading
from time import sleep
import serial

from . import i2cbus

id_string= u'WHITEBAIT_USB_I2C_BRIDGE'
baud_rate= 115200

class DeviceTimeout(Exception):
    pass

def alert_handler(ll_logger, exit_event, alert_queue, alert_handlers):
    ll_logger.info("Alert thread %s starting.",
                   threading.current_thread().name)

    try:
        while True:
            if exit_event.is_set(): break

            try:
                event= alert_queue.get(block=True, timeout=0.25)
            except Queue.Empty:
                continue

            if event in alert_handlers:
                ll_logger.info("Calling handler for %s.", event)
                handler, args= alert_handlers[event]
                handler(self, event, *args)
            else:
                ll_logger.warning("No handler for %s, dropped.", event)
    except:
        ll_logger.error("Alert thread %s exiting on exception.",
                        threading.current_thread().name, exc_info=True)
        return

    ll_logger.info("Alert thread %s exiting.",
                   threading.current_thread().name)

def reader(ll_logger, exit_event, serial, alert_queue, rx_queue):
    ll_logger.info("RX thread %s starting.", threading.current_thread().name)

    while True:
        if exit_event.is_set(): break

        S= serial.readline()
        # Timeout
        if len(S) == 0: continue
        # Strip trailing newlines.
        S= S.rstrip("\r\n")

        # Don't forward empty lines
        if len(S) == 0: continue

        # Filter alerts
        if S[0] == '$':
            if len(S) == 9:
                try:
                    if S[3] == "B": alert_queue.put("BUS_ERROR")
                    if S[4] == "R": alert_queue.put("ARB_LOST")
                    if S[5] == "O": alert_queue.put("OVERRUN")
                    if S[6] == "P": alert_queue.put("CRC_ERROR")
                    if S[7] == "T": alert_queue.put("TIMEOUT")
                    if S[8] == "A": alert_queue.put("ALERT")
                except Queue.Full:
                    ll_logger.error("Alert overflow")
            else:
                ll_logger.warning("Dropped malformed alert: %s", S)

            continue

        ll_logger.debug("RX \"%s\"", S)

        try:
            rx_queue.put(S, block=False)
        except Queue.Full:
            # If the queue isn't processed fast enough, drop lines.
            ll_logger.error("RX overflow")

    ll_logger.info("RX thread %s exiting.", threading.current_thread().name)

### Programs

class PStatement(object):
    pass

class PAddress(PStatement):
    def __init__(self, address):
        self.address= address

    def encode(self, lines, io_maxlen):
        S= u"<%02X" % self.address

        if io_maxlen - len(lines[-1]) < len(S):
            lines.append(u"")
        lines[-1]+= S

    def __str__(self):
        return "Address 0x%02X" % self.address

class PWrite(PStatement):
    def __init__(self, payload):
        self.payload= payload

    def encode(self, lines, io_maxlen):
        S= u"W"

        if io_maxlen - len(lines[-1]) < len(S):
            lines.append(u"")
        lines[-1]+= S

        for x in self.payload:
            S= u"%02X" % x
            if io_maxlen - len(lines[-1]) < len(S):
                lines.append(u"+")
            lines[-1]+= S

    def __str__(self):
        return "Write " + "".join("%02X" % x for x in self.payload)

class PRead(PStatement):
    def __init__(self, length, variable=False):
        self.length= length
        self.variable= variable

    def encode(self, lines, io_maxlen):
        if self.variable:
            S= u"V%02X" % self.length
        else:
            S= u"R%02X" % self.length

        if io_maxlen - len(lines[-1]) < len(S):
            lines.append(u"")
        lines[-1]+= S

    def __str__(self):
        if self.variable:
            return "Read Variable (max %d)" % self.length
        else:
            return "Read %d" % self.length

class PDone(PStatement):
    def encode(self, lines, io_maxlen):
        S= u">"

        if io_maxlen - len(lines[-1]) < len(S):
            lines.append(u"")
        lines[-1]+= S

    def __str__(self):
        return "Done"

def encode_program(P, io_maxlen, prefix=u""):
    lines= [prefix]
    for S in P:
        S.encode(lines, io_maxlen)
    return lines

### Device Responses

class DeviceMessage(object):
    def is_complete(self):
        return False

    def __add__(self, other):
        if self.is_complete():
            raise Exception("Tried to extend a complete message.")
        return other

    def __str__(self):
        return "Empty message"

class UnknownMessage(DeviceMessage):
    def __init__(self, string):
        self.string= string

    def is_complete(self):
        return False

    def __str__(self):
        return "Unknown message type: %s" % self.string

class DebugMessage(DeviceMessage):
    def __init__(self, string):
        self.string= string

    def is_complete(self):
        return False

    def __str__(self):
        return "Debug output: %s" % self.string

class AsyncMessage(DeviceMessage):
    def is_complete(self):
        return True

    pass

class AlertMessage(AsyncMessage):
    def __str__(self):
        return "SMBus alert"

class ResponseMessage(DeviceMessage):
    pass

class ErrorResponse(ResponseMessage):
    def __init__(self, string):
        self.string= string

    def is_complete(self):
        return True

    def __str__(self):
        return "Command error: %s" % self.string

class NAckError(ErrorResponse):
    def __init__(self, pos):
        self.pos= pos

    def __str__(self):
        return "NAck after %d bytes" % self.pos

class StatusResponse(ResponseMessage):
    def __init__(self, field_map, last):
        self.field_map= field_map
        self.last= last

    def is_complete(self):
        return self.last

    def __add__(self, other):
        if self.is_complete():
            raise Exception("Tried to extend a complete message.")
        if not isinstance(other, StatusResponse):
            raise Exception("Tried to extend with non-StatusResponse message.")

        FM= dict(self.field_map)
        FM.update(other.field_map)
        return StatusResponse(FM, other.last)

    def __str__(self):
        return ("Status:\n\t" + \
            "\n\t".join("%s = %s" % (f,v) for \
                            f,v in self.field_map.items()))

class OKResponse(ResponseMessage):
    def __init__(self, payload, remaining):
        self.payload= payload
        self.remaining= remaining

    def is_complete(self):
        return self.remaining <= len(self.payload)

    def __add__(self, other):
        if self.is_complete():
            raise Exception("Tried to extend a complete message.")
        if not isinstance(other, OKResponse):
            raise Exception("Tried to extend with non-OKResponse message.")

        return OKResponse(self.payload + other.payload, self.remaining)

    def __str__(self):
        return ("Command OK, length %d, payload: " % self.remaining +
                "".join("%02X" % x for x in self.payload))

class ReadyResponse(ResponseMessage):
    def is_complete(self):
        return True

    def __str__(self):
        return "Ready"

def decode_line(S):
    """Decode one line from the device.  Responses may be split across
    several."""

    if len(S) == 0:
        return UnknownMessage(S)
    elif S[0] == '#':
        return DebugMessage(S[1:])
    elif S[0] == '*':
        if len(S) < 2: return UnknownMessage(S)
        elif S[1] == 'E':
            if len(S) >= 10 and S[3:7] == "NACK":
                pos= int(S[8:10], 16)
                return NAckError(pos)
            return ErrorResponse(S[2:])
        elif S[1] == 'O':
            if len(S) < 5: return UnknownMessage(S)
            else:
                # Response with payload
                remaining= int(S[3:5], 16)
                payload= [int(S[i:i+2], 16) for i in range(6, len(S)-1, 2)]
                return OKResponse(payload, remaining)
        elif S[1] == 'S':
            if len(S) < 3: return UnknownMessage(S)
            if S[2] == "+":
                last= False
            elif S[2] == "$":
                last= True
            else:
                return UnknownMessage(S)

            try:
                field, value= S[3:].split(':')
            except ValueError:
                return UnknownMessage(S)

            return StatusResponse({field : value}, last)
        elif S[1] == 'R':
            return ReadyResponse()
        else:
            return UnknownMessage(S)
    elif S[0] == '$':
        if S[1] != 'I': return UnknownMessage(S)

        # SMBAlert - All other alerts are ignored.
        if len(S) >= 8 and S[8] == 'A':
            return AlertMessage()
        else:
            return UnknownMessage(S)
    else:
        return UnknownMessage(S)

class Whitebait_I2CBus(i2cbus.I2CBus):
    """ An 'Whitebait_I2CBus' object implements the 'I2CBus' interface by
    offloading communication to a Whitebait USB I2C dongle.

    Section references are to the I2C-bus specification and user manual,
    revision 6 (2014) - UM10204.
    """

    initialised= False
    devfd= None
    tx_file= None
    rx_file= None
    rx_queue= None
    rx_thread= None
    alert_queue= None
    alert_thread= None
    alert_handlers= {}
    exit_event= None
    io_maxlen= None
    identity= None
    execute_lock= threading.Lock()
    exit_now= False

    def __init__(self, devpath, *args, **kwargs):
        self.identity= os.path.basename(devpath)

        super(Whitebait_I2CBus,self).__init__(*args, **kwargs)

        # Open the underlying device read-write
        self.ll_logger.debug("Opening %s", devpath)
        #self.devfd= os.open(devpath, os.O_RDWR)
        self.serial= serial.Serial(port=devpath, baudrate=baud_rate,
                                   rtscts=1, timeout=0.25)

        # Do a hardware reset.
        self.ll_logger.debug("Sending hardware reset.")
        self.hard_reset()

        if not self.probe_bridge():
            raise i2cbus.I2CBus_ConfigError("Bridge not found")

        # Used to signal the handler threads.
        self.exit_event= threading.Event()

        # Start alert handler thread.
        self.alert_queue= Queue.Queue()
        self.alert_thread= threading.Thread(
            target=alert_handler,
            args=(self.ll_logger, self.exit_event, self.alert_queue,
                  self.alert_handlers
            )
        )
        self.alert_thread.start()

        # Start RX thread to block on serial input.
        self.rx_queue= Queue.Queue()
        self.rx_thread= threading.Thread(
            target=reader,
            args=(self.ll_logger, self.exit_event, self.serial,
                  self.alert_queue, self.rx_queue
            )
        )
        self.rx_thread.start()

        self.initialised= True

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.ll_logger.info("Context manager exit")

        # Stop child processes
        self.stop_children()

        # Close the device file
        self.serial.close()

        self.initialised= False

    def __del__(self):
        self.ll_logger.info("Destructor")

        if not self.initialised: return

        # Stop child processes
        self.stop_children()

        # Close the device file
        self.serial.close()

    def stop_children(self):
        self.ll_logger.info("Cleanup in PID %d", os.getpid())
        self.ll_logger.info("Signalling all threads.")
        self.exit_event.set()

        if not self.rx_thread is None:
            self.ll_logger.info(
                "Waiting for RX thread %s", self.rx_thread.name)
            self.rx_thread.join(2)
            if self.rx_thread.is_alive():
                self.ll_logger.error("RX thread failed to stop")
            else:
                self.rx_thread= None
        else:
            self.ll_logger.info("RX thread already stopped")

        if not self.alert_thread is None:
            self.ll_logger.info(
                "Waiting for alert thread %s", self.alert_thread.name)
            self.alert_thread.join(2)
            if self.alert_thread.is_alive():
                self.ll_logger.error("Alert thread failed to stop")
            else:
                self.alert_thread= None
        else:
            self.ll_logger.info("Alert thread already stopped")

    def bus_id(self):
        return self.identity

    def sendline(self, S):
        self.ll_logger.debug("TX \"%s\"", S)
        self.serial.write(bytes(S + "\n"))

    def readline_raw(self):
        S= self.serial.readline()
        S= S.rstrip("\r\n")
        self.ll_logger.debug("RX \"%s\"", S)
        return S

    def recvline(self, timeout=1.0, raw=False):
        """Receive a single line from the bridge, with timeout."""
        try:
            if raw:
                return self.readline_raw()
            else:
                return self.rx_queue.get(block=True, timeout=timeout)
        except Queue.Empty:
            raise DeviceTimeout()

    def recv(self, timeout=5.0, raw=False):
        """Read as many lines as necessary to build a complete device response
        (OK, Error, ...)."""

        # Start with an empty message.
        M= DeviceMessage()

        # As long as the message is incomplete:
        while not M.is_complete():
            # Receive the next line.
            S= self.recvline(timeout, raw=raw)
            # Decode it.
            M_new= decode_line(S)
            # Extend (or replace) the message.
            M= M + M_new

        return M

    def hard_reset(self):
        """Perform a hard device reset.  Does not depend on serial
        configuration."""

        # Make sure nothing's waiting to be sent when the device comes back
        # up.
        #termios.tcflush(devfd, termios.TCIOFLUSH)
        self.serial.flushOutput()

        # Deassert DTR, to pull NRST low.
        self.serial.dtr= False

        # Hold it for 50ms.
        sleep(50e-3)

        # Reassert DTR, to pull NRST high.
        self.serial.dtr= True

        # Give it time.
        sleep(50e-3)

        # Drop any nonsense output during reset.
        self.serial.flushInput()

    def reset(self, raw=False):
        """Send reset and wait for ready."""

        # Perform a software (R)eset.
        self.ll_logger.debug("Sending software (R)eset.")
        self.sendline(u'R')

        self.ll_logger.debug("Waiting for response.")
        M= self.recv(raw=raw)

        # We may see one alert message if a device is asserting SMBAlert
        # already.
        if isinstance(M, AlertMessage):
            M= self.recv(raw=raw)

        if not isinstance(M, ReadyResponse):
            e= i2cbus.I2CBus_Exception()
            e.message= str(M)
            raise e

    def identify(self, raw=False):
        """Send the IDENTITY command."""

        self.ll_logger.debug("Sending (I)dentify.")
        self.sendline(u'I')
        self.ll_logger.debug("Waiting for response.")
        return self.recv(raw=raw)

    def probe_bridge(self):
        """Use the IDENTITY command to retrieve the ID string, and compare
        with the expected value."""

        # Probe uses direct IO (not via RX process), so cannot be called once
        # that's initialised.
        if self.initialised: return True

        try:
            M= self.identify(raw=True)
            self.ll_logger.debug("Identify response: %s", M)
            if not isinstance(M, StatusResponse):
                return False
            if not 'ID' in M.field_map:
                return False
            if M.field_map['ID'] != id_string:
                return False
            if not 'IO_MAXLEN' in M.field_map:
                return False
            self.io_maxlen= int(M.field_map['IO_MAXLEN'])
            return True
        except DeviceTimeout:
            return False

    def execute(self, P):
        """Send a program for immediate execution, and return the command
        response (or error)."""

        with self.execute_lock:
            lines= encode_program(P, self.io_maxlen, u"!")
            for L in lines:
                self.sendline(L)
            M= self.recv()
            if isinstance(M, OKResponse):
                self.tr_logger.debug(str(M))
                return bytearray(M.payload)
            elif isinstance(M, NAckError):
                self.tr_logger.debug(str(M))
                raise i2cbus.I2CBus_NACK()
            else:
                self.tr_logger.debug("Transport error")
                self.ll_logger.debug(str(M))
                e= i2cbus.I2CBus_Exception()
                e.message= str(M)
                raise e

    def probe(self, address):
        """Probe for an I2C device at `address`.
        
        Sends a 0-byte write and checks for an acknowledgement."""

        try:
            self.tr_logger.debug("probe(0x%02X)", address,
                                 extra={ "protocol" : "I2C" })
            self.execute([ PAddress(address), PWrite([]), PDone() ])
        except i2cbus.I2CBus_NACK:
            return False
        return True

    # Read/Write register operations are not part of the I2C specification,
    # but are extremely common, thus implemented in the base class.

    def write_single(self, address, data):
        """Write the single register (no register address) of a very simple
        device."""

        self.tr_logger.debug("write_single(0x%02X,%s)", address, data,
                             extra={ "protocol" : "I2C" })
        self.execute([
            PAddress(address),
            PWrite(data),
            PDone()
        ])

    def read_single(self, address, N):
        """Read the single register (no register address) of a very simple
        device (or reread the last register accessed)."""

        self.tr_logger.debug("read_single(0x%02X)", address,
                             extra={ "protocol" : "I2C" })
        return self.execute([
            PAddress(address),
            PRead(N),
            PDone()
        ])

    def write_reg(self, address, reg, data):
        """Write a single register at an 8-bit address."""

        self.tr_logger.debug("write_reg(0x%02X,0x%02X,%s)",
                             address, reg, data,
                             extra={ "protocol" : "I2C" })
        self.execute([
            PAddress(address),
            PWrite(bytearray([reg]) + data),
            PDone()
        ])

    def read_reg(self, address, reg, N):
        """Read a single register at an 8-bit address."""

        self.tr_logger.debug("read_reg(0x%02X,0x%02X)",
                             address, reg,
                             extra={ "protocol" : "I2C" })
        return self.execute([
            PAddress(address),
            PWrite([reg]),
            PRead(N),
            PDone()
        ])
