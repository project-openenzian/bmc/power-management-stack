from . import native_smbus
from . import pmbus

class Native_PMBus(native_smbus.Native_SMBus, pmbus.PMBus):

    def __init__(self, *args, **kwargs):
        super(Native_PMBus, self).__init__(*args, **kwargs)

    def group_command(self, commands):
        """Group Command (S5.2.3)"""
        raise pmbus.PMBus_Command_Error("unimplemented")

    def write_word_extended(self, address, command, data, ext_code=0xFF):
        """Extended Command: Write Byte/Word (S5.3.4/S5.2.5)
        
        Defaults to the PMBus extended command set (extension code 0xFF).
        Use write_word_mfr() for manufacturer-specific commands (extension
        code 0xFE)."""

        raise pmbus.PMBus_Command_Error("unimplemented")

    def read_word_extended(self, address, command, N=2, ext_code=0xFF):
        """Extended Command: Read Byte/Word (S5.3.4/S5.2.5)
        
        Defaults to the PMBus extended command set (extension code 0xFF).
        Use read_word_mfr() for manufacturer-specific commands (extension
        code 0xFE)."""

        raise pmbus.PMBus_Command_Error("unimplemented")
