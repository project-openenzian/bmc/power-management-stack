from copy import copy
import logging
import time

from . import formats as F
from .. import bitstruct

default_logfmt= "%(levelname)s:%(name)s:%(identity)s:%(message)s"

# Global root logger for all I2C events
bus_logger= logging.getLogger("i2c")
# Logging of low-level transport events e.g. communication with the I2C
# controller.
ll_logger= bus_logger.getChild("ll")
# Logging of transaction-level events e.g. SMBus Process Call
tr_logger= bus_logger.getChild("tr")

def write_addr(address):
    """Return the 8b 'write address' corresponding to the given 7b I2C device
    address (bit 0 = 0)."""
    return (address << 1)

def read_addr(address):
    """Return the 8b 'read address' corresponding to the given 7b I2C device
    address (bit 0 = 0)."""
    return ((address << 1) | 1)

class I2CBus_Exception(Exception):
    pass

class I2CBus_ConfigError(I2CBus_Exception):
    pass

class I2CBus_Command_Error(I2CBus_Exception):
    pass

class I2CBus_NoDevice(I2CBus_Exception):
    pass

class I2CBus_CacheMiss(I2CBus_Exception):
    pass

class I2CBus_NACK(I2CBus_Exception):
    """Device failed to acknowledge."""
    pass

# I2C reserved addresses (S3.1.12, Table 3).
i2c_rsvd_by_name = {
    "CALL_START"  : 0x00,
    "CBUS"        : 0x01,
    "FORMAT"      : 0x02,
    "FUTURE"      : 0x03,
    "HS_MODE_0"   : 0x04,
    "HS_MODE_1"   : 0x05,
    "HS_MODE_2"   : 0x06,
    "HS_MODE_3"   : 0x07,
    "TEN_BIT_0"   : 0x78,
    "TEN_BIT_1"   : 0x79,
    "TEN_BIT_2"   : 0x7a,
    "TEN_BIT_3"   : 0x7b,
    "DEVICE_ID_0" : 0x7c,
    "DEVICE_ID_1" : 0x7d,
    "DEVICE_ID_2" : 0x7e,
    "DEVICE_ID_3" : 0x7f
}
i2c_rsvd_addr = frozenset(i2c_rsvd_by_name.values())

class I2CBus(object):
    def __init__(self, *args, **kwargs):
        # Per-bus loggers
        self.bus_logger= \
            logging.LoggerAdapter(bus_logger, { "identity" : self.bus_id() })
        self.ll_logger= \
            logging.LoggerAdapter(ll_logger, { "identity" : self.bus_id() })
        self.tr_logger= \
            logging.LoggerAdapter(tr_logger, { "identity" : self.bus_id() })
        self.devices= {}

    def bus_id(self):
        """The ID of the bus

        @return int"""
        raise I2CBus_Command_Error("unimplemented")

    def probe(self, address):
        """Probe the bus at a specific address

        @param address int
        @return bool"""
        raise I2CBus_Command_Error("unimplemented")
    
    def scan(self, start=0x00, end=0x7F, exclude=set()):
        """Probe for I2C devices at addresses from `start` to `end`
        (inclusive), excluding reserved addresses."""
        responses= set()
        for address in range(start, end+1):
            if address in i2c_rsvd_addr or address in exclude: continue
            if self.probe(address): responses.add(address)
        return responses

    def register_device(self, address, device):
        self.devices[address]= device

    def deregister_device(self, address):
        if address in self.devices:
            del self.devices[address]

def nested_update(X, key, path, value):
    """Update a key in a nested dictionary."""
    if len(path) == 0:
        X[key]= value
    else:
        nested_update(X[key], path[0], path[1:], value)

class I2CBus_Device(object):
    """A device at a given address on the given I2CBus."""

    # Overriden by derived classes
    _parameters= []
    _LSB_first= False
    _runtime_params= {}

    def __init__(self, bus, address, skip_probe=False, *args, **kwargs):
        self.bus= bus
        self.address= address

        bus.register_device(address, self)

        # Per-device loggers
        self.dev_logger= \
            logging.LoggerAdapter(bus.bus_logger, { "devaddr" : address })

        # Build a by-name map of parameters.
        self._parameter_map= {}
        for p in self._parameters: self._parameter_map[p.name]= p

        # Dictionary of cached (non-volatile) parameters.
        self._parameter_cache= {}

        # Call the prepare() callback for all parameters.
        for p in self._parameters: p.prepare(self)

        # Initialise superclasses *before* probing.
        super(I2CBus_Device, self).__init__(*args, **kwargs)

        if not skip_probe and not self.probe():
            self.dev_logger.warning("Probe failed")
            raise I2CBus_NoDevice()

    def probe(self):
        raise Exception("Unimplemented base class method")

    def with_params(self, name, is_read, fn, param_fetch_kwargs, **fn_args):
        """Call fn (intended to be a pack() or unpack() method) with keyword
        arguments fn_args, and handle any ParameterRequest exception that it
        raises by reading the requested parameter, adding it to the 'coeff'
        argument and re-running fn.

        fn should have no side-effect if raising ParameterRequest."""
        coeff= {}
        request= []
        while True:
            # Retrieve any parameters requested by fn
            while len(request) > 0:
                param= request.pop()
                # A request may have been satisfied by a previous read (e.g.
                # m, b, R from COEFFICIENTS).
                if not param in coeff:
                    if is_read:
                        self.dev_logger.debug("Retrieving parameter %s " \
                            "for read from %s", param, name)
                    else:
                        self.dev_logger.debug("Retrieving parameter %s " \
                            "for write to %s", param, name)

                    coeff[param]= self.read(param, READ=is_read,
                                            **param_fetch_kwargs)
            try:
                fn_args["coeff"]= coeff
                return fn(**fn_args)
            except F.ParameterRequest as e:
                # Add missing coefficients to the requests
                request.append(e.args[0])

    def read(self, name, **kwargs):
        """Read parameter 'name'."""
        if not name in self._parameter_map:
            raise I2CBus_Command_Error("No such parameter: %s" % str(name))
        return self._parameter_map[name].read(self, **kwargs)

    def write(self, name, *args, **kwargs):
        """Write 'value' to parameter 'name'."""
        if not name in self._parameter_map:
            raise I2CBus_Command_Error("No such parameter: %s" % str(name))
        self._parameter_map[name].write(self, *args, **kwargs)

    def resolve(self, path, *args, **kwargs):
        """Read a nested value: (PARAM, KEY, KEY, ...)."""

        X= self.read(path[0], *args, **kwargs)
        for k in path[1:]: X= X[k]
        return X

    def update(self, path, value, *args, **kwargs):
        """Apply an update of the form (PARAM, KEY, KEY, ...) = VALUE."""

        if len(path[1:]) == 0:
            X= value
        else:
            # RMW subfield
            X= self.read(path[0], *args, **kwargs)
            nested_update(X, path[1], path[2:], value)
        self.write(path[0], X, *args, **kwargs)

    def is_volatile(self, name):
        return self._parameter_map[name].volatile

    def add_derived(self, name, P):
        self.dev_logger.debug("Marking %s as derived from %s", P.name, name)
        assert P.name != name
        self._parameter_map[name].add_derived(P)

    def query_persistent_config(self):
        cfg= {}
        for P in self._parameters:
            if P.tags.get("persistent_cfg", False) and \
               P.readable and P.writeable and not P.volatile:
                cfg[P.name]= P.read(self)
        return cfg

    def __str__(self):
        return "I2CBus device at (7b) 0x%X" % self.address

class Cacheable(object):
    """A possibly-cacheable value e.g. a parameter."""

    volatile= False

    def __init__(self, name, **kwargs):
        self.name = name
        # Parameters whose value may change if ours does
        self.derived= []

    def add_derived(self, other):
        self.derived.append(other)

    def cache_get(self, dev):
        if (not self.volatile) and (self.name in dev._parameter_cache):
            dev.dev_logger.debug("Returning cached value for \"%s\"",
                                 self.name)
            return dev._parameter_cache[self.name]
        else:
            return None

    def cache_set(self, dev, value):
        self.invalidate(dev)
        if not self.volatile: dev._parameter_cache[self.name]= value

    def invalidate(self, dev):
        """Invalidate any cached value."""
        if self.name in dev._parameter_cache:
            dev.dev_logger.debug("Invalidating cached value \"%s\"", self.name)
            del(dev._parameter_cache[self.name])

        # Invalidate derived values
        for V in self.derived:
            assert V.name != self.name
            dev.dev_logger.debug("Invalidating derived value \"%s\""
                                 " of %s", V.name, self.name)
            V.invalidate(dev)

class Runtime(object):
    """A software-side runtime parameter, not stored on the device."""

    volatile=False

    def __init__(self, name, default=None):
        self.name= name
        self.derived= []
        self.default= default

    def prepare(self, dev):
        dev._runtime_params[self.name]= self.default

    def add_derived(self, other):
        self.derived.append(other)

    def read(self, dev, **kwargs):
        try:
            return dev._runtime_params[self.name]
        except KeyError:
            return None

    def write(self, dev, value, **kwargs):
        dev.dev_logger.debug("Runtime parameter \"%s\" updated", self.name)
        for V in self.derived:
            dev.dev_logger.debug("Invalidating derived value \"%s\"", V.name)
            V.invalidate(dev)

        dev._runtime_params[self.name]= value

class Parameter(Cacheable):
    """A Parameter is a formatted value that can be read from and/or written
    to a device as an ordered string of bits."""

    _param_fetch_kwargs= {}

    def __init__(self, name, fmt, tags= {}, readable=True,
                 writeable=True, volatile=False):
        super(Parameter, self).__init__(name)
        # The parameter format
        self.fmt= fmt
        self.tags= tags
        self.readable= readable
        self.writeable= writeable
        # A volatile parameter may change without being explicitly written: it
        # is therefore not cacheable.
        self.volatile= volatile

    def get_args(self):
        return ([self.name, self.fmt], {
            "tags" : self.tags,
            "readable" : self.readable,
            "writeable" : self.writeable,
            "volatile" : self.volatile,
        })

    def prepare(self, dev):
        pass

    def unpack(self, dev, param_fetch_kwargs={}, **kwargs):
        """Unpack a value, handling parameter requests."""
        kwargs["LSB_first"]= dev._LSB_first
        param_fetch_kwargs.update(self._param_fetch_kwargs)
        return dev.with_params(self.name, True, self.fmt.unpack,
                               param_fetch_kwargs, **kwargs)

    def read(self, dev, param_fetch_kwargs={}, **kwargs):
        """Read and interpret a value."""

        if not self.readable:
            raise I2CBus_Command_Error("Parameter %s is not readable" %
                                       self.name)

        # For non-volatile parameters, return the previously-read value.
        value= self.cache_get(dev)
        if not value is None: return value

        # Read the uninterpreted binary value.
        raw= self.read_raw(dev)

        # Unpack it.
        kwargs["raw"]= raw
        value= self.unpack(dev, param_fetch_kwargs, **kwargs)

        # Cache non-volatile parameters.
        self.cache_set(dev, value)

        return value

    def pack(self, dev, param_fetch_kwargs={}, **kwargs):
        """Pack a value, handling parameter requests."""
        kwargs["LSB_first"]=  dev._LSB_first
        param_fetch_kwargs.update(self._param_fetch_kwargs)
        return dev.with_params(self.name, False, self.fmt.pack,
                               param_fetch_kwargs, **kwargs)

    def write(self, dev, value, param_fetch_kwargs={}, **kwargs):
        """Encode and write a value."""

        if not self.writeable:
            raise I2CBus_Command_Error("Parameter %s is not writeable" %
                                       self.name)

        # Pack the value.
        kwargs["value"]=  value
        raw= self.pack(dev, param_fetch_kwargs, **kwargs)

        # Update the cached value (if any).
        self.cache_set(dev, value)

        # Write the raw binary.
        self.write_raw(dev, raw)

    # read_raw() and write_raw() must be implemented by derived classes to
    # read and write the raw value as appropriate.
    def read_raw(self, dev):
        raise Exception("Abstract method.")

    def write_raw(self, dev, raw):
        raise Exception("Abstract method.")

class Register(Parameter):
    """A Register is a Parameter that occupies exactly one device register."""

    def __init__(self, regnum, name, fmt, *args, **kwargs):
        super(Register, self).__init__(name, fmt, *args, **kwargs)
        self.regnum= regnum

    def read_raw(self, dev):
        """Pass the register write directly to the device."""
        return dev.read_reg_raw(self.regnum)

    def write_raw(self, dev, raw):
        """Pass the register read directly to the device."""
        dev.write_reg_raw(self.regnum, raw)

class Field(Parameter):
    """A Field is a Parameter spread across portions of different device
    registers."""

    def __init__(self, name, parts, fmt, *args, **kwargs):
        super(Field, self).__init__(name, fmt, *args, **kwargs)
        self.parts= parts

    def prepare(self, dev):
        """Create the bitstruct patterns to slice the parts out of the
        containing registers (and put them back)."""
        length= 0
        self.patterns= []
        conversions= []
        reg_bits= dev._reg_width * 8

        # For every field part, generate a pattern to extract it from the
        # containing register.
        for reg, high, low in self.parts:
            assert 0 <= low <= high < reg_bits

            # bitstruct doesn't allow the pattern "r0" i.e. zero-length
            # fields, so we need to remember whether or not there's padding at
            # each end.
            if high < reg_bits - 1:
                msb_pad= "r%d" % (reg_bits - high - 1)
                has_msb_pad= True
            else:
                msb_pad= ""
                has_msb_pad= False

            C= "r%d" % (high - low + 1)
            conversions.append(C)
            length+= (high - low + 1)

            if 0 < low:
                lsb_pad= "r%d" % low
            else:
                lsb_pad= ""

            self.patterns.append((reg, msb_pad + C + lsb_pad, has_msb_pad))

        # Generate a pattern to glue the parts together.
        self.pack_pattern= "".join(conversions)

        super(Field, self).prepare(dev)

    def read_raw(self, dev):
        """Read individual parts, and reassemble the raw field."""
        parts= []
        for reg, pattern, has_msb_pad  in self.patterns:
            raw= dev.read_reg_raw(reg)

            regbits= bitstruct.unpack(pattern, raw)
            if has_msb_pad: P= regbits[1]
            else:           P= regbits[0]

            parts.append(P)

        raw= bitstruct.pack(self.pack_pattern, *parts)
        dev.dev_logger.debug("read_field(\"%s\") = %s", self.name, repr(raw))
        return raw

    def write_raw(self, dev, raw):
        """Split the field into parts, and perform a read-modify-write on each
        affected raw register."""
        dev.dev_logger.debug("write_field(\"%s\",%s)", self.name, raw)

        parts= bitstruct.unpack(self.pack_pattern, raw)

        for P, (reg, pattern, has_msb_pad) in zip(parts, self.patterns):
            # Read-modify-write each part.
            raw= dev.read_reg_raw(reg)

            regbits= list(bitstruct.unpack(pattern, raw))
            if has_msb_pad: regbits[1]= P
            else:           regbits[0]= P
            raw= bitstruct.pack(pattern, *regbits)

            dev.write_reg_raw(reg, raw)

class Computed(Cacheable):
    """A computed parameter is read-only, and derived purely from other
    parameters: it does not refer directly to any device register.

    Any required devices parameters are signalled by raising the
    ParameterRequest exception in the 'compute' function."""

    def __init__(self, name, compute, inputs):
        super(Computed, self).__init__(name)
        self.compute= compute
        self.inputs= inputs

    def prepare(self, dev):
        # A computed parameter if volatile iff at least one of its inputs is.
        self.volatile= False
        for I in self.inputs:
            self.volatile|= dev.is_volatile(I)
            # Record the dependency for invalidation.
            dev.add_derived(I, self)

    def read(self, dev, **kwargs):
        value= self.cache_get(dev)
        if not value is None: return value
        value= dev.with_params(self.name, True, self.compute, {}, **kwargs)
        self.cache_set(dev, value)
        return value

    def write(self, *args, **kwargs):
        raise I2CBus_Command_Error("Computed parameter %s is read only." %
                                   self.name)

class Polled(Cacheable):
    """A parameter whose value may by invalid or not ready on any given read.
    A read may be either blocking (polling) or non-blocking (return None on
    invalid data)."""

    def __init__(self, name, source_name, value_field, valid_field,
                       valid_value, *args, **kwargs):
        super(Polled, self).__init__(name, *args, **kwargs)
        self.source_name= source_name
        self.value_field= value_field
        self.valid_field= valid_field
        self.valid_value= valid_value

    def prepare(self, dev):
        # The value must always be checked for freshness.
        self.volatile= True

    def read(self, dev, timeout=1.0, interval=0.1,
             sleep=time.sleep, clock=time.monotonic, **kwargs):
        """Poll for a valid value.  Returns None on timeout."""
        dev.dev_logger.debug("Read from polled parameter %s(%s)",
                             self.name, self.source_name)
        
        start= clock()

        value= dev.read(self.source_name, **kwargs)
        if value[self.valid_field] == self.valid_value:
            return value[self.value_field]

        dev.dev_logger.debug("Polling %s", self.source_name)

        while clock() - start < timeout:
            sleep(interval)
            value= dev.read(self.source_name, **kwargs)
            if value[self.valid_field] == self.valid_value:
                return value[self.value_field]

        dev.dev_logger.debug("Timeout polling %s", self.source_name)
        return None

    def write(self, *args, **kwargs):
        raise I2CBus_Command_Error("Polled parameter %s is read only." %
                                   self.name)

class Multiplexed(Cacheable):
    """A multiplexed parameter is one that requires some configuration of the
    device (e.g. writing to a multiplexer control register) before it can be
    accessed."""

    def __init__(self, name, source_name, configuration, *args, **kwargs):
        """The list of (field, value) pairs in 'configuration' is written to
        the device before the value is read from/written to 'source_name'.
        """
        super(Multiplexed, self).__init__(name, *args, **kwargs)
        self.source_name= source_name
        self.configuration= configuration

    def prepare(self, dev):
        self.volatile= dev.is_volatile(self.source_name)

    def read(self, dev, **kwargs):
        """Keyword argument config_overrides specifies additional/replacement
        configuration values."""
        dev.dev_logger.debug("Read from multiplexed parameter %s", self.name)

        # Check cache.
        value= self.cache_get(dev)
        if not value is None: return value

        # Apply overrides.
        cfg= copy(self.configuration)
        if 'config_overrides' in kwargs:
            cfg.update(kwargs['config_overrides'])

        # Apply the configuration.
        dev.dev_logger.debug("Configuring multiplexer")
        for cname, cvalue in cfg:
            # XXX - this needn't write parameters that are in cache and
            # unchanged.
            dev.write(cname, cvalue, **kwargs)

        # Read the multiplexed value
        dev.dev_logger.debug("Reading underlying value")
        value= dev.read(self.source_name, **kwargs)
        dev.dev_logger.debug("Got %s", value)

        # Update cache.
        self.cache_set(dev, value)
        return value

    def write(self, dev, value, **kwargs):
        """Keyword argument config_overrides specifies additional/replacement
        configuration values."""
        # Apply overrides.
        cfg= copy(self.configuration)
        if 'config_overrides' in kwargs:
            cfg.update(kwargs['config_overrides'])

        # Apply the configuration.
        for cname, cvalue in cfg:
            dev.write(cname, cvalue, **kwargs)

        # Write the multiplexed value
        dev.write(self.source_name, value, **kwargs)

        # Update the cached value (if any).
        self.cache_set(dev, value)

class I2CBus_RegisterDevice(I2CBus_Device):
    """An I2C device with a simple register-array interface."""

    # To be overridden by subclasses
    _reg_width= None # In 8b bytes

    def __init__(self, *args, **kwargs):
        super(I2CBus_RegisterDevice,self).__init__(*args, **kwargs)
        self.last_reg= None

    def read_reg_raw(self, reg_addr):
        self.last_reg= reg_addr
        return self.bus.read_reg(self.address, reg_addr, self._reg_width)

    def write_reg_raw(self, reg_addr, raw):
        self.bus.write_reg(self.address, reg_addr, raw)

    def read_again(self):
        if self.last_reg is None:
            raise I2CBus_Command_Error("No previous register address")
        return self.read_reg_raw(self.last_reg)

class I2CBus_Dummy(I2CBus):
    """The dummy bus takes a list of dummy devices implementing
    read_reg_dummy() and write_reg_dummy(), and simulates a I2C bus."""

    def __init__(self, *args, **kwargs):
        super(I2CBus_Dummy, self).__init__(*args, **kwargs)

    def bus_id(self):
        return "I2CDummy"

    def probe(self, address):
        return address in self.devices

    def write_reg(self, address, reg, data, width):
        if address in self.devices:
            self.bus_logger.debug(
                "write(%02X,%02X,%s,%d) forwarded to dummy device" %
                (address, reg, repr(data), width))
            self.devices[address].write_reg_dummy(reg, data)
        else:
            self.bus_logger.debug("Dummy NACK write to %02X" % address)
            raise I2CBus_NACK()

    def read_reg(self, address, reg, width):
        if address in self.devices:
            self.bus_logger.debug(
                "read(%02X,%02X,%d) forwarded to dummy device" %
                (address, reg, width))
            return self.devices[address].read_reg_dummy(reg)
        else:
            self.bus_logger.debug("Dummy NACK read from %02X" % address)
            raise I2CBus_NACK()

class I2CBus_RegisterDummy(object):
    """Simulates a register-array device."""

    _registers= {}

    def __init__(self, bus, address, initial_values={}, *args, **kwargs):
        self._registers.update(initial_values)

        super(I2CBus_RegisterDummy, self).__init__(bus, address,
                                                   *args, **kwargs)

    def read_reg_dummy(self, reg_addr):
        try:
            return self._registers[reg_addr]
        except KeyError:
            self.dev_logger.debug(
                "No data for register %02X, reading zeroes." % reg_addr)
            return bytearray([0] * self._reg_width)

    def write_reg_dummy(self, reg_addr, value):
        self._registers[reg_addr]= value
