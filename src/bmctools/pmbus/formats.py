"""Generic Data Formats"""

import math
from .. import bitstruct

class FormatException(Exception):
    pass

class FormatOutOfRange(FormatException):
    pass

class UnknownTag(FormatException):
    pass

class MissingField(FormatException):
    pass

class ParameterRequest(FormatException):
    """This exception is raised by the pack/unpack/encode/interpret methods to
    indicate that more information is required in order to interpret or
    construct the format."""
    pass

# Table manipulation
def valid_values(table):
    return set(v for v in table.values() if not v is None)

def reverse_encoding(table, valid):
    return dict((y,x) for x,y in table.items() if y in valid)

class DataFormat(object):
    """Base class for all formats.
    
    Methods pack & unpack serialise and deserialise between a bytearray() and
    a record (a dictionary of field values).
    
    Methods encode and interpret construct and interpret this record , for
    example applying a numeric interpretation such as Linear11."""

    def __init__(self, *args, **kwargs):
        pass

    def interpret(self, **kwargs):
        raise Exception("unimplemented")

    def unpack(self, **kwargs):
        raise Exception("unimplemented")

    def encode(self, **kwargs):
        raise Exception("unimplemented")

    def pack(self, **kwargs):
        raise Exception("unimplemented")

    def length(self, **kwargs):
        """Length in bits.  None - variable."""
        raise Exception("unimplemented")

class Raw(DataFormat):
    """The Raw format simply passes the data returned by the device back
    unmodified e.g. for MFR_x fields."""

    def __init__(self, _length=None, name=None, *args, **kwargs):
        self._length= _length
        self.name= name
        super(Raw, self).__init__(*args, **kwargs)

    def unpack(self, **kwargs):
        return kwargs["raw"]

    def pack(self, **kwargs):
        return kwargs["value"]

    def length(self, **kwargs):
        return self._length

class Bitfield(DataFormat):
    """Bitfields

    The Bitfield format wraps the bitstruct interface to pull apart packed
    structures.  Most formats are an instance of this class.
    """

    def __init__(self, pattern, names, field_fmt={}, *args, **kwargs):
        """pattern - A bitstruct pattern string, decribing data layout in
        MSB-first, big-endian order.
        names - One identifier for each non-padding field in pattern.
        field_fmt - Optional: A format to interpret the given field, with must
        be specified as "rN" i.e. raw bits.
        """

        self.pattern= pattern
        self.names= names
        self.field_fmt= field_fmt
        super(Bitfield, self).__init__(*args, **kwargs)

    def interpret(self, raw_fields={}, **kwargs):
        """The Bitfield superclass itself passes the unpacked fields through
        any given per-field formatting, but does not otherwise interpret the
        record.  Subclasses override this to implement their own
        interpretation, using the base class' methods for packing."""

        for f in raw_fields:
            if f in self.field_fmt:
                # The raw data was reversed, if necessary, before interpret()
                # was called.  Thus the data is now MSB_first.  The
                # is_reversed flag is passed through to inner formats.
                raw_fields[f]= self.field_fmt[f].unpack(LSB_first=False,
                                                        raw=raw_fields[f],
                                                        **kwargs)

        return raw_fields

    def unpack(self, LSB_first=True, raw=None, **kwargs):
        """The default unpack method calls the (sub-)class' interpret method
        on the result.  This generally doesn't need to be overridden."""

        # Little-endian patterns don't seem to work properly in
        # bitstruct, which really only appears to work properly for
        # big-endian MSB-first.  We work around this by reversing the
        # LSB-first data from the PMBus, so that the bitstruct-visible
        # format is always MSB-first.
        if LSB_first:
            raw= bytearray(reversed(raw))
            # Let the interpret method know if the raw data was reversed, so that
            # (for example) array order can be corrected.
            # if we already have the is_reversed flag, our reversal cancelled it out otherwise it's reversed now
            kwargs["is_reversed"] = not kwargs["is_reversed"] if "is_reversed" in kwargs else True

        kwargs["raw_fields"]= \
            bitstruct.unpack_dict(self.pattern, self.names, raw)
        return self.interpret(**kwargs)

    def encode(self, value={}, **kwargs):
        """As for interpret, apply per-field formatting.  Fields not found in
        'fields' will be taken from kwargs."""

        fields= value

        # Complete 'fields' with keyword arguments.
        for f in self.names:
            if not f in fields:
                if f in kwargs: fields[f]= kwargs[f]
                else: raise MissingField(f)

        # Apply per-field formatting.
        raw_fields= {}
        for f in fields:
            if f in self.field_fmt:
                kwargs["value"]= fields[f]
                # As for interpret, inner formats are MSB_first.  The
                # 'will_reverse' flag is passed through (e.g. for arrays).
                raw_fields[f]= self.field_fmt[f].pack(LSB_first=False,
                                                      **kwargs)
            else:
                raw_fields[f]= fields[f]

        return raw_fields

    def pack(self, LSB_first=True, **kwargs):
        """See unpack."""
        if LSB_first:
            # Let inner formats know that their output will be reversed.
            # if we already have the will_reverse flag, our own reversal will cancel it out otherwise we will reverse
            kwargs["will_reverse"] = not kwargs["will_reverse"] if "will_reverse" in kwargs else True
        raw_fields= self.encode(**kwargs)
        raw= bitstruct.pack_dict(self.pattern, self.names, raw_fields)
        if LSB_first: return bytearray(reversed(raw))
        else:         return raw

    def length(self, **kwargs):
        return bitstruct.calcsize(self.pattern)

class Flags(Bitfield):
    """A Bitfield of booleans, interpreted as a set of flags."""

    def __init__(self, *args, **kwargs):
        super(Flags, self).__init__(*args, **kwargs)

    def interpret(self, **kwargs):
        raw_fields= kwargs["raw_fields"]
        return set(f for f in self.names if raw_fields[f])

    def encode(self, **kwargs):
        flags= kwargs["value"]
        return dict((n, n in flags) for n in self.names)

class Tabular(Bitfield):
    """A record with a single field (default "INDEX"), encoded according to
    the given table."""

    def __init__(self, pattern, table, name="INDEX", *args, **kwargs):
        self.table= table
        self.valid= valid_values(table)
        self.reverse= reverse_encoding(table, self.valid)
        self.name= name
        super(Tabular, self).__init__(pattern, [name], *args, **kwargs)

    def interpret(self, raw_fields={}, **kwargs):
        index= raw_fields[self.name]
        if index in self.table:
            X= self.table[index]
        else:
            X= None
        return X

    def encode(self, **kwargs):
        X= kwargs["value"]
        try:
            return {self.name : self.reverse[X]}
        except KeyError:
            raise FormatOutOfRange("Invalid field value: %s" % str(X))

class Array(Bitfield):
    """A fixed-sized array of elements of the given format."""
    def __init__(self, N, elem_pat, elem_fmt, *args, **kwargs):
        self.N= N
        self.elem_fmt= elem_fmt
        super(Array, self).__init__(
            elem_pat * N, range(N),
            field_fmt = dict((i,elem_fmt) for i in range(N)),
            *args, **kwargs)

    def interpret(self, is_reversed=False, **kwargs):
        """Decode all elements via the Bitfield interpretation, and return the
        values as a list."""

        # Bitfield.interpret() will give us one field per array element, with
        # its index as field name.
        fields= super(Array, self).interpret(is_reversed=is_reversed, **kwargs)

        if is_reversed:
            # If the raw data was reversed, our elements will appear in
            # reverse order.
            return [fields[i] for i in range(self.N-1, -1, -1)]
        else:
            return [fields[i] for i in range(0, self.N)]

    def encode(self, value=[], will_reverse=False, **kwargs):
        """Zip elements with their index to create a record, then encode as a
        Bitfield."""

        if will_reverse:
            # If the raw data *will be* reversed, reverse the element order.
            fields = { self.N-1-i : v for i, v in enumerate(value)}
        else:
            fields = { i : v for i, v in enumerate(value)}

        return super(Array, self).encode(fields, will_reverse=will_reverse,
                                         **kwargs)

    def length(self, **kwargs):
        return super(Array, self).length(**kwargs)

class Singleton(Bitfield):
    """A bitfield with a single element, interpreted as a scalar."""
    def __init__(self, pattern, name, *args, **kwargs):
        self.name= name
        super(Singleton, self).__init__(pattern, [name], *args, **kwargs)

    def interpret(self, **kwargs):
        # Bitfield.interpret() will give us a single-element dictionary.
        fields= super(Singleton, self).interpret(**kwargs)

        # Then extract the singleton field.
        return fields[self.name]

    def encode(self, value=None, **kwargs):
        # Any value passed is the singleton field value (e.g. an integer),
        # whereas Bitfield.encode() expects a singleton record.
        if not value is None:
            kwargs["value"]= { self.name : value }

        # Note that the value may also be passed as a keyword argument
        # <self.name>= value, in which case it'll be incorporated by
        # Bitfield.encode().

        # Apply field formatting.
        return super(Singleton, self).encode(**kwargs)

# Common singleton formats
Boolean=   Singleton("b1", "X")
def Unsigned(N, name="X"): return Singleton("u%d" % N, name)
def Signed(N, name="X"): return Singleton("s%d" % N, name)

class ASCII(DataFormat):
    """A fixed-sized array of ASCII characters."""

    def unpack(self, **kwargs):
        return kwargs["raw"].decode()

    def interpret(self, fields, **kwargs):
        return fields

    def pack(self, **kwargs):
        return kwargs["value"].encode()

    def encode(self, value, **kwargs):
        return value

    def length(self, **kwargs):
        return None

class TaggedUnion(Bitfield):
    """Tagged Union
    
    One field is specified as the tag (tag_pattern + tag_name), alongside a
    mapping (members) of tag value to interpretation (for the rest of the
    record)."""

    def __init__(self, tag_pattern, tag_name, tag_fmt, members,
                 *args, **kwargs):
        self.members= members
        self.tag_name= tag_name
        self.tag_fmt= tag_fmt
        super(TaggedUnion, self).__init__(tag_pattern, [tag_name],
                                          *args, **kwargs)

    def unpack(self, **kwargs):
        # Extract the tag.
        fields= super(TaggedUnion, self).unpack(**kwargs)

        # Decode the tag.
        tag_raw= fields[self.tag_name]
        raw= kwargs["raw"]
        kwargs["raw"]= tag_raw
        tag= self.tag_fmt.unpack(**kwargs)
        fields[self.tag_name]= tag
        if not tag in self.members: raise UnknownTag()

        # Select an apply a member format.
        member_format= self.members[tag]
        kwargs["raw"]= raw
        fields.update(member_format.unpack(**kwargs))
        return fields

    # pack() hasn't been needed yet.

class SignedScaled(Singleton):
    def __init__(self, N, LSB, offset=0.0, name="Y", *args, **kwargs):
        self.N= N
        self.LSB= LSB
        self.offset= offset
        super(SignedScaled, self).__init__("s%d" % N, name, *args, **kwargs)

    def least(self, **kwargs):
        """Least representable value."""
        LSB, offset= self.collect_coeff(**kwargs)
        return -(2 ** (self.N-1)) * LSB + offset

    def upper_bound(self, **kwargs):
        """Least upper bound of representable values."""
        LSB, offset= self.collect_coeff(**kwargs)
        return (2 ** (self.N-1) - 0.5) * LSB + offset

    def collect_coeff(self, **kwargs):
        coeff= kwargs["coeff"]
        try:
            if isinstance(self.LSB, str): LSB= coeff[self.LSB]
            else: LSB= self.LSB

            if isinstance(self.offset, str): offset= coeff[self.offset]
            else: offset= self.offset
        except KeyError as e:
            raise ParameterRequest(e.args[0])

        return (LSB, offset)

    def interpret(self, **kwargs):
        LSB, offset= self.collect_coeff(**kwargs)

        unscaled= super(SignedScaled, self).interpret(**kwargs)
        return unscaled * LSB + offset

    def encode(self, **kwargs):
        value= kwargs["value"]

        if value < self.least(**kwargs) or value >= self.upper_bound(**kwargs):
            raise FormatOutOfRange(
                    "Value %.12e out of range [%.12e,%.12e)" %
                    (value, self.least(**kwargs), self.upper_bound(**kwargs)))

        LSB, offset= self.collect_coeff(**kwargs)

        unscaled= int(round((value - offset) / LSB))

        kwargs["value"]= unscaled
        return super(SignedScaled, self).encode(**kwargs)

class UnsignedScaled(Singleton):
    def __init__(self, N, LSB, offset= 0.0, limit=None, hard_zero=False,
                 name="Y", *args, **kwargs):
        assert limit is None or limit < (1 << N)
        self.N= N
        self.LSB= LSB
        self.offset= offset
        self.limit= limit
        # Code 0 = 0.0, regardless of offset.
        self.hard_zero= hard_zero
        super(UnsignedScaled, self).__init__("u%d" % N, name, *args, **kwargs)

    def collect_coeff(self, **kwargs):
        coeff= kwargs["coeff"]
        try:
            if isinstance(self.LSB, str): LSB= coeff[self.LSB]
            else: LSB= self.LSB

            if isinstance(self.offset, str): offset= coeff[self.offset]
            else: offset= self.offset
        except KeyError as e:
            raise ParameterRequest(e.args[0])

        return (LSB, offset)

    def least(self, **kwargs):
        """Least representable value."""
        if self.hard_zero:
            return 0
        else:
            LSB, offset= self.collect_coeff(**kwargs)
            return offset

    def upper_bound(self, **kwargs):
        """Least upper bound of representable values."""
        if self.limit is None:
            max_raw= self.limit
        else:
            max_raw= (1 << self.N) -1

        LSB, offset= self.collect_coeff(**kwargs)
        return (max_raw + 0.5) * LSB + offset

    def interpret(self, **kwargs):
        LSB, offset= self.collect_coeff(**kwargs)
        unscaled= super(UnsignedScaled, self).interpret(**kwargs)
        if self.hard_zero and unscaled == 0:
            return 0.0
        else:
            return unscaled * LSB + offset

    def encode(self, **kwargs):
        value= kwargs["value"]
        LSB, offset= self.collect_coeff(**kwargs)

        if (value < offset and (not self.hard_zero or value != 0.0)) or \
           value >= self.upper_bound(**kwargs):
            raise FormatOutOfRange(
                    "Value %.12e out of range [%.12e,%.12e)" %
                    (value, self.least(**kwargs), self.upper_bound(**kwargs)))

        if self.hard_zero and value == 0.0:
            unscaled= 0
        else:
            unscaled= int(round((value - offset) / LSB))

        kwargs["value"]= unscaled
        return super(UnsignedScaled, self).encode(**kwargs)

class Alternate(DataFormat):
    """Represents a value that could be in one of several formats, depending
    on the value of some other parameter of the system."""

    def __init__(self, parameter, alternatives, *args, **kwargs):
        self.param_name= parameter[0]
        self.param_path= parameter[1:]
        self.alternatives= alternatives

    def choose(self, **kwargs):
        coeff= kwargs["coeff"]
        try:
            value= coeff[self.param_name]
            for field in self.param_path: value= value[field]
        except KeyError:
            raise ParameterRequest(self.param_name)

        return self.alternatives[value]

    def interpret(self, **kwargs):
        return self.choose(**kwargs).interpret(**kwargs)

    def unpack(self, **kwargs):
        return self.choose(**kwargs).unpack(**kwargs)

    def encode(self, **kwargs):
        return self.choose(**kwargs).encode(**kwargs)

    def pack(self, **kwargs):
        return self.choose(**kwargs).pack(**kwargs)

    def length(self, **kwargs):
        return max(self.alternatives[k].length() for k in self.alternatives)

class SemiLog(Bitfield):
    """A variant floating-point format representing only positive numbers (not
    including zero).  Uses a configurable exponent bias, and omits the leading
    bit of the significand (mantissa).  Both exponent and mantissa are stored
    unsigned, and there is no sign bit.  Exponent is stored before
    mantissa."""

    def __init__(self, mantissa_bits, exponent_bits, scale=1.0,
                       exponent_bias=0, *args, **kwargs):
        self.mantissa_bits= mantissa_bits
        self.exponent_bits= exponent_bits
        self.scale= scale
        self.exponent_bias= exponent_bias

        pattern= "u%du%d" % (exponent_bits, mantissa_bits)
        super(SemiLog, self).__init__(
            pattern, ["exponent", "mantissa"], *args, **kwargs)

    def least_exp(self):
        # When encoded exponent is all 0s
        return self.exponent_bias

    def greatest_exp(self):
        # When encoded exponent is all 1s
        return 2 ** self.exponent_bits - 1 + self.exponent_bias

    def least_mantissa(self):
        # The omitted leading bit means the significant is biased to lie in
        # [2**N, 2**(N+1))
        return 1 << self.mantissa_bits

    def greatest_mantissa(self):
        return (2 << (self.mantissa_bits + 1)) - 1

    def least(self, N=None):
        """Least representable value."""
        if N is None:
            # There are no negative values and no zero, so the least
            # representable value has the smallest exponent.
            N= self.least_exp()

        # Anything above this will round up to a representable value.
        return (self.least_mantissa() - 0.5) * (2 ** N) * self.scale

    def upper_bound(self, N=None):
        """Least upper bound of representable values."""
        if N is None: N= self.greatest_exp()

        # Anything below this will round down to a representable value.
        return (self.greatest_mantissa() + 0.5) * (2 ** N) * self.scale

    def interpret(self, **kwargs):
        raw_fields= kwargs["raw_fields"]
        M= raw_fields["mantissa"]
        E= raw_fields["exponent"]

        return self.scale * (M + (1 << self.mantissa_bits)) \
                          * 2 ** (E + self.exponent_bias)

    def encode(self, **kwargs):
        value= kwargs["value"]

        if value < self.least() or value >= self.upper_bound():
            raise FormatOutOfRange("Value %.12e out of range [%.12e,%.12e)" %
                (value, self.least(), self.upper_bound()))

        # Remove the scaling factor.
        value/= self.scale

        # Get the (IEEE754) mantissa and exponent
        m, e= math.frexp(value)

        assert m > 0

        # frexp() returns a mantissa in the range [0.5,1).  Bias the exponent
        # to map this to the range [2^N, 2^(N+1) - 1) i.e. an N+1 bit mantissa
        # with MSB=1.
        M= m * 2.0**(self.mantissa_bits + 1)
        E= e - (self.mantissa_bits + 1)

        # Round mantissa to nearest integer
        rM= int(round(M))

        # The range [-2^(mbits-1) - 0.5, 2^(mbits-1)) i.e. the top 1/2 LSB,
        # will round to 2^(mbits-1), which is not representable in the
        # mantissa.  In this case, increase the exponent by one.
        if rM == 2 ** (self.mantissa_bits + 1):
            M/= 2.0
            rM= int(round(M))
            # This won't produce a too-large exponent, as upper_bound() is set
            # halfway along the top LSB, so we won't get here in that case.
            E+= 1

        # There are no subnormal numbers, so we must already be in the valid
        # exponent range.
        assert rM >= self.least_mantissa()
        assert rM <= self.greatest_mantissa()
        assert E >= self.least_exp()
        assert E <= self.greatest_exp()

        # Subtract the mantissa and exponent bias
        return { "mantissa": rM - (1 << self.mantissa_bits),
                 "exponent": E - self.exponent_bias }
