from . import i2cbus
from . import smbus
from . import mpsse_smbus
from . import pmbus

class MPSSE_PMBus(mpsse_smbus.MPSSE_SMBus,pmbus.PMBus):
    """A PMBus object is an extension of an SMBus object, definining a few
    additional protocol elements, and a standard set of commands.

    This class implements the extended commands assuming an MPSSE-style I2C
    interface.

    Section references are to the PMBus Specification, revision 1.2 (2010).
    """

    def __init__(self, *args, **kwargs):
        super(MPSSE_PMBus, self).__init__(*args, **kwargs)

    def group_command(self, commands):
        """Group Command (S5.2.3)"""
        try:
            for (address, cmd_code, data) in commands:
                # Repeated start for targets after the first
                self.i2c_bus.Start()
                self.output_bytearray(bytearray([address, cmd_code]) + data)
        finally:
            self.i2c_bus.Stop()

    def write_word_extended(self, address, command, data, ext_code=0xFF):
        """Extended Command: Write Byte/Word (S5.3.4/S5.2.5)
        
        Defaults to the PMBus extended command set (extension code 0xFF).
        Use write_word_mfr() for manufacturer-specific commands (extension
        code 0xFE)."""
        assert len(data) <= 2

        try:
            self.i2c_bus.Start()
            self.output_bytearray(
                bytearray([i2cbus.write_addr(address), ext_code,
                           command]) + data)
        finally:
            self.i2c_bus.Stop()

    def read_word_extended(self, address, command, N=2, ext_code=0xFF):
        """Extended Command: Read Byte/Word (S5.3.4/S5.2.5)
        
        Defaults to the PMBus extended command set (extension code 0xFF).
        Use read_word_mfr() for manufacturer-specific commands (extension
        code 0xFE)."""
        try:
            self.i2c_bus.Start()
            self.output_bytearray(
                bytearray([i2cbus.write_addr(address), ext_code, command]))
            # Repeated start
            self.i2c_bus.Start()
            self.output_int(i2cbus.read_addr(address))
            data= self.input_bytearray(N)
        finally:
            self.i2c_bus.Stop()

        return data
