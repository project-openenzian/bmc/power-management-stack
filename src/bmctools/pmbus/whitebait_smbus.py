from . import formats as F
from . import i2cbus as I2C
from . import whitebait_i2cbus as wb_i2c
from . import smbus as SMB
import queue

# Response format for alert response.  Upper 7 bits are device address.
ara_fmt= F.Singleton("u7p1", "address")

def handle_smbus_alert(smbus, event, *args):
    """This handler is installed on the ALERT handler of the underlying I2C
    implementation, and is called whenever the SMBALERT signal is asserted.
    It uses the ALERT_RESPONSE protocol to find the alerting device's address,
    and pushes that to the SMBus object's smbalert_queue."""

    # Broadcast to find which device alerted.
    dev_addr= smbus.alert_response()
    if dev_addr is None:
        smbus.tr_logger.warning("Spurious SMBALERT",
                                extra={ "protocol" : "SMBus" })
    else:
        smbus.tr_logger.info("SMBALERT from 0x%02X", dev_addr,
                              extra={ "protocol" : "SMBus" })
        try:
            smbus.smbalert_queue.put(dev_addr)
        except queue.Full:
            smbus.tr_logger.error("SMBALERT queue overflow",
                                    extra={ "protocol" : "SMBus" })

class Whitebait_SMBus(wb_i2c.Whitebait_I2CBus,SMB.SMBus):
    """A 'Whitebait_SMBus' object wraps an object representing an I2C
    interface.  The `SMBus` object provides SMBus
    operations as primitives.

    Section references are to the SMBus Specification, revision 3.1 (2018).
    """

    smbalert_queue= None

    def __init__(self, *args, **kwargs):
        self.smbalert_queue= queue.Queue()
        self.alert_handlers["ALERT"] = (handle_smbus_alert, [])

        super(Whitebait_SMBus, self).__init__(*args, **kwargs)

    def scan(self, start=0x00, end=0x7F, exclude=set()):
        return super(Whitebait_SMBus, self).scan(start, end,
                           set.union(exclude, SMB.smbus_probe_exclude))

    def quick_cmd(self, address, read):
        """Quick command (S6.5.1) - R/W bit is the payload

        @param address int
        @param read bool"""

        self.tr_logger.debug("quick_cmd(0x%02X,%d)", address, read,
                             extra={ "protocol" : "SMBus" })
        if read:
            return self.execute([
                wb_i2c.PAddress(address),
                wb_i2c.PRead(0),
                wb_i2c.PDone()
            ])
        else:
            return self.execute([
                wb_i2c.PAddress(address),
                wb_i2c.PWrite([]),
                wb_i2c.PDone()
            ])

    def send_byte(self, address, value):
        """Send Byte (S6.5.2)

        @param address int
        @param value bytearray"""

        self.tr_logger.debug("send_byte(0x%02X,0x%02X)", address, value[0],
                             extra={ "protocol" : "SMBus" })
        self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite(value),
            wb_i2c.PDone()
        ])

    def receive_byte(self, address):
        """Receive Byte (S6.5.3)

        @param address int
        @return bytearray"""

        self.tr_logger.debug("receive_byte(0x%02X)", address,
                             extra={ "protocol" : "SMBus" })
        return self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PRead(1),
            wb_i2c.PDone()
        ])

    def write_byte(self, address, command, data):
        """Calls `write_word()`

        @param address int
        @param command int
        @param data bytearray
        @return bytearray"""
        return self.write_word(address, command, data)

    def write_word(self, address, command, data):
        """Write Byte/Word (S6.5.4)

        @param address int
        @param command int
        @param data bytearray"""
        assert 1 <= len(data) <= 2

        if len(data) == 1:
            self.tr_logger.debug("write_byte(0x%02X,0x%02X,0x%02X)", address,
                                 command, data[0],
                                 extra={ "protocol" : "SMBus" })
        else:
            self.tr_logger.debug("write_word(0x%02X,0x%02X,0x%02X%02X)",
                                 address, command, data[0], data[1],
                                 extra={ "protocol" : "SMBus" })
        self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite(bytearray([command]) + bytearray(data)),
            wb_i2c.PDone()
        ])

    def read_byte(self, address, command):
        """Calls `read_word()` with N=1.

        @param address int
        @param command int
        @return bytearray"""
        return self.read_word(address, command, 1)

    def read_word(self, address, command, N=2):
        """Read Byte/Word (S6.5.5)

        @param address int
        @param command int
        @param N int
        @return bytearray"""
        assert 1 <= N <= 2

        if N == 1:
            self.tr_logger.debug("read_byte(0x%02X,0x%02X)", address, command,
                                 extra={ "protocol" : "SMBus" })
        else:
            self.tr_logger.debug("read_word(0x%02X,0x%02X)", address, command,
                                 extra={ "protocol" : "SMBus" })
        return self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite([command]),
            wb_i2c.PRead(N),
            wb_i2c.PDone()
        ])

    def process_call(self, address, command, tx_data):
        """Process Call (S6.5.6)

        @param address int
        @param command int
        @param tx_data bytearray
        @return bytearray"""
        assert len(tx_data) == 2

        self.tr_logger.debug("process_call(0x%02X,0x%02X,0x%02X%02X)", address,
                             command, tx_data[0], tx_data[1],
                             extra={ "protocol" : "SMBus" })
        return self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite(bytearray([command]) + bytearray(tx_data)),
            wb_i2c.PRead(2),
            wb_i2c.PDone()
        ])

    def write_block(self, address, command, data):
        """Write Block (S6.5.7)

        @param address int
        @param command int
        @param data bytearray"""
        assert len(data) <= 255

        self.tr_logger.debug("write_block(0x%02X,0x%02X,0x%s)",
                             address, command,
                             "".join("%02X" % x for x in data),
                             extra={ "protocol" : "SMBus" })
        return self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite(bytearray([command, len(data)]) + bytearray(data)),
            wb_i2c.PDone()
        ])

    def write_block_noncompliant(self, address, command, data):
        """Write Block (S6.5.7)

        Performs a write block transaction, but does not transmit the block
        length.  This is needed for non-standard-compliant devices, such as
        the MAX31785.

        @param address int
        @param command int
        @param data bytearray"""
        assert len(data) <= 255

        self.tr_logger.debug("write_block_noncompliant(0x%02X,0x%02X,0x%s)",
                             address, command,
                             "".join("%02X" % x for x in data),
                             extra={ "protocol" : "SMBus" })
        return self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite(bytearray([command]) + bytearray(data)),
            wb_i2c.PDone()
        ])

    def read_block(self, address, command):
        """Read Block (S6.5.7)

        @param address int
        @param command int
        @return dbytearray"""

        self.tr_logger.debug("read_block(0x%02X,0x%02X)", address, command,
                             extra={ "protocol" : "SMBus" })
        return self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite([command]),
            wb_i2c.PRead(255, variable=True),
            wb_i2c.PDone()
        ])

    def read_block_noncompliant(self, address, command, N):
        """Read Block (S6.5.7)

        Performs a read block transaction, but takes the length of data to
        read as a parameter, rather than receiving it from the device.  This
        is needed for non-standard-compliant devices, such as the MAX31785.

        @param address int
        @param command int
        @return dbytearray"""

        self.tr_logger.debug("read_block_noncompliant(0x%02X,0x%02X)",
                             address, command,
                             extra={ "protocol" : "SMBus" })
        return self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite([command]),
            # The device *should* send N here, but doesn't.
            wb_i2c.PRead(N),
            wb_i2c.PDone()
        ])

    def process_call_block(self, address, command, tx_data):
        """Block Write/Block Read Process Call (S6.5.8)

        @param address int
        @param command int
        @param tx_data bytearray
        @return bytearray"""
        assert len(tx_data) <= 255

        self.tr_logger.debug("process_call_block(0x%02X,0x%02X,0x%s)",
                             address, command,
                             "".join("%02X" % x for x in tx_data),
                             extra={ "protocol" : "SMBus" })
        return self.execute([
            wb_i2c.PAddress(address),
            wb_i2c.PWrite(bytearray([command, len(tx_data)]) +
                          bytearray(tx_data)),
            wb_i2c.PRead(255, variable=True),
            wb_i2c.PDone()
        ])

    def alert_response(self):
        """SMBus Alert Reponse (A.2)

        Issues a read to the SMBus Alert Reponse Address (0x0C), to which the
        device pulling SMBALERT low must respond with its address.

        @return Alerting device address (or None)."""

        self.tr_logger.debug("alert_response()",
                             extra={ "protocol" : "SMBus" })
        try:
            raw= self.execute([
                wb_i2c.PAddress(SMB.smbus_rsvd_by_name["ALERT_RESPONSE"]),
                wb_i2c.PRead(1),
                wb_i2c.PDone()
            ])
            return ara_fmt.unpack(raw=raw)
        except I2C.I2CBus_NACK:
            # Nobody responded
            return None
