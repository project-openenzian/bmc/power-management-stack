import pyudev, struct, os, fcntl, termios, time

ID_VENDOR= u"ETH"
ID_MODEL= u"Whitebait_USB_I2C"

def find_devices(serial=None):
    """Search UDEV for devices matching our known vendor and model."""

    # Open a database connection
    context= pyudev.Context()

    # Filter for vendor and model, and for the subsystem so that we get the
    # ttyUSB* node, not the USB device itself.
    if serial is None:
        devices= context.list_devices(ID_VENDOR=ID_VENDOR,
                                      ID_MODEL=ID_MODEL,
                                      subsystem="tty")
    else:
        devices= context.list_devices(ID_VENDOR=ID_VENDOR,
                                      ID_MODEL=ID_MODEL,
                                      ID_SERIAL=serial,
                                      subsystem="tty")

    # Return the device path.
    return [D.device_node for D in devices]
