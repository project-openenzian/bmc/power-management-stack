#!/usr/bin/env python

import argparse
import readline # Needed for command history to work
import cmd
import code

import dbus

from bmctools.dbus.power import ALERT_ADDRESS_UNKNOWN
from bmctools.dbus.telemetry import DATA_UNINIT, DATA_UNKNOWN

from bmctools.dbus.util import run_mainloop, to_dbus, from_dbus

from enzianbmc.gpio_service import get_service as get_gpio_service
from enzianbmc.power_service import get_service as get_power_service
from enzianbmc.fault_service import get_service as get_fault_service
from enzianbmc.telemetry_service import get_service as get_telemetry_service

from enzianbmc.power_sequence import EnzianCommonUpSequence, EnzianCPUUpSequence, EnzianFPGAUpSequence


def get_dbus_daemon():
    return dbus.SystemBus()


def testing_setup():
    # Use session bus
    global get_dbus_daemon
    get_dbus_daemon = lambda: dbus.SessionBus()


class EnzianShell(cmd.Cmd, object):
    def __init__(self, gpio, power, fault, telemetry, debug=False):
        super().__init__()
        self.prompt = "Enzian PWR> "
        self.gpio = gpio
        self.power = power
        self.fault = fault
        self.telemetry = telemetry
        self.python_shell = None
        self.common_up = EnzianCommonUpSequence(gpio, power)
        self.cpu_up = EnzianCPUUpSequence(gpio, power)
        self.fpga_up = EnzianFPGAUpSequence(gpio, power)
        self.debug = debug


    def do_alerts(self, args):
        dev_sig_match = self.power.connect_to_signal(
            "device_alert",
            lambda d, a: print("Alert from '%s':\n%s" % (
                d,
                "\n".join([f"\t{m}: {c}" for m, c in from_dbus(a)])
            ))
        )
        unknown_sig_match = self.power.connect_to_signal(
            "unknown_device_alert",
            lambda b, a: print("Alert from %s on bus '%s'" % (
                "unknown address" if a == ALERT_ADDRESS_UNKNOWN else f"0x{a:02x}",
                b
            ))
        )
        run_mainloop()
        dev_sig_match.remove()
        unknown_sig_match.remove()


    def do_gpiomon(self, args):
        signal_match = self.gpio.connect_to_signal(
            "value_change",
            lambda p, v: print(f"{p}: {v}")
        )
        run_mainloop()
        signal_match.remove()


    def do_telemetry(self, args):
        def clear_console():
            print(f"{chr(0x1b)}[0;0H{chr(0x1b)}[J")

        def handler(data):
            out = "Telemetry data:\n"
            for device, values in from_dbus(data).items():
                out = out + (f"\t{device}:")
                if values == DATA_UNINIT:
                    # Device not initialized
                    out = out + " n/a\n"
                elif values == DATA_UNKNOWN:
                    # Device doesn't exist
                    out = out + " Unknown device\n"
                else:
                    out = out + "\n"
                    for monitor, value in values.items():
                        if value == DATA_UNKNOWN:
                            out = out + f"\t\t{monitor}: No such monitor\n"
                        else:
                            out = out + f"\t\t{monitor}: {value}\n"

            clear_console()
            print(out)
        signal_match = self.telemetry.connect_to_signal("data", handler)

        clear_console()
        run_mainloop()
        signal_match.remove()
        clear_console()


    def do_python(self, args):
        if self.python_shell is None:
            variables = {
                "gpio"         : self.gpio,
                "power"        : self.power,
                "telemetry"    : self.telemetry,
                "psup_on"      : lambda: self.gpio.set_value("B_PSUP_ON", True),
                "psup_off"     : lambda: self.gpio.set_value("B_PSUP_ON", False),
                "run_mainloop" : run_mainloop,
                "to_dbus"      : to_dbus,
                "common_up"    : self.common_up,
                "cpu_up"       : self.cpu_up,
                "fpga_up"      : self.fpga_up
            }
            self.python_shell = code.InteractiveConsole(variables)
            if self.debug:
                self.python_shell.runsource("from enzianbmc.debug_helpers import *")
                self.python_shell.runsource("init_debug_helpers(gpio, power, fault, telemetry)")
        self.python_shell.interact()

    def do_bringup(self, args):
        variables = {
            "gpio"         : self.gpio,
            "power"        : self.power,
            "fault"        : self.fault,
            "telemetry"    : self.telemetry
        }
        python_shell = code.InteractiveConsole(variables)
        # This is a bit of a hack to make the old bringup script work
        python_shell.runsource("from enzianbmc.power_sequence_bringup import *")
        python_shell.runsource("init_bringup_script(gpio, power, fault, telemetry)")
        if self.debug:
                python_shell.runsource("from enzianbmc.debug_helpers import *")
                python_shell.runsource("init_debug_helpers(gpio, power, fault, telemetry)")
        if args:
            python_shell.runsource(args)
        else:
            python_shell.interact()

    def do_exit(self, args):
        return True


def main(args):
    if args.test:
        testing_setup()

    dbus_daemon = get_dbus_daemon()

    gpio = get_gpio_service(dbus_daemon, follow_name_owner_changes=args.follow)
    power = get_power_service(dbus_daemon, follow_name_owner_changes=args.follow)
    fault = get_fault_service(dbus_daemon, follow_name_owner_changes=args.follow)
    telemetry = get_telemetry_service(dbus_daemon, follow_name_owner_changes=args.follow)

    shell = EnzianShell(gpio, power, fault, telemetry, debug=args.debug)
    if args.command is None:
        try:
            shell.cmdloop()
        except KeyboardInterrupt:
            pass
    else:
        shell.onecmd(' '.join(args.command))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Enzian Power Management Shell")
    parser.add_argument("--test", "-t", action="store_true",
        help="If set, shell will connect to the session bus instead of the system bus"
    )
    parser.add_argument("--follow", "-f", action="store_true",
        help="The shell will reconnect to services if they are restarted."
    )
    parser.add_argument("--debug", "-d", action="store_true",
        help="The shell will load helpers for debugging."
    )
    parser.add_argument(
        "command", metavar="CMD", type=str, nargs='*',
        help="Command to run"
    )
    args = parser.parse_args()
    main(args)
