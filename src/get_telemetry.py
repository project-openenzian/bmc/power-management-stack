#!/usr/bin/env python3

# Author: Kristina Martsenko

# script to
# 1) automatically configure sources (instead of manually in shell)
# 2) print telemetry with timestamps and into a file (instead of running
#    `enzian-shell telemetry` which does not print timestamps and
#    overwrites the display)
#    (alternatively could use the output of `journalctl -fu systems.enzian.Telemetry.service`)
# run as: ./get-telemetry.py > telemetry.dat

import datetime
import sys
import os

import dbus

from bmctools.dbus.telemetry import DATA_UNINIT, DATA_UNKNOWN
from bmctools.dbus.util import from_dbus, run_mainloop

from enzianbmc.telemetry_service import get_service as get_telemetry_service

class UnexpectedMonitorException(Exception):
    pass


# note: this prints to stdout
def handler(data):
#    out = "Telemetry data:\n"
#    for device, values in from_dbus(data).items():
#        out = out + ("\t%s:" % (device))
#        if values == DATA_UNINIT:
#            # Device not initialized
#            out = out + " n/a\n"
#        elif values == DATA_UNKNOWN:
#            # Device doesn't exist
#            out = out + " Unknown device\n"
#        else:
#            out = out + "\n"
#            for monitor, value in values.items():
#                if value == DATA_UNKNOWN:
#                    out = out + ("\t\t%s: No such monitor\n" % (monitor))
#                else:
#                    out = out + ("\t\t%s: %s\n" % (monitor, value))
#    print(out)

    # TODO: fix timestamps 
    # with timestamps
    timestamp = str(datetime.datetime.now())
    out = ""
    for device, values in from_dbus(data).items():
        if values == DATA_UNINIT:
            # Device not initialized
            # If the FPGA is off:
            if device == "max20751_mgtavtt_fpga":
                out += timestamp + " MGTAVTT_FPGA_V -\n"
                out += timestamp + " MGTAVTT_FPGA_I -\n"
            elif device == "ina226_ddr_fpga_13":
                out += timestamp + " VDD_DDRFPGA13_V -\n"
                out += timestamp + " VDD_DDRFPGA13_I -\n"
            elif device == "ina226_ddr_fpga_24":
                out += timestamp + " VDD_DDRFPGA24_V -\n"
                out += timestamp + " VDD_DDRFPGA24_I -\n"
            elif device == "max20751_mgtavcc_fpga":
                out += timestamp + " MGTAVCC_FPGA_I -\n"
            elif device == "max20751_vccint_fpga":
                out += timestamp + " VCCINT_FPGA_I -\n"
            else:
                raise UnexpectedMonitorException
        elif values == DATA_UNKNOWN:
            # Device doesn't exist
            raise UnexpectedMonitorException
        else:
            for monitor, value in values.items():
                if value == DATA_UNKNOWN:
                    raise UnexpectedMonitorException
                else:
                    if device == "pac_cpu":
                        if monitor == "VMON4":
                            out += timestamp + (" VDD_CORE_V %s\n" % (value))
                        elif monitor == "VMON5":
                            out += timestamp + (" 0V9_VDD_OCT_V %s\n" % (value))
                        elif monitor == "VMON6":
                            out += timestamp + (" 1V5_VDD_OCT_V %s\n" % (value))
                        elif monitor == "VMON9":
                            out += timestamp + (" VDD_DDRCPU13_V %s\n" % (value))
                        elif monitor == "VMON10":
                            out += timestamp + (" VDD_DDRCPU24_V %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "ir3581_loop_vdd_core":
                        if monitor == "IOUT":
                            out += timestamp + (" VDD_CORE_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "ir3581_loop_0v9_vdd_oct":
                        if monitor == "IOUT":
                            out += timestamp + (" 0V9_VDD_OCT_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "max15301_1v5_vdd_oct":
                        if monitor == "IOUT":
                            out += timestamp + (" 1V5_VDD_OCT_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "ina226_ddr_cpu_13":
                        if monitor == "CURRENT":
                            out += timestamp + (" VDD_DDRCPU13_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "ina226_ddr_cpu_24":
                        if monitor == "CURRENT":
                            out += timestamp + (" VDD_DDRCPU24_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "pac_fpga":
                        if monitor == "VMON6":
                            out += timestamp + (" VCCINT_FPGA_V %s\n" % (value))
                        elif monitor == "VMON10":
                            out += timestamp + (" VCCINTIO_BRAM_FPGA_V %s\n" % (value))
                        elif monitor == "VMON7":
                            out += timestamp + (" MGTAVCC_FPGA_V %s\n" % (value))
                        elif monitor == "VMON11_ATT":
                            out += timestamp + (" VCC1V8_FPGA_V %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "max20751_vccint_fpga":
                        if monitor == "IOUT":
                            out += timestamp + (" VCCINT_FPGA_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "max15301_vccintio_bram_fpga":
                        if monitor == "IOUT":
                            out += timestamp + (" VCCINTIO_BRAM_FPGA_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "max20751_mgtavcc_fpga":
                        if monitor == "IOUT":
                            out += timestamp + (" MGTAVCC_FPGA_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "max20751_mgtavtt_fpga":
                        if monitor == "VOUT":
                            out += timestamp + (" MGTAVTT_FPGA_V %s\n" % (value))
                        elif monitor == "IOUT":
                            out += timestamp + (" MGTAVTT_FPGA_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "max15301_vcc1v8_fpga":
                        if monitor == "IOUT":
                            out += timestamp + (" VCC1V8_FPGA_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "max15301_vadj_1v8":
                        if monitor == "VOUT":
                            out += timestamp + (" VADJ_1V8_FPGA_V %s\n" % (value))
                        elif monitor == "IOUT":
                            out += timestamp + (" VADJ_1V8_FPGA_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "ina226_ddr_fpga_13":
                        if monitor == "VOLTAGE":
                            out += timestamp + (" VDD_DDRFPGA13_V %s\n" % (value))
                        elif monitor == "CURRENT":
                            out += timestamp + (" VDD_DDRFPGA13_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    elif device == "ina226_ddr_fpga_24":
                        if monitor == "VOLTAGE":
                            out += timestamp + (" VDD_DDRFPGA24_V %s\n" % (value))
                        elif monitor == "CURRENT":
                            out += timestamp + (" VDD_DDRFPGA24_I %s\n" % (value))
                        else:
                            raise UnexpectedMonitorException
                    else:
                        raise UnexpectedMonitorException
    print(out, end='')
    sys.stdout.flush()

def main():
    # BMC time gets out of sync with NTP
    os.system("systemctl restart systemd-timesyncd")

    dbus_daemon = dbus.SystemBus()
    telemetry = get_telemetry_service(dbus_daemon)

    # set up telemetry
    telemetry.stop() # just in case
    
    # TODO: associate sample times with individual monitors
    # TODO: create tuples of monitors to enable and disable automatically

    # sample times drift (e.g. by 160ms, if all monitors are read)
    telemetry.set_interval(1000)
    #telemetry.set_interval(1000 - 160) # all
    #telemetry.set_interval(1000 - 76) # CPU, DRAM
    #telemetry.set_interval(100 - 75) # CPU, DRAM, 100ms
    #telemetry.set_interval(20 - 14) # CPU only, no voltages, 20ms
    #telemetry.set_interval(1000 - 26) # CPU, DRAM, no voltages
    #telemetry.set_interval(100 - 25) # CPU, DRAM, no voltages, 100ms
    #telemetry.set_interval(20 - 11) # DRAM only, no voltages, 20ms
    telemetry.set_interval(20 - 16) # guess
    # CPU
    # TODO time drift amount depends on how many monitors are being read; if
    # you change these, then change drift amount too
#    telemetry.add_monitor("pac_cpu", "VMON4") # VDD_CORE voltage
    telemetry.add_monitor("ir3581_loop_vdd_core", "IOUT") # VDD_CORE current
#    telemetry.add_monitor("pac_cpu", "VMON5") # 0V9_VDD_OCT voltage
#    telemetry.add_monitor("ir3581_loop_0v9_vdd_oct", "IOUT") # 0V9_VDD_OCT current
#    telemetry.add_monitor("pac_cpu", "VMON6") # 1V5_VDD_OCT voltage
#    telemetry.add_monitor("max15301_1v5_vdd_oct", "IOUT") # 1V5_VDD_OCT current
    # DDR
#    telemetry.add_monitor("pac_cpu", "VMON9") # VDD_DDRCPU13 voltage
    telemetry.add_monitor("ina226_ddr_cpu_13", "CURRENT") # VDD_DDRCPU13 current
#    telemetry.add_monitor("pac_cpu", "VMON10") # VDD_DDRCPU24 voltage
    telemetry.add_monitor("ina226_ddr_cpu_24", "CURRENT") # VDD_DDRCPU24 current
#    # FPGA
#    telemetry.add_monitor("pac_fpga", "VMON6") # VCCINT_FPGA voltage
    telemetry.add_monitor("max20751_vccint_fpga", "IOUT") # VCCINT_FPGA current
#    telemetry.add_monitor("pac_fpga", "VMON10") # VCCINTIO_BRAM_FPGA voltage
#    telemetry.add_monitor("max15301_vccintio_bram_fpga", "IOUT") # VCCINTIO_BRAM_FPGA current
#    telemetry.add_monitor("pac_fpga", "VMON7") # MGTAVCC_FPGA voltage
#    telemetry.add_monitor("max20751_mgtavcc_fpga", "IOUT") # MGTAVCC_FPGA current
#    telemetry.add_monitor("max20751_mgtavtt_fpga", "VOUT") # MGTAVTT_FPGA voltage
#    telemetry.add_monitor("max20751_mgtavtt_fpga", "IOUT") # MGTAVTT_FPGA current
#    telemetry.add_monitor("pac_fpga", "VMON11_ATT") # VCC1V8_FPGA voltage
#    telemetry.add_monitor("max15301_vcc1v8_fpga", "IOUT") # VCC1V8_FPGA current
#    telemetry.add_monitor("max15301_vadj_1v8", "VOUT") # VADJ_1V8_FPGA voltage
#    telemetry.add_monitor("max15301_vadj_1v8", "IOUT") # VADJ_1V8_FPGA current
#    # FPGA DDR
#    telemetry.add_monitor("ina226_ddr_fpga_13", "VOLTAGE") # VDD_DDRFPGA13 voltage
#    telemetry.add_monitor("ina226_ddr_fpga_13", "CURRENT") # VDD_DDRFPGA13 current
#    telemetry.add_monitor("ina226_ddr_fpga_24", "VOLTAGE") # VDD_DDRFPGA24 voltage
#    telemetry.add_monitor("ina226_ddr_fpga_24", "CURRENT") # VDD_DDRFPGA24 current

    signal_match = telemetry.connect_to_signal("data", handler)
    telemetry.start()
    print("Collecting data. Use Ctrl-C to stop.", file=sys.stderr)
    run_mainloop()
    telemetry.stop()
    signal_match.remove()

    # reset telemetry
    telemetry.set_interval(1000)
    # CPU
#    telemetry.remove_monitor("pac_cpu", "VMON4") # VDD_CORE
    telemetry.remove_monitor("ir3581_loop_vdd_core", "IOUT") # VDD_CORE current
#    telemetry.remove_monitor("pac_cpu", "VMON5") # 0V9_VDD_OCT voltage
#    telemetry.remove_monitor("ir3581_loop_0v9_vdd_oct", "IOUT") # 0V9_VDD_OCT current
#    telemetry.remove_monitor("pac_cpu", "VMON6") # 1V5_VDD_OCT voltage
#    telemetry.remove_monitor("max15301_1v5_vdd_oct", "IOUT") # 1V5_VDD_OCT current
    # DDR
#    telemetry.remove_monitor("pac_cpu", "VMON9") # VDD_DDRCPU13 voltage
    telemetry.remove_monitor("ina226_ddr_cpu_13", "CURRENT") # VDD_DDRCPU13 current
#    telemetry.remove_monitor("pac_cpu", "VMON10") # VDD_DDRCPU24 voltage
    telemetry.remove_monitor("ina226_ddr_cpu_24", "CURRENT") # VDD_DDRCPU24 current
#    # FPGA
#    telemetry.remove_monitor("pac_fpga", "VMON6") # VCCINT_FPGA voltage
    telemetry.remove_monitor("max20751_vccint_fpga", "IOUT") # VCCINT_FPGA current
#    telemetry.remove_monitor("pac_fpga", "VMON10") # VCCINTIO_BRAM_FPGA voltage
#    telemetry.remove_monitor("max15301_vccintio_bram_fpga", "IOUT") # VCCINTIO_BRAM_FPGA current
#    telemetry.remove_monitor("pac_fpga", "VMON7") # MGTAVCC_FPGA voltage
#    telemetry.remove_monitor("max20751_mgtavcc_fpga", "IOUT") # MGTAVCC_FPGA current
#    telemetry.remove_monitor("max20751_mgtavtt_fpga", "VOUT") # MGTAVTT_FPGA voltage
#    telemetry.remove_monitor("max20751_mgtavtt_fpga", "IOUT") # MGTAVTT_FPGA current
#    telemetry.remove_monitor("pac_fpga", "VMON11_ATT") # VCC1V8_FPGA voltage
#    telemetry.remove_monitor("max15301_vcc1v8_fpga", "IOUT") # VCC1V8_FPGA current
#    telemetry.remove_monitor("max15301_vadj_1v8", "VOUT") # VADJ_1V8_FPGA voltage
#    telemetry.remove_monitor("max15301_vadj_1v8", "IOUT") # VADJ_1V8_FPGA current
#    # FPGA DDR
#    telemetry.remove_monitor("ina226_ddr_fpga_13", "VOLTAGE") # VDD_DDRFPGA13 voltage
#    telemetry.remove_monitor("ina226_ddr_fpga_13", "CURRENT") # VDD_DDRFPGA13 current
#    telemetry.remove_monitor("ina226_ddr_fpga_24", "VOLTAGE") # VDD_DDRFPGA24 voltage
#    telemetry.remove_monitor("ina226_ddr_fpga_24", "CURRENT") # VDD_DDRFPGA24 current

if __name__ == "__main__":
    main()

