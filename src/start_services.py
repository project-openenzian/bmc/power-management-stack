#!/usr/bin/env python

import sys
import os
import errno
import argparse

import logging
import logging.handlers

import dbus

from enzianbmc.gpio_service import start_service as start_gpio_service
from enzianbmc.power_service import start_service as start_power_service
from enzianbmc.fault_service import start_service as start_fault_service
from enzianbmc.telemetry_service import start_service as start_telemetry_service

def get_dbus_daemon():
    return dbus.SystemBus()

def testing_setup():
    global get_dbus_daemon
    # Use the session bus
    get_dbus_daemon = lambda: dbus.SessionBus()

    # Setup GPIO service for testing
    from enzianbmc.gpio_service import testing_setup as gpio_testing_setup
    gpio_testing_setup()

    # Setup Power service for testing
    from enzianbmc.power_service import testing_setup as power_testing_setup
    power_testing_setup()

    # Setup Telemetry service for testing
    from enzianbmc.telemetry_service import testing_setup as telemetry_testing_setup
    telemetry_testing_setup()

SERVICES = {
    "gpio"      : start_gpio_service,
    "power"     : start_power_service,
    "fault"     : start_fault_service,
    "telemetry" : start_telemetry_service,
}

SERVICE_ORDER = [ "gpio", "power", "fault", "telemetry" ]

def start_service(service):
    if service in SERVICES:
        print("Starting service '%s'" % (service))
        try:
            dbus_daemon = get_dbus_daemon()
            SERVICES[service](dbus_daemon)
            print("Service '%s' exitting normally" % (service))
            sys.exit(0)
        except Exception as e:
            print("Service '%s' terminated unexpectedly (%s)" % (service, e), file=sys.stderr)
            sys.exit(1)
    else:
        print("No such service '%s'" % (service), file=sys.stderr)
        sys.exit(2)


def main(args):
    if args.test:
        testing_setup()

    log_level = logging.DEBUG if args.verbose else logging.INFO
    logging.root.setLevel(log_level)

    log_handler = None
    message_format = "[%(asctime)s.%(msecs)03d] %(levelname)s %(name)s: %(message)s"

    if args.log is None:
        # Log to stdout
        log_handler = logging.StreamHandler()
        if "JOURNAL_STREAM" in os.environ:
            # Started by systemd so journald takes care of timestamps
            message_format = "[%(levelname)s %(name)s] %(message)s"
    else:
        # Create the log directory if it doesn't exist
        log_dir = os.path.dirname(args.log)
        if not os.path.isdir(log_dir):
            os.makedirs(log_dir)

        # Log to file and rotate logs
        log_handler = logging.handlers.TimedRotatingFileHandler(
            filename=args.log,
            when='midnight', # XXX: We'd prefer 3am but this is what's available
            backupCount=7,
            encoding="utf-8"
        )

    formatter = logging.Formatter(
        fmt=message_format,
        datefmt="%Y-%m-%d %H:%M:%S"
    )
    log_handler.setFormatter(formatter)
    logging.root.addHandler(log_handler)

    if args.service is not None:
        # We don't need to fork
        start_service(args.service)
        # Doesn't return, exits directly

    for service in SERVICE_ORDER:
        service_pid = os.fork()
        if service_pid > 0:
            start_service(service)

    # Wait for all children
    while True:
        try:
            os.wait()
        except OSError as e:
            if e.errno == errno.ECHILD:
                # No more child processes
                sys.exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Starts Enzian DBus services")
    parser.add_argument("--log", "-l", type=str, metavar="FILE", default=None,
        help="logfile, stdout if not set"
    )
    parser.add_argument("--verbose", "-v", action="store_true",
        help="start services with log level DEBUG"
    )
    parser.add_argument("--test", "-t", action="store_true",
        help="if set, use session bus, mock PMBuses and mock GPIO pins in directory $MOCK_GPIOS"
    )
    parser.add_argument(
        "service", metavar="SERVICE", type=str, nargs="?", default=None,
        help="Service to start, all if not set"
    )
    args = parser.parse_args()
    main(args)
